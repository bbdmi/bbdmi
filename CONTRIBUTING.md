# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change. Please note we have a code of conduct, please follow it in all your interactions with the project.

## Pull Request Process

We make changes all to the BBDMI projects using merge requests from [public forks (https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) of the BBDMI repository. It is recommended practice to plan your changes beforehand, and collect them in a specific branch per issue.

## Did you find a bug?

Ensure the bug was not already reported by searching on GitHub under Issues. If you're unable to find an open issue addressing the problem, open a new one. Be sure to include a title and clear description, as much relevant information as possible, and a code sample or screenshots demonstrating the expected behavior that is not occurring.

## Did you fix a bug?

Open a new merge request with the patch. Ensure the merge request description clearly describes the problem and solution. Include the relevant issue number (e.g. #123) if applicable.

## Do you intend to add a new feature or change an existing one?

First suggest your change as an issue. Unless you have collected positive feedback about the suggested change, your change might not be accepted. Consider that before you start writing code.

## Do you have questions about the source code?

Please email us. Do not open an issue for a question.

## Do you want to create a new module?

We welcome any type of contribution, that is reporting a bug, requesting a new feature, improving a module, or especially creating a new one. As we follow a strict modular architecture, we ask you to respect our system architecture and requirements. Of course, we are always open to different views, ideas and implementations. 

### Max

If you wish to create a new Max module, we provide you with a detailed article explaining the main strategies and design choices of our system architecture: [An Interactive Modular System for Electrophysiological DMIs](http://camps.aptaracorp.com/ACM_PMS/PMS/ACM/AM23/31/feabd036-40e5-11ee-b37c-16bb50361d1f/OUT/am23-31.html). In bbdmi/max/source you can find a template folder containing an empty patcher that can be used as a starting point for your new module, a README.md file to explain the module functionalities, an abstraction used when multiple independent voices are needed, and an help file. If you have any question, comment or feedback, you can contact a project team member.

## References

Add references in issue or merge request descriptions or in comments. This will update the issue with info about anything related.

* To reference an issue: #123
* To reference a MR: !123
* To reference a snippet $123

You can make all the same references in comments or merge requests as you can in an issue description.

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or advances;
* Trolling, insulting/derogatory comments, and personal or political attacks;
* Public or private harassment;
* Publishing others' private information, such as a physical or electronic address, without explicit permission;
* Other conduct which could reasonably be considered inappropriate in a professional setting.

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable behavior and are expected to take appropriate and fair corrective action in response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, or to ban temporarily or permanently any contributor for other behaviors that they deem inappropriate, threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces when an individual is representing the project or its community. Examples of representing a project or community include using an official project e-mail address, posting via an official social media account, or acting as an appointed representative at an online or offline event. Representation of a project may be further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by contacting a project team member. All complaints will be reviewed and investigated and will result in a response that is deemed necessary and appropriate to the circumstances. The project team is obligated to maintain confidentiality with regard to the reporter of an incident. Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good faith may face temporary or permanent repercussions as determined by other members of the project’s leadership.

### Attribution

This Code of Conduct is adapted from the [PurpleBooth template][purpleboothpage], itself adapted from the [Contributor Covenant][homepage] ([version 1.4][version]).

[purpleboothpage]: https://gist.github.com/PurpleBooth/b24679402957c63ec426
[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/