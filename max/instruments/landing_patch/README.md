# landing patch

```landing_patch``` proposes a common workflow from ExG signal acquisition and processing (rms~, calibration, scaling) to control mapping. It allows to acquire electrophysiological signal from a different range of sources: EMG/EEG simulators, EAVI ExG board, recorder- (and playback) modules. In addition, it allows to forward the processed data internally to Max, or externally to MIDI, and OSC devices.

## Use

### Two "Modes" of interaction: abstractions (cables) and bpatchers (GUI)

1. Abstractions (with objects exposed): Each module is instantiated in the main patcher window as an abstraction, where the main parameters can be modified using a designated message box (e.g. onoff 1). Once the signal is acquired, it is routed through a root-mean-square module, a calibration step, a scaling section, and finally sent out to Max, MIDI, and OSC for further processing/mapping.

2. Bpatchers (with GUI exposed): The same process can be implemented by instantiating the relevant objects as bpatchers, exposing the main adjustable parameters in a graphical user interface. This mode gives us a wider overview of the signal processing chain, allowing faster prototyping, and visualization feedback. 

For more detailed info on the system architecture, see [BBDMI Max repository](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/blob/main/max/README.md?ref_type=heads).