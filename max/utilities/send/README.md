# send

## Use

Give the module a name, and it will send a (```list```) to the relative <receive> object.

### ATTRIBUTES

(symbol) *send (name)*

### MESSAGES 

(list) *to send*