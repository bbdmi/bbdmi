# list2~

```bbdmi.list2~``` converts messages (```list```) into multichannel audio signal (```mc.~```).

## Use

It takes one input (```list```) and outputs (```mc.~```) audio signal. 

### ARGUMENTS

(int) *number of channels*

### MESSAGES

(list) *to convert*

### OUTPUT

(mc.~ signal) *converted*