# Utilities

- The [Utilities](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/max/utilities) module contains functionality in the form of abstractions, that are more generally employed than in a specific module. These are typically small abstractions that make patching within other abstractions, or in patches, easier.