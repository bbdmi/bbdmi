# onepole~

```bbdmi.onepole~``` smooths a multichannel list (```mc.~```) of audio signals by applying a single-pole lowpass filter with adjustable cutoff frequency.

## Use

It takes one input (```mc.list```) and outputs a lowpass filtered version.

### ATTRIBUTES

(int) *number of channels*

### MESSAGES

(mc.~ signal) *to filter*

### OUTPUT

(mc.~ list) *filtered*