# adc~

```bbdmi.adc~``` converts audio to digital multichannel signals.

## Use

It outputs one (```mc.~ list```) of multichannel audio signal.

### ATTRIBUTES

(```@mute``` int) *mute/unmute output signal*

(```@dB``` int) *signal amplitude [range: -70, 6]*

### MESSAGES

(```mute``` int) *mute/unmute output signal*

(```dB``` int) *signal amplitude [range: -70, 6]*

### OUTPUT

(mc.~ signal) *converted*