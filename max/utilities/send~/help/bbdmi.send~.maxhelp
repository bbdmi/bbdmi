{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 6,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 79.0, 559.0, 629.0 ],
		"openrect" : [ 34.0, 79.0, 0.0, 0.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 2,
		"toptoolbarpinned" : 2,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 3,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "Default Max 7",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 273.0, 365.5, 184.0, 22.0 ],
					"text" : "bbdmi.send~ helpmc~ @chans 4",
					"varname" : "bbdmi.rand~[4]"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "helpmc~", "@chans", 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-4",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.receive~.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 19.5, 441.625000000000114, 198.0, 62.0 ],
					"varname" : "bbdmi.receive~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "helpmc~", "@chans", 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-7",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.send~.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 20.000000000000028, 345.0, 199.0, 63.0 ],
					"varname" : "bbdmi.send~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-17",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 351.0, 547.625000000000114, 46.0, 25.0 ],
					"text" : "dsp"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-18",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 324.0, 551.125000000000114, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "1",
					"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ "chans", 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-16",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.dac~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 19.5, 513.0, 197.999999999999972, 96.250000000000227 ],
					"varname" : "bbdmi.dac~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 273.0, 538.625000000000114, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 273.0, 250.5, 228.0, 22.0 ],
					"text" : "bbdmi.onepole~ 4 @mode 2 @cutoff 400",
					"varname" : "bbdmi.rand~[1]"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, "@mode", 2, "@cutoff", 400 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.onepole~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 20.000000000000028, 173.0, 197.999999999999972, 162.0 ],
					"varname" : "bbdmi.crosspatch~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 273.0, 57.5, 262.0, 22.0 ],
					"text" : "bbdmi_emg_simulator~ 4 @rate 1. @amount 1.",
					"varname" : "bbdmi.rand~[3]"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, "@rate", 1.0, "@amount", 1.0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-13",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_emg_simulator~.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 20.000000000000028, 23.0, 199.0, 140.0 ],
					"varname" : "bbdmi.rand~",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 29.500000000000028, 336.0, 29.500000000000028, 336.0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 29.500000000000028, 165.0, 29.500000000000028, 165.0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 282.5, 273.0, 282.5, 273.0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 1 ],
					"midpoints" : [ 525.5, 237.0, 491.5, 237.0 ],
					"source" : [ "obj-3", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 282.5, 81.0, 282.5, 81.0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"midpoints" : [ 29.0, 504.0, 29.0, 504.0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-16::obj-45" : [ "live.gain~[8]", "live.gain~", 0 ],
			"obj-1::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-1::obj-123::obj-33" : [ "tab[31]", "tab[1]", 0 ],
			"obj-1::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-1::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-1::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-1::obj-44::obj-33" : [ "tab[9]", "tab[1]", 0 ],
			"obj-1::obj-45::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-1::obj-46::obj-33" : [ "tab[11]", "tab[1]", 0 ],
			"obj-1::obj-47::obj-33" : [ "tab[12]", "tab[1]", 0 ],
			"obj-1::obj-48::obj-33" : [ "tab[13]", "tab[1]", 0 ],
			"obj-1::obj-49::obj-33" : [ "tab[14]", "tab[1]", 0 ],
			"obj-1::obj-50::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-1::obj-6::obj-33" : [ "tab[8]", "tab[1]", 0 ],
			"obj-1::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"obj-1::obj-8::obj-33" : [ "tab[7]", "tab[1]", 0 ],
			"obj-1::obj-9::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-2::obj-107::obj-33" : [ "tab[17]", "tab[1]", 0 ],
			"obj-2::obj-123::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-2::obj-36::obj-33" : [ "tab[19]", "tab[1]", 0 ],
			"obj-2::obj-40::obj-33" : [ "tab[20]", "tab[1]", 0 ],
			"obj-2::obj-41::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-2::obj-44::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-2::obj-45::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-2::obj-46::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-2::obj-47::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-2::obj-48::obj-33" : [ "tab[34]", "tab[1]", 0 ],
			"obj-2::obj-49::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-2::obj-50::obj-33" : [ "tab[35]", "tab[1]", 0 ],
			"obj-2::obj-6::obj-33" : [ "tab[23]", "tab[1]", 0 ],
			"obj-2::obj-74::obj-33" : [ "tab[16]", "tab[1]", 0 ],
			"obj-2::obj-8::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-2::obj-9::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"parameter_overrides" : 			{
				"obj-16::obj-45" : 				{
					"parameter_longname" : "live.gain~[8]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "bbdmi.dac~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/dac~",
				"patcherrelativepath" : "../../dac~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.onepole~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.receive~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/receive~",
				"patcherrelativepath" : "../../receive~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.send~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/send~",
				"patcherrelativepath" : "..",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_emg_pulse_4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bbdmi_emg_simulator~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/input/emg_simulator~",
				"patcherrelativepath" : "../../../input/emg_simulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/source/js",
				"patcherrelativepath" : "../../../source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../../source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.onepole~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
