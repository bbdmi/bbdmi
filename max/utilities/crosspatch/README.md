# crosspatch

```bbdmi.crosspatch``` routes connections between inputs and outputs.

## Use

It takes one input (```list```) and outputs a routed version.

### Note

The ```button``` exposed in the GUI clears the current connections.

### ARGUMENTS

>[order] (```message```, type) *description*

[1] (int) *number of input channels*

[2] (int) *number of output channels*

### MESSAGES

(list) *to route*

### OUTPUT

(list) *routed*