# receive~

## Use

Give the module a name specifying the number of channels, and it will receive a (```mc.~ list```) from the relative <mc.send> object.

### ATTRIBUTES

(symbol) *receive (name)*

### ARGUMENTS

(```@chans``` int) *number of channels*

### OUTPUT

(mc.~ signal) *received*