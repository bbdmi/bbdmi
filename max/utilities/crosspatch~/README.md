# crosspatch~

```bbdmi.crosspatch``` routes connections between multichannel audio inputs and outputs.

## Use

It takes one audio input (```mc.```) and outputs a routed version.

### Note

The ```button``` exposed in the GUI clears the current connections.

### ARGUMENTS

>[order] (```message```, type) *description*

[1] (int) *number of input channels*

[2] (int) *number of output channels*

### MESSAGES

(mc.~ signal) *to route*

### OUTPUT

(mc.~ signal) *routed*