# lores~

```bbdmi.lores~``` applies a resonant pass filter to a multichannel signal.

## Use

It outputs one (```mc.~ list```) of multichannel audio signal, and outputs a filtered version.

### ATTRIBUTES

(```@cutoff``` int) *cutoff frequency*

(```@reson``` int) *resonance [range: 0, 1]*

### MESSAGES

(```cutoff``` int) *cutoff frequency*

(```reson``` int) *resonance [range: 0, 1]*

### OUTPUT

(mc.~ signal) *filtered*