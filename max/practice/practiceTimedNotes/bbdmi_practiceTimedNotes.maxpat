{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 6,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ -374.0, -993.0, 1852.0, 959.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-15",
					"linecount" : 42,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 647.445252001285553, 15.0, 660.0, 570.0 ],
					"text" : "bbdmi_practiceTimedNotes~\n\nThe bbdmi_practiceTimedNotes~ module is designed for interactive practice using electrophysiological signals such as EMG (muscle activity) or EEG (brain activity). It generates enveloped impulses that can be used to create melodies or tones by controlling either the base frequency or its transposition. The module is structured into two main sections: ADSR envelope control and frequency modulation. Below is a detailed breakdown of its parameters and usage.\n\n1. ADSR Envelope Configuration\nThe module generates a series of dynamic envelopes at regular intervals. The ADSR envelope is defined by the following standard parameters:\n\t- Attack: Sets the time for the sound to reach its peak level after triggering.\n\t- Decay: Determines the time to fall from the peak to the sustain level.\n\t- Release: Controls the time for the sound to decay to zero after the note is released.\n\t- Time Spacing: Sets the interval between successive envelopes, controlling the rhythm of note generation.\n\n2. Frequency and Transposition Control\nThe module allows users to generate a series of tones by manipulating two main frequency parameters:\n+ \tBase Frequency:\n\t- Sets the fundamental frequency for the generated sound.\n- \tCan be defined statically (before the session starts) or dynamically during use.\n\t+ Transposition:\n\t- Adjusts the base frequency to achieve the desired note.\n\t- Enables the creation of melodies when connected to electrophysiological signals.\n\t+ Discrete Mode: Determines how frequency and transposition are applied:\n\t- Continuous Mode: Allows smooth transitions across all possible frequencies.\n- \tDiscrete Mode: Restricts frequency changes to the Western musical scale, snapping to semitone intervals.\tThis feature is particularly useful for practicing melodies or musical scales in a controlled manner.\n\n3. User Interface Layout\n\t- Left Section: Contains parameters for configuring the ADSR envelope and the spacing between envelopes.\n\t- \tRight Section: Displays controls for frequency and transposition, including the discrete parameter for selecting continuous or discrete frequency modes.\n\nRecommended Usage\n\t1.\tConfigure the ADSR envelope (Attack, Decay, Sustain, Release) to shape the dynamics of your sound.\n\t2.\tSet the Time Spacing between envelopes to define the rhythmic flow of note generation.\n\t3.\tUse the Transposition input to shift pitches dynamically, either manually or via EMG/EEG signals.\n\t4.\tUse the slider as an input to control de the base Frequency to set the fundamental tone.\n\t5.\tSelect Discrete Mode to practice melodies within a Western musical scale or leave it in Continuous Mode for smooth pitch changes.\n\t6.\tConnect EMG or EEG inputs to explore biofeedback-based musical control, creating interactive exercises for pitch and rhythm practice."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 351.0, 75.0, 282.0, 20.0 ],
					"text" : "=> Control signal input for the frequency of the tone"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "slider",
					"min" : 220.0,
					"mult" : 4.0,
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 326.0, 15.0, 20.0, 140.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-8",
					"maxclass" : "flonum",
					"maximum" : 880.0,
					"minimum" : 220.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 326.0, 164.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 326.0, 199.0, 46.0, 22.0 ],
					"text" : "freq $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 7.0, 719.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 7.0, 676.0, 84.0, 22.0 ],
					"text" : "mc.unpack~ 2"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_practiceTimedNotes_ui1.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 7.0, 232.0, 338.0, 424.0 ],
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 1 ],
					"order" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 1,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 1 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "bbdmi_practiceTimedNotes_ui1.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/faust/bbdmi_faust_lib/MAX_abstractions/bbdmi_practiceTimedNotes",
				"patcherrelativepath" : "../../../faust/bbdmi_faust_lib/MAX_abstractions/bbdmi_practiceTimedNotes",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_practiceTimedNotes_ui1~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "ui_bbdmi.js",
				"bootpath" : "~/Documents/GitLab/bbdmi/faust/bbdmi_faust_lib/Faust2GUI_wrapper_compiler",
				"patcherrelativepath" : "../../../faust/bbdmi_faust_lib/Faust2GUI_wrapper_compiler",
				"type" : "TEXT",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
