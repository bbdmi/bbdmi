# BBDMI bbdmi_practiceTimedNotes~

The bbdmi_practiceTimedNotes~ module is designed for interactive practice using electrophysiological signals such as EMG (muscle activity) or EEG (brain activity). It generates enveloped impulses that can be used to create melodies or tones by controlling either the base frequency or its transposition. The module is structured into two main sections: ADSR envelope control and frequency modulation. Below is a detailed breakdown of its parameters and usage.

1. ADSR Envelope Configuration
The module generates a series of dynamic envelopes at regular intervals. The ADSR envelope is defined by the following standard parameters:
	•	Attack: Sets the time for the sound to reach its peak level after triggering.
	•	Decay: Determines the time to fall from the peak to the sustain level.
	•	Release: Controls the time for the sound to decay to zero after the note is released.
	•	Time Spacing: Sets the interval between successive envelopes, controlling the rhythm of note generation.

2. Frequency and Transposition Control
The module allows users to generate a series of tones by manipulating two main frequency parameters:
	•	Base Frequency:
	•	Sets the fundamental frequency for the generated sound.
	•	Can be defined statically (before the session starts) or dynamically during use.
	•	Transposition:
	•	Adjusts the base frequency to achieve the desired note.
	•	Enables the creation of melodies when connected to electrophysiological signals.
	•	Discrete Mode:
	 Determines how frequency and transposition are applied:
	•	Continuous Mode: Allows smooth transitions across all possible frequencies.
	•	Discrete Mode: Restricts frequency changes to the Western musical scale, snapping to semitone intervals.
	•	This feature is particularly useful for practicing melodies or musical scales in a controlled manner.

3. User Interface Layout
	•	Left Section: Contains parameters for configuring the ADSR envelope and the spacing between envelopes.
	•	Right Section: Displays controls for frequency and transposition, including the discrete parameter for selecting continuous or discrete frequency modes.

Recommended Usage
	1.	Configure the ADSR envelope (Attack, Decay, Sustain, Release) to shape the dynamics of your sound.
	2.	Set the Time Spacing between envelopes to define the rhythmic flow of note generation.
	3.	Use the Transposition input to shift pitches dynamically, either manually or via EMG/EEG signals.
	4.	Use the slider as an input to control de the base Frequency to set the fundamental tone.
	5.	Select Discrete Mode to practice melodies within a Western musical scale or leave it in Continuous Mode for smooth pitch changes.
	6.	Connect EMG or EEG inputs to explore biofeedback-based musical control, creating interactive exercises for pitch and rhythm practice.
