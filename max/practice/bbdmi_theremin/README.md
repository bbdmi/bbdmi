# BBDMI bbdmi_theremin~

The bbdmi_theremin~ module emulates the functionality of a Theremin, enabling a musical interaction controlled via electrophysiological signals such as EMG (muscle activity) or EEG (brain activity). It generates a sinusoidal signal whose amplitude and pitch are dynamically modulated by the sensor outputs.

1. Frequency and Pitch Control
	•	freq:
	•	Defines the base frequency of the generated sinusoidal signal.
	•	Can be set statically before use or dynamically modulated by the input signals.
	•	transp:
	•	Adjusts the base frequency to control pitch transpositions.

2. Amplitude Control
	•	level:
	•	Controls the signal amplitude based on the output of the connected sensors.

3. Discretization Mode
	•	discrete:
	 Determines how transposition behave:
	•	Continuous Mode: Allows smooth, fluid transitions across all possible frequencies, emulating the classical Theremin effect.
	•	Discrete Mode: Limits frequency changes to Western scale notes in semitone intervals, enabling structured melodic control.

5. User Interface Layout

The graphical interface of bbdmi_theremin~ provides clear access to its main variables:
	•	freq: Base frequency control.
	•	transp: Pitch transposition adjustments.
	•	level: Amplitude modulation based on sensor inputs.
	•	discrete: Selection between continuous and discrete frequency behavior.

Recommended Usage
	1.	Connect input signals: Use two sensors (e.g., EMG or EEG) to control amplitude and pitch dynamically.
	2.	Set freq: Define the base frequency to serve as the starting pitch.
	3.	Adjust transp: Modulate the pitch by linking input signals to the transposition parameter.
	4.	Configure discrete variable:
	•	Choose Continuous Mode for smooth frequency transitions.
	•	Select Discrete Mode to snap to semitone intervals.
	5.	Experiment with control dynamics: Observe how variations in bodily activity influence both sound intensity (level) and pitch.