{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 6,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ -374.0, -993.0, 1852.0, 959.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 333.0, 225.0, 33.0, 22.0 ],
					"text" : "* 12."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 333.0, 188.0, 39.0, 22.0 ],
					"text" : "/ 127."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 78.0, 188.0, 39.0, 22.0 ],
					"text" : "/ 127."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 78.0, 41.0, 20.0, 140.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 100.0, 101.0, 225.0, 20.0 ],
					"text" : "=> Controls the level of the output sound"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 78.0, 261.0, 50.0, 22.0 ],
					"text" : "level $1"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_theremin_ui1.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 13.0, 291.0, 167.0, 413.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"linecount" : 41,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 424.0, 169.0, 587.0, 556.0 ],
					"text" : "bbdmi_theremin~\n\nThe bbdmi_theremin~ module emulates the functionality of a Theremin, enabling a musical interaction controlled via electrophysiological signals such as EMG (muscle activity) or EEG (brain activity). It generates a sinusoidal signal whose amplitude and pitch are dynamically modulated by the sensor outputs.\n\n1. Frequency and Pitch Control\n\t+ freq:\n\t- Defines the base frequency of the generated sinusoidal signal.\n\t- \tCan be set statically before use or dynamically modulated by the input signals.\n\t+ transp:\n- Adjusts the base frequency to control pitch transpositions.\n\n2. Amplitude Control\n\t+ \tlevel:\n\t- Controls the signal amplitude based on the output of the connected sensors.\n\n3. Discretization Mode\n\t+ \tdiscrete: Determines how transposition behave:\n\t- Continuous Mode: Allows smooth, fluid transitions across all possible frequencies, emulating the classical Theremin effect.\n\t- \tDiscrete Mode: Limits frequency changes to Western scale notes in semitone intervals, enabling structured melodic control.\n\n5. User Interface Layout\n\nThe graphical interface of bbdmi_theremin~ provides clear access to its main variables:\n\t- freq: Base frequency control.\n\t- transp: Pitch transposition adjustments.\n\t- level: Amplitude modulation based on sensor inputs.\n- \tdiscrete: Selection between continuous and discrete frequency behavior.\n\nRecommended Usage\n\t1.\tConnect input signals: Use two sensors (e.g., EMG or EEG) to control amplitude and pitch dynamically.\n\t2.\tSet freq: Define the base frequency to serve as the starting pitch.\n\t3.\tAdjust transp: Modulate the pitch by linking input signals to the transposition parameter.\n\t4.\tConfigure discrete variable:\n- \tChoose Continuous Mode for smooth frequency transitions.\n\t- Select Discrete Mode to snap to semitone intervals.\n\t5.\tExperiment with control dynamics: Observe how variations in bodily activity influence both sound intensity (level) and pitch."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 361.0, 95.0, 282.0, 33.0 ],
					"text" : "=> Control signal input for the transposition of the base frequency of the tone"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 333.0, 41.0, 20.0, 140.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 333.0, 261.0, 59.0, 22.0 ],
					"text" : "transp $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 14.0, 763.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 14.0, 720.0, 84.0, 22.0 ],
					"text" : "mc.unpack~ 2"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 1 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 1 ],
					"order" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 1,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 1 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "bbdmi_theremin_ui1.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/faust/bbdmi_faust_lib/MAX_abstractions/bbdmi_theremin",
				"patcherrelativepath" : "../../../faust/bbdmi_faust_lib/MAX_abstractions/bbdmi_theremin",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_theremin_ui1~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "ui_bbdmi.js",
				"bootpath" : "~/Documents/GitLab/bbdmi/faust/bbdmi_faust_lib/Faust2GUI_wrapper_compiler",
				"patcherrelativepath" : "../../../faust/bbdmi_faust_lib/Faust2GUI_wrapper_compiler",
				"type" : "TEXT",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
