{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 6,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 66.0, 1109.0, 639.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 2,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "Default Max 7",
		"assistshowspatchername" : 0,
		"globalpatchername" : "bbdmi.preset[1][1][1]",
		"boxes" : [ 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-13",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 646.5, 1119.612499999999955, 162.0, 20.0 ],
					"text" : " • Route input / output values"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-9",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 182.0, 1119.612499999999955, 199.0, 162.099999999999909 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 469.612499999999955, 199.0, 162.099999999999909 ],
					"varname" : "bbdmi.crosspatch",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"bubbleside" : 2,
					"id" : "obj-7",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 424.75, 1540.612500000000182, 141.0, 52.0 ],
					"text" : "automatically scales 0,1 \nvalues range to 0,127",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-23",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.~2list.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 298.0, 299.1875, 199.0, 35.0 ],
					"varname" : "bbdmi.~2list",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.list2~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 639.5, 360.6875, 199.0, 35.0 ],
					"varname" : "bbdmi.list2~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-57",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.eavi.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 639.5, 57.6875, 199.0, 88.0 ],
					"varname" : "bbdmi.eavi",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-47",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 412.5, 83.6875, 138.0, 37.0 ],
					"text" : "1. adjust parameters \n2. turn on simulator"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"bgoncolor" : [ 0.55, 0.55, 0.55, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-52",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 385.0, 92.1875, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "3",
					"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ],
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"textovercolor" : [ 0.1, 0.1, 0.1, 1.0 ],
					"usebgoncolor" : 1,
					"usetextovercolor" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-8",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 39.0, 57.6875, 128.0, 33.0 ],
					"text" : "• Simulate EMG signal\n  (e.g. 4 CHANNELS)"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 14.0,
					"id" : "obj-4",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 39.0, 35.6875, 50.0, 22.0 ],
					"text" : "INPUT",
					"underline" : 1
				}

			}
, 			{
				"box" : 				{
					"arrows" : 2,
					"border" : 2.0,
					"id" : "obj-80",
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 32.0, 57.6875, 11.0, 88.0 ]
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_emg_simulator~.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 182.0, 57.6875, 198.0, 140.549999999999955 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 7.6875, 198.0, 140.549999999999955 ],
					"varname" : "bbdmi.rand~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-58",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 386.0, 555.1875, 149.0, 51.0 ],
					"text" : "(optional) adjust window size for more or less responsiveness"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-31",
					"linecount" : 4,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 873.0, 1671.6875, 229.0, 64.0 ],
					"text" : "(otional) set /adjust sound synthesis \nparameters and forward back to Max\n(MIDI and/or OSC)\n(toggle a number to freeze parameter)"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-15",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 388.0, 1419.1875, 140.0, 37.0 ],
					"text" : "(optional) set / adjust parameters range"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-42",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 859.375, 1599.612499999999955, 216.0, 20.0 ],
					"text" : " • Forward values to Max, MIDI or OSC"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 14.0,
					"id" : "obj-43",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 859.375, 1575.612499999999955, 66.0, 22.0 ],
					"text" : "OUTPUT",
					"underline" : 1
				}

			}
, 			{
				"box" : 				{
					"arrows" : 2,
					"border" : 2.0,
					"id" : "obj-44",
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 852.5, 1599.612499999999955, 11.0, 162.099999999999909 ]
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-41",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2osc.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 611.5, 1599.612499999999955, 199.0, 162.099999999999909 ],
					"presentation" : 1,
					"presentation_rect" : [ 904.200000000000045, 469.612499999999955, 199.0, 162.099999999999909 ],
					"varname" : "bbdmi.2osc",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-40",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2midi.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 395.75, 1599.612499999999955, 199.0, 162.099999999999909 ],
					"presentation" : 1,
					"presentation_rect" : [ 679.900000000000091, 469.612499999999955, 199.0, 162.099999999999909 ],
					"varname" : "bbdmi.2midi",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-37",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2max.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 182.0, 1599.612499999999955, 199.0, 162.099999999999909 ],
					"presentation" : 1,
					"presentation_rect" : [ 455.60000000000008, 469.612499999999955, 199.0, 162.099999999999909 ],
					"varname" : "bbdmi.2max",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-29",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 298.0, 360.6875, 199.0, 106.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 231.300000000000011, 7.6875, 199.0, 141.0 ],
					"setstyle" : 3,
					"size" : 4
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 395.75, 1787.6875, 150.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-18",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1039.5, 281.662500000000364, 85.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 822.5, 202.662500000000364, 85.0, 24.0 ],
					"text" : "load preset"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"bgoncolor" : [ 0.55, 0.55, 0.55, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-25",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1012.0, 283.662500000000364, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 795.0, 204.662500000000364, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "2",
					"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ],
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"textovercolor" : [ 0.1, 0.1, 0.1, 1.0 ],
					"usebgoncolor" : 1,
					"usetextovercolor" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 903.0, 209.162500000000364, 63.0, 22.0 ],
					"text" : "writeagain"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 20,
					"id" : "obj-17",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 903.0, 279.162500000000364, 100.0, 29.0 ],
					"pattrstorage" : "2a",
					"presentation" : 1,
					"presentation_rect" : [ 728.0, 200.162500000000364, 53.0, 29.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 903.0, 242.1875, 270.0, 22.0 ],
					"priority" : 					{
						"bbdmi.crosspatch::Crosspatch" : 1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 100, 500, 600 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 200, 200, 800, 500 ]
					}
,
					"text" : "pattrstorage 2a @autorestore 1 @changemode 1",
					"varname" : "2a"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"id" : "obj-27",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 997.5, 89.6875, 72.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 822.5, 139.6875, 72.0, 24.0 ],
					"text" : "start dsp"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"bgoncolor" : [ 0.55, 0.55, 0.55, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-35",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 970.0, 91.6875, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 795.0, 141.6875, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "1",
					"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ],
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"textovercolor" : [ 0.1, 0.1, 0.1, 1.0 ],
					"usebgoncolor" : 1,
					"usetextovercolor" : 1
				}

			}
, 			{
				"box" : 				{
					"arrows" : 2,
					"border" : 2.0,
					"id" : "obj-24",
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 639.5, 523.369642857142708, 10.0, 87.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-12",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 646.5, 1331.612499999999955, 312.0, 20.0 ],
					"text" : " • Scale parameteres to adapt to sound synthesis module"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 646.5, 774.491071428571331, 370.0, 20.0 ],
					"text" : " • Calibrate dynamic minimum–maximum input over a window of 10''"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-10",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 646.5, 523.369642857142708, 310.0, 20.0 ],
					"text" : "• Calculate root-mean-square (RMS) on a sliding window"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 14.0,
					"id" : "obj-1",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 646.5, 499.369642857142708, 167.0, 22.0 ],
					"text" : "FEATURE EXTRACTION",
					"underline" : 1
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 14.0,
					"id" : "obj-36",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 646.5, 750.491071428571331, 174.0, 22.0 ],
					"text" : "CONTROL PROCESSING",
					"underline" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-83",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.scale.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 182.0, 1331.612499999999955, 199.0, 162.099999999999909 ],
					"presentation" : 1,
					"presentation_rect" : [ 231.300000000000011, 469.612499999999955, 199.0, 162.099999999999909 ],
					"varname" : "bbdmi.scale",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"arrows" : 2,
					"border" : 2.0,
					"id" : "obj-90",
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 639.5, 774.491071428571331, 11.8125, 719.221428571428532 ]
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"id" : "obj-87",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 298.0, 636.930357142857019, 199.0, 106.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 231.300000000000011, 171.162500000000364, 199.0, 87.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3,
					"size" : 4
				}

			}
, 			{
				"box" : 				{
					"candycane" : 8,
					"contdata" : 1,
					"id" : "obj-82",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 294.5, 963.051785714285643, 199.0, 106.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 231.300000000000011, 285.491071428571331, 199.0, 162.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 1,
					"signed" : 1,
					"size" : 4,
					"spacing" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 903.0, 74.4375, 54.5, 54.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 728.0, 124.4375, 53.5, 53.5 ]
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, "@time", 10, "@polarity", 0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-14",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 182.0, 774.491071428571331, 199.0, 162.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 285.491071428571331, 199.0, 162.0 ],
					"varname" : "bbdmi.calibrate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, "@winsize", 0.5, "@clock", 10 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.rms~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 182.0, 523.369642857142708, 198.0, 87.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 7.0, 171.162500000000364, 198.0, 87.0 ],
					"varname" : "bbdmi.rms",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"midpoints" : [ 191.5, 947.6875, 304.0, 947.6875 ],
					"order" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"midpoints" : [ 191.5, 938.6875, 191.5, 938.6875 ],
					"order" : 1,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 912.5, 233.6875, 912.5, 233.6875 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 649.0, 485.6875, 191.5, 485.6875 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"midpoints" : [ 307.5, 335.6875, 307.5, 335.6875 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"midpoints" : [ 191.5, 611.6875, 191.5, 611.6875 ],
					"order" : 1,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"midpoints" : [ 191.5, 623.6875, 307.5, 623.6875 ],
					"order" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 649.0, 146.6875, 649.0, 146.6875 ],
					"order" : 1,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"midpoints" : [ 649.0, 347.6875, 307.5, 347.6875 ],
					"order" : 0,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"midpoints" : [ 191.5, 284.6875, 307.5, 284.6875 ],
					"order" : 1,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 191.5, 200.6875, 191.5, 200.6875 ],
					"order" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"midpoints" : [ 191.5, 1496.6875, 191.5, 1496.6875 ],
					"order" : 2,
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"midpoints" : [ 191.5, 1583.6875, 405.25, 1583.6875 ],
					"order" : 1,
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"midpoints" : [ 191.5, 1526.6875, 621.0, 1526.6875 ],
					"order" : 0,
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-14::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-14::obj-123::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"obj-14::obj-34::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-14::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-14::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-14::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-14::obj-42::obj-33" : [ "tab[69]", "tab[1]", 0 ],
			"obj-14::obj-43::obj-33" : [ "tab[86]", "tab[1]", 0 ],
			"obj-14::obj-44::obj-33" : [ "tab[95]", "tab[1]", 0 ],
			"obj-14::obj-45::obj-33" : [ "tab[99]", "tab[1]", 0 ],
			"obj-14::obj-46::obj-33" : [ "tab[101]", "tab[1]", 0 ],
			"obj-14::obj-47::obj-33" : [ "tab[102]", "tab[1]", 0 ],
			"obj-14::obj-48::obj-33" : [ "tab[103]", "tab[1]", 0 ],
			"obj-14::obj-49::obj-33" : [ "tab[87]", "tab[1]", 0 ],
			"obj-14::obj-50::obj-33" : [ "tab[96]", "tab[1]", 0 ],
			"obj-14::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"obj-37::obj-107::obj-27::obj-18" : [ "toggle[39]", "toggle", 0 ],
			"obj-37::obj-107::obj-48" : [ "SendTo-TXT[15]", "SendTo-TXT", 0 ],
			"obj-37::obj-107::obj-8" : [ "tab[88]", "tab[1]", 0 ],
			"obj-37::obj-123::obj-27::obj-18" : [ "toggle[64]", "toggle", 0 ],
			"obj-37::obj-123::obj-48" : [ "SendTo-TXT[43]", "SendTo-TXT", 0 ],
			"obj-37::obj-123::obj-8" : [ "tab[141]", "tab[1]", 0 ],
			"obj-37::obj-34::obj-27::obj-18" : [ "toggle[40]", "toggle", 0 ],
			"obj-37::obj-34::obj-48" : [ "SendTo-TXT[16]", "SendTo-TXT", 0 ],
			"obj-37::obj-34::obj-8" : [ "tab[84]", "tab[1]", 0 ],
			"obj-37::obj-36::obj-27::obj-18" : [ "toggle[41]", "toggle", 0 ],
			"obj-37::obj-36::obj-48" : [ "SendTo-TXT[17]", "SendTo-TXT", 0 ],
			"obj-37::obj-36::obj-8" : [ "tab[38]", "tab[1]", 0 ],
			"obj-37::obj-40::obj-27::obj-18" : [ "toggle[38]", "toggle", 0 ],
			"obj-37::obj-40::obj-48" : [ "SendTo-TXT[18]", "SendTo-TXT", 0 ],
			"obj-37::obj-40::obj-8" : [ "tab[43]", "tab[1]", 0 ],
			"obj-37::obj-41::obj-27::obj-18" : [ "toggle[42]", "toggle", 0 ],
			"obj-37::obj-41::obj-48" : [ "SendTo-TXT[19]", "SendTo-TXT", 0 ],
			"obj-37::obj-41::obj-8" : [ "tab[45]", "tab[1]", 0 ],
			"obj-37::obj-42::obj-27::obj-18" : [ "toggle[43]", "toggle", 0 ],
			"obj-37::obj-42::obj-48" : [ "SendTo-TXT[20]", "SendTo-TXT", 0 ],
			"obj-37::obj-42::obj-8" : [ "tab[39]", "tab[1]", 0 ],
			"obj-37::obj-43::obj-27::obj-18" : [ "toggle[44]", "toggle", 0 ],
			"obj-37::obj-43::obj-48" : [ "SendTo-TXT[21]", "SendTo-TXT", 0 ],
			"obj-37::obj-43::obj-8" : [ "tab[40]", "tab[1]", 0 ],
			"obj-37::obj-44::obj-27::obj-18" : [ "toggle[45]", "toggle", 0 ],
			"obj-37::obj-44::obj-48" : [ "SendTo-TXT[22]", "SendTo-TXT", 0 ],
			"obj-37::obj-44::obj-8" : [ "tab[46]", "tab[1]", 0 ],
			"obj-37::obj-45::obj-27::obj-18" : [ "toggle[46]", "toggle", 0 ],
			"obj-37::obj-45::obj-48" : [ "SendTo-TXT[23]", "SendTo-TXT", 0 ],
			"obj-37::obj-45::obj-8" : [ "tab[81]", "tab[1]", 0 ],
			"obj-37::obj-46::obj-27::obj-18" : [ "toggle[47]", "toggle", 0 ],
			"obj-37::obj-46::obj-48" : [ "SendTo-TXT[24]", "SendTo-TXT", 0 ],
			"obj-37::obj-46::obj-8" : [ "tab[123]", "tab[1]", 0 ],
			"obj-37::obj-47::obj-27::obj-18" : [ "toggle[48]", "toggle", 0 ],
			"obj-37::obj-47::obj-48" : [ "SendTo-TXT[25]", "SendTo-TXT", 0 ],
			"obj-37::obj-47::obj-8" : [ "tab[42]", "tab[1]", 0 ],
			"obj-37::obj-48::obj-27::obj-18" : [ "toggle[49]", "toggle", 0 ],
			"obj-37::obj-48::obj-48" : [ "SendTo-TXT[26]", "SendTo-TXT", 0 ],
			"obj-37::obj-48::obj-8" : [ "tab[147]", "tab[1]", 0 ],
			"obj-37::obj-49::obj-27::obj-18" : [ "toggle[50]", "toggle", 0 ],
			"obj-37::obj-49::obj-48" : [ "SendTo-TXT[44]", "SendTo-TXT", 0 ],
			"obj-37::obj-49::obj-8" : [ "tab[85]", "tab[1]", 0 ],
			"obj-37::obj-50::obj-27::obj-18" : [ "toggle[65]", "toggle", 0 ],
			"obj-37::obj-50::obj-48" : [ "SendTo-TXT[45]", "SendTo-TXT", 0 ],
			"obj-37::obj-50::obj-8" : [ "tab[119]", "tab[1]", 0 ],
			"obj-37::obj-74::obj-27::obj-18" : [ "toggle[37]", "toggle", 0 ],
			"obj-37::obj-74::obj-48" : [ "SendTo-TXT[10]", "SendTo-TXT", 0 ],
			"obj-37::obj-74::obj-8" : [ "tab[82]", "tab[1]", 0 ],
			"obj-40::obj-107::obj-27::obj-18" : [ "toggle[67]", "toggle", 0 ],
			"obj-40::obj-107::obj-8" : [ "tab[109]", "tab[1]", 0 ],
			"obj-40::obj-123::obj-27::obj-18" : [ "toggle[22]", "toggle", 0 ],
			"obj-40::obj-123::obj-8" : [ "tab[150]", "tab[1]", 0 ],
			"obj-40::obj-34::obj-27::obj-18" : [ "toggle[68]", "toggle", 0 ],
			"obj-40::obj-34::obj-8" : [ "tab[105]", "tab[1]", 0 ],
			"obj-40::obj-37::obj-27::obj-18" : [ "toggle[73]", "toggle", 0 ],
			"obj-40::obj-37::obj-8" : [ "tab[90]", "tab[1]", 0 ],
			"obj-40::obj-38::obj-27::obj-18" : [ "toggle[72]", "toggle", 0 ],
			"obj-40::obj-38::obj-8" : [ "tab[89]", "tab[1]", 0 ],
			"obj-40::obj-39::obj-27::obj-18" : [ "toggle[69]", "toggle", 0 ],
			"obj-40::obj-39::obj-8" : [ "tab[47]", "tab[1]", 0 ],
			"obj-40::obj-40::obj-27::obj-18" : [ "toggle[70]", "toggle", 0 ],
			"obj-40::obj-40::obj-8" : [ "tab[48]", "tab[1]", 0 ],
			"obj-40::obj-41::obj-27::obj-18" : [ "toggle[71]", "toggle", 0 ],
			"obj-40::obj-41::obj-8" : [ "tab[75]", "tab[1]", 0 ],
			"obj-40::obj-44::obj-27::obj-18" : [ "toggle[74]", "toggle", 0 ],
			"obj-40::obj-44::obj-8" : [ "tab[76]", "tab[1]", 0 ],
			"obj-40::obj-45::obj-27::obj-18" : [ "toggle[75]", "toggle", 0 ],
			"obj-40::obj-45::obj-8" : [ "tab[143]", "tab[1]", 0 ],
			"obj-40::obj-46::obj-27::obj-18" : [ "toggle[76]", "toggle", 0 ],
			"obj-40::obj-46::obj-8" : [ "tab[144]", "tab[1]", 0 ],
			"obj-40::obj-47::obj-27::obj-18" : [ "toggle[77]", "toggle", 0 ],
			"obj-40::obj-47::obj-8" : [ "tab[168]", "tab[1]", 0 ],
			"obj-40::obj-48::obj-27::obj-18" : [ "toggle[78]", "toggle", 0 ],
			"obj-40::obj-48::obj-8" : [ "tab[97]", "tab[1]", 0 ],
			"obj-40::obj-49::obj-27::obj-18" : [ "toggle[79]", "toggle", 0 ],
			"obj-40::obj-49::obj-8" : [ "tab[146]", "tab[1]", 0 ],
			"obj-40::obj-50::obj-27::obj-18" : [ "toggle[80]", "toggle", 0 ],
			"obj-40::obj-50::obj-8" : [ "tab[171]", "tab[1]", 0 ],
			"obj-40::obj-74::obj-27::obj-18" : [ "toggle[66]", "toggle", 0 ],
			"obj-40::obj-74::obj-8" : [ "tab[98]", "tab[1]", 0 ],
			"obj-41::obj-107::obj-8" : [ "tab[178]", "tab[1]", 0 ],
			"obj-41::obj-123::obj-8" : [ "tab[165]", "tab[1]", 0 ],
			"obj-41::obj-34::obj-8" : [ "tab[179]", "tab[1]", 0 ],
			"obj-41::obj-37::obj-8" : [ "tab[181]", "tab[1]", 0 ],
			"obj-41::obj-38::obj-8" : [ "tab[173]", "tab[1]", 0 ],
			"obj-41::obj-39::obj-8" : [ "tab[172]", "tab[1]", 0 ],
			"obj-41::obj-40::obj-8" : [ "tab[180]", "tab[1]", 0 ],
			"obj-41::obj-41::obj-8" : [ "tab[148]", "tab[1]", 0 ],
			"obj-41::obj-44::obj-8" : [ "tab[149]", "tab[1]", 0 ],
			"obj-41::obj-45::obj-8" : [ "tab[174]", "tab[1]", 0 ],
			"obj-41::obj-46::obj-8" : [ "tab[182]", "tab[1]", 0 ],
			"obj-41::obj-47::obj-8" : [ "tab[175]", "tab[1]", 0 ],
			"obj-41::obj-48::obj-8" : [ "tab[183]", "tab[1]", 0 ],
			"obj-41::obj-49::obj-8" : [ "tab[184]", "tab[1]", 0 ],
			"obj-41::obj-50::obj-8" : [ "tab[151]", "tab[1]", 0 ],
			"obj-41::obj-74::obj-8" : [ "tab[177]", "tab[1]", 0 ],
			"obj-41::obj-95" : [ "number[162]", "number", 0 ],
			"obj-83::obj-107::obj-33" : [ "tab[104]", "tab[1]", 0 ],
			"obj-83::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-83::obj-34::obj-33" : [ "tab[70]", "tab[1]", 0 ],
			"obj-83::obj-36::obj-33" : [ "tab[72]", "tab[1]", 0 ],
			"obj-83::obj-40::obj-33" : [ "tab[73]", "tab[1]", 0 ],
			"obj-83::obj-41::obj-33" : [ "tab[74]", "tab[1]", 0 ],
			"obj-83::obj-42::obj-33" : [ "tab[71]", "tab[1]", 0 ],
			"obj-83::obj-43::obj-33" : [ "tab[106]", "tab[1]", 0 ],
			"obj-83::obj-44::obj-33" : [ "tab[124]", "tab[1]", 0 ],
			"obj-83::obj-45::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-83::obj-46::obj-33" : [ "tab[107]", "tab[1]", 0 ],
			"obj-83::obj-47::obj-33" : [ "tab[126]", "tab[1]", 0 ],
			"obj-83::obj-48::obj-33" : [ "tab[108]", "tab[1]", 0 ],
			"obj-83::obj-49::obj-33" : [ "tab[127]", "tab[1]", 0 ],
			"obj-83::obj-50::obj-33" : [ "tab[128]", "tab[1]", 0 ],
			"obj-83::obj-74::obj-33" : [ "tab[100]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "2a.json",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/examples/presets",
				"patcherrelativepath" : "./presets",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2max.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2max",
				"patcherrelativepath" : "../output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2midi.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2midi",
				"patcherrelativepath" : "../output/2midi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2osc.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2osc",
				"patcherrelativepath" : "../output/2osc",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.~2list.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/utilities/~2list",
				"patcherrelativepath" : "../utilities/~2list",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_emg_pulse_ui4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bbdmi_emg_simulator~.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/input/emg_simulator~",
				"patcherrelativepath" : "../input/emg_simulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/source/js",
				"patcherrelativepath" : "../source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2max.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2max",
				"patcherrelativepath" : "../output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.2midi.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2midi",
				"patcherrelativepath" : "../output/2midi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.2osc.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2osc",
				"patcherrelativepath" : "../output/2osc",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
