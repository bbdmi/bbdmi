{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 6,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 100.0, 1444.0, 848.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-12",
					"linecount" : 71,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 567.654187719027163, 29.700182259082794, 537.0, 958.0 ],
					"text" : "bbdmi_hybrid\n\nThe bbdmi_hybrid module facillitate the hybridisation of  EMG (muscle activity) and EEG (brain activity) signals using machine learning-based regression models. It allows both independent and combined analysis of these signals, enabling their use in creative contexts such as interactive music composition and performance. The module provides three independent outputs (EMG, EEG, hybrid) and a fourth \"Gated Output\" that serially integrates EMG and EEG data based on predefined thresholds.\n\n1. Core Functionality\n\ta.)\nIndependent Modules: The module includes three machine-learning subsystems, each utilizing linear regression algorithms:\n\t1.\tEMG Regression Module: Maps EMG input to target outputs.\n\t2.\tEEG Regression Module: Processes EEG band data to generate target outputs.\n\t3.\tHybrid Module: Combines EMG and EEG signals to produce integrated outputs.\n\nb.)\n\tGated Output:\nA dynamic output conditioned on the states of EMG and EEG signals. It serially alternates between the EMG and EEG outputs based on threshold levels, adding adaptive interaction capabilities.\n\n2. Outputs\n\t- EMG Output: Independent output from the EMG regression model.\n\t- \tEEG Output: Independent output from the EEG regression model.\n- \tHybrid Output: Combines EMG and EEG data to explore their interactions.\n\t- Gated Output: \tA serial output controlled by thresholds applied to EMG and EEG signals. \tIncludes a dual-threshold system for EEG to account for its nuanced role in cognitive and physiological states.\n\n3. Threshold-Based Controls\n\tEMG Threshold:\n\t- \tA single minimum threshold triggers changes in the Gated Output when the signal is below the threshold.\n\t- \tDesigned for straightforward detection of muscle activity.\n\tEEG Thresholds: \tA double-threshold system (minimum and maximum) enables nuanced control based on specific EEG states.\n\n4. Training and Calibration\n\t- \tEach module (EMG, EEG, Hybrid) includes:\n\t- \tRun Button: Starts regression training for the module.\n\t- Clear Button: Resets the module to remove prior training data.\n\t- Color-Coded Feedback:\n- \tRed: Indicates the minimum output value (0).\n\t- \tBlue: Indicates the maximum output value (1).\n\t- Purple: Represents intermediate values, with a green horizontal bar showing precise numeric output.\n\n\tTraining Delays:\n\t- Configurable delay times (0, 3, or 6 seconds) allow users to prepare specific states before recording training data.\n\t- \tParticularly useful for EEG signals, enabling calibration for states such as closed eyes, relaxation, or focus.\n\n5. Gated Output Functionality\n\t- The Gated Output switches between EMG and EEG outputs based on threshold levels.\n\t- When one signal exceeds its threshold, the other signal is deactivated.\n- \tTiming indicators (dials) show how long a signal must remain above its threshold to activate the Gated Output.\n\t- \tTransition Indicators:\n\t- \tVisual “bangs” mark transitions between EMG and EEG control.\n\t- \tThe selected output is represented with a yellow rectangle under the signal slider.\n\nRecommended Usage\n\t1.\tTrain Modules: Use the Run and Clear buttons to train the regression models for EMG, EEG, and Hybrid outputs.\n\t2.\tSet Thresholds: Configure EMG and EEG thresholds to define Gated Output behavior.\n\t3.\tRoute Outputs: Use delay buttons to capture the switching of the Gated Output.\n\t4.\tMonitor Outputs: Leverage color-coded feedback to track output states.\n\t5.\tConfigure Gated Output: Adjust thresholds and timings to control the serial behavior of the output dynamically."
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.929411764705882, 0.0, 1.0 ],
					"format" : 6,
					"id" : "obj-24",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 334.935131132602692, 377.88461908698082, 50.0, 22.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.443137254901961, 0.356862745098039, 0.0, 1.0 ],
					"blinkcolor" : [ 1.0, 0.803921568627451, 0.0, 1.0 ],
					"id" : "obj-16",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 469.60179779926932, 374.311754435300827, 29.145729303359985, 29.145729303359985 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 334.935131132602692, 343.216088175773621, 221.0, 22.0 ],
					"text" : "route GatedOutput EMGbang EEGbang"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.443137254901961, 0.356862745098039, 0.0, 1.0 ],
					"blinkcolor" : [ 1.0, 0.803921568627451, 0.0, 1.0 ],
					"id" : "obj-7",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 402.181505739688873, 374.406358331441879, 29.145729303359985, 29.145729303359985 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 409.174662450949313, 33.208954036235809, 150.0, 47.0 ],
					"text" : "Attributes :\n@serialtimer 5\n@beep 1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-6",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 11.56716376543045, 343.216088175773621, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-3",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 119.356486221154512, 343.216088175773621, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-1",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 227.145808676878602, 343.216088175773621, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 213.164174020290375, 7.089551985263824, 70.0, 20.0 ],
					"text" : "EEG bands"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 46.268655061721802, 7.089551985263824, 36.0, 20.0 ],
					"text" : "EMG"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 320.0, 268.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 138.749991536140442, 104.114730536937714, 70.0, 22.0 ],
									"text" : "loadmess 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 73.749997735023499, 107.23973023891449, 56.0, 22.0 ],
									"text" : "metro 10"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 161.614725053310394, 416.0, 22.0 ],
									"text" : "0.860638 0.925293 0.941457 0.941457 0.483482 0.628957 0.736715 0.747491"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 134.73972761631012, 218.0, 22.0 ],
									"text" : "0.246413 0. 0.074 0.23025"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 170.000003278255463, 40.000000093750003, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-17",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 447.000003278255463, 40.000000093750003, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-18",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 49.999999278255466, 243.614715093750021, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-19",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 84.999999278255473, 243.614715093750021, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 1 ],
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 1 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"order" : 1,
									"source" : [ "obj-9", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 11.56716376543045, 158.208949565887451, 66.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p streamer"
				}

			}
, 			{
				"box" : 				{
					"contdata" : 1,
					"id" : "obj-11",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 142.164174020290375, 33.208954036235809, 212.0, 116.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"size" : 8
				}

			}
, 			{
				"box" : 				{
					"contdata" : 1,
					"id" : "obj-10",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 11.56716376543045, 33.208954036235809, 105.0, 116.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"size" : 4
				}

			}
, 			{
				"box" : 				{
					"args" : [ "@serialtimer", 5, "@beep", 1 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.hybrid.maxpat",
					"numinlets" : 2,
					"numoutlets" : 4,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 11.56716376543045, 187.686560451984406, 342.367967367172241, 150.581389963626862 ],
					"varname" : "bbdmi.hybrid[1]",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"midpoints" : [ 21.06716376543045, 150.0, 21.06716376543045, 150.0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 1 ],
					"midpoints" : [ 151.664174020290375, 159.0, 78.0, 159.0, 78.0, 153.0, 68.06716376543045, 153.0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"midpoints" : [ 479.10179779926932, 366.0, 479.10179779926932, 366.0 ],
					"source" : [ "obj-13", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"midpoints" : [ 344.435131132602692, 366.0, 344.435131132602692, 366.0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 411.768464465936006, 366.0, 411.681505739688873, 366.0 ],
					"source" : [ "obj-13", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 236.645808676878602, 339.0, 236.645808676878602, 339.0 ],
					"source" : [ "obj-2", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"midpoints" : [ 344.435131132602692, 339.0, 344.435131132602692, 339.0 ],
					"source" : [ "obj-2", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 128.856486221154512, 339.0, 128.856486221154512, 339.0 ],
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"midpoints" : [ 21.06716376543045, 339.0, 21.06716376543045, 339.0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 1 ],
					"midpoints" : [ 68.06716376543045, 183.0, 344.435131132602692, 183.0 ],
					"source" : [ "obj-20", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 21.06716376543045, 183.0, 21.06716376543045, 183.0 ],
					"source" : [ "obj-20", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-2::obj-125::obj-25" : [ "live.numbox[8]", "live.numbox", 0 ],
			"obj-2::obj-125::obj-96" : [ "live.numbox[7]", "live.numbox", 0 ],
			"obj-2::obj-143::obj-25" : [ "live.numbox[2]", "live.numbox", 0 ],
			"obj-2::obj-143::obj-96" : [ "live.numbox[1]", "live.numbox", 0 ],
			"obj-2::obj-24" : [ "live.dial", "live.dial", 0 ],
			"obj-2::obj-251" : [ "live.dial[2]", "live.dial", 0 ],
			"obj-2::obj-266" : [ "live.dial[3]", "live.dial", 0 ],
			"obj-2::obj-267" : [ "live.dial[4]", "live.dial", 0 ],
			"obj-2::obj-36" : [ "live.dial[1]", "live.dial", 0 ],
			"obj-2::obj-3::obj-25" : [ "live.numbox[3]", "live.numbox", 0 ],
			"obj-2::obj-3::obj-96" : [ "live.numbox[4]", "live.numbox", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "bbdmi.hybrid.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/hybrid",
				"patcherrelativepath" : "../hybrid",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.regress.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/regress",
				"patcherrelativepath" : "../control_processing/regress",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rapid.regression.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0
	}

}
