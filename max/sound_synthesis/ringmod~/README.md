# ringmod~

```bbdmi.ringmod~``` performs [ring modulation] (https://en.wikipedia.org/wiki/Ring_modulation) synthesis.

## Use

It takes two inputs (```signal```) on first (carrier) and second (modulator) inlets.

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (signal) *carrier*
[2] (signal) *modulator*

### OUTPUT

(mc.~ signal) *ring modulated signal*