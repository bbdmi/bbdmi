{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 6,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ -266.0, -993.0, 1582.0, 923.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 4.838709712028503, 544.096778035163879, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 337.398373782634735, 501.645164906978607, 239.0, 33.0 ],
					"text" : "This implementation needs the ABC library:\nhttps://github.com/alainbonardi/abclib"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 4.838709712028503, 450.806454837322235, 60.0, 22.0 ],
					"text" : "mc.*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 257.000001788139343, 418.549999999999955, 107.0, 22.0 ],
					"text" : "loadmess stereo 1"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-16",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 25.838709712028503, 505.645164906978607, 308.0, 25.0 ],
					"text" : "1 - Outputs connected to an ambisisonic decoder"
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-9",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 25.838709712028503, 477.419358253479004, 143.0, 25.0 ],
					"text" : "0 - All outputs mixed"
				}

			}
, 			{
				"box" : 				{
					"disabled" : [ 0, 0 ],
					"flagmode" : 1,
					"id" : "obj-17",
					"itemtype" : 0,
					"maxclass" : "radiogroup",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : 29,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 4.838709712028503, 473.870971381664276, 19.0, 60.0 ],
					"size" : 2,
					"value" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 4.838709712028503, 575.806455731391907, 93.0, 23.0 ],
					"text" : "mc.selector~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 137.099999999999994, 418.550000000000011, 115.0, 22.0 ],
					"text" : "abc.hoa.decoder~ 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4.838709712028503, 772.580650687217712, 74.0, 22.0 ],
					"text" : "mc.dac~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 4.838709712028503, 413.709680378437042, 92.0, 22.0 ],
					"text" : "mc.mixdown~ 2"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-2",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_ambLagrangeMods16.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 4.838709712028503, 11.290322661399841, 913.0, 368.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"id" : "obj-1",
					"linecount" : 65,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 924.0, 6.0, 543.0, 878.0 ],
					"text" : "bbdmi_ambLagrangeMods~\n\nThe bbdmi_ambLagrangeMods~ module is a multichannel sound synthesis tool based on Lagrange polynomial interpolation. It is designed to generate complex, spatialized waveforms suitable for multichannel setups and integration with ambisonic decoders. This module builds upon the principles of Lagrange polynomial synthesis, with added features for transposition and channel-specific parameter distribution. Below is a detailed explanation of its functionality and parameters:\n\n1. Multichannel architecture\n\t- Independent channel generation:\nFor every channel instantiated, the module generates two additional internal channels with differing parameters. These are mixed at the output to enrich the spectral content.\n\t•\tParameter distribution:\nInternal parameters for each channel are distributed uniquely based on the selected indexdistr, ensuring that each channel exhibits distinct characteristics.\n\n2. Frequency control\n\t- Freq: Sets the fundamental frequency of the polynomial waveform for each channel, determining its base repetition rate.\n\t- modGlobalFreq: Defines the global modulation frequency that applies to the oscillations of interpolation points across all channels.\n\n3. Modulation of interpolation points\nThe intermediate points in the polynomial oscillate along the Y-axis:\n\t- y2modFactor, y3modFactor, y4modFactor, y5modFactor: Set the modulation frequencies for each point as multiples of modGlobalFreq. These values determine the spectral richness and dynamic behavior of the waveform.\n\n4. Parameters distribution\n\t- indexdistr: Selects from 22 transfer functions to vary the distribution of internal parameters and transposition settings. This ensures unique characteristics for each channel while shaping the overall spectral response.\n\n5. Delay and temporal effects\n\t- retardFactor: Introduces a delay (<1000 ms) for each channel, contributing to spatial and spectral diversity.\n\t- Short delays: Produce comb-filtering effects.\n\t- \tLonger delays: Create harmonic interferences and complex spatial textures.\n\n6. Transposition control (unique to this module)\n\t- maxtransp: Specifies the maximum transposition range (in semitones) applied to the output of each channel. Higher values introduce greater pitch shifts, expanding the harmonic possibilities.\n\t- transpSide: Determines the direction of the transposition:\n\t- Positive values: Shift the output to higher pitches.\n\t- \tNegative values: Shift the output to lower pitches.\n\n7. Random exploration and morphing\nThe module offers tools for randomizing parameters and exploring new configurations:\n\t- newRandom: Generates random values for all modifiable parameters within user-defined ranges.\n\t- \tdryWetRandom: Balances the direct signal with the randomized signal.\n\t- \trandomGetTime: Sets the morphing duration between the current state and a new randomized state.\n\t- \tfreqRandom: Maintains a fixed base frequency while allowing spectral exploration through randomized parameters.\n\nRecommended Usage\n\t1.\tSet up multichannel outputs: Choose the desired number of channels for your synthesis.\n\t2.\tConfigure Freq and modGlobalFreq: Define the base frequency and modulation rates.\n\t3.\tAdjust y2modFactor, y3modFactor, y4modFactor, y5modFactor: Fine-tune the oscillations of interpolation points for each channel.\n\t4.\tSelect a transfer function (indexdistr): Shape the spectral properties of your sound.\n\t5.\tUse maxtransp and transpSide: Add pitch-shifting effects to enhance harmonic complexity.\n\t6.\tActivate delays with retardFactor: Create temporal and spatial effects.\n\t7.\tExperiment with randomization tools: Discover unique textures by dynamically morphing parameters or introducing randomness."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "gain~",
					"multichannelvariant" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 4.838709712028503, 616.129036664962769, 22.0, 140.0 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 1 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"order" : 1,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"order" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 1 ],
					"midpoints" : [ 14.338709712028503, 474.0, 0.0, 474.0, 0.0, 540.0, 51.338709712028503, 540.0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 2 ],
					"midpoints" : [ 146.599999999999994, 450.0, 66.0, 450.0, 66.0, 447.0, 0.0, 447.0, 0.0, 540.0, 88.338709712028503, 540.0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "abc.hoa.decoder~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/abclib/patchers",
				"patcherrelativepath" : "../../../../../Max 8/Packages/abclib/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "abc_2d_decoder3_8~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_wrp.js",
				"bootpath" : "~/Documents/Max 8/Packages/abclib/javascript",
				"patcherrelativepath" : "../../../../../Max 8/Packages/abclib/javascript",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_ambLagrangeMods16.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/faust/bbdmi_faust_lib/MAX_abstractions/bbdmi_ambLagrangeMods",
				"patcherrelativepath" : "../../../faust/bbdmi_faust_lib/MAX_abstractions/bbdmi_ambLagrangeMods",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_ambLagrangeMods16~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "ui_bbdmi.js",
				"bootpath" : "~/Documents/GitLab/bbdmi/faust/bbdmi_faust_lib/Faust2GUI_wrapper_compiler",
				"patcherrelativepath" : "../../../faust/bbdmi_faust_lib/Faust2GUI_wrapper_compiler",
				"type" : "TEXT",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
