bbdmi_ambLagrangeMods~

The bbdmi_ambLagrangeMods~ module is a multichannel sound synthesis tool based on Lagrange polynomial interpolation. It is designed to generate complex, spatialized waveforms suitable for multichannel setups and integration with ambisonic decoders. This module builds upon the principles of Lagrange polynomial synthesis, with added features for transposition and channel-specific parameter distribution. Below is a detailed explanation of its functionality and parameters:

1. Multichannel architecture
	•	Independent channel generation:
For every channel instantiated, the module generates two additional internal channels with differing parameters. These are mixed at the output to enrich the spectral content.
	•	Parameter distribution:
Internal parameters for each channel are distributed uniquely based on the selected indexdistr, ensuring that each channel exhibits distinct characteristics.

2. Frequency control
	•	Freq: Sets the fundamental frequency of the polynomial waveform for each channel, determining its base repetition rate.
	•	modGlobalFreq: Defines the global modulation frequency that applies to the oscillations of interpolation points across all channels.

3. Modulation of interpolation points
The intermediate points in the polynomial oscillate along the Y-axis:
	•	y2modFactor, y3modFactor, y4modFactor, y5modFactor: Set the modulation frequencies for each point as multiples of modGlobalFreq. These values determine the spectral richness and dynamic behavior of the waveform.

4. Parameters distribution
	•	indexdistr:
Selects from 22 transfer functions to vary the distribution of internal parameters and transposition settings. This ensures unique characteristics for each channel while shaping the overall spectral response.

5. Delay and temporal effects
	•	retardFactor:
Introduces a delay (<1000 ms) for each channel, contributing to spatial and spectral diversity.
	•	Short delays: Produce comb-filtering effects.
	•	Longer delays: Create harmonic interferences and complex spatial textures.

6. Transposition control (unique to this module)
	•	maxtransp:
Specifies the maximum transposition range (in semitones) applied to the output of each channel. Higher values introduce greater pitch shifts, expanding the harmonic possibilities.
	•	transpSide:
Determines the direction of the transposition:
	•	Positive values: Shift the output to higher pitches.
	•	Negative values: Shift the output to lower pitches.

7. Random exploration and morphing
The module offers tools for randomizing parameters and exploring new configurations:
	•	newRandom: Generates random values for all modifiable parameters within user-defined ranges.
	•	dryWetRandom: Balances the direct signal with the randomized signal.
	•	randomGetTime: Sets the morphing duration between the current state and a new randomized state.
	•	freqRandom: Maintains a fixed base frequency while allowing spectral exploration through randomized parameters.

Recommended Usage
	1.	Set up multichannel outputs: Choose the desired number of channels for your synthesis.
	2.	Configure Freq and modGlobalFreq: Define the base frequency and modulation rates.
	3.	Adjust y2modFactor, y3modFactor, y4modFactor, y5modFactor: Fine-tune the oscillations of interpolation points for each channel.
	4.	Select a transfer function (indexdistr): Shape the spectral properties of your sound.
	5.	Use maxtransp and transpSide: Add pitch-shifting effects to enhance harmonic complexity.
	6.	Activate delays with retardFactor: Create temporal and spatial effects.
	7.	Experiment with randomization tools: Discover unique textures by dynamically morphing parameters or introducing randomness.