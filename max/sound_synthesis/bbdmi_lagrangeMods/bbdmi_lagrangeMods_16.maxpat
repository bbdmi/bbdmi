{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 6,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ -118.0, -930.0, 1444.0, 848.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"fontface" : 0,
					"id" : "obj-1",
					"linecount" : 45,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 765.0, 12.0, 538.0, 610.0 ],
					"text" : "The bbdmi_lagrangeMods~ module is a sound synthesis tool based on Lagrange polynomial interpolation. It generates modifiable waveforms by controlling the positions of defined points.\n\n\t1.\tStructure of interpolation points:\n- The polynomial is defined by 6 points.\n- Fixed points: The first and last points (y1, y6) are locked at 0 to ensure waveform continuity between repetitions.\n- Modifiable points: The intermediate points (y2, y3, y4, y5) can be moved along the Y-axis to sculpt the waveform.\n\n\t2.\tFrequency control:\n- Freq: Defines the fundamental frequency of the polynomial, determining the repetition rate of the generated waveform.\n- modGlobalFreq: Specifies the global modulation frequency of the intermediate points.\n\n\t3.\tModulation of intermediate points:\nThe modifiable points oscillate along the Y-axis at frequencies calculated as follows:\n- y2modFactor, y3modFactor, y4modFactor, y5modFactor: These parameters multiply the frequency defined by modGlobalFreq for each individual point.\n\n\t4.\tTransformation effects:\n- indexdistr: Allows selection among 22 transfer functions. These functions modify how the internal parameters interact, generating a variety of waveforms and spectral textures.\n\n\t5.\tDelay and effects:\n- retardFactor: Introduces a variable delay (<1000 ms) for each channel of the module.\n- Short delays produce comb-filter-like effects.\n- Longer delays generate complex harmonic interferences.\n\n\t6.\tRandom exploration and morphing:\nThe module includes exploration tools for quickly experimenting with different sound configurations:\n- newRandom: Generates random values for all modifiable parameters within user-defined limits.\n- dryWetRandom: Mixes the direct signal with the random signal, allowing gradual transitions between the two.\n- randomGetTime: Sets the morphing time between the current state and a new random configuration.\n- freqRandom: Determines if the frequency is controlled or not by the random values generator\n\nRecommended Usage:\n\t1.\tConfigure Freq to set the fundamental frequency of your waveform.\n\t2.\tAdjust modGlobalFreq and the multipliers (y2modFactor, etc.) to control the modulation of the intermediate points.\n\t3.\tSelect a transfer function via indexdistr to shape the spectral response.\n\t4.\tActivate and adjust delay with retardFactor to enrich the sound with temporal effects.\n\t5.\tExperiment with randomization tools to discover unique sonic textures."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 11.0, 578.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "gain~",
					"multichannelvariant" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 11.0, 406.0, 22.0, 140.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-6",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_lagrangeMods16.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 11.0, 12.0, 752.0, 370.0 ],
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 1 ],
					"order" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"order" : 1,
					"source" : [ "obj-7", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "bbdmi_lagrangeMods16.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/faust/bbdmi_faust_lib/MAX_abstractions/bbdmi_lagrangeMods",
				"patcherrelativepath" : "../../../faust/bbdmi_faust_lib/MAX_abstractions/bbdmi_lagrangeMods",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_lagrangeMods16~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "ui_bbdmi.js",
				"bootpath" : "~/Documents/GitLab/bbdmi/faust/bbdmi_faust_lib/Faust2GUI_wrapper_compiler",
				"patcherrelativepath" : "../../../faust/bbdmi_faust_lib/Faust2GUI_wrapper_compiler",
				"type" : "TEXT",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
