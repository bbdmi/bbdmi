# Sound Synthesis

bbdmi_NNaddSynth1 is a additive synthesiser that works with an internal perceptron to regulate the parameters like wave morphing, delay, and feedback intensities. It has to be used with the WeightsAndBias object under the controlprocessing folder in order to configure it's internal Weights and Biases as the learning algorithm haven't been implemented yet.