# vector sum

```bbdmi.vectorsum``` performs the polar-to-cartesian conversion of a list of control messages, combining x and y axes with a range of ```[-1,1]``` from the first outlet, amplitude and angle ```[-1,1 / -360,360]``` from the second outlet, points for ```xydisplay``` object from the third outlet.

## Use

It takes one input (```list```) and outputs the vector sum of (```list```) of values.

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *input*

[2] (```rotation``` float) *rotation factor [range: 0,1]*

### OUTPUT

>[outlet] (```message```, type) *description*

[1] (list) *combined xy [range: -1,1]*

[2] (list) *amplitude/angle [range: 1,1 / -360,360]*

[3] (list) *monitor for xydisplay object (CNMAT)*

## Dependencies

It requires [CNMAT-Externs](https://github.com/CNMAT/CNMAT-Externs).
