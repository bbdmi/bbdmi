# rms~

```bbdmi.rms~``` performs the Root-Mean-Square (RMS) of a (```mc.~ list```) of audio signals.

## Use

It takes one input (```mc.~ list```) and outputs a root-mean-squared version (```list```) for each channel on a common sliding time-window (ms). It outputs RMS values as messages in the range of ```[0,1]```.

### ARGUMENTS

(int) *number of channels*

### ATTRIBUTES

(```@winsize``` float) *window size (s) (range: [0.1, 10])*

(```@clock``` int) *clock interval (ms)*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (mc.~ list) *to average*

[2] (```winsize``` float) ```window size (ms) (range: [0.1, 10])*

[2] (```clock``` int) *clock interval (ms)*

### OUTPUT

(list) *averaged [range: 0,1]*