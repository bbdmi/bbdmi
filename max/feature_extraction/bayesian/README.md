# bayesian

```bbdmi.bayesian~``` performs the [bayesian filter estimation](https://en.wikipedia.org/wiki/Recursive_Bayesian_estimation) of a (```list```) of control signals. It is particularly useful for the envelope extraction of EMG data.

## Use

It takes one input (```list```) and outputs a filtered version (```list```).

### ARGUMENTS

(int) *number of channels*

### ATTRIBUTES

(```@jumprate``` int) *log-probability of sudden jumps (lower = less jumps)*

(```@diffrate``` int) *log of the diffusion rate (lower = slower changes)*

(```@mvc``` list) *maximum value contraction (vector)*

(```@calibrate``` toggle) *calibrate maximum value contraction, or force*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *to filter*

[2] (```jumprate``` int) *log-probability of sudden jumps (lower = less jumps)*

[2] (```diffrate``` int) *log of the diffusion rate (lower = slower changes)*

[2] (```mvc``` list) *maximum value contraction (vector)*

[2] (```calibrate``` toggle) *calibrate maximum value contraction, or force*

### OUTPUT

(list) *filtered [range: 0,1]*