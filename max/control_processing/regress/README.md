# regress

```bbdmi.regress``` is a wrapper around the [rapid](https://github.com/mzed/rapid/releases/tag/v0.0.4) object, which performs regression tasks in the style of [Wekinator](http://www.wekinator.org/), predicting continuous values in response to new input. The object is based on the [RapidLib C++]((http://gitlab.doc.gold.ac.uk/rapid-mix/RapidLib)) machine learning library. This module allows the user to interactively record examples pairs of input and output, train, edit (add/remove examples) and run models.

## Use

It takes one input (```list```) in the first inlet and one output (```int```) in the second inlet, and output a prediction (```list```) based on new input data.

### ARGUMENTS

>[order] (```message```, type) *description*

[1] (int) *number of input channels*

[2] (int) *number of output channels*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *input*

[2] (list) *output*

[3] (```record``` bang) *record (default: 20 examples at 40ms)*

[3]	(```train``` bang) *train*

[3]	(```run``` toggle) *run/stop*

[3]	(```undo``` bang) *delete last recording*

[3]	(```clear``` bang) *clear training set, and reset model*

[3]	(```read``` path) *read a pre-trained model (.json)*

[3]	(```write``` bang) save trained model to disk (.json)*

### OUTPUT

>[outlet] (```message```, type) *description*

[1] (list) *output/prediction*

[2] (```record``` done) *done recording*

[2]	(```train``` done) *done training*

## Externals

For these patch to work, the following external must be downloaded and placed in the Max search path: Max/Options/File Preferences..., add a new path.

- [rapid](https://github.com/mzed/rapid/releases/tag/v0.0.4) is machine learning object for gesture-sound interaction design. It provides a set of supervised machine learning algorithms that implement multilayer perceptron artificial neural networks, allowing to train linear regression and k-nearerst neighbour classification models between multiple dimensions of input and target output, and predict new output values based on new input data.

 
## DISCLAIMER:
This folder contains the 'rapid.regression.mxo' object to facilitate the use of the regression module for our users. However, we would like to acknowledge that the development of this object was not carried out by the BBDMI project. We extend our gratitude to Michael Zbyszyński for his contribution, which can be accessed via this link : [rapid](https://github.com/mzed/rapid/releases/tag/v0.0.4)