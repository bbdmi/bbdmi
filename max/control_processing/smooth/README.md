# smooth

```bbdmi.smooth``` linearly smooth messages over time using a [leaking integrator](https://en.wikipedia.org/wiki/Leaky_integrator) function.

## Use

It takes one input (```list```) and outputs a smoothed version (```list```). 

### ATTRIBUTES

(int) *number of channels*

### ARGUMENTS

(```@amount``` int) *smoothing factor [range: 0,1]*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (int, float) *to smooth*

[2] (```amount``` int) *smoothing factor [range: 0,1]*

### OUTPUT

(list, float) *smoothed [range: 0,1]*