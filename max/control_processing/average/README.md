# average

```bbdmi.average``` performs the arithmetic mean of a (```list```) of messages, i.e. the sum of a list of numbers divided by the count of numbers in the list.

## Use

It takes one input (```list```) and outputs an averaged version (```float, int```).  

### ARGUMENTS

(int) *number of channels (number count)*

### MESSAGES

(list) *to average*

### OUTPUT

(int, float) *averaged [range: 0,1]*