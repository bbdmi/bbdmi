{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 33.0, 79.0, 529.0, 674.0 ],
		"openrect" : [ 34.0, 79.0, 0.0, 0.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 2,
		"toptoolbarpinned" : 2,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 3,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "Default Max 7",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-35",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 246.0, 608.5, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "1",
					"textcolor" : [ 0.34902, 0.34902, 0.34902, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bubble" : 1,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-38",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 339.5, 606.0, 119.0, 25.0 ],
					"text" : "turn audio on/off"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 276.0, 592.0, 56.0, 56.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 276.0, 556.5, 164.0, 22.0 ],
					"text" : "bbdmi.multislider 4 @mode 0",
					"varname" : "bbdmi.rand~[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 276.0, 189.0, 223.0, 22.0 ],
					"text" : "bbdmi.rms~ 4 @winsize 0.25 @clock 20",
					"varname" : "bbdmi.rand~[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 276.0, 56.5, 207.0, 22.0 ],
					"text" : "bbdmi_rand~ 4 @rate 1. @amount 1.",
					"varname" : "bbdmi.rand~[4]"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4, "@winsize", 0.25, "@clock", 20 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-12",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.rms~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 23.000000000000028, 153.333333333333343, 199.0, 88.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.5, 234.0, 198.999999999999972, 131.0 ],
					"varname" : "bbdmi.rms~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, "mode", 0 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-13",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 23.000000000000028, 487.0, 199.0, 161.0 ],
					"varname" : "bbdmi.multislider[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, "rate", "1.amount", 1 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-15",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.rand~.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 23.0, 23.0, 199.000000000000028, 89.0 ],
					"varname" : "bbdmi.rand~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, "@time", 5 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-5",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 23.000000000000028, 283.666666666666686, 199.0, 161.0 ],
					"varname" : "bbdmi.calibrate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 276.0, 353.166666666666742, 212.0, 22.0 ],
					"text" : "bbdmi.calibrate 4 @time 5 @polarity 0",
					"varname" : "bbdmi.rand~[3]"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 1 ],
					"midpoints" : [ 473.5, 174.0, 489.5, 174.0 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 285.5, 90.0, 285.5, 90.0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 285.5, 213.0, 285.5, 213.0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 285.5, 378.0, 285.5, 378.0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-3::obj-107::obj-33" : [ "tab[19]", "tab[1]", 0 ],
			"obj-3::obj-123::obj-33" : [ "tab[227]", "tab[1]", 0 ],
			"obj-3::obj-34::obj-33" : [ "tab[20]", "tab[1]", 0 ],
			"obj-3::obj-36::obj-33" : [ "tab[17]", "tab[1]", 0 ],
			"obj-3::obj-40::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-3::obj-41::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-3::obj-42::obj-33" : [ "tab[23]", "tab[1]", 0 ],
			"obj-3::obj-43::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-3::obj-44::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-3::obj-45::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-3::obj-46::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-3::obj-47::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-3::obj-48::obj-33" : [ "tab[29]", "tab[1]", 0 ],
			"obj-3::obj-49::obj-33" : [ "tab[30]", "tab[1]", 0 ],
			"obj-3::obj-50::obj-33" : [ "tab[31]", "tab[1]", 0 ],
			"obj-3::obj-74::obj-33" : [ "tab[16]", "tab[1]", 0 ],
			"obj-5::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-5::obj-123::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"obj-5::obj-34::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-5::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-5::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-5::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-5::obj-42::obj-33" : [ "tab[7]", "tab[1]", 0 ],
			"obj-5::obj-43::obj-33" : [ "tab[8]", "tab[1]", 0 ],
			"obj-5::obj-44::obj-33" : [ "tab[9]", "tab[1]", 0 ],
			"obj-5::obj-45::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-5::obj-46::obj-33" : [ "tab[11]", "tab[1]", 0 ],
			"obj-5::obj-47::obj-33" : [ "tab[12]", "tab[1]", 0 ],
			"obj-5::obj-48::obj-33" : [ "tab[13]", "tab[1]", 0 ],
			"obj-5::obj-49::obj-33" : [ "tab[14]", "tab[1]", 0 ],
			"obj-5::obj-50::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-5::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitLab/backup_20_juin_2023/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../../../backup_20_juin_2023/bbdmi/max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/GitLab/backup_20_juin_2023/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../../../../backup_20_juin_2023/bbdmi/max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/GitLab/backup_20_juin_2023/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../../../../backup_20_juin_2023/bbdmi/max/feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_rand4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bbdmi_rand~.maxpat",
				"bootpath" : "~/Documents/GitLab/backup_20_juin_2023/bbdmi/max/input/rand~",
				"patcherrelativepath" : "../../../../../backup_20_juin_2023/bbdmi/max/input/rand~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitLab/backup_20_juin_2023/bbdmi/max/source/js",
				"patcherrelativepath" : "../../../../../backup_20_juin_2023/bbdmi/max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitLab/backup_20_juin_2023/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../../../backup_20_juin_2023/bbdmi/max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitLab/backup_20_juin_2023/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../../../../backup_20_juin_2023/bbdmi/max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
