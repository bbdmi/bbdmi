# scale

```bbdmi.scale``` scales control signals definable individually by the user as minimum and maximum output value.

## Use

It takes one input (```list```) and outputs a scaled version (```list```). 

### ARGUMENTS

(int) *number of channels*

### MESSAGES

(list) *to scale*

### OUTPUT

(list) *scaled [range: 0,1]*