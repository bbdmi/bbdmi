# ramp

```bbdmi.ramp``` generates ramps and line segments from one value to another within a specific amount of time.

## Use

It takes one input (```list```) and outputs a ramped version (```list```). 

### ARGUMENTS

(int) *number of channels*

### ATTRIBUTES

(```@time``` int) *ramp time (ms)*

(```@grain``` int) *output interval (grain (ms)*

### MESSAGES

>[inlet] (```message```, type) *description*

[1] (list) *to ramp*

[2] (```time``` int) *ramp time (ms)*

[2]	(```grain``` int) *output interval (grain (ms)*

### OUTPUT

(list) *ramped [range: 0,1]*
