# unicorn

```bbdmi.unicorn``` acquires and outputs EEG data from a g.tec Unicorn headset using UPD datagram packets.

## Use

It outputs a (```list```) of 8-channels EEG messages: individual channel average, total average, and bipolar deviation and average. It uses the <mxj net.udp.recv> object for receiving data over the network.

### MESSAGES

(```port``` int) *receive network port*

### OUTPUT

(list) *EEG [range: -1,1]*