# 2midi

```bbdmi.2midi``` sends out control signals as MIDI messages.

## Use

It takes one input (```list```) and sends each individual value using the MIDI format (value, number, channel). Values can be set/adjusted using GUI elements. A dropdown menu must be used to set the MIDI output port. The default output midi channel is set to ```1```.

### ATTRIBUTES

(int) *number of channels*

### MESSAGES

(list) *to forward*