# 2xbend

```bbdmi.2pitchbend``` sends out control signals as 14-bit high-resolution MIDI pitch-bend data.

## Use

It takes one input (```list```) and sends each individual value using the xbendout MIDI format (value, channel). Output range is: ```0. - 16383.```.

### ATTRIBUTES

(int) *number of channels*

### MESSAGES

(list) *to forward*