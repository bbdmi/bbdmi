// -----------------
// ui2.js
// -----------------
//
// Automatically (re)generate/destroy sliders according to Faust DSP module
// David Fierro - Alain Bonardi - CICM / BBDMI - ANR project, Sept 2023
//
// Currently missing : Many things; reported in a soon final version.
// Started from ui.js by  Edgar Berdahl, Stéphane Letz, July 2014/Grame, 2014-2016
//

// Global table
var dsp_ui_table = [];

var faust = faust || {};

var clownColors = false;

// Global variables
faust.mainFaustObject;
faust.numobjects = -1;
faust.numsliders = -1;
faust.numcomments = -1;
faust.numflonums = -1;
faust.nummessages = -1;
faust.numpanels = -1;
faust.numbuttons = -1;
faust.numnentrys = -1;


var thru = patcher.getnamed("thru");
var theOutlet = patcher.getnamed("outlet");
var firstInlet = patcher.getnamed("inlet1");
var secondInlet = patcher.getnamed("inlet2");

//Constants
var gap = 4;//gap between objects
var textlengthmult = 7;//number of points per character
var commentsize = [100, 20];
var flonumsize = [50, 23];//x,y=> y: is not the real height but the police size, 1.714 multiplier
var knobsize = [40, 40];
var vslidersize = [20, 140];
var hslidersize = [140, 20];
var buttonsize = [24, 24];
var checkboxsize = [24, 24];

// Arrays containing the objects
faust.theComments = new Array(128);
faust.theSliders = new Array(128);
faust.theButtons = new Array(128);
faust.theMessages = new Array(128);
faust.thenumberBoxes = new Array(128);
faust.thePanels = new Array(128);
faust.theObjects = new Array(128);

var isWrapped = false;

faust.ui = function (json, patcher) {
    starts_with = function (str, prefix) {
        return (str.lastIndexOf(prefix, 0) === 0);
    }

    get_dsp_name = function (patcher, name) {
        var obj = patcher.firstobject;
        while (obj) {
            if (starts_with(obj.varname, name)) {
                return obj;
            }
            obj = obj.nextobject;
        }
        return null;
    }

    //Not working: // I was planning to add a meter~ to every input and ouput...
    inoutsbargraphs = function (patcher) {
        const numObjects = patcher.count;
        var obj = patcher.firstobject;
        for (var i = 0; i < numObjects; i++) {
            //use first and nextb object
            //var object = patcher.getindex(i);
            //post(object.getattr("id"));

            post("id :  ", obj.getattr("id"));
            post("outlets :  ", obj.numoutlets);//obj.getattr("outlettype").length);
            obj = obj.nextobject;
        }
        //var testobj = patcher.getnamed(parsed_json.name + "~");
    }

    // JSON parsing
    parse_ui = function (ui, target, patcher) {
        for (var i = 0; i < ui.length; i++) {
            newparse_item(ui[i], target, patcher);
        }
        //TODO:AS faust.numbojecets IS A GLOBAL VARIABLE WE SHOULD REMOVE IT FROM THE ARGUMENTS OF THE METHODS.
        parenting(faust.numobjects);//Assign parent to each object and assign the childs to each parent.
        sizing(faust.numobjects);//Determine the size depending on the type of object and name size
        positioning(faust.numobjects);//Calculate the position of start and finish of each object
        createGUI(target, patcher);//Create the GUI

    }

    newparse_items = function (items, target, patcher) {
        for (var i = 0; i < items.length; i++) {
            newparse_item(items[i], target, patcher);
        }
    }

    newparse_item = function (item, target, patcher) {
        faust.numobjects++;
        //We assign the properties of each object to a new 'object' on JS
        const guiObject = new Object();
        if (item.type) guiObject.type = item.type;
        if (item.label) guiObject.label = item.label;
        if (item.meta) guiObject.meta = item.meta;
        if (item.shortname) guiObject.shortname = item.shortname;
        if (item.address) guiObject.address = item.address;
        if (item.init != null) guiObject.init = parseFloat(item.init);
        if (item.min != null) guiObject.min = parseFloat(item.min);
        if (item.max != null) guiObject.max = parseFloat(item.max);
        if (item.step != null) guiObject.step = parseFloat(item.step);
        if (item.items) {
            guiObject.items = item.items;
            guiObject.childs = {};
            for (var i = 0; i < guiObject.items.length; i++) {//check number of groups directly under its wing
                guiObject.childs[i] = guiObject.items[i].label;

            }
            guiObject.numchilds = guiObject.items.length;
        }
        guiObject.position = [0, 0];
        faust.theObjects[faust.numobjects] = guiObject;//The object is saved on an array which is used as an id(the arary index)
        if (item.items) newparse_items(item.items, target, patcher);
    }

    parenting = function (_numobjects) {
        for (var s = 0; s < _numobjects; s++) {//Only groups can have kids
            if (faust.theObjects[s].type === "vgroup" || faust.theObjects[s].type === "hgroup" || faust.theObjects[s].type === "tgroup") {
                if (faust.theObjects[s].childs) {
                    var numkids = 0;
                    faust.theObjects[s].childsnums = {};
                    do {
                        for (var l = s + 1; l <= _numobjects; l++) {
                            if (faust.theObjects[s].childs[numkids] == faust.theObjects[l].label) {
                                faust.theObjects[l].parent = s;
                                faust.theObjects[l].numkid = numkids;
                                faust.theObjects[s].childsnums[numkids] = l;
                                numkids++;
                            }
                        }

                    } while (numkids < faust.theObjects[s].childs.length);
                }
            }
        }
    }

    sizing = function (_numobjects) {
        //This method  can be optimized by doing everything under the last 'for' loop, the one going in the oppposite direction. Loading everyting under each group
        for (var s = 0; s <= _numobjects; s++) {

            if (faust.theObjects[s].type === "hbargraph" || faust.theObjects[s].type === "vbargraph") {
                //faust.theObjects[s].xsize = 10;
                //faust.theObjects[s].ysize = 10;
            } else if (faust.theObjects[s].type === "vslider" || faust.theObjects[s].type === "hslider") {
                if (faust.theObjects[s].meta && faust.theObjects[s].meta[1] && faust.theObjects[s].meta[1].style == "knob") {
                    var xminsize = flonumsize[0] + 2 * gap;//2 times the gab, left and right. 50 is because of the float that comes under the multislider, its x value is 50
                    if (faust.theObjects[s].label.length * textlengthmult > xminsize) xminsize = faust.theObjects[s].label.length * textlengthmult + 2 * gap;
                    faust.theObjects[s].xsize = xminsize;
                    faust.theObjects[s].ysize = gap / 2 + 2 * gap + commentsize[1] + knobsize[1] + flonumsize[1] + commentsize[1];
                } else if (faust.theObjects[s].type === "vslider") {
                    var xminsize = flonumsize[0] + 2 * gap;
                    if (faust.theObjects[s].label.length * textlengthmult > xminsize) xminsize = faust.theObjects[s].label.length * textlengthmult + 2 * gap;
                    faust.theObjects[s].xsize = xminsize;
                    faust.theObjects[s].ysize = gap / 2 + 2 * gap + commentsize[1] + vslidersize[1] + flonumsize[1] + commentsize[1];//comment, multislider, float. Not leaving space for the comment: should place them outside of the GUI area.
                } else {//hslider
                    var xminsize = hslidersize[0] + 2 * gap;
                    if (faust.theObjects[s].label.length * textlengthmult > xminsize) xminsize = faust.theObjects[s].label.length * textlengthmult + 2 * gap;
                    faust.theObjects[s].xsize = xminsize;
                    faust.theObjects[s].ysize = gap / 2 + 2 * gap + commentsize[1] + hslidersize[1] + flonumsize[1] + commentsize[1];

                }

            } else if (faust.theObjects[s].type === "button" || faust.theObjects[s].type === "checkbox") {
                if (faust.theObjects[s].type === "button") {
                    var xminsize = buttonsize[0] + 2 * gap;
                    if (faust.theObjects[s].label.length * textlengthmult > xminsize) xminsize = faust.theObjects[s].label.length * textlengthmult + 2 * gap;
                    faust.theObjects[s].xsize = xminsize;
                    faust.theObjects[s].ysize = gap / 2 + 2 * gap + commentsize[1] + buttonsize[1] + checkboxsize[1] + flonumsize[1] + commentsize[1];//+checkboxsize[0] becauqe button is a bang + a toggle
                } else {
                    var xminsize = checkboxsize[0] + 2 * gap;
                    if (faust.theObjects[s].label.length * textlengthmult > xminsize) xminsize = faust.theObjects[s].label.length * textlengthmult + 2 * gap;
                    faust.theObjects[s].xsize = xminsize;
                    faust.theObjects[s].ysize = gap / 2 + 2 * gap + commentsize[1] + checkboxsize[1] + flonumsize[1] + commentsize[1];
                }

            } else if (faust.theObjects[s].type === "nentry") {
                //faust.theObjects[s].xsize = 40;
                //faust.theObjects[s].ysize = 40;

                var xminsize = flonumsize[0] + 2 * gap;
                if (faust.theObjects[s].label.length * textlengthmult > xminsize) xminsize = faust.theObjects[s].label.length * textlengthmult + 2 * gap;
                faust.theObjects[s].xsize = xminsize;
                faust.theObjects[s].ysize = gap / 2 + 2 * gap + commentsize[1] + flonumsize[1] + commentsize[1];

            }

        }
        //Sizing of groups/panels:

        for (var s = _numobjects - 1; s >= 0; s--) {
            if (faust.theObjects[s].type === "vgroup" || faust.theObjects[s].type === "hgroup" || faust.theObjects[s].type === "tgroup") {
                faust.theObjects[s].xsize = 0;
                faust.theObjects[s].ysize = 0;
                for (var h = 0; h < faust.theObjects[s].numchilds; h++) {
                    if (faust.theObjects[s].type === "vgroup" || faust.theObjects[s].type === "tgroup") {//Treating tgroups as Vgroups
                        if (faust.theObjects[s].xsize < faust.theObjects[faust.theObjects[s].childsnums[h]].xsize) faust.theObjects[s].xsize = faust.theObjects[faust.theObjects[s].childsnums[h]].xsize + 2 * gap;
                        //faust.theObjects[s].xsize += faust.theObjects[faust.theObjects[s].childsnums[h]].xsize;
                        faust.theObjects[s].ysize += faust.theObjects[faust.theObjects[s].childsnums[h]].ysize + 2 * gap;
                    } else {
                        faust.theObjects[s].xsize += faust.theObjects[faust.theObjects[s].childsnums[h]].xsize + 2 * gap;
                        if (faust.theObjects[s].ysize < faust.theObjects[faust.theObjects[s].childsnums[h]].ysize) faust.theObjects[s].ysize = faust.theObjects[faust.theObjects[s].childsnums[h]].ysize + 2 * gap;
                    }
                    //First panels should be bigger than the next panels, so they can be seen on the bpatcher.
                    //faust.theObjects[s].xsize += _numobjects / (s + 1);
                    //faust.theObjects[s].ysize += _numobjects / (s + 1);
                }
            }
        }
    }
    //ATTENTION : NEED TO ADD SPACE FOR THE TITTLE OF THE GROUPS!!!:
    positioning = function (_numobjects) {
        faust.theObjects[0].xpos = 0;
        faust.theObjects[0].ypos = 0;
        //numkid != 0 ???
        for (var s = 1; s <= _numobjects; s++) {//Starting from second object as first should always be at 0,0

            //NEED TO ADD :  !faust.theObjects[s].xpos not to repeat objects that have already been positionned.
            if (faust.theObjects[s].parent == null) {

            }
            if (faust.theObjects[s].parent != null && (faust.theObjects[faust.theObjects[s].parent].type === "vgroup" || faust.theObjects[faust.theObjects[s].parent].type === "hgroup")) {
                faust.theObjects[s].xpos = gap + faust.theObjects[faust.theObjects[s].parent].xpos;
                faust.theObjects[s].ypos = gap + faust.theObjects[faust.theObjects[s].parent].ypos;
                for (var h = 1; h < faust.theObjects[faust.theObjects[s].parent].numchilds; h++) {
                    var lastobject = faust.theObjects[faust.theObjects[s].parent].childsnums[h - 1];

                    if (faust.theObjects[faust.theObjects[s].parent].type === "vgroup") {
                        faust.theObjects[faust.theObjects[faust.theObjects[s].parent].childsnums[h]].xpos = faust.theObjects[lastobject].xpos;
                        faust.theObjects[faust.theObjects[faust.theObjects[s].parent].childsnums[h]].ypos = faust.theObjects[lastobject].ypos + faust.theObjects[lastobject].ysize + gap;

                    } else {//hgroup:
                        faust.theObjects[faust.theObjects[faust.theObjects[s].parent].childsnums[h]].ypos = faust.theObjects[lastobject].ypos;
                        faust.theObjects[faust.theObjects[faust.theObjects[s].parent].childsnums[h]].xpos = faust.theObjects[lastobject].xpos + faust.theObjects[lastobject].xsize + gap;
                    }

                }
            }

        }
    }


    createGUI = function (target, patcher) {
        //Starting in the opposite direction to let the last pannels at the front, first panels should go to the back:
        for (var s = faust.numobjects; s >= 0; s--) {

            if (faust.theObjects[s].type === "hbargraph" || faust.theObjects[s].type === "vbargraph") {
                //Drink a beer and contact FAUST dev department ;)
            } else if (faust.theObjects[s].type === "vslider" || faust.theObjects[s].type === "hslider") {
                faust.numcomments++;
                faust.theComments[faust.numcomments] = patcher.newdefault(faust.theObjects[s].xpos, faust.theObjects[s].ypos, "comment");
                faust.theComments[faust.numcomments].message("set", faust.theObjects[s].label);
                faust.theComments[faust.numcomments].message('presentation_rect', faust.theObjects[s].xpos, faust.theObjects[s].ypos, faust.theObjects[s].label.length * textlengthmult, commentsize[1]);
                faust.theComments[faust.numcomments].message('presentation', 1);
                faust.numsliders++;
                faust.numflonums++;
                faust.nummessages++;
                if (faust.theObjects[s].meta && faust.theObjects[s].meta[1] && faust.theObjects[s].meta[1].style == "knob") {
                    faust.theSliders[faust.numsliders] = patcher.newobject("user", "dial", 0, 0);
                    faust.theSliders[faust.numsliders].message('patching_rect', faust.theObjects[s].xpos + (faust.theObjects[s].xsize - knobsize[0]) / 2, faust.theObjects[s].ypos + commentsize[1], knobsize[0], knobsize[1]);
                    faust.theSliders[faust.numsliders].message('presentation_rect', faust.theObjects[s].xpos + (faust.theObjects[s].xsize - knobsize[0]) / 2, faust.theObjects[s].ypos + commentsize[1], knobsize[0], knobsize[1]);
                    faust.theSliders[faust.numsliders].message('presentation', 1);
                    faust.theSliders[faust.numsliders].message('min', parseFloat(faust.theObjects[s].min));
                    faust.theSliders[faust.numsliders].message('size', parseFloat(faust.theObjects[s].max) * 1000);//had to multiply and divide later by 1000 as values lower than 2 are not being taken in account by the 'dial'
                    faust.theSliders[faust.numsliders].message('mult', (faust.theObjects[s].max - faust.theObjects[s].min) / (faust.theObjects[s].max * 1000));
                    if (parseFloat(faust.theObjects[s].step) == parseInt(faust.theObjects[s].step)) {
                        faust.theSliders[faust.numsliders].message('floatoutput', 0);
                    } else {
                        faust.theSliders[faust.numsliders].message('floatoutput', 1);
                    }
                    faust.thenumberBoxes[faust.numflonums] = patcher.newobject("flonum", faust.theObjects[s].xpos + (faust.theObjects[s].xsize - flonumsize[0]) / 2, gap / 2 + faust.theObjects[s].ypos + commentsize[1] + knobsize[1], flonumsize[0], flonumsize[1] * 0.625);
                    faust.thenumberBoxes[faust.numflonums].message('minimum', parseFloat(faust.theObjects[s].min));
                    faust.thenumberBoxes[faust.numflonums].message('maximum', parseFloat(faust.theObjects[s].max));
                    faust.theMessages[faust.nummessages] = patcher.newobject("message", faust.theObjects[s].xpos, faust.theObjects[s].ypos + commentsize[1] + knobsize[1] + flonumsize[1] + commentsize[1], 100, 9);
                    //faust.theMessages[faust.nummessages].message("set", faust.theObjects[s].label, "\$1");
                    faust.theMessages[faust.nummessages].message("set", faust.theObjects[s].shortname, "\$1");
                    if (faust.theObjects[s].meta) {
                        if (faust.theObjects[s].meta[0] != null || faust.theObjects[s].meta[1] != null || faust.theObjects[s].meta[2] != null || faust.theObjects[s].meta[3] != null) {
                            //NEED TO ADD AN IF .UNIT FOR ALL OF THEM TO BE SURE THAT THE COMMENT IS CREATED ONLY WHEN THERE IS A UNIT
                            faust.numcomments++;
                            faust.theComments[faust.numcomments] = patcher.newdefault(faust.theObjects[s].xpos + (faust.theObjects[s].xsize - flonumsize[0] * 0.625) / 2, faust.theObjects[s].ypos + commentsize[1] + knobsize[1] + flonumsize[1], "comment");
                            if (faust.theObjects[s].meta[0] != null && faust.theObjects[s].meta[0].unit != null) faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[0].unit);
                            if (faust.theObjects[s].meta[1] != null && faust.theObjects[s].meta[1].unit != null) faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[1].unit);
                            if (faust.theObjects[s].meta[2] != null && faust.theObjects[s].meta[2].unit != null) faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[2].unit);
                            if (faust.theObjects[s].meta[3] != null && faust.theObjects[s].meta[3].unit != null) faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[3].unit);
                            faust.theComments[faust.numcomments].message('presentation', 1);
                        }
                    }
                } else {//H and V sliders:
                    faust.theSliders[faust.numsliders] = patcher.newobject("user", "multiSlider", 0, 0, vslidersize[0], vslidersize[1], 0., 1., 1, 2936, 15, 0, 0, 2, 0, 0, 0);
                    if (faust.theObjects[s].type === "vslider") {
                        faust.theSliders[faust.numsliders].message('orientation', 1);
                        faust.theSliders[faust.numsliders].message('patching_rect', faust.theObjects[s].xpos + flonumsize[0] / 2, faust.theObjects[s].ypos + commentsize[1], vslidersize[0], vslidersize[1]);
                        faust.thenumberBoxes[faust.numflonums] = patcher.newobject("flonum", faust.theObjects[s].xpos, gap / 2 + faust.theObjects[s].ypos + commentsize[1] + vslidersize[1], flonumsize[0], flonumsize[1] * 0.625);
                        faust.theMessages[faust.nummessages] = patcher.newobject("message", faust.theObjects[s].xpos, faust.theObjects[s].ypos + commentsize[1] + vslidersize[1] + flonumsize[1] + commentsize[1], 100, 9);
                        //faust.theMessages[faust.nummessages].message("set", faust.theObjects[s].label, "\$1");
                        faust.theMessages[faust.nummessages].message("set", faust.theObjects[s].shortname, "\$1");
                    } else {//hslider
                        faust.theSliders[faust.numsliders].message('orientation', 0);
                        faust.theSliders[faust.numsliders].message('patching_rect', faust.theObjects[s].xpos, faust.theObjects[s].ypos + commentsize[1], hslidersize[0], hslidersize[1]);
                        faust.thenumberBoxes[faust.numflonums] = patcher.newobject("flonum", faust.theObjects[s].xpos, gap / 2 + faust.theObjects[s].ypos + commentsize[1] + hslidersize[1], flonumsize[0], flonumsize[1] * 0.625);
                        faust.theMessages[faust.nummessages] = patcher.newobject("message", faust.theObjects[s].xpos, faust.theObjects[s].ypos + commentsize[1] + hslidersize[1] + flonumsize[1] + commentsize[1], 100, 9);
                        //faust.theMessages[faust.nummessages].message("set", faust.theObjects[s].label, "\$1");
                        faust.theMessages[faust.nummessages].message("set", faust.theObjects[s].shortname, "\$1");
                    }
                    //Verify if int or float:
                    if (parseFloat(faust.theObjects[s].step) == parseInt(faust.theObjects[s].step)) {
                        faust.theSliders[faust.numsliders].message('settype', 0);//int
                    } else {
                        faust.theSliders[faust.numsliders].message('settype', 1);//float
                    }
                    faust.theSliders[faust.numsliders].message('contdata', 1);//Continous data
                    faust.theSliders[faust.numsliders].message('setstyle', 0);//Thin line

                    if (faust.theObjects[s].meta) {
                        if (faust.theObjects[s].meta[0] != null || faust.theObjects[s].meta[1] != null || faust.theObjects[s].meta[2] != null || faust.theObjects[s].meta[3] != null) {
                            //NEED TO ADD AN IF .UNIT FOR ALL OF THEM TO BE SURE THAT THE COMMENT IS CREATED ONLY WHEN THERE IS A UNIT
                            faust.numcomments++;
                            if (faust.theObjects[s].type === "vslider") faust.theComments[faust.numcomments] = patcher.newdefault(faust.theObjects[s].xpos, faust.theObjects[s].ypos + commentsize[1] + vslidersize[1] + flonumsize[1], "comment");
                            if (faust.theObjects[s].type === "hslider") faust.theComments[faust.numcomments] = patcher.newdefault(faust.theObjects[s].xpos, faust.theObjects[s].ypos + commentsize[1] + hslidersize[1] + flonumsize[1], "comment");
                            if (faust.theObjects[s].meta[0] != null && faust.theObjects[s].meta[0].unit != null) faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[0].unit);
                            if (faust.theObjects[s].meta[1] != null && faust.theObjects[s].meta[1].unit != null) faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[1].unit);
                            if (faust.theObjects[s].meta[2] != null && faust.theObjects[s].meta[2].unit != null) faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[2].unit);
                            if (faust.theObjects[s].meta[3] != null && faust.theObjects[s].meta[3].unit != null) faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[3].unit);
                            faust.theComments[faust.numcomments].message('presentation', 1);
                        }
                    }
                    faust.theSliders[faust.numsliders].message('setminmax', parseFloat(faust.theObjects[s].min), parseFloat(faust.theObjects[s].max));
                    faust.theSliders[faust.numsliders].message('presentation', 1);
                    faust.theSliders[faust.numsliders].message(parseFloat(faust.theObjects[s].init));  // Set initial value
                }
                faust.thenumberBoxes[faust.numflonums].message(parseFloat(faust.theObjects[s].init));
                faust.thenumberBoxes[faust.numflonums].message('minimum', parseFloat(faust.theObjects[s].min));
                faust.thenumberBoxes[faust.numflonums].message('maximum', parseFloat(faust.theObjects[s].max));
                faust.thenumberBoxes[faust.numflonums].message('presentation', 1);

                faust.theSliders[faust.numsliders].message(parseFloat(faust.theObjects[s].init));  // Set initial value
                faust.theSliders[faust.numsliders].message('varname', faust.theObjects[s].label);

                patcher.hiddenconnect(faust.theSliders[faust.numsliders], 0, faust.thenumberBoxes[faust.numflonums], 0);
                patcher.hiddenconnect(faust.thenumberBoxes[faust.numflonums], 0, faust.theMessages[faust.nummessages], 0);

                //patcher.hiddenconnect(faust.theMessages[faust.nummessages], 0, target, 0);
                patcher.hiddenconnect(faust.theMessages[faust.nummessages], 0, thru, 0);

            } else if (faust.theObjects[s].type === "button" || faust.theObjects[s].type === "checkbox") {
                faust.numcomments++;
                faust.theComments[faust.numcomments] = patcher.newdefault(faust.theObjects[s].xpos, faust.theObjects[s].ypos, "comment");
                faust.theComments[faust.numcomments].message("set", faust.theObjects[s].label);
                faust.theComments[faust.numcomments].message('presentation', 1);
                faust.numbuttons++;
                faust.numflonums++;
                faust.numcomments++;
                if (faust.theObjects[s].type === "button") {
                    //NEED TO PUT THIS BAND UNDER AN ARARY TO BE ABLE TO ADRESS IT AND MORE IMPORTANT TO DELETE IT IF NECESSEARY.
                    var bang = patcher.newdefault(faust.theObjects[s].xpos + (faust.theObjects[s].xsize - buttonsize[0]) / 2, faust.theObjects[s].ypos + commentsize[1], "button");
                    bang.message('presentation', 1);
                    faust.theButtons[faust.numbuttons] = patcher.newdefault(faust.theObjects[s].xpos + (faust.theObjects[s].xsize - buttonsize[0]) / 2, faust.theObjects[s].ypos + commentsize[1] + buttonsize[1], "toggle");  // Faust says always have default of zero--even for check buttons!  Ohhhh well...
                    faust.theButtons[faust.numbuttons].message('presentation', 1);
                    patcher.hiddenconnect(bang, 0, faust.theButtons[faust.numbuttons], 0);
                    faust.thenumberBoxes[faust.numflonums] = patcher.newobject("flonum", faust.theObjects[s].xpos + (faust.theObjects[s].xsize - flonumsize[0]) / 2, gap / 2 + faust.theObjects[s].ypos + commentsize[1] + buttonsize[1] + checkboxsize[1], flonumsize[0], flonumsize[1] * 0.625);
                    faust.thenumberBoxes[faust.numflonums].message('presentation', 1);
                    if (faust.theObjects[s].meta[2] != null) {
                        faust.theComments[faust.numcomments] = patcher.newdefault(faust.theObjects[s].xpos, faust.theObjects[s].ypos + commentsize[1] + buttonsize[1] + checkboxsize[1] + flonumsize[1], "comment");
                        faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[2].unit);
                        faust.theComments[faust.numcomments].message('presentation', 1);
                    }
                } else {
                    faust.theButtons[faust.numbuttons] = patcher.newdefault(faust.theObjects[s].xpos + (faust.theObjects[s].xsize - buttonsize[0]) / 2, faust.theObjects[s].ypos + commentsize[1], "toggle");  // Faust says always have default of zero--even for check buttons!  Ohhhh well...
                    faust.theButtons[faust.numbuttons].message('presentation', 1);
                    faust.thenumberBoxes[faust.numflonums] = patcher.newobject("flonum", faust.theObjects[s].xpos + (faust.theObjects[s].xsize - flonumsize[0]) / 2, gap / 2 + faust.theObjects[s].ypos + commentsize[1] + checkboxsize[1], flonumsize[0], flonumsize[1] * 0.625);
                    faust.thenumberBoxes[faust.numflonums].message('presentation', 1);
                    if (faust.theObjects[s].meta[2] != null) {
                        faust.theComments[faust.numcomments] = patcher.newdefault(faust.theObjects[s].xpos, faust.theObjects[s].ypos + commentsize[1] + checkboxsize[1] + flonumsize[1], "comment");
                        faust.theComments[faust.numcomments].message("set", faust.theObjects[s].meta[2].unit);
                        faust.theComments[faust.numcomments].message('presentation', 1);
                    }
                }
                faust.thenumberBoxes[faust.numflonums].message('min', 0);
                faust.thenumberBoxes[faust.numflonums].message('max', 1);

                patcher.hiddenconnect(faust.theButtons[faust.numbuttons], 0, faust.thenumberBoxes[faust.numflonums], 0);
                faust.nummessages++;
                faust.theMessages[faust.nummessages] = patcher.newobject("message", faust.theObjects[s].xpos, faust.theObjects[s].ypos + 90, 100, 9);
                faust.theMessages[faust.nummessages].message("set", faust.theObjects[s].shortname, "\$1");

                patcher.hiddenconnect(faust.thenumberBoxes[faust.numflonums], 0, faust.theMessages[faust.nummessages], 0);
                patcher.hiddenconnect(faust.theMessages[faust.nummessages], 0, thru, 0);

                // direct connection to faustgen~ does not work...
                //patcher.hiddenconnect(faust.thenumberBoxes[faust.numwidgets], 0, target, 0)

            } else if (faust.theObjects[s].type === "nentry") {
                faust.numcomments++;
                faust.theComments[faust.numcomments] = patcher.newdefault(faust.theObjects[s].xpos, faust.theObjects[s].ypos, "comment");
                faust.theComments[faust.numcomments].message("set", faust.theObjects[s].label);
                faust.theComments[faust.numcomments].message('presentation', 1);
                faust.numnentrys++;
                if (parseFloat(faust.theObjects[s].step) == parseInt(faust.theObjects[s].step)) {
                    faust.thenumberBoxes[faust.numnentrys] = patcher.newobject("number", faust.theObjects[s].xpos, gap / 2 + faust.theObjects[s].ypos + commentsize[1], flonumsize[0], flonumsize[1] * 0.625);
                } else {
                    faust.thenumberBoxes[faust.numnentrys] = patcher.newobject("flonum", faust.theObjects[s].xpos, gap / 2 + faust.theObjects[s].ypos + commentsize[1], flonumsize[0], flonumsize[1] * 0.625);
                }
                faust.thenumberBoxes[faust.numnentrys].message('presentation', 1);

                faust.thenumberBoxes[faust.numnentrys].message('min', parseFloat(faust.theObjects[s].min));
                faust.thenumberBoxes[faust.numnentrys].message('max', parseFloat(faust.theObjects[s].max));
                faust.thenumberBoxes[faust.numnentrys].message(parseFloat(faust.theObjects[s].init));
                faust.nummessages++;
                faust.theMessages[faust.nummessages] = patcher.newobject("message", faust.theObjects[s].xpos, gap / 2 + faust.theObjects[s].ypos + commentsize[1] + flonumsize[1], 100, 9);
                faust.theMessages[faust.nummessages].message("set", faust.theObjects[s].shortname, "\$1");

                patcher.hiddenconnect(faust.thenumberBoxes[faust.numnentrys], 0, faust.theMessages[faust.nummessages], 0);
                patcher.hiddenconnect(faust.theMessages[faust.nummessages], 0, thru, 0);


                // direct connection to faustgen~ does not work...
                //patcher.hiddenconnect(faust.thenumberBoxes[faust.numwidgets], 0, target, 0); 

            } else if (faust.theObjects[s].type === "vgroup" || faust.theObjects[s].type === "hgroup" || faust.theObjects[s].type === "tgroup") {
                faust.numpanels++;
                faust.thePanels[faust.numpanels] = patcher.newdefault(0, 0, "panel");//widgHeight* (faust.numwidgets)// * (faust.numpanels)
                faust.thePanels[faust.numpanels].message('varname', faust.theObjects[s].label);
                if (clownColors) {
                    faust.thePanels[faust.numpanels].message('bgfillcolor', Math.random() * 0.5 + 0.5, Math.random() * 0.5 + 0.5, Math.random() * 0.5 + 0.5);
                } else {
                    var color = Math.random() * 0.5 + 0.5;
                    faust.thePanels[faust.numpanels].message('bgfillcolor', color, color, color);
                }

                //MODIFY THIS IN FONCION OF THE SIZE OF THE PANEL!!! :
                faust.thePanels[faust.numpanels].message('shadow', 2);
                faust.thePanels[faust.numpanels].message('rounded', 15);
                faust.thePanels[faust.numpanels].message('border', 1);
                faust.thePanels[faust.numpanels].message('background', 1);
                faust.thePanels[faust.numpanels].message('presentation', 1);
                faust.thePanels[faust.numpanels].message('patching_rect', faust.theObjects[s].xpos, faust.theObjects[s].ypos, faust.theObjects[s].xsize, faust.theObjects[s].ysize);
                faust.thePanels[faust.numpanels].message('presentation_rect', faust.theObjects[s].xpos, faust.theObjects[s].ypos, faust.theObjects[s].xsize, faust.theObjects[s].ysize);//faust.thePanels[faust.numpanels][3], faust.thePanels[faust.numpanels][4], widgWidth + (1 - faust.thePanels[faust.numpanels][2]) * item.items.length * widgWidth, widgHeight + faust.thePanels[faust.numpanels][2] * item.items.length * widgHeight);


            }
        }
    }

    // Create new
    const parsed_json = JSON.parse(json);
    // Tries to find the compiled object from the "name" field in the JSON
    const dsp_object1 = patcher.getnamed(parsed_json.name + "~");
    if (dsp_object1 !== patcher.getnamed("null_object")) {
        parse_ui(parsed_json.ui, faust.mainFaustObject, patcher);
    } else {
        // Tries to find the compiled object from the "filename" field in the JSON
        var dsp_object2 = patcher.getnamed(parsed_json.filename.slice(0, -4) + "~");
        if (dsp_object2 !== patcher.getnamed("null_object")) {
            parse_ui(parsed_json.ui, faust.mainFaustObject, patcher);
        } else {
            // Tries to find the compiled object from the "name" argument (used with faustgen~)
            var dsp_object = get_dsp_name(patcher, "faustgen");
            if (dsp_object !== patcher.getnamed("null_object")) {
                parse_ui(parsed_json.ui, faust.mainFaustObject, patcher);
            } else {
                post("Error : missing dsp name in the patch\n");
            }
        }
    }


    function wrapping() {
        if (isWrapped == false) {
            var faustObject = patcher.getnamed(parsed_json.filename.slice(0, -4) + "~");
            var inlets = faustObject.getboxattr('numinlets');
            var outlets = faustObject.getboxattr('numoutlets') - 1;
            var theUnpack = patcher.newdefault(10, 569, "mc.unpack~ " + inlets);
            var thePack = patcher.newdefault(10, 635, "mc.pack~ " + outlets);
            for (i = 0; i < inlets; i++) {
                patcher.hiddenconnect(theUnpack,i,faustObject,i);
            }
            for (i = 0; i < outlets; i++) {
                patcher.hiddenconnect(faustObject,i,thePack,i);
            }
            isWrapped = true;
        }
        patcher.hiddenconnect(thru,0,faustObject,0);
        patcher.hiddenconnect(secondInlet,0,faustObject,0);
        patcher.hiddenconnect(firstInlet,0,theUnpack,0);
        patcher.hiddenconnect(thePack,0,theOutlet,0);
    }
    var task = new Task(wrapping, this);
    task.schedule(300);//In milliseconds
}


function findObjectByID(patcher, text) {
    var findobj = patcher.firstobject;
    while (findobj) {
        if (findobj.getattr("id") && findobj.getattr("id") === text) {
            return findobj;
        }
        //post("id :  ", findobj.getattr("id"));
        findobj = findobj.nextobject;
    }
    return null;
}


function anything() {
    var args = arrayfromargs(messagename, arguments);
    dsp_ui_table.push(faust.ui(args[1], this.patcher));
}

