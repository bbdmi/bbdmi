# BBDMI Faust2GUI_wrapper_compiler

## About

The files 'wrapper.maxpat', 'ui_bbdmi.js, 'faust2max6' are modified files of the Faust compiler. This two files modify the compiler to generate an abstraction that has a GUI corresponding to the DSP file. It also creates a wrapper around the object converting it to a multichannel abstraction.

## Installation

To modify the FAUST compiler you need to go to your compiler folder and replace the files 'wrapper.maxpat', 'ui.js for the 'ui_bbdmi.js' and the 'faust2max6' file.

On Mac this folder is situated here : 
'wrapper.maxpat' and 'ui.js' : /Applications/Faust-2.70.3/share/faust/max-msp
faust2max6 : /Applications/Faust-2.70.3/bin