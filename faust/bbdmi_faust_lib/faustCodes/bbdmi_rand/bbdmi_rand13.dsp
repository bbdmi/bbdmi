//--------------------------------------------------------------------------------------//
//-----------------------------------------bbdmilib-------------------------------------//
//
//----------------------------FAUST CODE FOR BBDMI ANR PROJECT--------------------------//
//
//--------------------------------------------------------------------------------------//

declare name "BBDMI Faust Lib";
declare author "Alain Bonardi";
declare author "Francesco Di Maggio";
declare author "David Fierro";
declare author "Anne Sedes";
declare author "Atau Tanaka";
declare author "Stephen Whitmarsch";
declare copyright "2022-2025 BBDMI TEAM";
declare name "bbdmi_rand13";
//
process = library("bbdmi.lib").bbdmi_multiRand_ui(13);
