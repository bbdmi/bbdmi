#!/bin/bash

# Replace 'sed' with 'gsed' if using macOS with Homebrew to install GNU sed (gsed).

# Set the folder where your files are located
folder_path="$HOME/Documents/Gitlab/bbdmi/faust/bbdmi_faust_lib/test_patches"
#"./"

# Define the patterns and replacements files
patterns_file="./faustGUIgen_SEARCH2.txt"
replacements_file="./faustGUIgen_REPLACE2.txt"

# Read patterns and replacements from their respective files into arrays
i=0
while IFS= read -r pattern; do
    patterns[i]=$pattern
    i=$((i+1))
done < "$patterns_file"

i=0
while IFS= read -r replacement; do
    replacements[i]=$replacement
    i=$((i+1))
done < "$replacements_file"

# Ensure that the number of patterns and replacements match
if [ "${#patterns[@]}" -ne "${#replacements[@]}" ]; then
    echo "Error: The number of patterns does not match the number of replacements."
    exit 1
fi

# Loop through all files in the folder and its subfolders
find "$folder_path" -type f -print0 | while IFS= read -r -d $'\0' file_path; do
    # Perform replacements for each pattern
    for ((i = 0; i < ${#patterns[@]}; i++)); do
        search_pattern="${patterns[i]}"
        replacement_text="${replacements[i]}"

        # Search for the section using 'grep'
        if grep -q "$search_pattern" "$file_path"; then
            # If the search pattern is found, modify the file using 'sed'
            sed -i '' "s/$search_pattern/$replacement_text/g" "$file_path"
            echo "Modification successful for pattern: $search_pattern in file: $file_path"
        else
            echo "Search pattern not found for pattern: $search_pattern in file: $file_path"
        fi
    done
done
