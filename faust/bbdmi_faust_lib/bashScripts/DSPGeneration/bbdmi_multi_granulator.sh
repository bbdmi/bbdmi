#!/bin/bash
#BBDMI eeg_simulator Faust code generation
cd ../../faustCodes/
#deletes the previous bbdmi_band_pulse folder
rm -R bbdmi_multi_granulator
mkdir bbdmi_multi_granulator
cd bbdmi_multi_granulator/
#maximum number of channels
let "Nch = 16"
#creates rand dsp files
headerfilename="../../bashScripts/DSPGeneration/bbdmiCodeHeader.txt"
for i in `seq 1 $Nch`
do
    sortie="bbdmi_multi_granulator$i.dsp"
#writes the header
    while IFS= read -r line
    do
        echo "$line" >> $sortie
    done <"$headerfilename"
#writes the declared name
echo "declare name \"bbdmi_multi_granulator$i\";" >> $sortie
#writes the process line
echo "//
process = library(\"bbdmi.lib\").bbdmi_multi_granulator($i);" >> $sortie
done
