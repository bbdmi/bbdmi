#!/bin/bash
#BBDMI rand Faust code generation
cd ../../faustCodes/
#deletes the previous bbdmi_rand folder
rm -R bbdmi_rand
mkdir bbdmi_rand
cd bbdmi_rand/
#maximum number of channels
let "Nch = 16"
#creates rand dsp files
headerfilename="../../bashScripts/DSPGeneration/bbdmiCodeHeader.txt"
for i in `seq 1 $Nch`
do
    sortie="bbdmi_rand$i.dsp"
#writes the header
    while IFS= read -r line
    do
        echo "$line" >> $sortie
    done <"$headerfilename"
#writes the declared name
echo "declare name \"bbdmi_rand$i\";" >> $sortie
#writes the process line
echo "//
process = library(\"bbdmi.lib\").bbdmi_multiRand_ui($i);" >> $sortie
done