#!/bin/bash
#BBDMI 50hz_filter Faust code generation
cd ../../faustCodes/
#deletes the previous bbdmi_50hz_filter folder
rm -R bbdmi_50hz_filter
mkdir bbdmi_50hz_filter
cd bbdmi_50hz_filter/
#maximum number of channels
let "Nch = 16"
#creates rand dsp files
headerfilename="../../bashScripts/DSPGeneration/bbdmiCodeHeader.txt"
for i in `seq 1 $Nch`
do
    sortie="bbdmi_50hz_filter_$i.dsp"
#writes the header
    while IFS= read -r line
    do
        echo "$line" >> $sortie
    done <"$headerfilename"
#writes the declared name
echo "declare name \"bbdmi_50hz_filter_$i\";" >> $sortie
#writes the process line
echo "//
process = library(\"bbdmi.lib\").bbdmi_multi_50notch($i);" >> $sortie
done