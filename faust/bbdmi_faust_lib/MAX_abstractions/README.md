# MAX_abstractions

This folder contain Max/Msp abstractions of every Faust compiled object on the Externals folder. This abstractions include a GUI.

As the externals folder is composed by binary files, you will be able to download them from our latest release.