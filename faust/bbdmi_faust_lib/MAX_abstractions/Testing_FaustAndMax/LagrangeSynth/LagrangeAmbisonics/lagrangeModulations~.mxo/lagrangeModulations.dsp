declare filename "lagrangeModulations.dsp";
declare name "lagrangeModulations";
import("stdfaust.lib");

//-------`bbdmi_lagrangeMods`----------
//
//
// #### Usage
//
// ```
// _ : bbdmi_lagrangeMods(2*echoVoices) : _
// ```
//
// Where:
//
// * `2*echoVoices`: Number of parallel curves created at the same time.

//-----------------------------

//5th order Lagrange polynoms
//Attention: It clicks when changing the distribution transfer funcitons!!!
//All this code can be optimized by using recurrent functions, and do N order of Lagrange instead of 5. Dynamic UI needed.
//NOTE: because of the retards this modules acts as a comb filter if the retardFactor is different from 0 and the '2*echoVoices' is higher than 1. Flanger like effect can be achieved.

//ECHOVOICES*MULTIPLIER(INSTEAD OF 2)

bbdmi_ambLagrangeMods(echoVoices) = vgroup("LagrangeModSynth",bbdmi_lagrangeMods : par(k,echoVoices,fi.dcblocker : clipper))
with{
    bbdmi_lagrangeMods = par(i,2*echoVoices, x, yCoords(i) : it.lagrangeInterpolation(5, (0,40,80,120,160,200)):de.sdelay(ma.SR,2048,indexfactor(indexdistr,i)*(_retardFactor*ma.SR):fi.avg_tau(randomGetTime):int) : transp(indexfactor(indexdistr,i)*(_maxtransp*_transpSide)): fi.dcblocker : clipper):> si.bus(echoVoices): par(k,echoVoices,/(8));
    transp(tr) = ef.transpose(10000,10000,tr);
    clipper = _, -1 : max : _, 1 : min;
    x = os.phasor(200, _freq);
    getindfactor(tf,index) = (indexdistr == tf) * th(tf, index, 2*echoVoices);
    indexfactor(tf,index) = sum(tf, 22, getindfactor(tf,index)):fi.avg_tau(randomGetTime);
    xmod = os.osc(_modGlobalFreq)*200;
    modfreq = (xmod/200)*2*ma.PI*((x/100):sin:+(1):/(2));
    yCoords(i) = (y1,
                 y2(i)*indexfactor(indexdistr,i), 
                 y3(i)*indexfactor(indexdistr,i), 
                 y4(i)*indexfactor(indexdistr,i), 
                 y5(i)*indexfactor(indexdistr,i), 
                 y6);
    y1 = 0;
    y2(i) = y2mod(i): *(y2mod(i));
    y2mod(i) = (((modfreq*_y2modFactor+2*ma.PI)*indexfactor(indexdistr,i)):sin:+(1):/(2));
    y3(i) = -1*y3mod(i):*(y3mod(i));
    y3mod(i) = (((modfreq*_y3modFactor+2*ma.PI)*indexfactor(indexdistr,i)):sin:+(1):/(2));
    y4(i) = y4mod(i): *(y4mod(i));
    y4mod(i) = (((modfreq*_y4modFactor+2*ma.PI)*indexfactor(indexdistr,i)):sin:+(1):/(2));
    y5(i) = -1*y5mod(i): *(y5mod(i));
    y5mod(i) = (((modfreq*_y5modFactor+2*ma.PI)*indexfactor(indexdistr,i)):sin:+(1):/(2));
    y6 = 0;
    //Randomizer UI
    dryWetRandom = vslider("v:lagrangeModulations/h:[00]Randomizer/[00] dryWetRandom[style:knob]", 1, 0,1,0.001):si.smoo;
    newRandom = button("v:lagrangeModulations/h:[00]Randomizer/[01] newRandom");
    randomGetTime = vslider("v:lagrangeModulations/h:[00]Randomizer/[02] randomGetTime[unit:secs][style:knob]", 5, 0.1,300,0.1):si.smoo;
    freqRandom = checkbox("v:lagrangeModulations/h:[00]Randomizer/[03]freqRandom"):1-_:fi.avg_tau(randomGetTime);

    indexdistr = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2),vslider("v:lagrangeModulations/h:[01]Parameters/[00] indexdistr",0,0,21,1):si.smoo,no.noise:ba.sAndH(newRandom):+(1):/(2):*(21):fi.avg_tau(randomGetTime)):int;

    _freq = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2):-(freqRandom):(_,0:max),freq,no.noise:ba.sAndH(newRandom):+(1):/(2):*(freq):+(20):fi.avg_tau(randomGetTime));
    freq = vslider("v:lagrangeModulations/h:[01]Parameters/[01] Freq", 110, 10,10000,0.01):si.smoo;

    _retardFactor = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2),retardFactor,no.noise:ba.sAndH(newRandom):+(1):/(2):*(retardFactor):fi.avg_tau(randomGetTime));
    retardFactor = vslider("v:lagrangeModulations/h:[01]Parameters/[02] retardFactor[style:knob]", 0,0, 1,0.001):si.smoo;

    _maxtransp = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2),maxtransp,no.noise:ba.sAndH(newRandom):+(1):/(2):*(maxtransp):fi.avg_tau(randomGetTime));
    maxtransp = vslider("v:lagrangeModulations/h:[01]Parameters/[03] maxtransp", 0, 0,20,0.001):si.smoo;

    _transpSide = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2),transpSide,no.noise:ba.sAndH(newRandom):+(1):/(2):*(transpSide):fi.avg_tau(randomGetTime));
    transpSide = vslider("v:lagrangeModulations/h:[01]Parameters/[04] transpSide", 0, -1, 1, 0.001) : si.smoo;

    _modGlobalFreq = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2),modGlobalFreq,no.noise:ba.sAndH(newRandom):+(1):/(2):*(modGlobalFreq):fi.avg_tau(randomGetTime));
    modGlobalFreq = vslider("v:lagrangeModulations/h:[01]Parameters/[05] modGlobalFreq", 500, 0.01,10000,0.01):si.smoo;



    _y2modFactor = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2),y2modFactor,no.noise:ba.sAndH(newRandom):+(1):/(2):*(y2modFactor):fi.avg_tau(randomGetTime));
    y2modFactor = vslider("v:lagrangeModulations/h:[01]Parameters/[06] y2modFactor", 0.2, 0,10,0.001):si.smoo;
    _y3modFactor = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2),y3modFactor,no.noise:ba.sAndH(newRandom):+(1):/(2):*(y3modFactor):fi.avg_tau(randomGetTime));
    y3modFactor = vslider("v:lagrangeModulations/h:[01]Parameters/[07] y3modFactor", 0.2, 0,10,0.001):si.smoo;
    _y4modFactor = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2),y4modFactor,no.noise:ba.sAndH(newRandom):+(1):/(2):*(y4modFactor):fi.avg_tau(randomGetTime));
    y4modFactor = vslider("v:lagrangeModulations/h:[01]Parameters/[08] y4modFactor", 0.2, 0,10,0.001):si.smoo;
    _y5modFactor = it.interpolate_linear(dryWetRandom:fi.avg_tau(0.2),y5modFactor,no.noise:ba.sAndH(newRandom):+(1):/(2):*(y5modFactor):fi.avg_tau(randomGetTime));
    y5modFactor = vslider("v:lagrangeModulations/h:[01]Parameters/[09] y5modFactor", 0.2, 0,10,0.001):si.smoo;
};

//outputGain = vslider("OutputGain", 0.6, 0,1,0.001):si.smoo;
process =  bbdmi_ambLagrangeMods(8);//ambisonic outputTEST AT LABO!!!!





th(0, i, p) = (i+1) / p;
    th(1, i, p) = ((i+1) / p)^2;
    th(2, i, p) = sin(ma.PI * 0.5 * (i+1) / p);
    th(3, i, p) = log10(1 + (i+1) / p) / log10(2);
    th(4, i, p) = sqrt((i+1) / p);
    th(5, i, p) = 1 - cos(ma.PI * 0.5 * (i+1) / p);
    th(6, i, p) = (1 - cos(ma.PI * (i+1) / p)) * 0.5;
    th(7, i, p) = 1 - (1 - (i+1) / p )^2;
    th(8, i, p) = ((i+1) / p < 0.5) * 2 * ((i+1) / p)^2 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^2 * 0.5);
    th(9, i, p) = ((i+1) / p)^3;
    th(10, i, p) = 1 - (1 - (i+1) / p)^3;
    th(11, i, p) = ((i+1) / p < 0.5) * 4 * ((i+1) / p)^3 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^3 * 0.5);
    th(12, i, p) = ((i+1) / p)^4;
    th(13, i, p) = 1 - (1 - (i+1) / p)^4;
    th(14, i, p) = ((i+1) / p < 0.5) * 8 * ((i+1) / p)^4 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^4 * 0.5);
    th(15, i, p) = ((i+1) / p)^5;
    th(16, i, p) = 1 - (1 - (i+1) / p)^5;
    th(17, i, p) = ((i+1) / p < 0.5) * 16 * ((i+1) / p)^5 + ((i+1) / p >= 0.5) * (1 - (-2 * (i+1) / p + 2)^5 * 0.5);
    th(18, i, p) = 2^(10 * (i+1) / p - 10);
    th(19, i, p) = ((i+1) / p < 1) * (1 - 2^(-10 * (i+1) / p)) + ((i+1) / p == 1);
    th(20, i, p) = 1 - sqrt(1 - ((i+1) / p)^2);
    th(21, i, p) = sqrt(1 - ((i+1) / p - 1)^2);
