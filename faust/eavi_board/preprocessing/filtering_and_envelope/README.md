# BBDMI Faust eavi_board/patches/faust/preprocessing/filtering_and_envelope repository

## About

This folder contains [faust](https://faust.grame.fr/)* DSP patches to filter and create an envelope follower over the EMG signals captured by the [EAVI board](https://research.gold.ac.uk/id/eprint/26476/) running an [OpenWare](https://github.com/RebelTechnology/OpenWare) firmware codebase.

*"Faust (Functional Audio Stream) is a functional programming language for sound synthesis and audio processing with a strong focus on the design of synthesizers, musical instruments, audio effects, etc. Faust targets high-performance signal processing applications and audio plug-ins for a variety of platforms and standards." (https://faust.grame.fr/)"

## Acknowledgments

This repository is in constant development, and will include further functionality and documentation as time goes by. We intend to make all abstractions backwards-compatible, and will try to keep the module patches and compositions up-to-date. If you have any questions please feel free to contact us.

© BBDMI, MSH Paris Nord, ANR & CNRS.
