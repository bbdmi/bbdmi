# BBDMI EAVI repository

## About

In this folder you can find specific information for the use of the [EAVI](https://www.researchgate.net/publication/333699608_EAVI_EMG_board)* board and EAVI board firmware versions.

The patches folder contains patches to install on the EAVI board for specific signal processing and synthesis tasks.

This [link](https://docs.google.com/document/d/1Uj0343W310tb-t3E7CdxNFw3vJ5byjwEywdBJ7TWpkM/edit?usp=sharing) provides the initial steps for setting up your EAVI board.

The lead electrodes are available [here](https://www.pluxbiosignals.com/en-fr/collections/cables/products/3-lead-electrode-cable?variant=40904236204223), and if longer cables are required, this [link](https://www.pluxbiosignals.com/en-fr/collections/cables/products/sensor-extension-cable) offers a cable extension. Gel electrodes can be found [here](https://shop.medcat.nl/Webwinkel-Product-259912097/ECG-Electrode-foam-24mm.html).

The Hardware design can be found [here](https://github.com/RebelTechnology/BioSignals) and the main EMG Biosignals firmware can be found [here](https://github.com/RebelTechnology/OpenWare/tree/master/BioSignals).

*"The EAVI EMG board features six EMG channels and a 3-axis accelerometer. Dry electrodes are attached to a Plux SnapBit Trio with an EMG sensor seated on top of it and housed in a custom case. Each sensor is connected the board via micro-USB." (https://www.researchgate.net/publication/333699608_EAVI_EMG_board)