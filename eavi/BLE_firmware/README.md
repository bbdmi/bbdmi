# BLE firmware

## About

This folder contains 20 binary files to be used as firmware for the Bluetooth Low Energy chip inside the EAVI board. The selected file will change the name and number that is broadcasted by each board, allowing the use of multiple boards at the same time.

To change the broadcasted name of the BLE chip, the firmware can be modified by editing the relevant files on this [repository](https://gitlab.huma-num.fr/bbdmi/OpenWare/-/tree/main/MidiBLE?ref_type=heads).