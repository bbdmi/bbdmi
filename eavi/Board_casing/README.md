# BBDMI EAVI board casing

## About

This folder contains the prototypes of the EAVI board casing designed for 3D printing. It includes various versions, both with and without a battery compartment.