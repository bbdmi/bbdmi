# BBDMI EAVI board casing No battery V1

## About

This folder contains a prototype of the EAVI board casing, which does not include a battery compartment. The 3D model includes the numbers of the channels, two lateral slots for attaching it with a belt, and the BBDMI logo on one side. The folder contains the same model exported in various formats.