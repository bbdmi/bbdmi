# BBDMI EAVI firmware repository

## About

This folder contains multiple firmware versions for the EAVI board.

In order to install the firmware in your EAVI board you should follow the instructions described in this document: https://docs.google.com/document/d/1ZNCd8OyWAIq23DCj5iS6TGUz_xcomUJq4LWW_sqyUiI/edit

The "flash_eavi.sh" file is a mac shell file that automates the transfer of the V22.5 firmware if your repository is in the Documents/GitHub/ folder.