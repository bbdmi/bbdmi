#!/bin/bash
echo --EAVI board flashing started--
echo ---------BDMI project---------
cd
cd Documents/GitLab/bbdmi/eavi/firmware/V22_5rc3/
openocd -f openocd.cfg -c "init" -c "reset halt" -c "stm32f4x unlock 0" -c "exit"
openocd -f openocd.cfg -c "init" -c "reset halt" -c "flash protect 0 0 11 off" -c "flash info 0" -c "exit"
openocd -f openocd.cfg -c "program BioSignalsEMG_5rc3.elf exit"
echo --------Flashing finished--------
echo ---------Happy EAVIing---------
#openocd -f openocd.cfg -c "init" -c "reset halt" -c "stm32f4x unlock 0" -c "exit"
