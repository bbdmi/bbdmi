#!/bin/bash
echo --EAVI board flashing started--
echo ---------BDMI project---------
/Applications/Utilities/openocd-0.12.0-2/bin/openocd -f openocd.cfg -c "init" -c "reset halt" -c "stm32f4x unlock 0" -c "exit"
/Applications/Utilities/openocd-0.12.0-2/bin/openocd -f openocd.cfg -c "init" -c "reset halt" -c "flash protect 0 0 11 off" -c "flash info 0" -c "exit"
/Applications/Utilities/openocd-0.12.0-2/bin/openocd -f openocd.cfg -c "program BioSignalsEMG_5rc4.elf exit"
echo --------Flashing finished--------
echo ---------Happy EAVIing---------
#openocd -f openocd.cfg -c "init" -c "reset halt" -c "stm32f4x unlock 0" -c "exit"
