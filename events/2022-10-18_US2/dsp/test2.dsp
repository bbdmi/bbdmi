import("stdfaust.lib");
n = 11;
length = hslider("length", 1.023, 0.25, 10, 0.01);
chorus = hslider("chorus", 0, 0, 0.5, 0.01);
density = hslider("density", 1, 0.01, 100, 0.01);
f(i, n, delta) = pow(2, delta*(-1+2*i/(n-1)));
process = par(i, n, (no.sparse_noise(density*(1+i*0.01)) : pm.guitar(length*f(i, n, chorus), 0.5, 0.2)));