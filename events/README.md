# BBDMI Events repository

This folder contains patches created for different events, including user studies, concerts and conferences.

## Disclaimer

Due to the work-in-progress status of the repository at the time of the events, these patches might not work. Please, report to us if you encounter any issue, incompatibility or bug.