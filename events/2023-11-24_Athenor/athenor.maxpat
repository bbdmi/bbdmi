{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 6,
			"revision" : 2,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 0.0, 53.0, 794.0, 622.0 ],
		"openrect" : [ 0.0, 0.0, 794.0, 622.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 3,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 646.0, 450.0, 95.0, 22.0 ],
					"text" : "prepend winsize"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 722.137264415642903, 394.627892763209616, 32.0, 22.0 ],
					"presentation_linecount" : 2,
					"text" : "0.25"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 685.25, 394.627892763209616, 29.5, 22.0 ],
					"text" : "0.6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 646.0, 394.627892763209616, 29.5, 22.0 ],
					"text" : "0.4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 678.137264415642903, 344.300427794456482, 58.0, 22.0 ],
					"text" : "sel -1 4 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 835.5, 202.877888166666821, 59.0, 22.0 ],
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.168627450980392, 0.168627450980392, 0.168627450980392, 1.0 ],
					"border" : 3.0,
					"bordercolor" : [ 0.188235294117647, 0.188235294117647, 0.188235294117647, 1.0 ],
					"id" : "obj-23",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 835.5, 157.755776333333642, 157.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.811747271024672, 131.719806581735611, 183.509526550769749, 22.0 ],
					"rounded" : 0.0,
					"text" : "0 -1 1 2 3 -2 4 -3 5 -4 6",
					"varname" : "Send-TX"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 872.5, 796.821580626283662, 51.0, 22.0 ],
					"text" : "line -70."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 674.500000000000114, 796.821580626283662, 51.0, 22.0 ],
					"text" : "line -70."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 458.5, 796.821580626283662, 51.0, 22.0 ],
					"text" : "line -70."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 76.0, 191.0, 490.0, 460.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-14",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 96.400000000000006, 276.0, 107.0, 35.0 ],
									"text" : ";\rlifting-gain -3 1000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 151.0, 397.0, 99.0, 20.0 ],
									"text" : "<< all to 0 (reset)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 343.600000000000023, 126.0, 124.0, 35.0 ],
									"text" : ";\rAT-perf-gain -70 8000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 308.199999999999989, 176.0, 114.0, 35.0 ],
									"text" : ";\rAT-perf-gain 0 2000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 272.800000000000011, 226.0, 118.0, 35.0 ],
									"text" : ";\rleloup-gain -70 8000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 237.0, 276.0, 107.0, 35.0 ],
									"text" : ";\rleloup-gain 0 2000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.400000000000006, 326.0, 114.0, 35.0 ],
									"text" : ";\rlifting-gain -70 8000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 60.400000000000006, 326.0, 103.0, 35.0 ],
									"text" : ";\rlifting-gain 0 2000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"linecount" : 4,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 25.0, 376.0, 118.0, 62.0 ],
									"text" : ";\rleloup-gain -70 500;\rlifting-gain -70 500;\rAT-perf-gain -70 500"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"maxclass" : "newobj",
									"numinlets" : 11,
									"numoutlets" : 11,
									"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "" ],
									"patching_rect" : [ 25.0, 83.0, 373.0, 22.0 ],
									"text" : "sel 0 -1 1 2 3 -2 4 -3 5 -4"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-93",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 25.000003584357046, 23.000002205543524, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"midpoints" : [ 317.699999999999989, 108.0, 317.699999999999989, 108.0 ],
									"source" : [ "obj-65", 8 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"midpoints" : [ 353.100000000000023, 108.0, 353.100000000000023, 108.0 ],
									"source" : [ "obj-65", 9 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"midpoints" : [ 140.699999999999989, 261.0, 105.900000000000006, 261.0 ],
									"source" : [ "obj-65", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"midpoints" : [ 34.5, 108.0, 34.5, 108.0 ],
									"source" : [ "obj-65", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"midpoints" : [ 176.099999999999994, 261.0, 69.900000000000006, 261.0 ],
									"source" : [ "obj-65", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"midpoints" : [ 105.299999999999997, 261.0, 69.900000000000006, 261.0 ],
									"source" : [ "obj-65", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"midpoints" : [ 69.900000000000006, 108.0, 69.900000000000006, 108.0 ],
									"source" : [ "obj-65", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"midpoints" : [ 211.5, 108.0, 211.900000000000006, 108.0 ],
									"source" : [ "obj-65", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"midpoints" : [ 246.900000000000006, 108.0, 246.5, 108.0 ],
									"source" : [ "obj-65", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"midpoints" : [ 282.300000000000011, 108.0, 282.300000000000011, 108.0 ],
									"source" : [ "obj-65", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"midpoints" : [ 34.500003584357046, 54.0, 34.5, 54.0 ],
									"source" : [ "obj-93", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 974.0, 344.300427794456482, 47.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 351.321273821794421, 264.238025498323054, 47.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p gains"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 872.5, 765.600855588912964, 80.0, 22.0 ],
					"text" : "r AT-perf-gain"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 674.500000000000114, 765.600855588912964, 71.0, 22.0 ],
					"text" : "r lifting-gain"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 458.5, 765.600855588912964, 75.0, 22.0 ],
					"text" : "r leloup-gain"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1066.0, 724.614638577320989, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 716.0, 127.0921117666594, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1291.0, 287.0, 31.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 709.004433319174609, 556.050427794456482, 31.0, 20.0 ],
					"text" : "INIT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1325.788854598262787, 127.0921117666594, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1325.788854598262787, 155.755776333333642, 147.0, 22.0 ],
					"text" : "read athenor.json, recall 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 87.0, 173.0, 257.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 33.25, 170.000012222222267, 120.0, 22.0 ],
									"text" : "prepend pattrstorage"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 14.0, 60.500012222222267, 63.0, 22.0 ],
									"text" : "route read"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 14.0, 133.500012222222267, 96.0, 22.0 ],
									"text" : "regexp (.+)\\\\..+$"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-72",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 14.0, 97.000012222222267, 57.0, 22.0 ],
									"text" : "strippath"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-86",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 14.000000023437501, 16.000012222222267, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-87",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 33.25, 206.500012222222267, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"midpoints" : [ 42.75, 156.0, 42.75, 156.0 ],
									"source" : [ "obj-66", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"midpoints" : [ 23.5, 120.0, 23.5, 120.0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"midpoints" : [ 23.5, 84.0, 23.5, 84.0 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"midpoints" : [ 42.75, 195.0, 42.75, 195.0 ],
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"midpoints" : [ 23.500000023437501, 48.0, 23.5, 48.0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1144.0, 240.034059300400486, 52.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p preset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1219.0, 155.755776333333642, 89.0, 22.0 ],
					"text" : "storagewindow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-250",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1144.0, 155.755776333333642, 63.0, 22.0 ],
					"text" : "writeagain"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1144.0, 204.780928985937408, 441.0, 22.0 ],
					"priority" : 					{
						"lifting::bbdmi.granulator~::Transp" : 1,
						"lifting::bbdmi.granulator~[1]::Transp" : 1,
						"lifting::bbdmi.granulator~[3]::Transp" : 1,
						"lifting::bbdmi.granulator~[2]::Transp" : 1,
						"lifting::bbdmi.crosspatch::Crosspatch" : 1,
						"lifting::bbdmi.crosspatch[1]::Crosspatch" : 1,
						"lifting::bbdmi.crosspatch[2]::Crosspatch" : 1,
						"lifting[1]::bbdmi.granulator~::Transp" : 1,
						"lifting[1]::bbdmi.crosspatch::Crosspatch" : 1,
						"lifting[1]::bbdmi.granulator~[1]::Transp" : 1,
						"lifting[3]::bbdmi.multislider::Multislider" : 1,
						"lifting[3]::bbdmi.crosspatch::Crosspatch" : 1,
						"lifting[2]::bbdmi.multislider::Multislider" : 1,
						"lifting[2]::bbdmi.crosspatch::Crosspatch" : 1,
						"bbdmi.multislider::Multislider" : 1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 157, 454, 866 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 583, 69, 1034, 197 ]
					}
,
					"text" : "pattrstorage athenor @greedy 1 @savemode 1 @autorestore 1 @changemode 1",
					"varname" : "athenor"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 23,
					"embed" : 0,
					"id" : "obj-19",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 1144.0, 280.589345813229556, 140.0, 33.0 ],
					"pattrstorage" : "athenor",
					"presentation" : 1,
					"presentation_rect" : [ 645.635225030982042, 550.300427794456482, 59.4132818814046, 31.5 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 14.0,
					"id" : "obj-40",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 993.5, 677.614638577320989, 70.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 721.767703410982904, 12.238025498323054, 67.0, 22.0 ],
					"text" : "MASTER"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 334.5, 318.784998154641926, 89.0, 22.0 ],
					"text" : "loadmess set 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 835.5, 124.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"id" : "obj-80",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 901.0, 125.0921117666594, 81.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.811747271024672, 104.800427794456482, 81.0, 20.0 ],
					"text" : "CUE ORDER"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "eavi-raw" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-64",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.send.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 458.533720153772322, 157.755776333333642, 199.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.712446, 97.300427999999997, 199.0, 64.000000000000014 ],
					"varname" : "bbdmi.send[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 798.5, 249.0, 56.0, 22.0 ],
					"text" : "zl.lookup"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-73",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 20.0, 606.863040288660386, 199.0, 161.000000000000057 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.533720153772322, 387.300427794456482, 199.0, 161.0 ],
					"varname" : "bbdmi.multislider",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 334.5, 1539.614638577320875, 150.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"channels" : 4,
					"id" : "obj-69",
					"lastchannelcount" : 4,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"orientation" : 1,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 753.5, 892.042305663654361, 199.0, 48.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.533720153772322, 292.0, 199.0, 48.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ -70.0 ],
							"parameter_initial_enable" : 1,
							"parameter_longname" : "live.gain~[4]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_modmode" : 0,
							"parameter_shortname" : "AT-perf~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "live.gain~[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"lastchannelcount" : 2,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"orientation" : 1,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 546.5, 892.042305663654361, 199.0, 36.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.533720153772322, 184.499999897228236, 199.0, 36.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ -70.0 ],
							"parameter_initial_enable" : 1,
							"parameter_longname" : "live.gain~[3]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_modmode" : 0,
							"parameter_shortname" : "lifting~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "live.gain~[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"lastchannelcount" : 2,
					"maxclass" : "mc.live.gain~",
					"numinlets" : 1,
					"numoutlets" : 4,
					"orientation" : 1,
					"outlettype" : [ "multichannelsignal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 334.5, 892.042305663654361, 199.0, 36.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.533720153772322, 75.300427999999997, 199.0, 36.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ -70.0 ],
							"parameter_initial_enable" : 1,
							"parameter_longname" : "live.gain~",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_modmode" : 0,
							"parameter_shortname" : "leloup~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"showname" : 0,
					"varname" : "live.gain~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 704.637264415642903, 521.477678865981375, 37.0, 22.0 ],
					"text" : "close"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 594.637264415642903, 521.477678865981375, 37.0, 22.0 ],
					"text" : "close"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 658.637264415642903, 605.477678865981375, 93.0, 22.0 ],
					"text" : "RD-perf.maxpat",
					"varname" : "lifting[2]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 658.637264415642903, 521.477678865981375, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 658.637264415642903, 570.477678865981375, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 550.637264415642903, 605.477678865981375, 90.0, 22.0 ],
					"text" : "AT-perf.maxpat",
					"varname" : "lifting[3]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 550.637264415642903, 521.477678865981375, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 550.637264415642903, 570.477678865981375, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "AT-perf~", "@chans", 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-51",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.receive~.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 753.5, 677.614638577320989, 199.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.533720153772322, 225.0, 199.0, 65.0 ],
					"varname" : "bbdmi.receive~[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "lifting~", "@chans", 2 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-49",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.receive~.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 546.5, 677.614638577320989, 199.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.533720153772322, 114.900213794456477, 199.0, 65.000000205543515 ],
					"varname" : "bbdmi.receive~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ "leloup~", "@chans", 2 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-48",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.receive~.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 334.5, 677.614638577320989, 199.0, 65.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.533720153772322, 8.300427794456482, 199.000000000000057, 65.000000205543515 ],
					"varname" : "bbdmi.receive~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-38",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.dac~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 334.5, 1420.323610907641978, 199.0, 96.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.533720153772322, 518.300427794456482, 199.0, 96.0 ],
					"varname" : "bbdmi.dac~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-30",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 334.5, 1233.323610907641978, 199.0, 162.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 423.533720153772322, 354.300427794456482, 199.0, 162.0 ],
					"varname" : "bbdmi.crosspatch~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"fontsize" : 22.0,
					"id" : "obj-25",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 864.5, 282.351204795590775, 55.0, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 707.004433319174609, 354.300427794456482, 55.0, 31.0 ],
					"text" : "CUE"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 20.0,
					"id" : "obj-239",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 798.5, 282.351204795590775, 64.0, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 645.635225030982042, 354.300427794456482, 59.0, 31.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-90",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1145.0, 1076.827457819778601, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 753.605938196384159, 296.500000000000227, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "2",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usetextovercolor" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-39",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1104.172322416305178, 1073.137805424551289, 35.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 715.004433319174495, 293.0, 35.0, 29.0 ],
					"text" : "dB"
				}

			}
, 			{
				"box" : 				{
					"contdata" : 1,
					"ghostbar" : 70,
					"id" : "obj-21",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 994.0, 776.149581395008568, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 645.635225030982042, 14.238025498323054, 60.0, 272.0 ],
					"setminmax" : [ 0.0, 127.0 ],
					"slidercolor" : [ 0.882352941176471, 0.243137254901961, 0.149019607843137, 1.0 ],
					"thickness" : 4
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 992.5, 1113.637805424551289, 57.0, 22.0 ],
					"text" : "pack f 40"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 992.5, 1145.137805424551289, 54.0, 22.0 ],
					"text" : "mc.line~"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-154",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1000.0, 783.10698591311791, 96.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 692.767703410982904, 37.785301862727465, 96.0, 29.0 ],
					"text" : "–– 0.0 dB"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"format" : 6,
					"id" : "obj-140",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1018.683905839920044, 1073.137805424551289, 78.0, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 645.635225030982042, 292.0, 60.0, 31.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1018.683905839920044, 1044.137805424551289, 39.0, 22.0 ],
					"text" : "atodb"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-144",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 992.5, 992.137805424551289, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 334.5, 1195.47435097864809, 53.0, 22.0 ],
					"text" : "mc.*~ 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 993.5, 699.614638577320989, 67.0, 20.0 ],
					"text" : "MIDI CTL1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-150",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 992.5, 931.137805424551289, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 992.5, 961.521135668376928, 123.0, 22.0 ],
					"text" : "scale 0 127 0. 3. 1.06"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-162",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 993.5, 724.614638577320989, 60.0, 22.0 ],
					"text" : "ctlin a 1 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-163",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1055.5, 699.614638577320989, 67.0, 20.0 ],
					"text" : "input pedal"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.788235, 0.470588, 1.0 ],
					"fontname" : "Arial Bold",
					"hint" : "",
					"id" : "obj-110",
					"ignoreclick" : 1,
					"legacytextcolor" : 1,
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 217.000000000000028, 934.208972330321103, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 717.004433319174609, 418.600855588912964, 20.0, 20.0 ],
					"rounded" : 60.0,
					"text" : "1",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usetextovercolor" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 158.773621000000304, 921.708972330321103, 45.0, 45.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 645.635225030982042, 399.100855588912964, 59.0, 59.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 158.773621000000304, 892.042305663654361, 87.0, 22.0 ],
					"text" : "loadmess start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 20.000000000000028, 952.922027940977841, 48.0, 22.0 ],
					"text" : "change"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 20.000000000000028, 892.042305663654361, 119.0, 22.0 ],
					"text" : "metro 500 @active 1"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"fontsize" : 20.0,
					"id" : "obj-55",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 88.226378999999753, 985.49331694097782, 51.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 707.004433319174609, 471.300427794456482, 51.0, 29.0 ],
					"text" : "CPU"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 20.0,
					"id" : "obj-47",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 20.000000000000028, 983.755361274311099, 64.0, 31.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 645.635225030982042, 470.300427794456482, 59.0, 31.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-56",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 20.000000000000028, 921.708972330321103, 84.0, 23.0 ],
					"text" : "adstatus cpu"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "eavi-main" ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-8",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.send.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 20.0, 781.555720999999949, 199.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.533720153772322, 550.300427794456482, 199.0, 64.0 ],
					"varname" : "bbdmi.send",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 271.977111839843474, 186.976609666667287, 82.0, 22.0 ],
					"text" : "zmap 0 1 -1 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 271.977111839843474, 157.755776333333642, 155.0, 22.0 ],
					"text" : "bbdmi.calibrate 4 @mode 1",
					"varname" : "bbdmi.calibrate[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 271.977111839843474, 216.197443000000931, 87.0, 22.0 ],
					"text" : "prepend setlist"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.168627450980392, 0.168627450980392, 0.168627450980392, 1.0 ],
					"candycane" : 8,
					"contdata" : 1,
					"id" : "obj-20",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 271.977111839843474, 267.351204795590775, 67.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 13.212446331977901, 168.411840796470642, 187.0, 122.0 ],
					"setstyle" : 3,
					"signed" : 1,
					"size" : 4,
					"slidercolor" : [ 0.850980392156863, 0.784313725490196, 0.207843137254902, 1.0 ],
					"spacing" : 2
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-17",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.speedlim.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 69.0, 529.17035999999996, 199.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.533720153772322, 550.300427794456482, 199.0, 64.0 ],
					"varname" : "bbdmi.speedlim",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "multichannelsignal", "" ],
					"patching_rect" : [ 20.000000000000028, 157.755776151021337, 83.0, 22.0 ],
					"text" : "bbdmi.list2~ 4",
					"varname" : "bbdmi.list2~[1]"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 4 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-14",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 20.000000000000028, 354.477678865981375, 199.0, 161.000000000000057 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.533720153772322, 387.300427794456482, 199.0, 161.0 ],
					"varname" : "bbdmi.calibrate",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 4, "@winsize", 0.25, "@clock", 5 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-190",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.rms~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 20.000000000000028, 252.239791611264309, 199.0, 88.545206543377617 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.533720153772322, 297.500000000000227, 199.0, 87.0 ],
					"varname" : "bbdmi.rms~",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-192",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.eavi.maxpat",
					"numinlets" : 1,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 20.000000000000028, 20.0, 198.999999999999972, 87.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.712446331977844, 8.300427794456482, 198.999999999999943, 87.0 ],
					"varname" : "bbdmi.eavi",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0 ],
					"id" : "obj-6",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 246.298385661637923, 252.239791793576615, 118.357452356411159, 52.22282600402832 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.533720153772322, 163.300427794456482, 199.178726178205523, 133.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 975.5, 424.600855588912964, 79.0, 22.0 ],
					"text" : "loadmess set"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "bang", "bang", "bang", "bang", "" ],
					"patching_rect" : [ 334.5, 468.787204951834951, 64.0, 22.0 ],
					"text" : "sel 0 1 2 3"
				}

			}
, 			{
				"box" : 				{
					"htabcolor" : [ 0.152941176470588, 0.729411764705882, 0.980392156862745, 1.0 ],
					"id" : "obj-44",
					"maxclass" : "tab",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 334.5, 354.477678865981375, 85.0, 102.300427794456482 ],
					"presentation" : 1,
					"presentation_rect" : [ 212.811747271024672, 168.411840796470642, 98.857143878936768, 121.588159203529358 ],
					"rounded" : 12.0,
					"tabcolor" : [ 0.188235294117647, 0.188235294117647, 0.188235294117647, 1.0 ],
					"tabs" : [ "Le Loup", "Lifting", "AT-perf", "RD-perf" ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 488.5, 521.477678865981375, 37.0, 22.0 ],
					"text" : "close"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 378.5, 521.477678865981375, 37.0, 22.0 ],
					"text" : "close"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 864.5, 377.800427794456482, 77.0, 22.0 ],
					"text" : "route symbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"linecount" : 4,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 998.0, 20.0, 83.0, 60.0 ],
					"presentation" : 1,
					"presentation_linecount" : 4,
					"presentation_rect" : [ 314.0, 168.0, 83.0, 60.0 ],
					"text" : "RESET = 0\nLifting = 1-5\nLe Loup = 6-7\nAT-perf = 8-9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 864.5, 424.600855588912964, 72.0, 22.0 ],
					"text" : "prepend set"
				}

			}
, 			{
				"box" : 				{
					"border" : 12.0,
					"bordercolor" : [ 0.188235294117647, 0.188235294117647, 0.188235294117647, 1.0 ],
					"fontface" : 0,
					"fontsize" : 16.0,
					"id" : "obj-13",
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 864.5, 472.300427794456482, 190.0, 45.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.216509181463209, 332.309523820877303, 187.0, 45.0 ],
					"text" : "AT-perf: PLAY"
				}

			}
, 			{
				"box" : 				{
					"coll_data" : 					{
						"count" : 11,
						"data" : [ 							{
								"key" : 0,
								"value" : [ "reset" ]
							}
, 							{
								"key" : -1,
								"value" : [ "Lifting:", "ARM" ]
							}
, 							{
								"key" : 1,
								"value" : [ "Lifting:", "sample", 1 ]
							}
, 							{
								"key" : 2,
								"value" : [ "Lifting:", "sample", 2 ]
							}
, 							{
								"key" : 3,
								"value" : [ "Lifting:", "sample", 3 ]
							}
, 							{
								"key" : -2,
								"value" : [ "Lifting:", "STOP" ]
							}
, 							{
								"key" : 4,
								"value" : [ "Le", "Loup:", "PLAY" ]
							}
, 							{
								"key" : -3,
								"value" : [ "Le", "Loup:", "STOP" ]
							}
, 							{
								"key" : 5,
								"value" : [ "AT-perf:", "PLAY" ]
							}
, 							{
								"key" : -4,
								"value" : [ "AT-perf:", "STOP" ]
							}
, 							{
								"key" : 6,
								"value" : [ "end", "(click", "reset)" ]
							}
 ]
					}
,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 864.5, 344.300427794456482, 89.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 309.321273821794477, 304.500000000000227, 89.0, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 1,
						"precision" : 6
					}
,
					"text" : "coll @embed 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"linecolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 994.859939220749993, 86.0, 75.140060779250007, 5.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.533720153772322, 298.0, 199.0, 5.0 ],
					"saved_attribute_attributes" : 					{
						"linecolor" : 						{
							"expression" : ""
						}

					}

				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 442.5, 605.477678865981375, 79.0, 22.0 ],
					"text" : "lifting.maxpat",
					"varname" : "lifting[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 442.5, 521.477678865981375, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 442.5, 570.477678865981375, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 334.5, 605.477678865981375, 83.0, 22.0 ],
					"text" : "leloup.maxpat",
					"varname" : "lifting"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 334.5, 521.477678865981375, 35.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 334.5, 570.477678865981375, 51.0, 22.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 798.5, 344.300427794456482, 37.0, 22.0 ],
					"text" : "s cue"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-1",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.cue.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 798.5, 20.0, 187.0, 87.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.533720153772322, 8.300427794456482, 199.0, 87.0 ],
					"varname" : "bbdmi.cue",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-32",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 974.0, 393.600855588912964, 82.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 213.811747271024672, 301.000000000000227, 192.72197288274765, 29.0 ],
					"text" : "Monitor"
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0 ],
					"id" : "obj-61",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 980.341952919960022, 769.149581395008568, 45.816094160079956, 155.957404518109342 ],
					"presentation" : 1,
					"presentation_rect" : [ 639.533720153772379, 8.300427794456482, 72.203009754419327, 322.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"background" : 1,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.203921568627451, 0.203921568627451, 0.203921568627451, 1.0 ],
					"id" : "obj-33",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 994.859939220749993, 17.0, 75.140060779250007, 64.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.533720153772322, 96.699531674385071, 199.0, 287.800468325615157 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"midpoints" : [ 808.0, 108.0, 808.0, 108.0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 344.0, 546.0, 344.0, 546.0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 1153.5, 228.0, 1153.5, 228.0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"midpoints" : [ 874.0, 369.0, 874.0, 369.0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"midpoints" : [ 29.500000000000028, 516.0, 29.5, 516.0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-140", 0 ],
					"midpoints" : [ 1028.183905839920044, 1068.0, 1028.183905839920044, 1068.0 ],
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"midpoints" : [ 1002.0, 1029.0, 1028.183905839920044, 1029.0 ],
					"order" : 0,
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"midpoints" : [ 1002.0, 1017.0, 1002.0, 1017.0 ],
					"order" : 1,
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"midpoints" : [ 344.0, 1218.0, 344.0, 1218.0 ],
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 0 ],
					"midpoints" : [ 1002.0, 954.0, 1002.0, 954.0 ],
					"source" : [ "obj-150", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"midpoints" : [ 168.273621000000304, 915.0, 168.273621000000304, 915.0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"midpoints" : [ 1002.0, 984.0, 1002.0, 984.0 ],
					"source" : [ "obj-161", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"midpoints" : [ 1003.0, 747.0, 1003.5, 747.0 ],
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"midpoints" : [ 874.0, 447.0, 874.0, 447.0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"midpoints" : [ 29.500000000000028, 342.0, 29.500000000000028, 342.0 ],
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"midpoints" : [ 29.500000000000028, 108.0, 29.500000000000028, 108.0 ],
					"order" : 1,
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"midpoints" : [ 29.500000000000028, 144.0, 468.033720153772322, 144.0 ],
					"order" : 2,
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-83", 0 ],
					"midpoints" : [ 29.500000000000028, 144.0, 281.477111839843474, 144.0 ],
					"order" : 0,
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 0 ],
					"midpoints" : [ 1002.0, 918.0, 1002.0, 918.0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"midpoints" : [ 845.0, 180.0, 845.0, 180.0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"midpoints" : [ 808.0, 330.0, 874.0, 330.0 ],
					"order" : 1,
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 808.0, 315.0, 808.0, 315.0 ],
					"order" : 2,
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"midpoints" : [ 808.0, 330.0, 687.637264415642903, 330.0 ],
					"order" : 3,
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 808.0, 330.0, 983.5, 330.0 ],
					"order" : 0,
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"midpoints" : [ 452.0, 546.0, 452.0, 546.0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 1153.5, 180.0, 1153.5, 180.0 ],
					"source" : [ "obj-250", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"midpoints" : [ 452.0, 594.0, 452.0, 594.0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 1335.288854598262787, 189.0, 1153.5, 189.0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 1335.288854598262787, 150.0, 1335.288854598262787, 150.0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"midpoints" : [ 344.0, 342.0, 344.0, 342.0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 1 ],
					"midpoints" : [ 93.500000000000028, 237.0, 209.500000000000028, 237.0 ],
					"source" : [ "obj-3", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 0 ],
					"midpoints" : [ 29.500000000000028, 180.0, 29.500000000000028, 180.0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"midpoints" : [ 344.0, 1398.0, 344.0, 1398.0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"midpoints" : [ 932.0, 411.0, 874.0, 411.0 ],
					"source" : [ "obj-35", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"midpoints" : [ 874.0, 402.0, 874.0, 402.0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 388.0, 555.0, 344.0, 555.0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"midpoints" : [ 498.0, 555.0, 452.0, 555.0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"midpoints" : [ 985.0, 459.0, 874.0, 459.0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 725.5, 330.0, 808.0, 330.0 ],
					"order" : 1,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"midpoints" : [ 725.5, 330.0, 983.5, 330.0 ],
					"order" : 0,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"midpoints" : [ 281.477111839843474, 240.0, 281.477111839843474, 240.0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"midpoints" : [ 344.0, 459.0, 344.0, 459.0 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"midpoints" : [ 344.0, 492.0, 344.0, 492.0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"midpoints" : [ 355.25, 507.0, 452.0, 507.0 ],
					"source" : [ "obj-46", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"midpoints" : [ 377.75, 507.0, 668.137264415642903, 507.0 ],
					"source" : [ "obj-46", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"midpoints" : [ 366.5, 507.0, 560.137264415642903, 507.0 ],
					"source" : [ "obj-46", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"midpoints" : [ 344.0, 744.0, 344.0, 744.0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"midpoints" : [ 556.0, 744.0, 556.0, 744.0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"midpoints" : [ 344.0, 594.0, 344.0, 594.0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"midpoints" : [ 763.0, 744.0, 763.0, 744.0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"midpoints" : [ 808.0, 273.0, 808.0, 273.0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"midpoints" : [ 714.137264415642903, 555.0, 668.137264415642903, 555.0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"midpoints" : [ 604.137264415642903, 555.0, 560.137264415642903, 555.0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"midpoints" : [ 29.500000000000028, 945.0, 29.500000000000028, 945.0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"midpoints" : [ 668.137264415642903, 546.0, 668.137264415642903, 546.0 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"midpoints" : [ 668.137264415642903, 594.0, 668.137264415642903, 594.0 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"midpoints" : [ 560.137264415642903, 546.0, 560.137264415642903, 546.0 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"midpoints" : [ 560.137264415642903, 594.0, 560.137264415642903, 594.0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"midpoints" : [ 687.637264415642903, 381.0, 655.5, 381.0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"midpoints" : [ 700.637264415642903, 381.0, 694.75, 381.0 ],
					"source" : [ "obj-65", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"midpoints" : [ 713.637264415642903, 381.0, 731.637264415642903, 381.0 ],
					"source" : [ "obj-65", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"midpoints" : [ 344.0, 930.0, 344.0, 930.0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"midpoints" : [ 556.0, 1182.0, 344.0, 1182.0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"midpoints" : [ 763.0, 1182.0, 344.0, 1182.0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"midpoints" : [ 1153.5, 264.0, 1153.5, 264.0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 1 ],
					"midpoints" : [ 845.0, 225.0, 845.0, 225.0 ],
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"midpoints" : [ 1002.0, 1137.0, 1002.0, 1137.0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"midpoints" : [ 29.5, 768.0, 29.5, 768.0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"midpoints" : [ 1075.5, 762.0, 1003.5, 762.0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 1 ],
					"midpoints" : [ 1002.0, 1182.0, 378.0, 1182.0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"midpoints" : [ 655.5, 417.0, 655.5, 417.0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"midpoints" : [ 845.0, 147.0, 845.0, 147.0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"midpoints" : [ 882.0, 789.0, 882.0, 789.0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"midpoints" : [ 281.477111839843474, 180.0, 281.477111839843474, 180.0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"midpoints" : [ 281.477111839843474, 210.0, 281.477111839843474, 210.0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"midpoints" : [ 29.500000000000028, 915.0, 29.500000000000028, 915.0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"midpoints" : [ 684.000000000000114, 789.0, 684.000000000000114, 789.0 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"midpoints" : [ 468.0, 789.0, 468.0, 789.0 ],
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"midpoints" : [ 694.75, 435.0, 655.5, 435.0 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"midpoints" : [ 29.500000000000028, 975.0, 29.500000000000028, 975.0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 1228.5, 189.0, 1153.5, 189.0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"midpoints" : [ 731.637264415642903, 435.0, 655.5, 435.0 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-190", 1 ],
					"midpoints" : [ 655.5, 501.0, 231.0, 501.0, 231.0, 246.0, 209.500000000000028, 246.0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"midpoints" : [ 468.0, 879.0, 344.0, 879.0 ],
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"midpoints" : [ 684.000000000000114, 879.0, 556.0, 879.0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"midpoints" : [ 882.0, 879.0, 763.0, 879.0 ],
					"source" : [ "obj-97", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-14::obj-107::obj-33" : [ "tab[184]", "tab[1]", 0 ],
			"obj-14::obj-123::obj-33" : [ "tab[182]", "tab[1]", 0 ],
			"obj-14::obj-34::obj-33" : [ "tab[185]", "tab[1]", 0 ],
			"obj-14::obj-36::obj-33" : [ "tab[186]", "tab[1]", 0 ],
			"obj-14::obj-40::obj-33" : [ "tab[187]", "tab[1]", 0 ],
			"obj-14::obj-41::obj-33" : [ "tab[188]", "tab[1]", 0 ],
			"obj-14::obj-42::obj-33" : [ "tab[189]", "tab[1]", 0 ],
			"obj-14::obj-43::obj-33" : [ "tab[190]", "tab[1]", 0 ],
			"obj-14::obj-44::obj-33" : [ "tab[191]", "tab[1]", 0 ],
			"obj-14::obj-45::obj-33" : [ "tab[192]", "tab[1]", 0 ],
			"obj-14::obj-46::obj-33" : [ "tab[193]", "tab[1]", 0 ],
			"obj-14::obj-47::obj-33" : [ "tab[194]", "tab[1]", 0 ],
			"obj-14::obj-48::obj-33" : [ "tab[195]", "tab[1]", 0 ],
			"obj-14::obj-49::obj-33" : [ "tab[196]", "tab[1]", 0 ],
			"obj-14::obj-50::obj-33" : [ "tab[197]", "tab[1]", 0 ],
			"obj-14::obj-74::obj-33" : [ "tab[183]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-107::obj-27::obj-18" : [ "toggle[53]", "toggle", 0 ],
			"obj-15::obj-219::obj-107::obj-48" : [ "SendTo-TXT[82]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-107::obj-8" : [ "tab[215]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-123::obj-27::obj-18" : [ "toggle[64]", "toggle", 0 ],
			"obj-15::obj-219::obj-123::obj-48" : [ "SendTo-TXT[43]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-123::obj-8" : [ "tab[141]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-34::obj-27::obj-18" : [ "toggle[54]", "toggle", 0 ],
			"obj-15::obj-219::obj-34::obj-48" : [ "SendTo-TXT[83]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-34::obj-8" : [ "tab[216]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-36::obj-27::obj-18" : [ "toggle[93]", "toggle", 0 ],
			"obj-15::obj-219::obj-36::obj-48" : [ "SendTo-TXT[84]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-36::obj-8" : [ "tab[217]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-40::obj-27::obj-18" : [ "toggle[94]", "toggle", 0 ],
			"obj-15::obj-219::obj-40::obj-48" : [ "SendTo-TXT[85]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-40::obj-8" : [ "tab[218]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-41::obj-27::obj-18" : [ "toggle[95]", "toggle", 0 ],
			"obj-15::obj-219::obj-41::obj-48" : [ "SendTo-TXT[86]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-41::obj-8" : [ "tab[219]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-42::obj-27::obj-18" : [ "toggle[96]", "toggle", 0 ],
			"obj-15::obj-219::obj-42::obj-48" : [ "SendTo-TXT[87]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-42::obj-8" : [ "tab[220]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-43::obj-27::obj-18" : [ "toggle[97]", "toggle", 0 ],
			"obj-15::obj-219::obj-43::obj-48" : [ "SendTo-TXT[88]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-43::obj-8" : [ "tab[221]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-44::obj-27::obj-18" : [ "toggle[98]", "toggle", 0 ],
			"obj-15::obj-219::obj-44::obj-48" : [ "SendTo-TXT[89]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-44::obj-8" : [ "tab[1]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-45::obj-27::obj-18" : [ "toggle[99]", "toggle", 0 ],
			"obj-15::obj-219::obj-45::obj-48" : [ "SendTo-TXT[90]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-45::obj-8" : [ "tab[2]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-46::obj-27::obj-18" : [ "toggle[100]", "toggle", 0 ],
			"obj-15::obj-219::obj-46::obj-48" : [ "SendTo-TXT[91]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-46::obj-8" : [ "tab[3]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-47::obj-27::obj-18" : [ "toggle[101]", "toggle", 0 ],
			"obj-15::obj-219::obj-47::obj-48" : [ "SendTo-TXT[92]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-47::obj-8" : [ "tab[222]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-48::obj-27::obj-18" : [ "toggle[102]", "toggle", 0 ],
			"obj-15::obj-219::obj-48::obj-48" : [ "SendTo-TXT[93]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-48::obj-8" : [ "tab[223]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-49::obj-27::obj-18" : [ "toggle[103]", "toggle", 0 ],
			"obj-15::obj-219::obj-49::obj-48" : [ "SendTo-TXT[94]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-49::obj-8" : [ "tab[224]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-50::obj-27::obj-18" : [ "toggle[104]", "toggle", 0 ],
			"obj-15::obj-219::obj-50::obj-48" : [ "SendTo-TXT[95]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-50::obj-8" : [ "tab[225]", "tab[1]", 0 ],
			"obj-15::obj-219::obj-74::obj-27::obj-18" : [ "toggle[52]", "toggle", 0 ],
			"obj-15::obj-219::obj-74::obj-48" : [ "SendTo-TXT[81]", "SendTo-TXT", 0 ],
			"obj-15::obj-219::obj-74::obj-8" : [ "tab[214]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-107::obj-33" : [ "tab[227]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-34::obj-33" : [ "tab[228]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-36::obj-33" : [ "tab[229]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-40::obj-33" : [ "tab[230]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-41::obj-33" : [ "tab[231]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-42::obj-33" : [ "tab[232]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-43::obj-33" : [ "tab[233]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-44::obj-33" : [ "tab[234]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-45::obj-33" : [ "tab[235]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-46::obj-33" : [ "tab[236]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-47::obj-33" : [ "tab[237]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-48::obj-33" : [ "tab[238]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-49::obj-33" : [ "tab[239]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-50::obj-33" : [ "tab[240]", "tab[1]", 0 ],
			"obj-15::obj-221::obj-74::obj-33" : [ "tab[226]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-107::obj-27::obj-18" : [ "toggle[107]", "toggle", 0 ],
			"obj-15::obj-224::obj-107::obj-48" : [ "SendTo-TXT[98]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-107::obj-8" : [ "tab[243]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-123::obj-27::obj-18" : [ "toggle[105]", "toggle", 0 ],
			"obj-15::obj-224::obj-123::obj-48" : [ "SendTo-TXT[96]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-123::obj-8" : [ "tab[241]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-34::obj-27::obj-18" : [ "toggle[108]", "toggle", 0 ],
			"obj-15::obj-224::obj-34::obj-48" : [ "SendTo-TXT[99]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-34::obj-8" : [ "tab[244]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-36::obj-27::obj-18" : [ "toggle[109]", "toggle", 0 ],
			"obj-15::obj-224::obj-36::obj-48" : [ "SendTo-TXT[100]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-36::obj-8" : [ "tab[245]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-40::obj-27::obj-18" : [ "toggle[110]", "toggle", 0 ],
			"obj-15::obj-224::obj-40::obj-48" : [ "SendTo-TXT[101]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-40::obj-8" : [ "tab[246]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-41::obj-27::obj-18" : [ "toggle[111]", "toggle", 0 ],
			"obj-15::obj-224::obj-41::obj-48" : [ "SendTo-TXT[102]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-41::obj-8" : [ "tab[247]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-42::obj-27::obj-18" : [ "toggle[112]", "toggle", 0 ],
			"obj-15::obj-224::obj-42::obj-48" : [ "SendTo-TXT[103]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-42::obj-8" : [ "tab[248]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-43::obj-27::obj-18" : [ "toggle[113]", "toggle", 0 ],
			"obj-15::obj-224::obj-43::obj-48" : [ "SendTo-TXT[104]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-43::obj-8" : [ "tab[249]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-44::obj-27::obj-18" : [ "toggle[114]", "toggle", 0 ],
			"obj-15::obj-224::obj-44::obj-48" : [ "SendTo-TXT[105]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-44::obj-8" : [ "tab[250]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-45::obj-27::obj-18" : [ "toggle[115]", "toggle", 0 ],
			"obj-15::obj-224::obj-45::obj-48" : [ "SendTo-TXT[106]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-45::obj-8" : [ "tab[251]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-46::obj-27::obj-18" : [ "toggle[116]", "toggle", 0 ],
			"obj-15::obj-224::obj-46::obj-48" : [ "SendTo-TXT[107]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-46::obj-8" : [ "tab[252]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-47::obj-27::obj-18" : [ "toggle[117]", "toggle", 0 ],
			"obj-15::obj-224::obj-47::obj-48" : [ "SendTo-TXT[108]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-47::obj-8" : [ "tab[253]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-48::obj-27::obj-18" : [ "toggle[118]", "toggle", 0 ],
			"obj-15::obj-224::obj-48::obj-48" : [ "SendTo-TXT[109]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-48::obj-8" : [ "tab[254]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-49::obj-27::obj-18" : [ "toggle[119]", "toggle", 0 ],
			"obj-15::obj-224::obj-49::obj-48" : [ "SendTo-TXT[110]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-49::obj-8" : [ "tab[255]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-50::obj-27::obj-18" : [ "toggle[120]", "toggle", 0 ],
			"obj-15::obj-224::obj-50::obj-48" : [ "SendTo-TXT[111]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-50::obj-8" : [ "tab[256]", "tab[1]", 0 ],
			"obj-15::obj-224::obj-74::obj-27::obj-18" : [ "toggle[106]", "toggle", 0 ],
			"obj-15::obj-224::obj-74::obj-48" : [ "SendTo-TXT[97]", "SendTo-TXT", 0 ],
			"obj-15::obj-224::obj-74::obj-8" : [ "tab[242]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-107::obj-27::obj-18" : [ "toggle[123]", "toggle", 0 ],
			"obj-15::obj-226::obj-107::obj-48" : [ "SendTo-TXT[114]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-107::obj-8" : [ "tab[259]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-123::obj-27::obj-18" : [ "toggle[121]", "toggle", 0 ],
			"obj-15::obj-226::obj-123::obj-48" : [ "SendTo-TXT[112]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-123::obj-8" : [ "tab[257]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-34::obj-27::obj-18" : [ "toggle[124]", "toggle", 0 ],
			"obj-15::obj-226::obj-34::obj-48" : [ "SendTo-TXT[115]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-34::obj-8" : [ "tab[260]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-36::obj-27::obj-18" : [ "toggle[125]", "toggle", 0 ],
			"obj-15::obj-226::obj-36::obj-48" : [ "SendTo-TXT[116]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-36::obj-8" : [ "tab[261]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-40::obj-27::obj-18" : [ "toggle[126]", "toggle", 0 ],
			"obj-15::obj-226::obj-40::obj-48" : [ "SendTo-TXT[117]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-40::obj-8" : [ "tab[262]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-41::obj-27::obj-18" : [ "toggle[127]", "toggle", 0 ],
			"obj-15::obj-226::obj-41::obj-48" : [ "SendTo-TXT[118]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-41::obj-8" : [ "tab[263]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-42::obj-27::obj-18" : [ "toggle[128]", "toggle", 0 ],
			"obj-15::obj-226::obj-42::obj-48" : [ "SendTo-TXT[119]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-42::obj-8" : [ "tab[264]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-43::obj-27::obj-18" : [ "toggle[129]", "toggle", 0 ],
			"obj-15::obj-226::obj-43::obj-48" : [ "SendTo-TXT[120]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-43::obj-8" : [ "tab[265]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-44::obj-27::obj-18" : [ "toggle[130]", "toggle", 0 ],
			"obj-15::obj-226::obj-44::obj-48" : [ "SendTo-TXT[121]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-44::obj-8" : [ "tab[266]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-45::obj-27::obj-18" : [ "toggle[131]", "toggle", 0 ],
			"obj-15::obj-226::obj-45::obj-48" : [ "SendTo-TXT[122]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-45::obj-8" : [ "tab[267]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-46::obj-27::obj-18" : [ "toggle[132]", "toggle", 0 ],
			"obj-15::obj-226::obj-46::obj-48" : [ "SendTo-TXT[123]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-46::obj-8" : [ "tab[268]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-47::obj-27::obj-18" : [ "toggle[133]", "toggle", 0 ],
			"obj-15::obj-226::obj-47::obj-48" : [ "SendTo-TXT[124]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-47::obj-8" : [ "tab[269]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-48::obj-27::obj-18" : [ "toggle[134]", "toggle", 0 ],
			"obj-15::obj-226::obj-48::obj-48" : [ "SendTo-TXT[125]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-48::obj-8" : [ "tab[270]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-49::obj-27::obj-18" : [ "toggle[135]", "toggle", 0 ],
			"obj-15::obj-226::obj-49::obj-48" : [ "SendTo-TXT[126]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-49::obj-8" : [ "tab[271]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-50::obj-27::obj-18" : [ "toggle[136]", "toggle", 0 ],
			"obj-15::obj-226::obj-50::obj-48" : [ "SendTo-TXT[127]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-50::obj-8" : [ "tab[272]", "tab[1]", 0 ],
			"obj-15::obj-226::obj-74::obj-27::obj-18" : [ "toggle[122]", "toggle", 0 ],
			"obj-15::obj-226::obj-74::obj-48" : [ "SendTo-TXT[113]", "SendTo-TXT", 0 ],
			"obj-15::obj-226::obj-74::obj-8" : [ "tab[258]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-107::obj-27::obj-18" : [ "toggle[139]", "toggle", 0 ],
			"obj-15::obj-228::obj-107::obj-48" : [ "SendTo-TXT[130]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-107::obj-8" : [ "tab[275]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-123::obj-27::obj-18" : [ "toggle[137]", "toggle", 0 ],
			"obj-15::obj-228::obj-123::obj-48" : [ "SendTo-TXT[128]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-123::obj-8" : [ "tab[273]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-34::obj-27::obj-18" : [ "toggle[140]", "toggle", 0 ],
			"obj-15::obj-228::obj-34::obj-48" : [ "SendTo-TXT[131]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-34::obj-8" : [ "tab[276]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-36::obj-27::obj-18" : [ "toggle[141]", "toggle", 0 ],
			"obj-15::obj-228::obj-36::obj-48" : [ "SendTo-TXT[132]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-36::obj-8" : [ "tab[277]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-40::obj-27::obj-18" : [ "toggle[142]", "toggle", 0 ],
			"obj-15::obj-228::obj-40::obj-48" : [ "SendTo-TXT[133]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-40::obj-8" : [ "tab[278]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-41::obj-27::obj-18" : [ "toggle[143]", "toggle", 0 ],
			"obj-15::obj-228::obj-41::obj-48" : [ "SendTo-TXT[134]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-41::obj-8" : [ "tab[279]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-42::obj-27::obj-18" : [ "toggle[144]", "toggle", 0 ],
			"obj-15::obj-228::obj-42::obj-48" : [ "SendTo-TXT[135]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-42::obj-8" : [ "tab[280]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-43::obj-27::obj-18" : [ "toggle[145]", "toggle", 0 ],
			"obj-15::obj-228::obj-43::obj-48" : [ "SendTo-TXT[136]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-43::obj-8" : [ "tab[281]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-44::obj-27::obj-18" : [ "toggle[146]", "toggle", 0 ],
			"obj-15::obj-228::obj-44::obj-48" : [ "SendTo-TXT[137]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-44::obj-8" : [ "tab[282]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-45::obj-27::obj-18" : [ "toggle[147]", "toggle", 0 ],
			"obj-15::obj-228::obj-45::obj-48" : [ "SendTo-TXT[138]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-45::obj-8" : [ "tab[283]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-46::obj-27::obj-18" : [ "toggle[148]", "toggle", 0 ],
			"obj-15::obj-228::obj-46::obj-48" : [ "SendTo-TXT[139]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-46::obj-8" : [ "tab[284]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-47::obj-27::obj-18" : [ "toggle[149]", "toggle", 0 ],
			"obj-15::obj-228::obj-47::obj-48" : [ "SendTo-TXT[140]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-47::obj-8" : [ "tab[285]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-48::obj-27::obj-18" : [ "toggle[150]", "toggle", 0 ],
			"obj-15::obj-228::obj-48::obj-48" : [ "SendTo-TXT[141]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-48::obj-8" : [ "tab[286]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-49::obj-27::obj-18" : [ "toggle[151]", "toggle", 0 ],
			"obj-15::obj-228::obj-49::obj-48" : [ "SendTo-TXT[142]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-49::obj-8" : [ "tab[287]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-50::obj-27::obj-18" : [ "toggle[152]", "toggle", 0 ],
			"obj-15::obj-228::obj-50::obj-48" : [ "SendTo-TXT[143]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-50::obj-8" : [ "tab[288]", "tab[1]", 0 ],
			"obj-15::obj-228::obj-74::obj-27::obj-18" : [ "toggle[138]", "toggle", 0 ],
			"obj-15::obj-228::obj-74::obj-48" : [ "SendTo-TXT[129]", "SendTo-TXT", 0 ],
			"obj-15::obj-228::obj-74::obj-8" : [ "tab[274]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-107::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-123::obj-33" : [ "tab[19]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-34::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-36::obj-33" : [ "tab[23]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-40::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-41::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-42::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-43::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-44::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-45::obj-33" : [ "tab[29]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-46::obj-33" : [ "tab[30]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-47::obj-33" : [ "tab[31]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-48::obj-33" : [ "tab[32]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-49::obj-33" : [ "tab[33]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-50::obj-33" : [ "tab[34]", "tab[1]", 0 ],
			"obj-22::obj-105::obj-74::obj-33" : [ "tab[20]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-107::obj-27::obj-18" : [ "toggle[155]", "toggle", 0 ],
			"obj-22::obj-57::obj-107::obj-48" : [ "SendTo-TXT[146]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-107::obj-8" : [ "tab[6]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-123::obj-27::obj-18" : [ "toggle[153]", "toggle", 0 ],
			"obj-22::obj-57::obj-123::obj-48" : [ "SendTo-TXT[144]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-123::obj-8" : [ "tab[4]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-34::obj-27::obj-18" : [ "toggle[156]", "toggle", 0 ],
			"obj-22::obj-57::obj-34::obj-48" : [ "SendTo-TXT[147]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-34::obj-8" : [ "tab[7]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-36::obj-27::obj-18" : [ "toggle[157]", "toggle", 0 ],
			"obj-22::obj-57::obj-36::obj-48" : [ "SendTo-TXT[148]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-36::obj-8" : [ "tab[8]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-40::obj-27::obj-18" : [ "toggle[158]", "toggle", 0 ],
			"obj-22::obj-57::obj-40::obj-48" : [ "SendTo-TXT[149]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-40::obj-8" : [ "tab[9]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-41::obj-27::obj-18" : [ "toggle[159]", "toggle", 0 ],
			"obj-22::obj-57::obj-41::obj-48" : [ "SendTo-TXT[150]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-41::obj-8" : [ "tab[10]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-42::obj-27::obj-18" : [ "toggle[160]", "toggle", 0 ],
			"obj-22::obj-57::obj-42::obj-48" : [ "SendTo-TXT[151]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-42::obj-8" : [ "tab[11]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-43::obj-27::obj-18" : [ "toggle[161]", "toggle", 0 ],
			"obj-22::obj-57::obj-43::obj-48" : [ "SendTo-TXT[152]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-43::obj-8" : [ "tab[12]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-44::obj-27::obj-18" : [ "toggle[162]", "toggle", 0 ],
			"obj-22::obj-57::obj-44::obj-48" : [ "SendTo-TXT[153]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-44::obj-8" : [ "tab[13]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-45::obj-27::obj-18" : [ "toggle[163]", "toggle", 0 ],
			"obj-22::obj-57::obj-45::obj-48" : [ "SendTo-TXT[154]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-45::obj-8" : [ "tab[14]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-46::obj-27::obj-18" : [ "toggle[164]", "toggle", 0 ],
			"obj-22::obj-57::obj-46::obj-48" : [ "SendTo-TXT[155]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-46::obj-8" : [ "tab[15]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-47::obj-27::obj-18" : [ "toggle[165]", "toggle", 0 ],
			"obj-22::obj-57::obj-47::obj-48" : [ "SendTo-TXT[156]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-47::obj-8" : [ "tab[289]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-48::obj-27::obj-18" : [ "toggle[166]", "toggle", 0 ],
			"obj-22::obj-57::obj-48::obj-48" : [ "SendTo-TXT[157]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-48::obj-8" : [ "tab[16]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-49::obj-27::obj-18" : [ "toggle[167]", "toggle", 0 ],
			"obj-22::obj-57::obj-49::obj-48" : [ "SendTo-TXT[158]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-49::obj-8" : [ "tab[17]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-50::obj-27::obj-18" : [ "toggle[168]", "toggle", 0 ],
			"obj-22::obj-57::obj-50::obj-48" : [ "SendTo-TXT[159]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-50::obj-8" : [ "tab[18]", "tab[1]", 0 ],
			"obj-22::obj-57::obj-74::obj-27::obj-18" : [ "toggle[154]", "toggle", 0 ],
			"obj-22::obj-57::obj-74::obj-48" : [ "SendTo-TXT[145]", "SendTo-TXT", 0 ],
			"obj-22::obj-57::obj-74::obj-8" : [ "tab[5]", "tab[1]", 0 ],
			"obj-38::obj-45" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-57::obj-2::obj-122" : [ "number[3]", "number[1]", 0 ],
			"obj-57::obj-2::obj-16" : [ "number[4]", "number[1]", 0 ],
			"obj-57::obj-86::obj-107::obj-27::obj-18" : [ "toggle[186]", "toggle", 0 ],
			"obj-57::obj-86::obj-107::obj-8" : [ "tab[346]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-123::obj-27::obj-18" : [ "toggle[184]", "toggle", 0 ],
			"obj-57::obj-86::obj-123::obj-8" : [ "tab[344]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-34::obj-27::obj-18" : [ "toggle[187]", "toggle", 0 ],
			"obj-57::obj-86::obj-34::obj-8" : [ "tab[347]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-37::obj-27::obj-18" : [ "toggle[192]", "toggle", 0 ],
			"obj-57::obj-86::obj-37::obj-8" : [ "tab[352]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-38::obj-27::obj-18" : [ "toggle[191]", "toggle", 0 ],
			"obj-57::obj-86::obj-38::obj-8" : [ "tab[351]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-39::obj-27::obj-18" : [ "toggle[188]", "toggle", 0 ],
			"obj-57::obj-86::obj-39::obj-8" : [ "tab[348]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-40::obj-27::obj-18" : [ "toggle[189]", "toggle", 0 ],
			"obj-57::obj-86::obj-40::obj-8" : [ "tab[349]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-41::obj-27::obj-18" : [ "toggle[190]", "toggle", 0 ],
			"obj-57::obj-86::obj-41::obj-8" : [ "tab[350]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-44::obj-27::obj-18" : [ "toggle[193]", "toggle", 0 ],
			"obj-57::obj-86::obj-44::obj-8" : [ "tab[353]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-45::obj-27::obj-18" : [ "toggle[194]", "toggle", 0 ],
			"obj-57::obj-86::obj-45::obj-8" : [ "tab[354]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-46::obj-27::obj-18" : [ "toggle[195]", "toggle", 0 ],
			"obj-57::obj-86::obj-46::obj-8" : [ "tab[355]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-47::obj-27::obj-18" : [ "toggle[196]", "toggle", 0 ],
			"obj-57::obj-86::obj-47::obj-8" : [ "tab[356]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-48::obj-27::obj-18" : [ "toggle[197]", "toggle", 0 ],
			"obj-57::obj-86::obj-48::obj-8" : [ "tab[357]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-49::obj-27::obj-18" : [ "toggle[198]", "toggle", 0 ],
			"obj-57::obj-86::obj-49::obj-8" : [ "tab[358]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-50::obj-27::obj-18" : [ "toggle[199]", "toggle", 0 ],
			"obj-57::obj-86::obj-50::obj-8" : [ "tab[359]", "tab[1]", 0 ],
			"obj-57::obj-86::obj-74::obj-27::obj-18" : [ "toggle[185]", "toggle", 0 ],
			"obj-57::obj-86::obj-74::obj-8" : [ "tab[345]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-107::obj-33" : [ "tab[330]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-123::obj-33" : [ "tab[328]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-34::obj-33" : [ "tab[331]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-36::obj-33" : [ "tab[332]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-40::obj-33" : [ "tab[333]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-41::obj-33" : [ "tab[334]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-42::obj-33" : [ "tab[335]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-43::obj-33" : [ "tab[336]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-44::obj-33" : [ "tab[337]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-45::obj-33" : [ "tab[338]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-46::obj-33" : [ "tab[339]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-47::obj-33" : [ "tab[340]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-48::obj-33" : [ "tab[341]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-49::obj-33" : [ "tab[342]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-50::obj-33" : [ "tab[343]", "tab[1]", 0 ],
			"obj-57::obj-93::obj-74::obj-33" : [ "tab[329]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-107::obj-33" : [ "tab[314]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-123::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-36::obj-33" : [ "tab[316]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-40::obj-33" : [ "tab[317]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-41::obj-33" : [ "tab[318]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-44::obj-33" : [ "tab[321]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-45::obj-33" : [ "tab[322]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-46::obj-33" : [ "tab[323]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-47::obj-33" : [ "tab[324]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-48::obj-33" : [ "tab[325]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-49::obj-33" : [ "tab[326]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-50::obj-33" : [ "tab[327]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-6::obj-33" : [ "tab[320]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-74::obj-33" : [ "tab[313]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-8::obj-33" : [ "tab[319]", "tab[1]", 0 ],
			"obj-60::obj-175::obj-9::obj-33" : [ "tab[315]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-107::obj-33" : [ "tab[292]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-123::obj-33" : [ "tab[290]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-34::obj-33" : [ "tab[293]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-36::obj-33" : [ "tab[294]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-40::obj-33" : [ "tab[295]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-41::obj-33" : [ "tab[296]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-42::obj-33" : [ "tab[297]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-43::obj-33" : [ "tab[298]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-44::obj-33" : [ "tab[299]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-45::obj-33" : [ "tab[300]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-46::obj-33" : [ "tab[301]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-47::obj-33" : [ "tab[302]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-48::obj-33" : [ "tab[303]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-49::obj-33" : [ "tab[304]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-50::obj-33" : [ "tab[305]", "tab[1]", 0 ],
			"obj-60::obj-40::obj-74::obj-33" : [ "tab[291]", "tab[1]", 0 ],
			"obj-60::obj-45::obj-122" : [ "number[2]", "number[1]", 0 ],
			"obj-60::obj-45::obj-16" : [ "number[1]", "number[1]", 0 ],
			"obj-60::obj-86::obj-107::obj-27::obj-18" : [ "toggle[170]", "toggle", 0 ],
			"obj-60::obj-86::obj-107::obj-8" : [ "tab[307]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-123::obj-27::obj-18" : [ "toggle[22]", "toggle", 0 ],
			"obj-60::obj-86::obj-123::obj-8" : [ "tab[150]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-34::obj-27::obj-18" : [ "toggle[171]", "toggle", 0 ],
			"obj-60::obj-86::obj-34::obj-8" : [ "tab[378]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-37::obj-27::obj-18" : [ "toggle[176]", "toggle", 0 ],
			"obj-60::obj-86::obj-37::obj-8" : [ "tab[383]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-38::obj-27::obj-18" : [ "toggle[175]", "toggle", 0 ],
			"obj-60::obj-86::obj-38::obj-8" : [ "tab[382]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-39::obj-27::obj-18" : [ "toggle[172]", "toggle", 0 ],
			"obj-60::obj-86::obj-39::obj-8" : [ "tab[379]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-40::obj-27::obj-18" : [ "toggle[173]", "toggle", 0 ],
			"obj-60::obj-86::obj-40::obj-8" : [ "tab[380]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-41::obj-27::obj-18" : [ "toggle[174]", "toggle", 0 ],
			"obj-60::obj-86::obj-41::obj-8" : [ "tab[381]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-44::obj-27::obj-18" : [ "toggle[177]", "toggle", 0 ],
			"obj-60::obj-86::obj-44::obj-8" : [ "tab[384]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-45::obj-27::obj-18" : [ "toggle[178]", "toggle", 0 ],
			"obj-60::obj-86::obj-45::obj-8" : [ "tab[385]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-46::obj-27::obj-18" : [ "toggle[179]", "toggle", 0 ],
			"obj-60::obj-86::obj-46::obj-8" : [ "tab[308]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-47::obj-27::obj-18" : [ "toggle[180]", "toggle", 0 ],
			"obj-60::obj-86::obj-47::obj-8" : [ "tab[309]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-48::obj-27::obj-18" : [ "toggle[181]", "toggle", 0 ],
			"obj-60::obj-86::obj-48::obj-8" : [ "tab[310]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-49::obj-27::obj-18" : [ "toggle[182]", "toggle", 0 ],
			"obj-60::obj-86::obj-49::obj-8" : [ "tab[311]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-50::obj-27::obj-18" : [ "toggle[183]", "toggle", 0 ],
			"obj-60::obj-86::obj-50::obj-8" : [ "tab[312]", "tab[1]", 0 ],
			"obj-60::obj-86::obj-74::obj-27::obj-18" : [ "toggle[169]", "toggle", 0 ],
			"obj-60::obj-86::obj-74::obj-8" : [ "tab[306]", "tab[1]", 0 ],
			"obj-67" : [ "live.gain~", "leloup~", 0 ],
			"obj-68" : [ "live.gain~[3]", "lifting~", 0 ],
			"obj-69" : [ "live.gain~[4]", "AT-perf~", 0 ],
			"obj-83::obj-107::obj-33" : [ "tab[37]", "tab[1]", 0 ],
			"obj-83::obj-123::obj-33" : [ "tab[35]", "tab[1]", 0 ],
			"obj-83::obj-34::obj-33" : [ "tab[38]", "tab[1]", 0 ],
			"obj-83::obj-36::obj-33" : [ "tab[39]", "tab[1]", 0 ],
			"obj-83::obj-40::obj-33" : [ "tab[40]", "tab[1]", 0 ],
			"obj-83::obj-41::obj-33" : [ "tab[41]", "tab[1]", 0 ],
			"obj-83::obj-42::obj-33" : [ "tab[42]", "tab[1]", 0 ],
			"obj-83::obj-43::obj-33" : [ "tab[43]", "tab[1]", 0 ],
			"obj-83::obj-44::obj-33" : [ "tab[44]", "tab[1]", 0 ],
			"obj-83::obj-45::obj-33" : [ "tab[45]", "tab[1]", 0 ],
			"obj-83::obj-46::obj-33" : [ "tab[46]", "tab[1]", 0 ],
			"obj-83::obj-47::obj-33" : [ "tab[47]", "tab[1]", 0 ],
			"obj-83::obj-48::obj-33" : [ "tab[48]", "tab[1]", 0 ],
			"obj-83::obj-49::obj-33" : [ "tab[49]", "tab[1]", 0 ],
			"obj-83::obj-50::obj-33" : [ "tab[50]", "tab[1]", 0 ],
			"obj-83::obj-74::obj-33" : [ "tab[36]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "AT-perf-a.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-11-24_Athenor/AT/AT-perf/preset",
				"patcherrelativepath" : "./AT/AT-perf/preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "AT-perf.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-11-24_Athenor/AT/AT-perf",
				"patcherrelativepath" : "./AT/AT-perf",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "RD-perf-a.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-11-24_Athenor/RD/preset",
				"patcherrelativepath" : "./RD/preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "RD-perf.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-11-24_Athenor/RD",
				"patcherrelativepath" : "./RD",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "athenor.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-11-24_Athenor",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2max.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2xbend.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/output/2xbend",
				"patcherrelativepath" : "../../max/output/2xbend",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.average.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/average",
				"patcherrelativepath" : "../../max/control_processing/average",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/crosspatch~",
				"patcherrelativepath" : "../../max/utilities/crosspatch~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.cue.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/cue",
				"patcherrelativepath" : "../../max/utilities/cue",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.dac~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/dac~",
				"patcherrelativepath" : "../../max/utilities/dac~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../../max/input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.granulator~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/sound_synthesis/granulator~",
				"patcherrelativepath" : "../../max/sound_synthesis/granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.latch.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/latch",
				"patcherrelativepath" : "../../max/control_processing/latch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../max/utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.lores~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/lores~",
				"patcherrelativepath" : "../../max/utilities/lores~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.onepole~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.receive.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/receive",
				"patcherrelativepath" : "../../max/utilities/receive",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.receive~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/receive~",
				"patcherrelativepath" : "../../max/utilities/receive~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.recorder.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/recorder",
				"patcherrelativepath" : "../../max/utilities/recorder",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../max/feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.schmitt.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/schmitt",
				"patcherrelativepath" : "../../max/control_processing/schmitt",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.send.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/send",
				"patcherrelativepath" : "../../max/utilities/send",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.send~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/send~",
				"patcherrelativepath" : "../../max/utilities/send~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.smooth.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/smooth",
				"patcherrelativepath" : "../../max/control_processing/smooth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.speedlim.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/speedlim",
				"patcherrelativepath" : "../../max/control_processing/speedlim",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.steer.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/steer",
				"patcherrelativepath" : "../../max/control_processing/steer",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "leloup.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-11-24_Athenor/AT/leloup/preset",
				"patcherrelativepath" : "./AT/leloup/preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "leloup.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-11-24_Athenor/AT/leloup",
				"patcherrelativepath" : "./AT/leloup",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "lifting.json",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-11-24_Athenor/AT/lifting/preset",
				"patcherrelativepath" : "./AT/lifting/preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "lifting.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/events/2023-11-24_Athenor/AT/lifting",
				"patcherrelativepath" : "./AT/lifting",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/source/js",
				"patcherrelativepath" : "../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2max.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.2xbend.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/output/2xbend",
				"patcherrelativepath" : "../../max/output/2xbend",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.gran~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/sound_synthesis/granulator~",
				"patcherrelativepath" : "../../max/sound_synthesis/granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.onepole~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
