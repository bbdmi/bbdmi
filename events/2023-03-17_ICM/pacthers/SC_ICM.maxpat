{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 6,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ -366.0, -985.0, 1836.0, 943.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1026.666697263717651, 860.000025629997253, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 845.833332657814026, 567.36252490234142, 93.0, 20.0 ],
					"text" : "BBDMI GITLAB"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 151.800001800060272, 72.215223937748675, 159.0, 22.0 ],
					"text" : "OSC-route /EEGsynth/alpha"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 151.800001800060272, 41.096590912342194, 97.0, 22.0 ],
					"text" : "udpreceive 8000"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-34",
					"items" : [ "off", ",", "Unicorn", ",", "Mentalab" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 45.20000159740448, 72.215223937748675, 100.0, 23.0 ],
					"pattrmode" : 1,
					"presentation" : 1,
					"presentation_rect" : [ -59.000005424022675, 67.588183790923239, 100.0, 23.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 45.20000159740448, 130.615225499390363, 52.0, 22.0 ],
					"text" : "switch 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 980.199999928474426, 1212.432955164195391, 50.0, 22.0 ],
					"text" : "-127."
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-52",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 558.199999928474426, 584.000000953674316, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 590.399928778409958, 538.000000953674316, 39.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 863.333327531814575, 39.0, 20.0 ],
					"text" : "on-off"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 558.199999928474426, 523.000000953674316, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 836.783261507749557, 885.333327531814575, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 558.199999928474426, 560.000000953674316, 32.0, 22.0 ],
					"text" : "gate"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 601.149928778409958, 1346.000000953674316, 59.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 897.333332657814026, 863.333327531814575, 59.0, 20.0 ],
					"text" : "MODELS"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 783.199999928474426, 554.000000953674316, 54.0, 22.0 ],
					"text" : "deferlow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 783.199999928474426, 524.434772235199034, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 601.149928778409958, 524.434772235199034, 135.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 656.230281195762245, 135.0, 20.0 ],
					"text" : "ALPHA - State Detector"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 690.149928778409958, 778.000000953674316, 79.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 887.333332657814026, 834.480281195762245, 79.0, 20.0 ],
					"text" : "model trigger"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 712.149928778409958, 555.000000953674316, 35.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 881.283261507749557, 701.480281195762245, 35.0, 20.0 ],
					"text" : "reset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 682.196765080094451, 1476.562007306151827, 128.0, 22.0 ],
					"text" : "read modelAbove.json"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 539.196765080094451, 1476.562007306151827, 132.0, 22.0 ],
					"text" : "read modelDefault.json"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 509.199999928474426, 1374.215047898200737, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 509.199999928474426, 1346.000000953674316, 87.0, 22.0 ],
					"text" : "r chooseModel"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 569.250071078538895, 1434.430094842727158, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 942.333332657814026, 885.333327531814575, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 539.250071078538895, 1434.430094842727158, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 914.833332657814026, 885.333327531814575, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 509.250071078538895, 1434.430094842727158, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 887.833332657814026, 885.333327531814575, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 509.199999928474426, 1402.430094842727158, 109.150213450193291, 22.0 ],
					"text" : "sel -1 0 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 601.149928778409958, 812.000000953674316, 89.0, 22.0 ],
					"text" : "s chooseModel"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "number",
					"maximum" : 1,
					"minimum" : -1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 601.149928778409958, 778.000000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 833.480281195762245, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 749.149928778409958, 665.000000953674316, 75.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 881.283261507749557, 737.480281195762245, 75.0, 20.0 ],
					"text" : "threshold (s)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 737.899928766691346, 633.750000953674316, 41.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 905.783261507749557, 772.230281195762245, 41.0, 20.0 ],
					"text" : "above"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 556.399928778409958, 633.750000953674316, 40.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 834.283261507749557, 772.230281195762245, 40.0, 20.0 ],
					"text" : "below"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 0.392156862745098, 0.392156862745098, 1.0 ],
					"id" : "obj-94",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 697.149928778409958, 664.000000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 736.480281195762245, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 352.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "", "int", "int" ],
									"patching_rect" : [ 114.0, 393.0, 48.0, 22.0 ],
									"text" : "change"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 114.0, 357.0, 29.5, 22.0 ],
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.0, 308.5, 29.5, 22.0 ],
									"text" : "0"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 196.0, 139.0, 29.5, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patching_rect" : [ 114.0, 139.0, 29.5, 22.0 ],
									"text" : "t i i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "int" ],
									"patching_rect" : [ 196.0, 189.5, 29.5, 22.0 ],
									"text" : "t b i"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 302.54295742225213, 214.5, 29.5, 22.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 302.492886272187661, 179.5, 34.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.050071150064468, 169.5, 29.5, 22.0 ],
									"text" : "-1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 50.0, 134.5, 34.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-52",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.0, 268.5, 184.0, 22.0 ],
									"text" : "if $i1 == 0 && $i2 == 0 then bang"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 196.0, 100.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 114.0, 100.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-84",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 114.000022150064524, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-86",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 196.000022150064524, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-88",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 114.0, 428.5, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-53", 0 ],
									"source" : [ "obj-1", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-45", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-52", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"source" : [ "obj-53", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-58", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"source" : [ "obj-63", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 1 ],
									"source" : [ "obj-66", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-52", 0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 601.149928778409958, 746.500000953674316, 101.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p condition"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 683.149928778409958, 553.000000953674316, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 682.277054909110348, 49.203226286651898, 49.203226286651898 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 683.149928778409958, 631.750000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 901.283261507749557, 794.230281195762245, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 683.149928778409958, 710.000000953674316, 33.0, 22.0 ],
					"text" : ">= 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 601.149928778409958, 631.750000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 829.283261507749557, 794.230281195762245, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 601.149928778409958, 710.000000953674316, 33.0, 22.0 ],
					"text" : ">= 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 396.196765080094451, 1476.562007306151827, 126.0, 22.0 ],
					"text" : "read modelBelow.json"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 628.463439717888946, 1402.430094842727158, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1226.199999928474426, 375.000000953674316, 31.0, 22.0 ],
					"text" : "* -1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 601.149928778409958, 604.000000953674316, 101.0, 22.0 ],
					"text" : "bbdmi.timethresh"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-300",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 113.366668283939362, 873.000000953674316, 71.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 455.6333367228508, 220.796771269557667, 71.0, 20.0 ],
					"text" : "To mapping"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-298",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 123.866668283939362, 898.000000953674316, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 466.1333367228508, 245.796771269557667, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-296",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 278.199999928474426, 1476.562007306151827, 54.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.666667699813843, 585.333327531814575, 54.0, 20.0 ],
					"text" : "ML input"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-292",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 106.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 310.113972431900152, 98.0, 22.0 ],
									"text" : "zmap 0. 30. 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-39",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 120.0, 277.313971943142064, 38.0, 20.0 ],
									"text" : "alpha"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-38",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 89.0, 207.0, 50.0, 22.0 ],
									"text" : "70"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-40",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 120.0, 177.0, 37.0, 22.0 ],
									"text" : "zl.len"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-41",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 140.0, 143.0, 22.0 ],
									"text" : "fromsymbol @separator \\,"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 277.313971943142064, 54.0, 22.0 ],
									"text" : "zl.nth 59"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-52",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 100.0, 163.0, 22.0 ],
									"text" : "mxj net.udp.recv @port 8888"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-291",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 392.113983000000019, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-291", 0 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 1 ],
									"source" : [ "obj-40", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 1,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-40", 0 ],
									"order" : 0,
									"source" : [ "obj-41", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-41", 0 ],
									"source" : [ "obj-52", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 61.600006604194618, 42.516643261910218, 84.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p fromUnicorn"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-289",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 311.642815050597619, 763.500000953674316, 80.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 928.917504192427259, 935.666694343090057, 80.0, 20.0 ],
					"text" : "EEG Joystick"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-288",
					"maxclass" : "newobj",
					"numinlets" : 10,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-193",
									"maxclass" : "newobj",
									"numinlets" : 10,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 6,
											"revision" : 5,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-55",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 137.0, 42.0, 22.0 ],
													"text" : "127. 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 270.25, 134.0, 75.0, 22.0 ],
													"text" : "127. 360000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 270.25, 107.0, 62.0, 22.0 ],
													"text" : "0. 360000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-3",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 50.0, 100.0, 29.5, 22.0 ],
													"text" : "0. 0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-109",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 389.583333611488342, 134.0, 75.0, 22.0 ],
													"text" : "127. 600000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-110",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 389.583333611488342, 107.0, 62.0, 22.0 ],
													"text" : "0. 600000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-107",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 180.25, 134.0, 75.0, 22.0 ],
													"text" : "127. 120000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-108",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 180.25, 107.0, 62.0, 22.0 ],
													"text" : "0. 120000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-85",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 104.25, 134.0, 69.0, 22.0 ],
													"text" : "127. 60000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-84",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 104.25, 107.0, 55.0, 22.0 ],
													"text" : "0. 60000"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-119",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 50.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-120",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 85.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-121",
													"index" : 3,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 120.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-122",
													"index" : 4,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 155.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-123",
													"index" : 5,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 190.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-124",
													"index" : 6,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 225.0, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-125",
													"index" : 7,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 270.25, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-126",
													"index" : 8,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 305.25, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-127",
													"index" : 9,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 389.583344000000011, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-128",
													"index" : 10,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 424.583344000000011, 39.999999231979331, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-129",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 209.407409999999999, 218.999999231979331, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-107", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-108", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-109", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-110", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-3", 0 ],
													"source" : [ "obj-119", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-55", 0 ],
													"source" : [ "obj-120", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-84", 0 ],
													"source" : [ "obj-121", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-85", 0 ],
													"source" : [ "obj-122", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-108", 0 ],
													"source" : [ "obj-123", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-107", 0 ],
													"source" : [ "obj-124", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-10", 0 ],
													"source" : [ "obj-125", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-9", 0 ],
													"source" : [ "obj-126", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-110", 0 ],
													"source" : [ "obj-127", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-109", 0 ],
													"source" : [ "obj-128", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-3", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-84", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-85", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-129", 0 ],
													"source" : [ "obj-9", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 50.0, 100.0, 113.5, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p lines"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-219",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 50.0, 167.198597180843308, 39.0, 22.0 ],
									"text" : "/ 127."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-237",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 50.0, 137.865264463424637, 67.0, 22.0 ],
									"text" : "line 20000."
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-277",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 49.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-278",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 84.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-279",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 119.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-280",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 154.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-281",
									"index" : 5,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 189.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-282",
									"index" : 6,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 224.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-283",
									"index" : 7,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 259.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-284",
									"index" : 8,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 294.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-285",
									"index" : 9,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 329.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-286",
									"index" : 10,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 364.999969667026562, 40.000013018795016, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-287",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 49.999969667026562, 249.198621018794938, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-237", 0 ],
									"midpoints" : [ 59.5, 123.715974676608994, 59.5, 123.715974676608994 ],
									"source" : [ "obj-193", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-287", 0 ],
									"source" : [ "obj-219", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-219", 0 ],
									"midpoints" : [ 59.5, 162.715974676608994, 59.5, 162.715974676608994 ],
									"source" : [ "obj-237", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 0 ],
									"source" : [ "obj-277", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 1 ],
									"source" : [ "obj-278", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 2 ],
									"source" : [ "obj-279", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 3 ],
									"source" : [ "obj-280", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 4 ],
									"source" : [ "obj-281", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 5 ],
									"source" : [ "obj-282", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 6 ],
									"source" : [ "obj-283", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 7 ],
									"source" : [ "obj-284", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 8 ],
									"source" : [ "obj-285", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-193", 9 ],
									"source" : [ "obj-286", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 231.899928778410072, 1349.430094842727158, 113.5, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p TimeLines"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-269",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 109.399928778410072, 1414.430094842727158, 70.0, 22.0 ],
					"text" : "loadmess 2"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-271",
					"items" : [ "off", ",", "Time", ",", "EEG" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 109.399928778410072, 1441.430094842727158, 100.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 179.466667652130127, 554.36252490234142, 100.0, 23.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-272",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 216.399928778410072, 1441.430094842727158, 50.0, 21.0 ],
					"text" : "switch 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-276",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 211.399928778410072, 1373.430094842727158, 81.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 188.966667652130127, 529.36252490234142, 81.0, 20.0 ],
					"text" : "Time/Joystick"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-261",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 16.866668283939362, 775.500000953674316, 70.0, 22.0 ],
					"text" : "loadmess 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 139.366668283939362, 730.817384130241408, 93.0, 22.0 ],
					"text" : "r EEGcalibrated"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 45.20000159740448, 490.752155411766125, 95.0, 22.0 ],
					"text" : "s EEGcalibrated"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-252",
					"items" : [ "off", ",", "Calibrated", ",", "Joystick" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 16.866668283939362, 802.500000953674316, 100.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 441.1333367228508, 198.999997556209564, 100.0, 23.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595186999999999,
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 123.866668283939362, 802.500000953674316, 50.0, 21.0 ],
					"text" : "switch 2"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-250",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 45.20000159740448, 340.701420253994002, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-248",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-155",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 395.833364963531494, 153.706809508800461, 80.0, 22.0 ],
									"text" : "loadmess 0.1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-140",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 281.333364963531494, 153.706809508800461, 107.0, 22.0 ],
									"text" : "loadmess 0.00632"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-139",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 186.333364963531494, 153.706809508800461, 77.0, 22.0 ],
									"text" : "loadmess 10"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-88",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 100.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-170",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 88.0, 153.706809508800461, 80.0, 22.0 ],
									"text" : "loadmess 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-53",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 88.0, 182.512198847053241, 45.0, 20.0 ],
									"text" : "restVal"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-67",
									"maxclass" : "flonum",
									"maximum" : 1.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 88.0, 210.212208228824352, 80.0, 23.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-68",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 88.0, 242.942132788417666, 48.0, 19.0 ],
									"text" : "restVal $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-69",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 381.833364963531494, 182.512198847053241, 57.0, 20.0 ],
									"text" : "restDelta"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-71",
									"maxclass" : "flonum",
									"maximum" : 0.5,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 370.333364963531494, 210.212208228824352, 80.0, 23.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-72",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 370.333364963531494, 242.942132788417666, 93.0, 19.0 ],
									"text" : "restDelta $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 281.333364963531494, 182.512198847053241, 92.0, 20.0 ],
									"text" : "derivativeDepth"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-75",
									"maxclass" : "flonum",
									"maximum" : 100.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 281.333364963531494, 210.212208228824352, 80.0, 23.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-76",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 281.333364963531494, 245.43299627304043, 84.0, 19.0 ],
									"text" : "derivativeDepth $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 186.333364963531494, 182.512198847053241, 19.0, 20.0 ],
									"text" : "dt"
								}

							}
, 							{
								"box" : 								{
									"fontsize" : 13.0,
									"format" : 6,
									"id" : "obj-89",
									"maxclass" : "flonum",
									"maximum" : 1000.0,
									"minimum" : 1.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 186.333364963531494, 210.212208228824352, 80.0, 23.0 ],
									"triscale" : 0.9
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"id" : "obj-90",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 186.333364963531494, 242.942132788417666, 29.5, 19.0 ],
									"text" : "dt $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-60",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 88.0, 331.31220043253677, 81.0, 22.0 ],
									"text" : "snapshot~ 10"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-93",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 50.0, 129.706809508800347, 31.0, 22.0 ],
									"text" : "sig~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-449",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "list" ],
									"patching_rect" : [ 88.0, 303.71220995735905, 146.0, 22.0 ],
									"text" : "bbdmi_joystick_loop_ui1~"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-246",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 49.999990963531445, 40.000022508800498, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-247",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 87.999990963531445, 413.312156508800399, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-89", 0 ],
									"source" : [ "obj-139", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-75", 0 ],
									"source" : [ "obj-140", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-71", 0 ],
									"source" : [ "obj-155", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"source" : [ "obj-170", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-88", 0 ],
									"source" : [ "obj-246", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-60", 0 ],
									"source" : [ "obj-449", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-247", 0 ],
									"source" : [ "obj-60", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"hidden" : 1,
									"source" : [ "obj-67", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"hidden" : 1,
									"source" : [ "obj-68", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"hidden" : 1,
									"source" : [ "obj-71", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"hidden" : 1,
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"hidden" : 1,
									"source" : [ "obj-75", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"hidden" : 1,
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"source" : [ "obj-88", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"hidden" : 1,
									"source" : [ "obj-89", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"hidden" : 1,
									"source" : [ "obj-90", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-449", 0 ],
									"source" : [ "obj-93", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 154.866668283939362, 763.500000953674316, 87.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p AlainJoystick"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-241",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 176.700001597404366, 150.000000417232513, 64.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 157.0, 7.0, 79.0, 20.0 ],
					"text" : "EEG Input"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-197",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 470.507381111383552, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 582.574119985103607, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-199",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 470.507381111383552, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 582.574119985103607, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 406.399928778410072, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 518.466667652130127, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-202",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 406.399928778410072, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 518.466667652130127, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-203",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 343.399928778410072, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 455.466667652130127, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-205",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 343.399928778410072, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 455.466667652130127, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-206",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 283.399928778410072, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 395.466667652130127, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-208",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 283.399928778410072, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 395.466667652130127, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-209",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 231.899928778410072, 1301.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 343.966667652130127, 567.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-210",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 206.649928778410072, 1303.16344682393219, 19.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 318.716667652130127, 569.36252490234142, 19.0, 20.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-211",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 206.649928778410072, 1274.16344682393219, 19.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 318.716667652130127, 540.36252490234142, 19.0, 20.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-214",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 231.899928778410072, 1272.16344682393219, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 343.966667652130127, 538.36252490234142, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-220",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 221.899928778410072, 1243.16344682393219, 44.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 333.966667652130127, 509.36252490234142, 44.0, 20.0 ],
					"text" : "instant"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-233",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 449.007381111383552, 1243.16344682393219, 67.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 561.074119985103607, 509.36252490234142, 67.0, 20.0 ],
					"text" : "10 minutes"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-234",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 387.899928778410072, 1243.16344682393219, 61.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 499.966667652130127, 509.36252490234142, 61.0, 20.0 ],
					"text" : "3 minutes"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 324.899928778410072, 1243.16344682393219, 61.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 436.966667652130127, 509.36252490234142, 61.0, 20.0 ],
					"text" : "2 minutes"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-236",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 267.899928778410072, 1243.16344682393219, 55.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 379.966667652130127, 509.36252490234142, 55.0, 20.0 ],
					"text" : "1 minute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-184",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1476.280934866304733, 105.698582404373155, 77.0, 22.0 ],
					"text" : "r pluckGuitar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-172",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1387.280934866304733, 315.698582404373155, 40.0, 22.0 ],
					"text" : "*~ 0.5"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-169",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 216.399928778410072, 1476.562007306151827, 50.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.666667699813843, 607.333327531814575, 50.0, 22.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 247.399928778410072, 1402.430094842727158, 32.0, 22.0 ],
					"text" : "r ML"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"linecount" : 18,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1681.866655945777893, 828.000000953674316, 214.0, 263.0 ],
					"text" : ";\rinstr transpout 12.079097;\rinstr feedback 0.803479;\rinstr gain 1.;\rinstr variability 0.057971;\rinstr transpgrain 2.;\rinstr modfreqmod 0.138644;\rinstr modmorph 1.25175;\rinstr modfreq 6303.972739;\rinstr modfactor 0.903244;\rinstr spacing 668;\rinstr grainoffset 468;\rinstr grainsize 688;\rinstr maxdelay 1626.286065;\rinstr lpffreq 17798.063684;\rinstr hpffreq 676.093274;\rinstr grainenvmorph 0.035929;\rinstr indexdistr 6;\r"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1496.199999928474426, 1110.000000953674316, 78.59999942779541, 97.800000011920929 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-158",
					"linecount" : 18,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2136.199999928474426, 828.000000953674316, 214.0, 263.0 ],
					"text" : ";\rinstr transpout -3.659462;\rinstr feedback 0.378896;\rinstr gain 1.;\rinstr variability 0.971125;\rinstr transpgrain 1.547342;\rinstr modfreqmod 0.229642;\rinstr modmorph 1.739886;\rinstr modfreq 886.140406;\rinstr modfactor 0.074165;\rinstr spacing 26;\rinstr grainoffset 390;\rinstr grainsize 80;\rinstr maxdelay 1908.8943;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.97429;\rinstr indexdistr 5;\r"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1405.614268199637991, 1079.000000953674316, 53.0, 22.0 ],
					"text" : "mc.*~ 2."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-135",
					"linecount" : 18,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1905.533340096473694, 828.000000953674316, 214.0, 263.0 ],
					"text" : ";\rinstr transpout -0.935548;\rinstr feedback 0.378165;\rinstr gain 1.;\rinstr variability 0.715379;\rinstr transpgrain 24.248604;\rinstr modfreqmod 0.303656;\rinstr modmorph 2.406309;\rinstr modfreq 10260.421883;\rinstr modfactor 0.117533;\rinstr spacing 773;\rinstr grainoffset 842;\rinstr grainsize 383;\rinstr maxdelay 490.;\rinstr lpffreq 19143.983002;\rinstr hpffreq 1011.509643;\rinstr grainenvmorph 0.604853;\rinstr indexdistr 6;\r"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"linecount" : 18,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1676.199999928474426, 1106.200713748218277, 214.0, 263.0 ],
					"text" : ";\rinstr transpout -32.404112;\rinstr feedback 0.77;\rinstr gain 1.;\rinstr variability 0.706869;\rinstr transpgrain -22.594593;\rinstr modfreqmod 0.732305;\rinstr modmorph 0.641584;\rinstr modfreq 5449.36574;\rinstr modfactor 0.163719;\rinstr spacing 891;\rinstr grainoffset 356;\rinstr grainsize 270;\rinstr maxdelay 9121.055892;\rinstr lpffreq 13283.696337;\rinstr hpffreq 1552.110202;\rinstr grainenvmorph 0.466103;\rinstr indexdistr 12;\r"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "@time", 15 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-59",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.calibrate.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 45.20000159740448, 373.018803430561093, 198.0, 113.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 385.000001668930054, 51.698581450698839, 198.0, 113.0 ],
					"varname" : "bbdmi.calibrate[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 45.20000159740448, 524.434772235199034, 352.0, 159.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 595.163439795374984, 32.5, 352.0, 159.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 212.642815050597619, 790.000000953674316, 278.0, 186.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 794.417504192427259, 961.166694343090057, 349.0, 158.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 51.800006949901558, 20.511813967942118, 103.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -59.000005424022675, 40.400000751018524, 103.0, 20.0 ],
					"text" : "Unicorn/Mentalab"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 45.20000159740448, 182.400001704692841, 320.0, 133.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 49.0, 41.698581450698839, 320.0, 133.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"setstyle" : 3
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-240",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1689.280934866304733, 98.817384130241408, 72.0, 22.0 ],
					"text" : "r noteGuitar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-239",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1313.199999928474426, 178.698582404373155, 69.0, 22.0 ],
					"text" : "r rateGuitar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1038.199999928474426, 1212.432955164195391, 48.0, 22.0 ],
					"text" : "gain $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1038.199999928474426, 1172.432955164195391, 109.0, 22.0 ],
					"text" : "zmap 0 127 -127 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-198",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1038.199999928474426, 1139.432955164195391, 57.0, 22.0 ],
					"text" : "ctlin 14 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-169",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 20.0, 215.0, 106.0, 22.0 ],
									"text" : "loadmess speed 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "number",
									"maximum" : 1000,
									"minimum" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 175.428571428571502, 81.999997322116826, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"id" : "obj-80",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 227.428571428571502, 81.999997322116826, 63.0, 20.0 ],
									"text" : "(msec)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 175.428571428571502, 40.999997322116826, 77.0, 22.0 ],
									"text" : "loadmess 20"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-84",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 175.428571428571502, 116.999997322116826, 79.0, 22.0 ],
									"text" : "returntime $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 134.0, 273.333328664302826, 118.428571428571502, 22.0 ],
									"text" : "mc.pack~ 7"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 14.0,
									"id" : "obj-38",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 7,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 134.000000000000114, 215.0, 135.0, 24.0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0,
										"parameter_mappable" : 0
									}
,
									"text" : "abc_2d_encoder3~",
									"textcolor" : [ 0.968627450980392, 0.968627450980392, 0.968627450980392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 133.999992716514612, 36.999997322116826, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-26",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 133.999992716514612, 355.333317322116841, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-169", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 6 ],
									"source" : [ "obj-38", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 5 ],
									"source" : [ "obj-38", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 4 ],
									"source" : [ "obj-38", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 3 ],
									"source" : [ "obj-38", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 2 ],
									"source" : [ "obj-38", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"source" : [ "obj-38", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1405.614268199637991, 463.544983531001776, 67.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p encoding"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 102.866668283939362, 703.817384130241408, 116.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 433.1333367228508, 175.317380732776655, 116.0, 20.0 ],
					"text" : "Calibrated / Joystick"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 807.630106427861392, 956.000000953674316, 51.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 718.1333367228508, 300.999997556209564, 51.0, 20.0 ],
					"text" : "Outputs"
				}

			}
, 			{
				"box" : 				{
					"candycane" : 10,
					"contdata" : 1,
					"id" : "obj-35",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 744.642815050597619, 986.931927614925371, 198.0, 233.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 657.609508481736611, 330.796771269557667, 191.0, 165.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"signed" : 1,
					"size" : 7,
					"slidercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"spacing" : 4,
					"thickness" : 4
				}

			}
, 			{
				"box" : 				{
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-36",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2max.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 539.99997495968455, 986.931927614925371, 197.533333301544189, 232.664822733401934 ],
					"presentation" : 1,
					"presentation_rect" : [ 452.966668390823543, 269.796771269557667, 197.533333301544189, 232.664822733401934 ],
					"varname" : "bbdmi.2max[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-12",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.scale.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 332.83330824971199, 986.931927614925371, 198.199999988079071, 232.664822733401934 ],
					"presentation" : 1,
					"presentation_rect" : [ 245.800001680850983, 269.796771269557667, 198.199999988079071, 232.664822733401934 ],
					"varname" : "bbdmi.scale[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 5 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-37",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.crosspatch.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 123.866668283939362, 986.931927614925371, 194.666666626930237, 225.864847052097048 ],
					"presentation" : 1,
					"presentation_rect" : [ 42.133335053920746, 269.796771269557667, 194.666666626930237, 225.864847052097048 ],
					"varname" : "bbdmi.crosspatch[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 176.700001597404366, 490.752155411766125, 89.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 726.66343979537487, -1.182616823432909, 89.0, 20.0 ],
					"text" : "EEG calibrated"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 479.36343972384941, 1538.434054512264311, 77.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 459.833342343568802, 643.767381090404569, 77.0, 20.0 ],
					"text" : "ML OUTPUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 59.399928778410072, 1538.434054512264311, 120.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 39.869831398129463, 643.767381090404569, 120.0, 20.0 ],
					"text" : "ML desired OUTPUT"
				}

			}
, 			{
				"box" : 				{
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-153",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 417.196765080094451, 1560.434054512264311, 197.199999988079071, 445.766659235954194 ],
					"presentation" : 1,
					"presentation_rect" : [ 397.666667699813843, 665.767381090404569, 197.199999988079071, 445.766659235954194 ],
					"varname" : "bbdmi.multislider[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-162",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.2max.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 815.630106427861392, 1560.434054512264311, 195.533333301544189, 445.766659235954194 ],
					"varname" : "bbdmi.2max",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 283.596765115857238, 1765.434054512264311, 67.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 264.06666773557663, 870.767381090404569, 67.0, 20.0 ],
					"text" : "OutputsML"
				}

			}
, 			{
				"box" : 				{
					"candycane" : 10,
					"contdata" : 1,
					"id" : "obj-213",
					"maxclass" : "multislider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"orientation" : 0,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 218.596765115857238, 1788.469228797733649, 197.0, 211.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 199.06666773557663, 893.802555375873908, 197.0, 211.0 ],
					"setminmax" : [ 0.0, 1.0 ],
					"signed" : 1,
					"size" : 16,
					"slidercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"spacing" : 4,
					"thickness" : 4
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-163",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.scale.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 608.463439717888946, 1560.434054512264311, 196.199999988079071, 445.766659235954194 ],
					"varname" : "bbdmi.scale",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"args" : [ 13 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-164",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.multislider.maxpat",
					"numinlets" : 2,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 22.203092440962905, 1560.668094111446408, 194.393672674894333, 442.766659235954194 ],
					"presentation" : 1,
					"presentation_rect" : [ 2.672995060682297, 666.001420689586666, 194.393672674894333, 442.766659235954194 ],
					"varname" : "bbdmi.multislider[3]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"annotation" : "",
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"hint" : "",
					"id" : "obj-165",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi.regress.maxpat",
					"numinlets" : 3,
					"numoutlets" : 2,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 218.596765115857238, 1560.434054512264311, 196.599999964237213, 199.799998104572296 ],
					"presentation" : 1,
					"presentation_rect" : [ 199.06666773557663, 665.767381090404569, 196.599999964237213, 199.799998104572296 ],
					"varname" : "bbdmi.regress[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.450980392156863, 0.898039215686275, 1.0, 1.0 ],
					"id" : "obj-174",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 803.599998259544464, 182.400001704692841, 134.400000095367432, 27.199998497962952 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 1.0, 0.596078431372549, 0.596078431372549, 1.0 ],
					"id" : "obj-176",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 803.599998259544464, 146.400001168251038, 134.400000095367432, 27.199998497962952 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.913725490196078, 1.0, 0.592156862745098, 1.0 ],
					"id" : "obj-178",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 803.599998259544464, 110.696590960025901, 134.400000095367432, 27.199998497962952 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.611764705882353, 1.0, 0.662745098039216, 1.0 ],
					"id" : "obj-180",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 803.599998259544464, 77.896590471267814, 134.400000095367432, 27.199998497962952 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 218.596765115857238, 1518.650716788055433, 47.0, 22.0 ],
					"text" : "pack f f"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1795.28093486630496, 136.698582404373155, 70.0, 22.0 ],
					"text" : "loadmess 6"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "number",
					"maximum" : 7,
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1795.28093486630496, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1795.28093486630496, 217.698582404373155, 83.0, 22.0 ],
					"text" : "stereoShift $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1689.280934866304733, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1689.280934866304733, 136.698582404373155, 77.0, 22.0 ],
					"text" : "loadmess 64"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1689.280934866304733, 217.698582404373155, 76.0, 22.0 ],
					"text" : "reference $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1592.280934866304733, 136.698582404373155, 70.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-13",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1592.280934866304733, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1592.280934866304733, 217.698582404373155, 48.0, 22.0 ],
					"text" : "gain $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1476.280934866304733, 136.698582404373155, 80.0, 22.0 ],
					"text" : "loadmess 0.5"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-8",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1476.280934866304733, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1476.280934866304733, 217.698582404373155, 96.0, 22.0 ],
					"text" : "pluckPosition $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1387.280934866304733, 136.698582404373155, 73.0, 22.0 ],
					"text" : "loadmess 1."
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-21",
					"maxclass" : "flonum",
					"maximum" : 50.0,
					"minimum" : 0.01,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1387.280934866304733, 178.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1387.280934866304733, 217.698582404373155, 55.0, 22.0 ],
					"text" : "factor $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"library_path0" : "/Users/alainbonardi/Documents/Max 8/Packages/faustgen/externals/msp/faustgen~.mxo/Contents/Resources/",
					"library_path1" : "/Users/davidfierro/Documents/Max 8/Packages/faustgen/externals/msp/faustgen~.mxo/Contents/Resources/",
					"machinecode" : "z/rt/gwAAAEAAAAAAQAAAAUAAAB4AQAAACAAAAAAAAAZAAAA6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFqAAAAAAAAmAEAAAAAAAABagAAAAAAAAcAAAAHAAAAAgAAAAAAAABfX3RleHQAAAAAAAAAAAAAX19URVhUAAAAAAAAAAAAAAAAAAAAAAAAqFIAAAAAAACYAQAAAgAAAKBrAAAKAAAAAAQAgAAAAAAAAAAAAAAAAF9fY29uc3QAAAAAAAAAAABfX1RFWFQAAAAAAAAAAAAAsFIAAAAAAABRFwAAAAAAAEhUAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMgAAABgAAAABAAAAAAAPAAAAAAAAAAAALgAAABAAAADwawAAIAAAAAIAAAAYAAAAEGwAAA4AAADwbAAA8BcAAAsAAABQAAAAAAAAAAMAAAADAAAABwAAAAoAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwANf1vxvuqn6ZwGp+F8CqfZXA6n0TwSp/XsFqfMDAKofNAC5AAABkQghglJ0AgiLfw4x+QDkAG9gipg9CEeMUglpnFJ/ain4eQIIi2DWuD0IbZxSaVZAkSnhEZE/AQD5egIIiyCBgDxoVkCRaWZAkSmhFpE/AQD5GGESkSCBgDxoZkCRaXpAkSlhG5E/AQD5GyEXkSCBgDxoekCRFeEbkWiaQJEIISCRAAGAPR8JAPlomkCRF6EgkWi2QJEI4SSRAAGAPR8JAPlotkCRFmElkX/+AKkcAACQnANA+QEKglKAAz/WAOQAb2A6hD1gNoQ9YDKEPWAuhD3gAxSqASGKUoADP9bgAxmqASGQUoADP9bgAxqqASGOUoADP9bgAxiqASGIUoADP9bgAxuqASGKUoADP9bgAxWqASGQUoADP9bgAxeqASGOUoADP9bgAxaqARaGUoADP9b9e0Wp9E9EqfZXQ6n4X0Kp+mdBqfxvxqjAA1/WCJhAkQgBIJEJZECRKYEWkQponFIKAAqLARgAuSAAYh4BEG4eAGhhHgsAztLrIOjyYgFnngJ4Yh7rxwWyC+vn8mABZ55ACGAeAMAB/EtCgtJLQqLyS0LC8kvw5/JgAWeeQAhgHgAcAP0gGGIeC76G0gsnofJLlMzyixjo8mMBZ54DCGMeA0gI/auZmdKLmbnyi5nB8ssI6PJjAWeeAwhjHgNMCP0hOGMeAVQI/Ys/ldIrXrryq0nM8gvs5/JhAWeeQQhhHgF4CP3ro5DSaz2q8gvXw/Ir/ufyYQFnngEIYR4BfAj9q1Ke0kuJofILG8byaxfo8mEBZ54BCGEeAQgx/csehdKL67HyS7je8gv+5/JhAWeeAQhhHgEYMf1LbYjSy3Cx8ku5z/IrF+jyYQFnngEIYR5BAQD9a4+C0sv1qPIrXM/yC/7n8mEBZ54BCGEeQREA/QsditKLuqHya97S8ssX6PJhAWeeAQhhHkGZOP1LCpfSy6Ow8ms9yvIL/ufyYQFnngEIYR5BqTj9ypWH0upLqfLKJt7yyhjo8kEBZ54BCGEeIQEA/ernArKqmZnyCv7n8kEBZ54BCGEeIREA/Qq+htIKJ6HySpTM8ooW6PJBAWeeAQhhHiGZKP0gqSj9CR2K0om6ofJp3tLyyRXo8iEBZ54BCGEeAQEA/SmFi9LpUbjyqR7F8gn+5/IhAWeeAQhhHgERAP0p5oXSKQK58qkB2/IpGOjyIQFnngEIYR4BmTj9iRSO0qlHofLpetTyCf7n8iEBZ54ACGEeAKk4/cADX9bAA1/WwANf1gAAAJAAAED5wANf1u87tm3tMwFt6ysCbekjA238bwSp+mcFqfhfBqn2Vwep9E8Iqf17Can/wy7R/AMBqvMDAKoItECRFYEkkQiYQJEWwR+RCHhAkRcBG5EIZECRGEEWkQhUQJEZgRGRCGacUhoACItoAED56HcF+WgEQPnocwX5DwBA/QCQYx7gKWAeGwAAkHsDQPkBEGQeYAM/1uBHBP1pQkL8YMJC/OjzALKo8vfyAQFnnujzALKo/ufyAgFnngAIQR8IAACQCAFA+QABP9YIQGAeKLKd0ojvp/KoxsvyCPzn8gABZ57o5wGySDOT8ij39/INAWeeADVAH2HCQfwrhUEfYglgHui4k9JIOrjySP/f8uj+9/IOAWee4qcF/UIobh7iowX9SAB4HmweQP0fAQBxCcGfGulHCLkJBQARPwEAcenXiBrpQwi5CQkAET8BAHEpwZ8a6T8IuQkNABE/AQBxKcGfGuk7CLkIEQARHwEAcQjBnxroNwi5KghpHkAJYB7gnwX9AChuHuCbBf0IAHgeHwEAcQnBnxrp2we5CQUAET8BAHHp14ga6ecHuQkJABE/AQBxKcGfGunrB7kJDQARPwEAcSnBnxrp7we5CBEAER8BAHEIwZ8a6PMHuWBKSP0BEG4eKRhoHgAIaR4UAACQlAJA+YACP9bgvwX9YE5I/eDrA/1gUkj94OcD/WCCSP3gywX9YH5I/eDHBf0AEGMe4ClgHgEQZB5gAz/W4EME/UiLjNJo57vy6KnR8gj95/IAAWeeADVAH2EJYB7hlwX9IShuHuGTBf0oAHgeHwEAcQnBnxrpMwi5CQUAET8BAHHp14ga6S8IuQkJABE/AQBxKcGfGukrCLkJDQARPwEAcSnBnxrpJwi5CBEAER8BAHEIwZ8a6CMIuUAJYB7gjwX9AChuHuCLBf0IAHgeHwEAcQnBnxrp9we5CQUAET8BAHHp14ga6fsHuQkJABE/AQBxKcGfGun/B7kJDQARPwEAcSnBnxrpAwi5CBEAER8BAHEIwZ8a6AcIuWAKcf0ACGkegAI/1uC3Bf1gGnH94MMF/QAQYR7gKWAeARBkHmADP9bgPwT9iIOP0ihMpvKoCsbySP3n8gABZ54ANUAfYQlgHuGHBf0hKG4e4YMF/SgAeB4fAQBxCcGfGukfCLkJBQARPwEAcenXiBrpGwi5CQkAET8BAHEpwZ8a6RcIuQkNABE/AQBxKcGfGukTCLkIEQARHwEAcQjBnxroDwi5QAlgHuB/Bf0AKG4e4HsF/QgAeB4fAQBxCcGfGukLCLkJBQARPwEAcenXiBrpkwi5CQkAET8BAHEpwZ8a6ZsIuQkNABE/AQBxKcGfGumjCLkIEQARHwEAcQjBnxroqwi5QAtA/QAIaR6AAj/W4K8F/frXBflAG0D94LsF/QAQYB7gKWAeARBkHmADP9bgOwT9iO2c0mg/tfIoXtryqPzn8gABZ54ANUAfYQlgHuFbBf0hKG4e4VcF/SgAeB4fAQBxCcGfGumzCLkJBQARPwEAcenXiBrpuwi5CQkAET8BAHEpwZ8a6cMIuQkNABE/AQBxKcGfGunLCLkIEQARHwEAcQjBnxro0wi5QAlgHuBfBf0AKG4e4GMF/QgAeB4fAQBxCcGfGunbCLkJBQARPwEAcenXiBrp4wi5CQkAET8BAHEpwZ8a6esIuQkNABE/AQBxKcGfGunzCLkIEQARHwEAcQjBnxro+wi5IAtA/QAIaR6AAj/W4FMF/SAbQP3gswX9AJBiHuApYB4BEGQeYAM/1uA3BP2oPJHSSP+m8kgow/Ko++fyAAFnngA1QB9hCWAe4UcF/SEobh7hQwX9KAB4Hh8BAHEJwZ8a6QMJuQkFABE/AQBx6deIGukLCbkJCQARPwEAcSnBnxrpEwm5CQ0AET8BAHEpwZ8a6RsJuQgRABEfAQBxGsGfGkAJYB7gPwX9AChuHuA7Bf0IAHgeHwEAcQnBnxrpIwm5CQUAET8BAHHp14ga6SsJuQkJABE/AQBxKcGfGukzCbkJDQARPwEAcSnBnxrpOwm5CBEAER8BAHEIwZ8a6NsKuQALQP0ACGkegAI/1uBPBf0AG0D94KsF/QEQZB7gQWAeYAM/1uAzBP0osp3SiO+n8qjGy/II/ufyAAFnngA1QB9hCWAe4TcF/SEobh7hLwX9KAB4Hh8BAHEJwZ8a6UMJuQkFABE/AQBx6deIGulLCbkJCQARPwEAcSnBnxrp0wq5CQ0AET8BAHEpwZ8a6VMJuQgRABEfAQBxCMGfGuhbCblACWAe4CsF/QAobh7gHwX9CAB4Hh8BAHEJwZ8a6csKuQkFABE/AQBx6deIGulzCbkJCQARPwEAcSnBnxrpawm5CQ0AET8BAHEpwZ8a6WMJuQgRABEfAQBxCMGfGuiDCbngCkD9AAhpHoACP9bgSwX94BpA/eDLBP0AEG4e4ClgHgEQZB5gAz/W4C8E/YjtnNJoP7XyKF7a8qj+5/IAAWeeADVAH2EJYB7hMwX9IShuHuEjBf0oAHgeHwEAcQnBnxrpewm5CQUAET8BAHHp14ga6ZsJuQkJABE/AQBxKcGfGumjCbkJDQARPwEAcSnBnxrpqwm5CBEAER8BAHEIwZ8a6LMJuUAJYB7gGwX9AChuHuAXBf0IAHgeHwEAcQnBnxrpuwm5CQUAET8BAHHp14ga6cMJuQkJABE/AQBxKcGfGunTCbkJDQARPwEAcSnBnxrpywm5CBEAER8BAHEIwZ8a6OMJucAKQP0ACGkegAI/1uAnBf3AGkD94McE/QAQYh7gKWAeARBkHmADP9bgKwT9CKeP0oh/pPLomdbySPzn8gABZ54ANUAfawlgHm0pbh6oAXgeHwEAcQnBnxrp2wm5CQUAET8BAHHp14ga6esJuQkJABE/AQBxKcGfGunzCbkJDQARPwEAcSnBnxrpAwq5CBEAER8BAHEIwZ8a6PsJuUgJYB4KKW4eSAF4Hh8BAHEJwZ8a6RMKuQkFABE/AQBx6deIGukLCrkJCQARPwEAcSnBnxrpIwq5CQ0AET8BAHEpwZ8a6RsKuQgRABEfAQBxG8GfGvUnBPmgCkD96c8F/QAIaR6AAj/WnwcAcSssAlTtAxyq7AMWquB/Av3g60P94edD/SAIYB7giwP96EdIuQABYh6AeWAeCAB4HuhHCLnoQ0i5AAFiHoB5YB4IAHge6EMIueg/SLkAAWIegHlgHggAeB7oPwi56DtIuQABYh6AeWAeCAB4Hug7CLnoN0i5AAFiHoB5YB4IAHge6DcIuejbR7kAAWIegHlgHggAeB7o8wO56OdHuQABYh6AeWAeCAB4HujrA7no60e5AAFiHoB5YB4IAHge6OMDuejvR7kAAWIegHlgHggAeB7o2wO56PNHuQABYh6AeWAeCAB4HujTA7noM0i5AAFiHoB5YB4IAHge6DMIuegvSLkAAWIegHlgHggAeB7oLwi56CtIuQABYh6AeWAeCAB4HugrCLnoJ0i5AAFiHoB5YB4IAHge6CcIuegjSLkAAWIegHlgHggAeB7oIwi56PdHuQABYh6AeWAeHgB4Huj7R7kAAWIegHlgHggAeB7owwO56P9HuQABYh6AeWAeCAB4Hui7A7noA0i5AAFiHoB5YB4IAHge6LMDuegHSLkAAWIegHlgHggAeB7oqwO56B9IuQABYh6AeWAeCAB4HugfCLnoG0i5AAFiHoB5YB4IAHge6BsIuegXSLkAAWIegHlgHggAeB7oFwi56BNIuQABYh6AeWAeCAB4HugTCLnoD0i5AAFiHoB5YB4IAHge6A8IuegLSLkAAWIegHlgHggAeB7oowO56JNIuQABYh6AeWAeCAB4HuiTCLnom0i5AAFiHoB5YB4IAHge6JsIueijSLkAAWIegHlgHggAeB7oowi56KtIuQABYh6AeWAeCAB4HuirCLnos0i5AAFiHoB5YB4IAHge6AsIuei7SLkAAWIegHlgHggAeB7oBwi56MNIuQABYh6AeWAeCAB4HugDCLnoy0i5AAFiHoB5YB4IAHge6P8HuejTSLkAAWIegHlgHggAeB7o+we56NtIuQABYh6AeWAeCAB4HujbCLno40i5AAFiHoB5YB4IAHge6OMIuejrSLkAAWIegHlgHggAeB7o6wi56PNIuQABYh6AeWAeCAB4HujzCLno+0i5AAFiHoB5YB4IAHge6PsIuegDSbkAAWIegHlgHggAeB7o9we56AtJuQABYh6AeWAeCAB4HujzB7noE0m5AAFiHoB5YB4IAHge6O8HuegbSbkAAWIegHlgHggAeB7o6we5QANiHoB5YB4IAHge6OcHuegjSbkAAWIegHlgHggAeB7oIwm56CtJuQABYh6AeWAeCAB4HugrCbnoM0m5AAFiHoB5YB4IAHge6DMJueg7SbkAAWIegHlgHggAeB7oOwm56NtKuQABYh6AeWAeCAB4HugbCbngo0X9AEBlHhkQch4hO2Ae4qdF/SEoYh7hhwP9GhBxHkE7YB4hKGIe4e8D/RsQcB5hO2AeIShiHuHrA/0cEH4egTtgHiEoYh7hPwP9QDhgHuBLA/3gm0X9AEBlHiE7YB7in0X9IShiHuFvBf1BO2AeIShiHuGnBf1hO2AeIShiHuGjBf2BO2AeIShiHuFHA/3oQ0m5AQFiHkA4YB7gOwP9gHlhHggAeB7oywe5iHmH0ujPrPLIE9TyyP7n8hIQbh7qQwP96r9F/UAaah4RAWeeAShxHiFIQB/hXwT9iHmH0ujPrPLIE9TyyP738gcBZ54BKGceIEhAH+CDA/3gk0X9AEBlHiE7YB7il0X9IShiHuF/A/1BO2AeIShiHuHjA/1hO2AeIShiHuHfA/2BO2AeIShiHuEfA/3oS0m5AQFiHkA4YB7gGwP9gHlhHggAeB7otwe56NNKuQABYh6AeWAeCAB4HuizB7ngi0X9AEBlHiE7YB7ij0X9IShiHuFrBf1BO2AeIShiHuGfBf1hO2AeIShiHuGbBf2BO2AeIShiHuETA/1AOGAe4A8D/ehTSbkAAWIegHlgHggAeB7orwe5/7dF/UAafx4BKHEeIUhAH+FXAv0BKGce6FtJuQIBYh4gSEAf4HsD/YB5Yh4IAHge6KsHuejLSrkAAWIegHlgHggAeB7oWwm54INF/QBAZR4hO2Ae4odF/SEoYh7hdwP9QTtgHiEoYh7h0wP9YTtgHiEoYh7hzwP9gTtgHiEoYh7hCwP9QDhgHuAHA/3ge0X9AEBlHiE7YB7if0X9IShiHuFnBf1BO2AeIShiHuGXBf1hO2AeIShiHuGTBf2BO2AeIShiHuH7Av3oc0m5AQFiHkA4YB7g/wL9gHlhHggAeB7oUwm56GtJuQABYh6AeWAeCAB4HuhLCbnoY0m5AAFiHoB5YB4IAHge6GMJue+vRf1AGm8eAShxHiFIQB/huwT9AShnHiBIQB/gcwP94FdF/QBAZR4hO2Ae4ltF/SEoYh7hbwP9QTtgHiEoYh7hywP9YTtgHiEoYh7hxwP9gTtgHiEoYh7h9wL96INJuQEBYh5AOGAe4LcE/YB5YR4IAHge6EMJueh7SbkAAWIegHlgHggAeB7ohwe54GNF/QBAZR4hO2Ae4l9F/SEoYh7hYwX9QTtgHiEoYh7hjwX9YTtgHiEoYh7hiwX9gTtgHiEoYh7h6wL9XThgHuibSbkAAWIegHlgHggAeB7ogwe55VNF/UAaZR4BKHEeIUhAH+HDBP0BKGce6KNJuQIBYh4gSEAf4GsD/YB5Yh4IAHge6H8HueirSbkAAWIegHlgHggAeB7oewe54ENF/QBAZR4hO2Ae4kdF/SEoYh7hZwP9QTtgHiEoYh7huwP9YTtgHiEoYh7htwP9gTtgHiEoYh7h1wT9VzhgHuA7Rf0AQGUeITtgHuI/Rf0hKGIe4V8F/UE7YB4hKGIe4YcF/WE7YB4hKGIe4YMF/YE7YB4hKGIe4dME/eizSbkBAWIeVThgHoB5YR4IAHge6GcHuei7SbkAAWIegHlgHhwAeB7ow0m5AAFiHoB5YB7oAxuqGwB4HuRPRf1AGmQeAShxHiFIQB/hvwT9AShnHiBIQB/gYwP94C9F/QBAZR4hO2Ae4jdF/SEoYh7hXwP9QTtgHiEoYh7hrwP9YTtgHiEoYh7hqwP9gTtgHiEoYh7h4wT96dNJuSEBYh5TOGAegHlhHhQAeB7py0m5IAFiHoB5YB4HAHge4B9F/QBAZR4hO2Ae4itF/SEoYh7hWwX9QTtgHiEoYh7hfwX9YTtgHiEoYh7hewX9gTtgHiEoYh7h2wT9UDhgHunjSbkgAWIegHlgHgYAeB7jS0X9QBpjHgEocR4hSEAf4UcC/QEoZx7p20m5IgFiHiBIQB/gWwP9gHliHgkAeB7pTwe56etJuSABYh6AeWAeCQB4HulLB7ngI0X9AEBlHiE7YB7iM0X9IShiHuFXA/1BO2AeIShiHuGjA/1hO2AeIShiHuGfA/2BO2AeLihiHkY4YB7gF0X9AEBlHiE7YB7iG0X9IShiHuFXBf1BO2AeIShiHuGbA/1hO2AeIShiHuGXA/2BO2AetkFgHi0oYh7p80m5IQFiHkk4YB6AeWEeCQB4HuknB7npA0q5IAFiHoB5YB4JAHge6SMHuen7SbkgAWIegHlgHgkAeB7pHwe59CdF/UAadB4BKHEeIUhAH+FDAv0BKGceIEhAH+BTA/3AQmUeITtgHiEoax7hTwP9QTtgHj4oax5hO2AeOChrHoE7YB42KGse6RNKuSEBYh5iOWAegXlhHiUAeB7pC0q5IQFiHoF5YR4kAHgeQQlqHmAbYR7gvwX94Qt/HmAbYR7ggwT94QlvHmAbYR7gZwT9oQhlHmAbYR7gYwT9gQhkHmAbYR7gWwT9YQhjHmAbYR7giwT9gQp0HmAbYR7ghwT99H9C/YEKdB5gG2Ee4GsE/eBDQ/0BQGUeOTthHiAraB7gQwP9WTthHiUraB55O2EeIytoHpk7YR45K2ge6SNKuTsBYh4aOWEegXl7HiIAeB7pG0q5IQFiHoF5YR4pAHge6vMAsqr05/JbAWee4T9D/SAIex7gNwP94EtD/SEIYB7q8wCyqvjn8lwBZ54ACHwe4DMD/QAQah4vCGAe7y8D/eTrQ/0hCGQe5O9D/Y8Iex7vCWEe7ysD/SEIfB7hJwP95EdD/YEIex7hUwX9/ztD/YEIfx7vC3we708F/S8IYB7vSwX95KNF/SEIZB7kp0X9jwh7Hu8JYR7vRwX9IQh8HuFDBf3kH0P9gQh7HuEjA/3/G0P9gQh/Hu8LfB7vPwX9LwhgHu8fA/3k30P9IQhkHuTjQ/2PCHse7wlhHu8bA/0hCHwe4RcD/eQTQ/2BCHse4TsF/f8PQ/2BCH8e7wt8Hu83Bf0vCGAe7zMF/eSbRf0hCGQe5J9F/Y8Iex7vCWEe7y8F/SEIfB7hKwX95AtD/YEIex7hEwP9/wdD/YEIfx7rC3we6w8D/SsIYB7rCwP95M9D/SEIZB7k00P9iwh7HmsJYR7rBwP9IQh8HuEDA/3/+0L94Qt7HuEnBf3k/0L94QtkHooIfB7qIwX9KghgHuofBf3kk0X9IQhkHuSXRf2KCHseSglhHuobBf0hCHwe4RcF/f/3Qv3hC3se4f8C/eS3RP3hC2Qenwh8Hv/7Av0/CGAe//cC/eTHQ/0hCGQe5MtD/Z8Iex7/C2Ee//MC/SEIfB7h7wL95OtC/YEIex7hEwX9gQh9Hr0LfB79DwX9PQhgHv0LBf3ki0X9IQhkHuSPRf2dCHsevQthHv0HBf0hCHwe4QMF/eTXRP2BCHse4esC/YEIdx73Cnwe9+cC/TcIYB734wL95LdD/SEIZB7ku0P9lwh7HvcKYR733wL9IQh8HuHbAv3k00T9gQh7HuH/BP2BCHUetQp8HvX7BP01CGAe9fcE/eSDRf0hCGQe5IdF/ZUIex61CmEe9fME/SEIfB7h7wT95ONE/YEIex7h6wT9gQhzHnMKfB7z5wT9MwhgHvPjBP3kq0P9IQhkHuSvQ/2TCHsecwphHvPXAv0hCHwe4dMC/eTbRP2BCHse4d8E/YEIcB4QCnwe8NsE/TAIYB7w1wT95HtF/SEIZB7kf0X9kAh7HhAKYR7w0wT9IQh8HuHPBP3BCXse4c8C/cEJZh7GCHwe5ssC/SYIYB7mxwL95J9D/SEIZB7ko0P9hgh7HsYIYR7mwwL9IQh8HuG/Av2hCXse4bsC/aEJaR4kCXwe5LcC/SQIYB7kswL95JdD/SEIZB7km0P9hAh7HoQIYR7krwL9IQh8HuGrAv3BCnse4acC/cEKYh5CCHwe4qMC/SIIYB7inwL9+EcD/SEIeB7+SwP9wgt7HkIIYR7imwL9IQh8HuGXAv0hC3se4ZMC/SELeh5CC3we4o8C/SAIYB7giwL94zsD/SAIYx4BCHwe4YcC/eU/A/2hCHseIAhgHuCDAv0AAWIegHlgHhoAeB5AGnQeAShxHiFIQB8CKGceQEhAH+B/Av3iy0X94MdF/QAIYh7gewL94MNF/QAIYh7gdwL94LtF/QAIYh7gcwL94LNF/QAIYh7gbwL94KtF/QAIYh7gawL94MtE/QAIYh7gZwL94MdE/QAIYh7gYwL96ydE+WAZQP0ACGIe4F8C/YtviNJrTLzyyxva8gv55/JgAWee8c9F/SAKYB5COmAeAIhCH8uogtKriqryK6rI8qv25/JiAWeeIgpiHkM6Yh5CjEMfSzaO0isOrvKLjcvyi/fn8mMBZ54jCmMeRDpjHmOQRB8r/ZPS68+/8iv/3PLr+efyZAFnniQKZB5FOmQehJRFH8uogtKriqryK6rI8qv45/JlAWeeJQplHkY6ZR6lmEYf6+Gc0svIuPJrONfyK/jn8mYBZ54mCmYeRzpmHsacRx+LeYnSy16p8gsJ3fJL+OfyZwFnnicKZx5QOmce58BQH0s2jtIrDq7yi43L8ov55/JwAWeeMApwHlE6cB4QxlEfcXpI/QAIcR5CCHEeYwhxHoQIcR6lCHEexghxHucIcR4QCnEe8V9E/VEacR7xWwL98VdC/VEacR7xVwL98btE/VEacR7xUwL98cNE/VEacR7xTwL98b9E/VEacR7xSwL98UdC/VEacR7xRwL98UNC/VEacR7xQwL9QRphHuE/Av1LkoTSK0my8oskyfJL+OfyYQFnnvEzRP0xykEf8TsC/fEvRP0xykEf8TcC/fE7RP0xykEf8TMC/fE/RP0xykEf8S8C/fErRP0xykEf8SsC/fE3RP0xykEf8ScC/fFDRP0xykEf8SMC/fFHRP0hykEf4R8C/QBoch5BaHIeYmhyHoNoch6kaHIexWhyHuZoch4HanIeRxpnHucbAv1GGmYe5hcC/UUaZR7lEwL9RBpkHuQPAv1DGmMe4wsC/UIaYh7iBwL9QRphHuEDAv1AGmAe4P8B/esDDSrrywT5a8JAkWvhJ5HrxwT5a75AkfbTBflgQSeRa7JAkW8hI5FsqkCRkYEikWySQJGNYR6RbIpAkYPBHZFsckCRgaEZkWxuQJGWARmRbGJAkZXhFJFsXkCRkEEUkWxSQJGLIRCRbEpAkY6BD5EMW5hSagIMi4xPjFJsAgyL7LcE+ewDPirszwO5zH+AUp4BHkv+ywO56MNDuf4DKCr+xwO5ngEIS/7DA7nou0O5/gMoKv6/A7meAQhL/rsDueizQ7n+Aygq/rcDuZ4BCEv+swO56KtDuf4DKCr+rwO5ngEIS/6rA7noo0O5/gMoKv6nA7meAQhL/qMDueiTSLn+Aygq/p8DuZ4BCEv+mwO56JtIuf4DKCr+lwO5ngEIS/6TA7noo0i5/gMoKv6PA7meAQhL/osDueirSLn+Aygq/ocDuZ4BCEv+gwO56FtJuf4DKCr+fwO5ngEIS/57A7noU0m5/gMoKv53A7meAQhL/nMDuehLSbn+Aygq/m8DuZ4BCEv+awO56GNJuf4DKCr+ZwO5ngEIS/5jA7noQ0m5/gMoKv5fA7meAQhL/lsDuf4DPCr+VwO5nAEcS/xTA7n8Azsq/E8DuZsBG0v7SwO5+wM0KvtHA7mUARRL9EMDuQAQYB7hW0T9IShgHuGfAf30Aycq9DcDuYcBB0vnMwO5jAEGS+wvA7nsAyYq7CsDuebzQ7nsAyYq7CcDucw/gFKGAQZL5iMDuefrQ7nmAycq5h8DuYYBB0vmGwO55+NDueYDJyrmFwO5hgEHS+YTA7nn20O55gMnKuYPA7mGAQdL5gsDuefTQ7nmAycq5gcDuYYBB0vmAwO56NtIueYDKCrm/wK5hgEIS+b7Arno40i55gMoKub3ArmGAQhL5vMCuejrSLnmAygq5u8CuYYBCEvm6wK56PNIueYDKCrm5wK5hgEIS+bjArno+0i55gMoKubfArmGAQhL5tsCuegjSbnmAygq5tcCuYYBCEvm0wK56CtJueYDKCrmzwK5hgEIS+bLArnhY0T9IShgHuFjAf3oM0m55gMoKua/ArmGAQhL5rsCueg7SbnmAygq5rcCuYYBCEvmswK56BtJueYDKCrmrwK5hgEIS+arArnmAyUq5qcCuYUBBUvlowK55QMkKuWfArmEAQRL5JsCueQDIirklwK5ggECS+KTArniAykq4o8CuYkBCUvpiwK5iQEaS+mHArnoAzoq6IMCuQgwhlJoAgiL6D8B+QgxhlJoAgiL6LME+Qg1hlJoAgiL6DsB+QgsglJoAgiL6DcB+QguhFJoAgiL6DMB+QhSkFIJBIRSbAIIi4gBCYvoLwH5CAWEUuyvBPmIAQiL6KsE+QhblFJoAgiL6CsB+Qg8iFJoAgiL6CcB+eFnRP0hKGAe4SMB/QhAiFJoAgiL6B8B+Qg9iFJoAgiL6KcE+QgbglIJNIpSaAIIi+gbAfkIAQmL6KME+QhSjFJoAgiL6BcB+QhUkFJoAgiL6BMB+WhKQJEIIRCR6A8B+QhmmFJoAgiL6AsB+Qh4nFJoAgiL6AcB+WhCQJEJQQ+RaF5AkQjhFJHopx+paFJAkQmBEZFoVkCRCMETkeinHqloWkCRCQEUkWhuQJEIoRmR6KcdqWhiQJEJQRaRaGZAkQiBGJHopxyp4YNE/SEoYB7h4wD9aGpAkQnBGJFoikCRCGEekegnG6lockCRCQEbkWh6QJEIQR2R6CcaqWiCQJEJgR2RaKpAkQghI5HoJxmpaJJAkQnBH5FomkCRCAEikegnGKlookCRCUEikWi+QJEI4SeR6CcXqWiyQJEJgSSRaLZAkQjBJpHoJxapaLpAkQgBJ5HorwD54b9F/SIoYB7hi0T9IShgHuGLFG3hh0T9IihgHuFrRP0gKGAe4IsTbehHSLnoAygq6DcBuehDSLnoAygq6DMBueg/SLnoAygq6C8Bueg7SLnoAygq6CsBueg3SLnoAygq6CcBuegzSLnoAygq6CMBuegvSLnoAygq6B8BuegrSLnoAygq6BsBuegnSLnoAygq6BcBuegjSLnoAygq6BMBuegfSLnoAygq6A8BuegbSLnoAygq6AsBuegXSLnoAygq6AcBuegTSLnoAygq6AMBuegPSLnoAygq6P8AuegLSLnoAygq6PsAuegHSLnoAygq6PcAuegDSLnoAygq6PMAuej/R7noAygq6O8Auej7R7noAygq6OsAuej3R7noAygq6OcAuejzR7noAygq6OMAuejvR7noAygq6N8AuejrR7noAygq6NsAuejnR7noAygq6NcAuejLR7noAygq6NMAuei3R7noAygq6M8AueizR7noAygq6MsAueivR7noAygq6McAueirR7noAygq6MMAueiHR7noAygq6L8AueiDR7noAygq6LsAueh/R7noAygq6LcAueh7R7noAygq6LMAuehnR7noAygq6K8AuehPR7noAygq6KsAuehLR7noAygq6KcAuegnR7noAygq6KMAuegjR7noAygq6J8AuegfR7noAygq6JsAuWmCAZFoAgKR6KcIqWmiAZHurwX5yCEAkeifBPlI4QCR6KcHqeqzBflIAQGR6JsE+fC3BfkJIgCRaOEAkeinBqnruwT5aAEBkeiXBPn2vwT5ySIAkajiAJHopwWp9bsF+akCAZHjwwT5aCAAkeinBKkp4ACR4esB+SgAAZHopwOp8e8B+SkiAJGo4QCR6KcCqe33AfmpAQGR4PsB+QggAJHopwGp6eEAke/zAfnoAQGR6KcAqfoDDapxpnG9ZMpY/XrGWP36jwT9Zc5Y/X42QLnoN0G5yAMICwghABLpM0G5yQMJCykhABLtO0H5oFlo/OgvQbnIAwgLCCEAEuorQbnKAwoLSiEAEqJZafzpJ0G5yQMJCykhABJhJkD96+cDssv95/JrAWeepllo/CMIax5nLkD96OcBskgzk/Io9efyFgFnnmHaYP2wWWr8YTIA/cgjQJIc8X3T6OcDssj85/IBAWeesllp/HTORm1zCmEeLkBgHuFLBP11PkD9tCp0HvkMVh/KQmAe6OcAsmj45/INAWeeh05NH2hBkNJoHrryqMrd8uj99/IdAWee6CNDuQgBHgsJIQAS/zND/UIIfx7oG0O5CAEeCwohABLoE0O5CAEeCwwhABLoC0O5CAEeC+kvQ/3UCGkeDiEAEugDQ7kIAR4LECEAEmZWSP1zXkj94ydD/RUKYx7wi0P9cEJGH/DHBf1oxlC5q82JUss4qHIvB4ZSCD0LG+gjCbkGAWIe4StD/VMKYR4IBKDSCMDn8ggBZ55ydkj9dnJI/WgWQLl3ikj97DdD/RiIbB/ge0L94CpgHh8FAHH1AwiqAuQALwAMYh4CQGUeGzhiHmZmCP18O3ceYJJI/QggYB7ox58aiCNgHt4IaB7g158aCAAICmCaSP0AAQBSAgBjHgYBYx7GC2Ye/s8F/QIYQh/ifwT9ABBsHkAAQB95KgD9YmpI/eK/Bf1gI2Ae6LefGuAiYB7gp58apkNgHvcIfR4IAQAKAAFjHkAIYB7ggwT9Z6JI/QAIaB7gcwT94AhoHuCLBP1gTlG56EdIucIDCEvoQ0i5xQMIS+g/SLnGAwhL4utD/RRTQh/oO0i5xwMIS+g3SLnUAwhLyAcAUTGmIA/vR0D592k8/DHaYV6ECG4epSh6HqQQTR/3WWn8hURdH/3DBf3gg0P9RApgHuCrQP3EEkAf4FtC/Zj4QB/xWWr8e4YI/XyOCP3EKnYelihyHvJZbPwbCmAeZK5I/YQIax4JIQAS/Flu/CnxfdPrN0H5fWlp/KQTSh9HQWAe/Vlw/GRpPPxKIAASfllq/ODvQ/2U1mAfZEZI/eR7BP2qIAASdVlq/LUKfx7kT0X9PwpkHsogABJxWWr8KQppHuogABLkS0X9TgpkHnJZavyKIgASallq/ORDRf2RC2QeXApjHlIJYR55shD9YbZQ/eFvBP3qJ0O5ygMKC+GHQ/2TTkEfRiEAEuofQ7nKAwoLSiEAEusXQ7nMAwsLjiEAEnhuCP3rD0O5zAMLC5AhABLrB0O5zAMLC4whABJ0vlj92Cp4HnTCGP2laTz85a9E+aNEYL3jdwT9thhQ/d7XbB+lFFD95YcE/esjQbnCAwsLQiQAEu0rQfm1WWL85FNF/ff+ZB/rH0G5wgMLC0IkABK0WWL84z9F/Z8KYx7kR0X9tAtkHu8jQ/2//m8f6xtBucIDCwtCJAASqlli/GMLeB7jVwT9vRxQ/fUfQ/1YCXUe7N9D/fhjTB/rF0G5wgMLC0IkABLbJ0Ifvlli/PkXQ/3eC3ke+uND/Rj7eh/rE0G5wgMLC+KjRf3/OkIfQiQAErdZYvzwG0P99wpwHuR/Q/0YX0QfcyIA/XPmYP1zCmsed+5g/eNAYB73TkcfwidAknjiIP1R8H3Tc/pg/eJLRP1zCmIeePZg/X7+YP1782Af2Ct4HhNPTR9zCmYe7x9B+fNpMfwfAABxd+og/evLQ7liAR4LQiQAEvNZYvwA1IAa4bNF+TgEQP3rw0O5YgEeC0IkABL8WWL84ItE/eZzRP3AIGAeePIg/eA3Rf2GC2Ae4DtF/WaaYB/ru0O5YgEeC0IkABIA0J8a51li/OAzRf3nCGAe4JtF/cYcQB/rs0O5YgEeCwPUgBrjEwm5QCQAEudZYPzrq0O5YAEeCwAkABK/BgBx+Flg/H4Wcf18InH94HdC/ZMrYB7gp0X9/8dgHwDkAC9xDmAeM0JlHjM6cx5xOnweaSpx/eArRf3nCGAeKCFgHuDHnxooImAe4tefGkAAAArgn0X9yZxgHwIAAFJGAGMeBwBjHntLQR9yMnH94c9F/ScIZx5GHkYf5nME/eB7Q/3HC2AeABBsHtIAQB/gL0X9GAtgHmAich7gt58agCNyHmoScf3g40D9Th1AH+BvRf38U0Af4qefGgAAAgoHAGMe4L9F/QYIZx7mXwT94GtF/ThhQB/oywX9wAhoHuBnBP10OnH9gApoHuBbBP3rt0T5YgFAueszSLnHAwtL4FdC/d+FQB/rL0i51AMLS+srSLnEAwtL1gpiHkZAYB69K2UetltNH7RBYB7hf0T9YZYI/WJGcf0IJQASAPF90+8XQfn9aWD8fw4x/W1BYB5CCGseogtDH3JAYB7iaTH8YgBiHugkABL9WWj86CdIucgDCEvhd0T9JaQgD+EbQv1CCGEepdhhXufDRf3WFkcfRSlqHqUofh6HJgASARBgHj44Yh7pWWf86yNIuccDC0ulKH8e4cdF/T8IYB5eeH4e4AtlHuBrBP1gBnH94HcE/eA/Rf0/CWAevf9vH4QkABIA5AAv3mtgHg/kAC//WWT8/wt1Hr1/TB8IJQAS4FdE/R8Ifh71WWj8tQp5HrXXeh/oJAAS4G9E/eMrYB79WWj8vQtwHrV2RB/rM0H5Y2k8/LcAAP2gBED94FcE/ejPQ7nIAwgLFCUAEn5ZZvzox0O5yAMICwclABLov0O5yAMICwYlABLoD0G5yAMIC3u6GP0IJQAS7w9B+ftZaPzoC0G5yAMICwglABJ8Qgj9/Flo/Oi3Q7nIAwgLCCUAEmlZavzqB0G5ygMKC0olABLqWWr86gNBucoDCgt4AjH9SiUAEu5Zavzq/0C5ygMKC0olABLgg0T9YJ4I/eBZavzqr0O5ygMKC0olABK1CBD9cx4x/bMMUP2zEBD9tmkx/O2vRfmhJUC94W8E/XEmMf2j3UBt438E/bgNQP3jAw2q+g9D/ZELeh75E0P9ccd5H+F7RP3kK2Ee6AtD/VMJaB7wz0P9MU5QH+wDQ/3TCWwe7tND/THObh/rB0P9AAhrHmSqCP3ld0P9IAJFHyAMAP0gEED9okFgHgAIbR5zWW78JBhA/UFCYB7yqwX9kQBSHzEUAP37u0T5dgdA/WBZcPw7EERtnAhmHtVAYB4/KED9ZFls/OR7BP37K3sehEJgHntzVB97C2ce7QtB+btpMfzmT0X9KQlmHjYcAP3ro0O5bAEeC4wlABK2WWz8XwAAceubQ7lsAR4LjCUAErtZbPz8I0X9ewt8HkzUghr8J0X91u58H+uTQ7luAR4LziUAErtZbvzmZ0T98ltE/cAgch7yH0X9cgtyHuuLQ7luAR4LziUAErRZbvyM0Z8a+5NF/dJKWx/2F0X9lAp2HvaXRf1S0nYf64NDuW4BHgvOJQASgtWMGuIDCbm0WW789htF/ZQKdh72Z0X9UlJWH+TXRfmSAAD9vwYAcZ9wQm2WIED98nNC/dIqch5SDm8e5nNE/WYuMf1GQmUeWzpmHnI7dh6GKED9yCBgHuZTRf3Gp2Yf7MefGkgiYB7u158azAEMCpQwQP2OAQBS3gFjHokBYx7qz0X9SQlpHv1LRf1zCn0elCZeH/RnBP3+c0P9ngt+Hv0jQf3+e10fHRBsHol2XR9gI2ke7LefGsAiaR7up58ajAEOCpYBYx70o0X9xkxUH/1TQv3eq10f879F/W0Kdh7tcwT9lkRA/e0HQfmpaWD89ENF/QoIdB7ACmIeVEBgHiABQR+WOED9oGkx/OHLRf2gCWEe1gphHpB8QLnrH0i5zAMLS+sbSLnOAwtL6xdIucADC0vhp0X9xqhhH4wlABKpWWz86xNIucwDC0utQmAe9wp1Hp4MAP0YK2Mek0BgHhdfRB/OJQASuFlu/OFHRf3je0T9ZAhhHusPSLnOAwtL4W9E/TWkIA+12mFe6kBgHvdWRx/1K38e4W9F/d8QQR+kKnwehCh+Hu/HRf3mCX0ewQhkHuFvBP2BBED94VsE/X/WIP0VC3oeNdV5HwAkABK4WWD84V9E/WE2Mf0HC2gejCUAErhZbPxcAGIepx5QHxULbB7MJQASuFls/JscAP3n1G4fFQtrHvVURR/3aTH84RdC/ZcLYR6RSBD96/tAucwDCwuMIQAS7/9A+fhZbPySJAD9gUxQ/eFTBP3r90C5zAMLC4whABL7WWz8ARBgHjE4dx7rp0O5zAMLC+vzQLnOAwsLziEAEudZbvz3enEejCUAEuvvQLnOAwsLziEAEvFZbvwE5AAv/GpkHuufQ7nOAwsL6+tAucADCwsAIAAS91lg/OFrRP0lCHweziUAEn5zRG2cC20efytA/eFXRP2jKGEe/it+HtxzUx+cC2oeXUFgHu37QPm8aTz8HwIAcZVQIP3r+0K5YAEeCwAgABK8WWD8ENaQGpVUYP3r80K5YAEeCwAgABK+WWD8ACB2HnUAAP3r60K5YAEeCwAgABKgWWD8ENKfGvr7Qv11C3oe6+NCuWABHgsAIAAStllg/BbWkBr28wi5+f9C/R/XeR/r20K5cAEeCxAiABKpWXD8vwYAceQDFaoqI0D94W9C/VUpYR61DmQeuEJlHu0TQfmjaTH8uDp4Hhs7ah4jK0D9aCBgHrVZdPzwx58aaCNgHuDXnxoQABAK65dDucADCwsUJAAS6PdC/eMIaB7sx0P94w9MH/LvQv0nCnIe8MtD/WOccB+/WWf8AAIAUgcAYx7rj0O5wAMLCwckABLr80L98QprHuZvQ/1jREYfFwJjHuuHQ7nQAwsLYw8A/eF3RP2iKGEeYxNA/YFCYB5jCHQeZRtA/eSrRf2jDEQf4rtF+UUEQP0QJgASZR8A/eu3RfllJUC98Q9F/dELcR70E0X9nMd0H3H5QG1iQjH98XcE/eILRf0ACGIe4otF/YADQh9iDUD94wMLqmMXAP30A0X91gp0HvSPRf0A2HQf9AdF/TYJdB70Y0X9AFhUHyADAP2gWWb8NjNA/e7PRf3XCXcex15HH+dPBP0UEGwe9lBUH7dZaPwAI3Ye6LefGkAhdh4pW0JtvFlq/N4LbR5CKHEeQnhTH2dCYB7+a0P93gp+HvE3Rf3/C3Ee8WNB/T55UR/xT0L93rtRHz4PAP3qp58aCAEKCvQzRf0KCHQeoKQgDwDYYV5OAF0fAAFjHuIrRf33CmIe5b9F/aIIYB7iVwT9IClpHhModh4gR0D9AAhhHilAYB7o10X54WdE/QEtAP3t90D5tGlp/IACRB+gaTz84ctF/UAIYR40O0D9lAphHih/QLnqC0i5ygMKS+sHSLnAAwtL4TtF/bX+YR9KIQAStllq/OoDSLnKAwpLACAAEuGbRf21KkEfv1lg/Ov/R7nAAwtLYip+Hv4JcR7hL0X9nAthHsELYh7hZwT9SiEAEr5Zavzq+0e5ygMKS+GfRf213mEfIQdA/eFrBP3/C3oe1v55H94LaB7Wekwf4WtF/bVyQR8AIAASvFlg/JwLch7W8nAfNQAA/UohABK1WWr8tQprHtVWRh/q10X54XNE/UE1AP0jSwj9IU9I/eFfBP01UxD9M1dQ/dYCYh5zAAD97mk8/Ou/RPl1JUC9b81AbTgfAP3vcwT96udAucoDCgtKIQAS7e9A+bxZavzhE0L91gphHngNQP3gAwuq6uNAucoDCgtKIQASvllq/DsnAP3550L92wt5HurfQLnKAwoLSiEAEr5ZavwBEGAePzh2HvHrQv2b73Ef+uNC/dwLeh7qt0P9e3NKH+rbQLnKAwoLSiEAEtZ6fx68WWr869tC/ZwLax7wu0P9e/NwH+rXQLnKAwoLA+QAL9ZqYx5KIQASvFlq/OjfQv2cC2ge4WdD/XtzQR/ib0T9Rgh2HlsMAP1WEED91gppHlsYQP2fQGAee1tEH+JbRP3EKGIeWxQA/VxYRG3WCm0eXihA/eJTRP3GKGIe0it8HlJaRx9SCn0e6+tA+XJpPPzq10X5REEA/e/rQfnkBUD96tNCuUoBHgtKIQAScllq/B8BAHFEHAD99gMCqurLQrlKAR4LSiEAEmRZavwI1Yga4vtE/YQIYh7qu0K5SgEeC0ohABJ2WWr8ACB0HuD/RP1AkmAf4vdE/cQKYh7ig0X9ABBCH+qzQrlKAR4LSiEAEgjRnxpkWWr84u9E/YQIYh7ih0X9AJBiH+qrQrlKAR4LAdWIGuHbCLlIIQASZFlo/OLzRP2ECGIe4l9F/QAQQh+/BgBxAAMA/QAjQP3ka0L9BChkHoQMYx7rA0H5Zmkx/IZAZR6SOGYeRjpgHgQrQP12WWz8iCBgHujHnxrIIGAe6tefGkgBCAoKAQBSRAFjHhQBYx58WW78HjNA/ffPRf30CnQe1FNEHwIQbB6ECkIfQCJkHui3nxoAIGQe6qefGggBCgoAR0D9fll0/AAIaR4jQWAe7OdA+YRpafyAAF8f4kNgHn9ZZ/wEAWMegGk8/GBZcPyuCGQeCTtA/Wc2QLno90e56AAIS+nzR7npAAlL5CNF/ZwLZB7q70e56gAKS7WmIA+12mFecwptHhgrbx7kH0X93gtkHhNPRx/sQGAec1ZdHwghABKVWWj85BdF/fgLZB4oIQASn1lo/P8LeR61/nEf5BtF/R8IZB5IIQASgFlo/AAIeh6qAkof78tF/cAJbx41CW8eCH9AuenrR7npAAlL6udHueoACkspIQAS5E9E/SQvAP2HWWn85whrHkedcB9JIQAS5CdF/dbyZB+cWWn8nAtoHudwQR8bSwj94ZNF/dZ6QR/pIECSKfF90wdTEP0HV1D9BwAA/eGXRf3H4mEfs2kp/OrDRPla4UBt+r8F/VMNQP3hZ0X953xBH+rTQLnqAAoLSiUAEu7bQPnWWWr86s9AueoACgtnAwD9SiUAEsdZavzh50T95whhHuHrRP3HnmEf4VdE/SE3AP3qy0C56gAKC0olABLFWWr8NgBiHuHjRP2lCGEe6sdAueoACgtKJQAS21lq/BIfAP3oq0P95RRIH+rTQv1nC2oe669D/aWcax/qw0C56gAKC0olABLhD0L9xwphHtJZavz510L9Ugp5HvFfQ/2lSFEf5Q0A/QYnAP3lEUD9fkBgHuNjBP2lCGMe5hlA/URAYB7SFEIf6iRAkgEQYB4lOGceRvF90/YZRG3GCG0e+ylA/eV4ZR5nK3Ye5hhMH8YIfR7r10D5Zmkm/BDkAC+laHAe8hUA/ep7Q7lKAQcLSiUAEmZZavwfAQBxRwdA/epzQ7lKAQcLSiUAEnZZavwI1Yga5x0A/eHbRP3HCmEe4d9E/cacYR/qa0O5SgEHC0olABIAIHUeYFlq/OHXRP0ACGEe4XtF/cAAQR/qY0O5SgEHCwjRnxpKJQASZllq/OHPRP3GCGEe4X9F/QCYYR8R1Yga6FtDuQgBBwsIJQASZllo/OHTRP3GCGEevwYAceFbRf0AGEEf4AIA/eAiQP3hZ0L9BihhHsYMcB7hZ0T9JwhlHsJAZR7fOGIe5TtgHuIqQP1IIGAe6MefGqggYB7q158aSAEICgoBAFJCAWMe5jJA/RUBYx79QmAe9Qp1HsJUQh/ha0T95ihhHgEQbB5VBEEfEBBsHuAjdR7ot58aACB1HuqnnxomQwD9CAEKCgABYx7hX0T94yhhHu3zQPmjaTz8YWpI/fY6QP3o/0K5yAMICwghABKmWWj86H5Auer3QrnKAwoLSiEAEuvvQrnMAwsLp1lq/IohABLjBABRbCQAEoLxfdO8WWr84NNA+QNoYvzq50K5ygMKC0ohABK1WWr890ZA/erfQrnKAwoLSiEAEvcKfh6pWWr8Y1xEHwNoJvzyShD9/u9B+dfvQG0ULwD992sE/dIPQP0jCGAePkBgHuFXBP1gCG8e1ApvHh8BAHEI1YgaACB0HvXTRfmgIkD9tCpA/eEPRf3nCGEe4RNF/cacYR/hC0X9hwthHgjRnxoQ1YganwQAceFjQv0WKGEe4YtF/dwcQR8B5AAvxg5hHsdAZR7HOGce4ThgHuFTBP2IImAe6MefGiggYB7q158aSAEICgoBAFJUAWMe4QNF/bUKYR7hj0X9lddhHxYBYx7hB0X9PAlhHuFjRf2pckEftTJA/bYLdh6mQ2AeoVpUH+FbBP1cV0RtyQIA/TZAUB/gIHYe6LefGgAgdh5JK0D9DjcA/eqnnxoIAQoKAAFjHrY6QP3/HgD9qH5AuQQLbR5zKnoefxJMH+r7QflUzUBt9GcE/TgpfB5cDUD99AMKqrUKbR4VV0wf2gtgHkALbx7WCm8eHwEAcQjViBoAIHYe4fNB+SlYRG1gC20eUip3Hl4CTB8yKED9YAptHpMrdB5gAkwf4F8E/fYnRPnAIkD9wQptHtYqQP1SKmke3DJA/QjRnxoI1YganwQAcVsGTB/hX0L9AShhHuUmAP0X5AAvIQx3HiVAZR4lOGUeoThgHuFPBP3IImAe6sefGiggYB7s158aigEKCkwBAFKSAWMeVgFjHrYLdh6BW1If4UsE/RxbQm3wY0P9yQpwHuSfQf2JJ0QfnCt8HpYrdh7kS0L9PPVEHxwPAP3WKnwePAJiHvALQv2cC3AeGBBgHgk7fB6ce2ke8MdF/XBaCP0JCmQeNgl2HgkHQP2ca3ce1gp8HurLR7nqAApLSiUAEhxYavzJKmkeCUMA/QlPSP3WKmke6rdHueoACktKJQASCVhq/OqzR7nqAApL5OdE/SkJZB7k60T9nKdkH0olABIJWGr86q9HueoACkvk40T9KQlkHpwnSB9KJQASCVhq/CkJah6cp2sf6qtHueoACktKJQAS699A+XZpKfwWWGr81gp5HpZbUR/2UiD9/MNE+ZYnQL3WpiAP1tphXvTDRf32W1Qf1mkm/PZWYP2WAwD96r9AueoACgtKJQAS7INYqRZYavy1CnQelWkm/Oq7QLnqAAoLSiUAEhVYavzoy0L9tQpoHv/PQv3V1n8f6rdAueoACgtKJQASFlhq/OnHQv3WCmke+Z9D/bVaWR/qs0C56gAKC0olABIWWGr8679C/dYKax7xo0P9tdpxH+qvQLnqAAoLSiUAEhZYavzq10K56gAKC0ohABLqw0L91gpqHvBXQ/21WlAfdllq/FUPAP3qz0K56gAKC0ohABJ1WWr84i4A/eq/QrnqAAoLSiEAEmJZavzk+0T9tQpkHuT/RP3V1mQf6rdCueoACgtKIQASdllq/OT3RP1CCGQe5INF/aIKRB/qr0K56gAKC0ohABJ1WWr85O9E/dYKZB7kh0X9QthkHzYEQP3k80T9tQpkHuRfRf1CVEQf6lNDuUoBBwtKJQAS4gEA/YJZavzqS0O5SgEHC0olABKVWWr8Vh8A/epDQ7lKAQcLSiUAEpZZavztt0L9tQptHuy7Qv1C1Gwf6jNDuUoBBwtKJQASlVlq/O+zQv3WCm8e/ZdD/UJYXR/qL0O5SgEHC0olABKWWWr896tC/bUKdx78m0P9QtR8H+6vQv3VCm4e5FdF/UJURB8EEGweNRBEH6AgdR7qt58aACB1HvUCQm2iAgD9QhNA/eM2AP1DG0D982NE/UIIcx7yq0X9YghSH+3DQPmjaWL8px4A/adGQP3nCHMeYxxSH+dbQ/0HCGce5KdA/aceRB/kR0L9wUBgHueYRB/nDgD9QhcA/aNpJvwDAmIeokoQ/XZaSP21KnUeoCpgHvUGQP3iU0T9oiYA/eZOUP3iB0L9YwhiHgAoZx4HO2Me64dHuewAC0tjeGcejCUAEqdZbPwACGQeAAh2HhjkAC9jaHgeAAhjHuuDR7nsAAtLjCUAEqNZbPwVKHUeAChmHut/R7nsAAtL63tHue4AC0uMJQASplls/GMIaB7jjH8fzCUAEqdZbPzGCGkeYxhZH+YIax5jmHEf62dHuewAC0uMJQAS9UIA/aZZbPzGCGoeYxhQH+3PQPmgaSb8o1Ig/et/Q7nsAAsLjCUAEqBZbPziW0T9oi4A/et3Q7nsAAsLjCUAEqNZbPzi20T9YwhiHuLfRP0AjGIf629DuewACwuMJQASo1ls/OLXRP1jCGIe4ntF/QAMQh/rZ0O57AALC4wlABKjWWz84s9E/WMIYh7if0X9AIxiH+tfQ7nsAAsLjCUAEqNZbPzi00T9YwhiHuJbRf0ADEIfwydAvWOkIA9j2GFei0JgHsMPVB8mEED9QAMA/SAYQP3GCHMeAhhSH8ZGQP3GCHMeujYA/WwgABLks0D5h1hs/OAYUh+0HkJtA2gm/ONTQ/3jCGMe5KNA/YMORB/kQ0L9Y4REH6MOAP2UKnQehypnHhQBYh7jKGMe5gNC/YcKZh4BEGAeNDhnHud4dB7UCmQegwpjHrQGQP3naHgeYwhnHqdWYP10KHQe7KefGkoBDAq0QgD9VAFjHuRXRP2UCHQe1TpA/cp+QLnrq0C57AALC4whABLky0X9tQpkHpAKZB5fAQBxStWKGgAidR7uu0D50Fls/OunQLnsAAsLjCEAEtVZbPzHAwD966NAuewACwuMIQASx1ls/P+jQv21Cn8e/qdC/RDWfh/rn0C57AALC4whABLVWWz86J9C/ecIaB75R0P9Bx5ZH+ubQLnsAAsLjCEAEtBZbPzql0L9tQpqHvhLQ/3n1Hgf6ZtC/RAKaR7xT0P950BRHycMAP3rx0T5ZwVA/eADC6pwC2se7bdA+bBpKfzro0K5bAEHC4whABKwWWz8JxwA/eubQrlsAQcLjCEAEqdZbPzzj0L95whzHuuTQv0Hnmsf65NCuWwBBwuMIQASsFls/OaLQv0QCmYe+ztD/edAWx/ri0K5bAEHC4whABKwWWz88odC/RAKch76P0P958B6H+uHQrlsAQcLjCEAErBZbPzkg0L9EApkHvVDQ/3nQFUfxwIA/evTRflnTVD9YyhnHu2/QPmjaSb8xR4A/eFPRP3BJgD961dDuewACwuMJQASoVls/IBoKfzrT0O57AALC4wlABKjWWz84EtE/cAuAP3rR0O57AALC4wlABKlWWz8YwhtHiGMbB/rN0O57AALC4wlABKjWWz8pQhvHiEUXR/rK0O57AALC4wlABKlWWz8Ywh3HiGMfB8iFAD9owhuHuBXRf0hDEAfIQAA/cEOQm3CSgj94H9C/WAIYB7in0D9IABCH+I/Qv3lz0X9AJRCH9Q2AP1K0Z8aStWKGiEoYR4hKGMeQwFiHsAOAP3l/0H9YwhlHiAoYB4BEGAeIThjHmF4YR7CCmIeQAhgHgLkAC8haGIeAAhhHsEGQP0BKGEewUIA/cFOSP3rT0e57AALS4whABIAKGEegVhs/O2vQPmgaSn860tHuewAC0uMIQASgFhs/AAIfx4ggH4f6ydHuewAC0uMIQASgVhs/CEIaB4ABFkf6yNHuewAC0uMIQASgVhs/CEIah4AhHgf6x9HuewAC0uMIQASgVhs/CEIaR4ABFEfwFIQ/YAmQL0ApCAPANhhXuFfRP3iw0X9IABCH8FWUP3rp0K57AALC4whABKiWWz8wGkp/OmfQrnpAAkLKSEAEqBZafyBAgD99QMUqumXQrnpAAkLKSEAEqFZafwACHMeQIBrH+mPQrnpAAkLKSEAEqJZafwhCGYeAARbH+mDQrnpAAkLKSEAEqFZafxCCHIeAIh6HyEIZB7iR0T95I9E/YIIYh7jQ0T95YdE/aIIQx/jP0T95n9E/cIIQx8ABFUf4TtE/ed3RP3hCEEf4jdE/fBzRP0BBkIf4jNE/fG/Rf0hBkIf4i9E/fJrRP1BBkIfSZKE0ilJsvKJJMnySfjn8iIBZ57jK0T982dE/WEGQx8hCGIe4h9C/YIIYh7jI0L9oghDH+MvQv3CCEMf4zNC/eIIQx8AAAD9fwoAuSkAgFJpEgC5aQ5AuesjSblrwhC5aaIxuekTSblpShG55BtB+Z/gMLmJ5HC5qUAgueyzRfmfEQC5iRVAuesDSbnto0T5qwEAuevzSLnt10X5q3kAueuvRflpIQC5fxMAuWkXQLnut0X5ySEAuenbSLkpewC547tF+X8QALlpFEC55wMDqu2/RPmpIQC5/xEAuekVQLkRewC58HoAuYkjALlfEwC5SRdAuckjALnp00X5KHkAuT8QALkoFEC5ynoAuYgiALnod0X5AYUA/Oh3BfngJ0L9AApAH+E7Qv0gAkEf4TdC/UACQR/hK0L9YAJBH+hzRfkAhQD86HMF+WAiQP1gJgD9YCpA/elLQPkoCUD56M8F+SEBwD1gLgD96ENA+QEBgD1gQkj9YEYI/WBaSP1hZkj9YF4I/WFqCP2AAMA9YDqEPWCGSP1hjkj9YIoI/WGSCP1glkj9YJoI/WCeSP1hqkj9YKII/WGuCP1gslD9YLYQ/WC6WP3pP0H5IQHAPWC+GP0pCUD56csF+emzRPkhAYA9YNZg/WDaIP1g4mD9YOYg/WDqYP1g7iD96SdB+SABwD0pCUD56ccF+WECcf3pp0T5IAGAPWEGMf1gEnH9YBYx/WAOcf1hHnH9YBIx/WEiMf1gJnH9YCox/WAucf1hNnH9YDIx/WE6Mf1gQnH9YEYx/aAAQP2hCFD9oAQA/aEMEP3qL0H5QAHAPUkJQPnpwwX56atE+SABgD3qAwyqgAFA/YAFAP2ADUD9gBEA/YAVQP3pP0D5IQHAPSkJQPnpvwX5gBkA/embRPkhAYA96ddF+SABQP3p10X5IAUA/enXRfkgEUD96ddF+SENQP3p10X5IBUA/enXRfkhEQD96ddF+SAdQP3p10X5ICEA/enXRfkgJUD96ddF+SEtQP3p10X5ICkA/enXRfkhMQD96ddF+SA1QP3p10X5IDkA/enXRfkgQUD96ddF+SFJUP3p10X5IEUA/enXRfkhTRD96ddF+SBRYP3p10X5IFUg/ekDC6pgAcA9aQlA+emrBfnpn0T5IAGAPWADQP1gBwD9YA9A/WATAP1gF0D9YBsA/ek3QPkgAcA9KQlA+emTBPnpl0T5IAGAPSADQP0gBwD9IIdBbSAHAm0gH0D9ISdA/SAjAP0hKwD9IC9A/SAzAP0gN0D9IUNA/SA7AP0hRwD9IEtI/SBPCP0gU1D96QMOqsEBwD0gVxD9yQlA+emLBPn0O0D5gQKAPekDA6pgAED9YAQA/WAMQP1gEAD9YBRA/WAYAP3mJ0WpIAHAPSkJQPnphwT5AQNA/cAAgD0BBwD9AYNBbQEDAm0AH0D9ACMA/QAnQP0AKwD9AC9A/QE3QP0AMwD9ATsA/QBDQP0ARwD9AEtI/QFTUP0ATwj9AVcQ/ecDDaqgAcA9qQlA+el/BPniM0D5QACAPeUDD6rgAUD94AUA/eANQP3gEQD94BVA/fGnQ6khAcA9KQlA+emPBPngGQD9IQKAPeACQP3gBgD94YJBbeECAm3gHkD94CIA/eAmQP3hLkD94CoA/eEyAP3gNkD94DoA/eBCQP3hSlD94EYA/eFOEP3gUmD94FYg/YADwD2JC0D56YME+eMDHKrwJ0D5AAKAPUADQP1ABwD9QA9A/UATAP1AF0D9QBsA/e4nQqkgAcA9OwlA+cABgD3r00X5YAFA/evTRflhEUD969NF+WAFAP3r00X5YA1A/evTRflhFQD969NF+WARAP3r00X5YB1A/evTRflhJUD969NF+WAhAP3r00X5YSkA/evTRflgLUD969NF+WAxAP3r00X5YDVA/evTRflhQUD969NF+WA5AP3r00X5YUUA/evTRflgSVD969NF+WBNEP3r00X5YFFg/e8DHqrBA8A969NF+WBVIP3AC0D57BtA+YEBgD0gAED9IAQA/SAMQP0gEAD9IBRA/SAYAP3qp0CpIAHAPTwJQPnBAkD9QAGAPcEGAP3BgkFtwQICbcAeQP3AIgD9wCZA/cAqAP3ALkD9wDIA/cA2QP3AOgD9wEJA/cBGAP3ASkj9wE4I/cBSUP3AVhD97QMVqqACwD2+CkD56Q9A+SABgD3rx0T5YAFA/WAFAP3100X5awpAuWsOALlrEkC5axYAuevPRfkLCQD5aMJQuWjGELloSlG5aE4RuejLRfnrs0T5aAkA+WiicblopjG5iOBwuYjkMLnox0X566dE+WgJAPnoo0T5CAFAueu3RPloAQC56MNF+eurRPloCQD5669E+WhBYLloRSC567NF+WgRQLloFQC56JtE+eu/RfkLCQD56NdF+Qh5QLnr10X5aH0AueifRPnrq0X5CwkA+euvRfloIUC5aCUAueu7RPloEUC5aBUAueiXRPnrk0T5CwkA+Sh7QLkofwC56ItE+YgKAPnrt0X5aCFAuWglALnru0X5aBFAuWgVALnoh0T5yAgA+Qh7QLkIfwC56H9E+UgIAPnoIEC56CQAuagQQLmoFAC56I9E+SgKAPnoekC56H4AueiDRPkICgD5aCBAuWgkALlIE0C5SBcAudsJAPmoekC5qH4AuYAJAPnoIUC56CUAuSgQQLkoFAC5XAkA+Wg2QLkIBQARaDYAuch6QLnIfgC5PgkA+aghQLmoJQC56MtE+QgFAPHoywT5oar+VP/DLpH9e0mp9E9IqfZXR6n4X0ap+mdFqfxvRKnpI0Nt6ytCbe0zQW3vO8pswANf1gAAAAAAAAAAeyJuYW1lIjogImZhdXN0Z2VuLTEiLCJmaWxlbmFtZSI6ICJmYXVzdGdlbi0xIiwidmVyc2lvbiI6ICIyLjcwLjMiLCJjb21waWxlX29wdGlvbnMiOiAiLWxhbmcgbGx2bSAxNS4wLjcgLWN0IDEgLWVzIDEgLW1jZCAxNiAtbWRkIDEwMjQgLW1keSAzMyAtZG91YmxlIC1mdHogMCIsImxpYnJhcnlfbGlzdCI6IFsiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL3N0ZGZhdXN0LmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9ub2lzZXMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL29zY2lsbGF0b3JzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9tYXRocy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvcGxhdGZvcm0ubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL2Jhc2ljcy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvcGh5c21vZGVscy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvc2lnbmFscy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvc3BhdHMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL2ZpbHRlcnMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL2VudmVsb3Blcy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvcm91dGVzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9kZWxheXMubGliIl0sImluY2x1ZGVfcGF0aG5hbWVzIjogWyIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMiLCIvc2hhcmUvZmF1c3QiLCIvdXNyL2xvY2FsL3NoYXJlL2ZhdXN0IiwiL3Vzci9zaGFyZS9mYXVzdCIsIi4iXSwic2l6ZSI6IDE5OTE3NiwiaW5wdXRzIjogMCwib3V0cHV0cyI6IDIsInNyX2luZGV4IjogMjQsIm1ldGEiOiBbIHsgImJhc2ljcy5saWIvbmFtZSI6ICJGYXVzdCBCYXNpYyBFbGVtZW50IExpYnJhcnkiIH0seyAiYmFzaWNzLmxpYi90YWJ1bGF0ZU5kIjogIkNvcHlyaWdodCAoQykgMjAyMyBCYXJ0IEJyb3VucyA8YmFydF9tYWduZXRvcGhvbi5ubD4iIH0seyAiYmFzaWNzLmxpYi92ZXJzaW9uIjogIjEuMTIuMCIgfSx7ICJjb21waWxlX29wdGlvbnMiOiAiLWxhbmcgbGx2bSAxNS4wLjcgLWN0IDEgLWVzIDEgLW1jZCAxNiAtbWRkIDEwMjQgLW1keSAzMyAtZG91YmxlIC1mdHogMCIgfSx7ICJkZWxheXMubGliL2ZkZWxheTQ6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZGVsYXlzLmxpYi9mZGVsYXlsdHY6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZGVsYXlzLmxpYi9uYW1lIjogIkZhdXN0IERlbGF5IExpYnJhcnkiIH0seyAiZGVsYXlzLmxpYi92ZXJzaW9uIjogIjEuMS4wIiB9LHsgImVudmVsb3Blcy5saWIvYXI6YXV0aG9yIjogIllhbm4gT3JsYXJleSwgU3TDqXBoYW5lIExldHoiIH0seyAiZW52ZWxvcGVzLmxpYi9hdXRob3IiOiAiR1JBTUUiIH0seyAiZW52ZWxvcGVzLmxpYi9jb3B5cmlnaHQiOiAiR1JBTUUiIH0seyAiZW52ZWxvcGVzLmxpYi9saWNlbnNlIjogIkxHUEwgd2l0aCBleGNlcHRpb24iIH0seyAiZW52ZWxvcGVzLmxpYi9uYW1lIjogIkZhdXN0IEVudmVsb3BlIExpYnJhcnkiIH0seyAiZW52ZWxvcGVzLmxpYi92ZXJzaW9uIjogIjEuMy4wIiB9LHsgImZpbGVuYW1lIjogImZhdXN0Z2VuLTEiIH0seyAiZmlsdGVycy5saWIvZmlyOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL2Zpcjpjb3B5cmlnaHQiOiAiQ29weXJpZ2h0IChDKSAyMDAzLTIwMTkgYnkgSnVsaXVzIE8uIFNtaXRoIElJSSA8am9zX2Njcm1hLnN0YW5mb3JkLmVkdT4iIH0seyAiZmlsdGVycy5saWIvZmlyOmxpY2Vuc2UiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi9paXI6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvaWlyOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NfY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi9paXI6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3MwX2hpZ2hwYXNzMSI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NfY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi9sb3dwYXNzMF9oaWdocGFzczE6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvbG93cGFzczphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi9sb3dwYXNzOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NfY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi9sb3dwYXNzOmxpY2Vuc2UiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi9uYW1lIjogIkZhdXN0IEZpbHRlcnMgTGlicmFyeSIgfSx7ICJmaWx0ZXJzLmxpYi90ZjI6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvdGYyOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NfY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi90ZjI6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL3RmMnM6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvdGYyczpjb3B5cmlnaHQiOiAiQ29weXJpZ2h0IChDKSAyMDAzLTIwMTkgYnkgSnVsaXVzIE8uIFNtaXRoIElJSSA8am9zX2Njcm1hLnN0YW5mb3JkLmVkdT4iIH0seyAiZmlsdGVycy5saWIvdGYyczpsaWNlbnNlIjogIk1JVC1zdHlsZSBTVEstNC4zIGxpY2Vuc2UiIH0seyAiZmlsdGVycy5saWIvdmVyc2lvbiI6ICIxLjMuMCIgfSx7ICJtYXRocy5saWIvYXV0aG9yIjogIkdSQU1FIiB9LHsgIm1hdGhzLmxpYi9jb3B5cmlnaHQiOiAiR1JBTUUiIH0seyAibWF0aHMubGliL2xpY2Vuc2UiOiAiTEdQTCB3aXRoIGV4Y2VwdGlvbiIgfSx7ICJtYXRocy5saWIvbmFtZSI6ICJGYXVzdCBNYXRoIExpYnJhcnkiIH0seyAibWF0aHMubGliL3ZlcnNpb24iOiAiMi43LjAiIH0seyAibmFtZSI6ICJmYXVzdGdlbi0xIiB9LHsgIm5vaXNlcy5saWIvbmFtZSI6ICJGYXVzdCBOb2lzZSBHZW5lcmF0b3IgTGlicmFyeSIgfSx7ICJub2lzZXMubGliL3ZlcnNpb24iOiAiMS40LjAiIH0seyAib3NjaWxsYXRvcnMubGliL2xmX3Nhd3BvczphdXRob3IiOiAiQmFydCBCcm91bnMsIHJldmlzZWQgYnkgU3TDqXBoYW5lIExldHoiIH0seyAib3NjaWxsYXRvcnMubGliL2xmX3Nhd3BvczpsaWNlbmNlIjogIlNUSy00LjMiIH0seyAib3NjaWxsYXRvcnMubGliL25hbWUiOiAiRmF1c3QgT3NjaWxsYXRvciBMaWJyYXJ5IiB9LHsgIm9zY2lsbGF0b3JzLmxpYi92ZXJzaW9uIjogIjEuNS4wIiB9LHsgInBoeXNtb2RlbHMubGliL25hbWUiOiAiRmF1c3QgUGh5c2ljYWwgTW9kZWxzIExpYnJhcnkiIH0seyAicGh5c21vZGVscy5saWIvdmVyc2lvbiI6ICIxLjEuMCIgfSx7ICJwbGF0Zm9ybS5saWIvbmFtZSI6ICJHZW5lcmljIFBsYXRmb3JtIExpYnJhcnkiIH0seyAicGxhdGZvcm0ubGliL3ZlcnNpb24iOiAiMS4zLjAiIH0seyAicm91dGVzLmxpYi9uYW1lIjogIkZhdXN0IFNpZ25hbCBSb3V0aW5nIExpYnJhcnkiIH0seyAicm91dGVzLmxpYi92ZXJzaW9uIjogIjEuMi4wIiB9LHsgInNpZ25hbHMubGliL25hbWUiOiAiRmF1c3QgU2lnbmFsIFJvdXRpbmcgTGlicmFyeSIgfSx7ICJzaWduYWxzLmxpYi92ZXJzaW9uIjogIjEuNS4wIiB9LHsgInNwYXRzLmxpYi9uYW1lIjogIkZhdXN0IFNwYXRpYWxpemF0aW9uIExpYnJhcnkiIH0seyAic3BhdHMubGliL3ZlcnNpb24iOiAiMS4xLjAiIH1dLCJ1aSI6IFsgeyJ0eXBlIjogInZncm91cCIsImxhYmVsIjogImZhdXN0Z2VuLTEiLCJpdGVtcyI6IFsgeyJ0eXBlIjogIm5lbnRyeSIsImxhYmVsIjogInJlZmVyZW5jZSIsInNob3J0bmFtZSI6ICJyZWZlcmVuY2UiLCJhZGRyZXNzIjogIi9mYXVzdGdlbi0xL3JlZmVyZW5jZSIsImluZGV4IjogNDQsIm1ldGEiOiBbeyAiMCI6ICIiIH1dLCJpbml0IjogNjQsIm1pbiI6IDAsIm1heCI6IDEyNywic3RlcCI6IDF9LHsidHlwZSI6ICJoc2xpZGVyIiwibGFiZWwiOiAiZmFjdG9yIiwic2hvcnRuYW1lIjogImZhY3RvciIsImFkZHJlc3MiOiAiL2ZhdXN0Z2VuLTEvZmFjdG9yIiwiaW5kZXgiOiA0MzUyLCJtZXRhIjogW3sgIjEiOiAiIiB9XSwiaW5pdCI6IDEsIm1pbiI6IDAuMDEsIm1heCI6IDUwLCJzdGVwIjogMC4wMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJwbHVja1Bvc2l0aW9uIiwic2hvcnRuYW1lIjogInBsdWNrUG9zaXRpb24iLCJhZGRyZXNzIjogIi9mYXVzdGdlbi0xL3BsdWNrUG9zaXRpb24iLCJpbmRleCI6IDM2LCJtZXRhIjogW3sgIjIiOiAiIiB9XSwiaW5pdCI6IDAuNSwibWluIjogMCwibWF4IjogMSwic3RlcCI6IDAuMDF9LHsidHlwZSI6ICJoc2xpZGVyIiwibGFiZWwiOiAiZ2FpbiIsInNob3J0bmFtZSI6ICJnYWluIiwiYWRkcmVzcyI6ICIvZmF1c3RnZW4tMS9nYWluIiwiaW5kZXgiOiA0MjU2LCJtZXRhIjogW3sgIjMiOiAiIiB9XSwiaW5pdCI6IDAuMiwibWluIjogMCwibWF4IjogMSwic3RlcCI6IDAuMDF9LHsidHlwZSI6ICJuZW50cnkiLCJsYWJlbCI6ICJzdGVyZW9TaGlmdCIsInNob3J0bmFtZSI6ICJzdGVyZW9TaGlmdCIsImFkZHJlc3MiOiAiL2ZhdXN0Z2VuLTEvc3RlcmVvU2hpZnQiLCJpbmRleCI6IDAsIm1ldGEiOiBbeyAiNCI6ICIiIH1dLCJpbml0IjogMCwibWluIjogMCwibWF4IjogNywic3RlcCI6IDF9XX1dfQAAAAAAAAAAxAUAAA0AAGzABQAADQAAXZgEAAALAABslAQAAAsAAF1gBAAADAAAbFwEAAAMAABd1AMAAAEAAGzQAwAAAQAAXdgAAAAKAABs1AAAAAoAAF0IAtQB2AEIAtAH1AcIAsALxAsIApQJmAkIAtwI4AgAAOYXAAAOAQAAAAAAAAAAAAABAAAADgIAALBSAAAAAAAA4BcAAA4CAACwUgAAAAAAAKsXAAAPAQAAyAMAAAAAAABhFwAADwEAAAAAAAAAAAAAnRcAAA8BAADcAwAAAAAAAFMXAAAPAQAAzAMAAAAAAAC6FwAADwEAANADAAAAAAAAiRcAAA8BAAAEAAAAAAAAAHEXAAAPAQAAdAEAAAAAAADIFwAAAQAAAAAAAAAAAAAA2hcAAAEAAAAAAAAAAAAAANQXAAABAAAAAAAAAAAAAADPFwAAAQAAAAAAAAAAAAAAAF97Im5hbWUiOiAiZmF1c3RnZW4tMSIsImZpbGVuYW1lIjogImZhdXN0Z2VuLTEiLCJ2ZXJzaW9uIjogIjIuNzAuMyIsImNvbXBpbGVfb3B0aW9ucyI6ICItbGFuZyBsbHZtIDE1LjAuNyAtY3QgMSAtZXMgMSAtbWNkIDE2IC1tZGQgMTAyNCAtbWR5IDMzIC1kb3VibGUgLWZ0eiAwIiwibGlicmFyeV9saXN0IjogWyIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvc3RkZmF1c3QubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL25vaXNlcy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvb3NjaWxsYXRvcnMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL21hdGhzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9wbGF0Zm9ybS5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvYmFzaWNzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9waHlzbW9kZWxzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9zaWduYWxzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9zcGF0cy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvZmlsdGVycy5saWIiLCIvVXNlcnMvZGF2aWRmaWVycm8vRG9jdW1lbnRzL01heCA4L1BhY2thZ2VzL2ZhdXN0Z2VuL2V4dGVybmFscy9tc3AvZmF1c3RnZW5+Lm14by9Db250ZW50cy9SZXNvdXJjZXMvZW52ZWxvcGVzLmxpYiIsIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcy9yb3V0ZXMubGliIiwiL1VzZXJzL2RhdmlkZmllcnJvL0RvY3VtZW50cy9NYXggOC9QYWNrYWdlcy9mYXVzdGdlbi9leHRlcm5hbHMvbXNwL2ZhdXN0Z2Vufi5teG8vQ29udGVudHMvUmVzb3VyY2VzL2RlbGF5cy5saWIiXSwiaW5jbHVkZV9wYXRobmFtZXMiOiBbIi9Vc2Vycy9kYXZpZGZpZXJyby9Eb2N1bWVudHMvTWF4IDgvUGFja2FnZXMvZmF1c3RnZW4vZXh0ZXJuYWxzL21zcC9mYXVzdGdlbn4ubXhvL0NvbnRlbnRzL1Jlc291cmNlcyIsIi9zaGFyZS9mYXVzdCIsIi91c3IvbG9jYWwvc2hhcmUvZmF1c3QiLCIvdXNyL3NoYXJlL2ZhdXN0IiwiLiJdLCJzaXplIjogMTk5MTc2LCJpbnB1dHMiOiAwLCJvdXRwdXRzIjogMiwic3JfaW5kZXgiOiAyNCwibWV0YSI6IFsgeyAiYmFzaWNzLmxpYi9uYW1lIjogIkZhdXN0IEJhc2ljIEVsZW1lbnQgTGlicmFyeSIgfSx7ICJiYXNpY3MubGliL3RhYnVsYXRlTmQiOiAiQ29weXJpZ2h0IChDKSAyMDIzIEJhcnQgQnJvdW5zIDxiYXJ0X21hZ25ldG9waG9uLm5sPiIgfSx7ICJiYXNpY3MubGliL3ZlcnNpb24iOiAiMS4xMi4wIiB9LHsgImNvbXBpbGVfb3B0aW9ucyI6ICItbGFuZyBsbHZtIDE1LjAuNyAtY3QgMSAtZXMgMSAtbWNkIDE2IC1tZGQgMTAyNCAtbWR5IDMzIC1kb3VibGUgLWZ0eiAwIiB9LHsgImRlbGF5cy5saWIvZmRlbGF5NDphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJkZWxheXMubGliL2ZkZWxheWx0djphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJkZWxheXMubGliL25hbWUiOiAiRmF1c3QgRGVsYXkgTGlicmFyeSIgfSx7ICJkZWxheXMubGliL3ZlcnNpb24iOiAiMS4xLjAiIH0seyAiZW52ZWxvcGVzLmxpYi9hcjphdXRob3IiOiAiWWFubiBPcmxhcmV5LCBTdMOpcGhhbmUgTGV0eiIgfSx7ICJlbnZlbG9wZXMubGliL2F1dGhvciI6ICJHUkFNRSIgfSx7ICJlbnZlbG9wZXMubGliL2NvcHlyaWdodCI6ICJHUkFNRSIgfSx7ICJlbnZlbG9wZXMubGliL2xpY2Vuc2UiOiAiTEdQTCB3aXRoIGV4Y2VwdGlvbiIgfSx7ICJlbnZlbG9wZXMubGliL25hbWUiOiAiRmF1c3QgRW52ZWxvcGUgTGlicmFyeSIgfSx7ICJlbnZlbG9wZXMubGliL3ZlcnNpb24iOiAiMS4zLjAiIH0seyAiZmlsZW5hbWUiOiAiZmF1c3RnZW4tMSIgfSx7ICJmaWx0ZXJzLmxpYi9maXI6YXV0aG9yIjogIkp1bGl1cyBPLiBTbWl0aCBJSUkiIH0seyAiZmlsdGVycy5saWIvZmlyOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NfY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi9maXI6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL2lpcjphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi9paXI6Y29weXJpZ2h0IjogIkNvcHlyaWdodCAoQykgMjAwMy0yMDE5IGJ5IEp1bGl1cyBPLiBTbWl0aCBJSUkgPGpvc19jY3JtYS5zdGFuZm9yZC5lZHU+IiB9LHsgImZpbHRlcnMubGliL2lpcjpsaWNlbnNlIjogIk1JVC1zdHlsZSBTVEstNC4zIGxpY2Vuc2UiIH0seyAiZmlsdGVycy5saWIvbG93cGFzczBfaGlnaHBhc3MxIjogIkNvcHlyaWdodCAoQykgMjAwMy0yMDE5IGJ5IEp1bGl1cyBPLiBTbWl0aCBJSUkgPGpvc19jY3JtYS5zdGFuZm9yZC5lZHU+IiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3MwX2hpZ2hwYXNzMTphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi9sb3dwYXNzOmF1dGhvciI6ICJKdWxpdXMgTy4gU21pdGggSUlJIiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3M6Y29weXJpZ2h0IjogIkNvcHlyaWdodCAoQykgMjAwMy0yMDE5IGJ5IEp1bGl1cyBPLiBTbWl0aCBJSUkgPGpvc19jY3JtYS5zdGFuZm9yZC5lZHU+IiB9LHsgImZpbHRlcnMubGliL2xvd3Bhc3M6bGljZW5zZSI6ICJNSVQtc3R5bGUgU1RLLTQuMyBsaWNlbnNlIiB9LHsgImZpbHRlcnMubGliL25hbWUiOiAiRmF1c3QgRmlsdGVycyBMaWJyYXJ5IiB9LHsgImZpbHRlcnMubGliL3RmMjphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi90ZjI6Y29weXJpZ2h0IjogIkNvcHlyaWdodCAoQykgMjAwMy0yMDE5IGJ5IEp1bGl1cyBPLiBTbWl0aCBJSUkgPGpvc19jY3JtYS5zdGFuZm9yZC5lZHU+IiB9LHsgImZpbHRlcnMubGliL3RmMjpsaWNlbnNlIjogIk1JVC1zdHlsZSBTVEstNC4zIGxpY2Vuc2UiIH0seyAiZmlsdGVycy5saWIvdGYyczphdXRob3IiOiAiSnVsaXVzIE8uIFNtaXRoIElJSSIgfSx7ICJmaWx0ZXJzLmxpYi90ZjJzOmNvcHlyaWdodCI6ICJDb3B5cmlnaHQgKEMpIDIwMDMtMjAxOSBieSBKdWxpdXMgTy4gU21pdGggSUlJIDxqb3NfY2NybWEuc3RhbmZvcmQuZWR1PiIgfSx7ICJmaWx0ZXJzLmxpYi90ZjJzOmxpY2Vuc2UiOiAiTUlULXN0eWxlIFNUSy00LjMgbGljZW5zZSIgfSx7ICJmaWx0ZXJzLmxpYi92ZXJzaW9uIjogIjEuMy4wIiB9LHsgIm1hdGhzLmxpYi9hdXRob3IiOiAiR1JBTUUiIH0seyAibWF0aHMubGliL2NvcHlyaWdodCI6ICJHUkFNRSIgfSx7ICJtYXRocy5saWIvbGljZW5zZSI6ICJMR1BMIHdpdGggZXhjZXB0aW9uIiB9LHsgIm1hdGhzLmxpYi9uYW1lIjogIkZhdXN0IE1hdGggTGlicmFyeSIgfSx7ICJtYXRocy5saWIvdmVyc2lvbiI6ICIyLjcuMCIgfSx7ICJuYW1lIjogImZhdXN0Z2VuLTEiIH0seyAibm9pc2VzLmxpYi9uYW1lIjogIkZhdXN0IE5vaXNlIEdlbmVyYXRvciBMaWJyYXJ5IiB9LHsgIm5vaXNlcy5saWIvdmVyc2lvbiI6ICIxLjQuMCIgfSx7ICJvc2NpbGxhdG9ycy5saWIvbGZfc2F3cG9zOmF1dGhvciI6ICJCYXJ0IEJyb3VucywgcmV2aXNlZCBieSBTdMOpcGhhbmUgTGV0eiIgfSx7ICJvc2NpbGxhdG9ycy5saWIvbGZfc2F3cG9zOmxpY2VuY2UiOiAiU1RLLTQuMyIgfSx7ICJvc2NpbGxhdG9ycy5saWIvbmFtZSI6ICJGYXVzdCBPc2NpbGxhdG9yIExpYnJhcnkiIH0seyAib3NjaWxsYXRvcnMubGliL3ZlcnNpb24iOiAiMS41LjAiIH0seyAicGh5c21vZGVscy5saWIvbmFtZSI6ICJGYXVzdCBQaHlzaWNhbCBNb2RlbHMgTGlicmFyeSIgfSx7ICJwaHlzbW9kZWxzLmxpYi92ZXJzaW9uIjogIjEuMS4wIiB9LHsgInBsYXRmb3JtLmxpYi9uYW1lIjogIkdlbmVyaWMgUGxhdGZvcm0gTGlicmFyeSIgfSx7ICJwbGF0Zm9ybS5saWIvdmVyc2lvbiI6ICIxLjMuMCIgfSx7ICJyb3V0ZXMubGliL25hbWUiOiAiRmF1c3QgU2lnbmFsIFJvdXRpbmcgTGlicmFyeSIgfSx7ICJyb3V0ZXMubGliL3ZlcnNpb24iOiAiMS4yLjAiIH0seyAic2lnbmFscy5saWIvbmFtZSI6ICJGYXVzdCBTaWduYWwgUm91dGluZyBMaWJyYXJ5IiB9LHsgInNpZ25hbHMubGliL3ZlcnNpb24iOiAiMS41LjAiIH0seyAic3BhdHMubGliL25hbWUiOiAiRmF1c3QgU3BhdGlhbGl6YXRpb24gTGlicmFyeSIgfSx7ICJzcGF0cy5saWIvdmVyc2lvbiI6ICIxLjEuMCIgfV0sInVpIjogWyB7InR5cGUiOiAidmdyb3VwIiwibGFiZWwiOiAiZmF1c3RnZW4tMSIsIml0ZW1zIjogWyB7InR5cGUiOiAibmVudHJ5IiwibGFiZWwiOiAicmVmZXJlbmNlIiwic2hvcnRuYW1lIjogInJlZmVyZW5jZSIsImFkZHJlc3MiOiAiL2ZhdXN0Z2VuLTEvcmVmZXJlbmNlIiwiaW5kZXgiOiA0NCwibWV0YSI6IFt7ICIwIjogIiIgfV0sImluaXQiOiA2NCwibWluIjogMCwibWF4IjogMTI3LCJzdGVwIjogMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJmYWN0b3IiLCJzaG9ydG5hbWUiOiAiZmFjdG9yIiwiYWRkcmVzcyI6ICIvZmF1c3RnZW4tMS9mYWN0b3IiLCJpbmRleCI6IDQzNTIsIm1ldGEiOiBbeyAiMSI6ICIiIH1dLCJpbml0IjogMSwibWluIjogMC4wMSwibWF4IjogNTAsInN0ZXAiOiAwLjAxfSx7InR5cGUiOiAiaHNsaWRlciIsImxhYmVsIjogInBsdWNrUG9zaXRpb24iLCJzaG9ydG5hbWUiOiAicGx1Y2tQb3NpdGlvbiIsImFkZHJlc3MiOiAiL2ZhdXN0Z2VuLTEvcGx1Y2tQb3NpdGlvbiIsImluZGV4IjogMzYsIm1ldGEiOiBbeyAiMiI6ICIiIH1dLCJpbml0IjogMC41LCJtaW4iOiAwLCJtYXgiOiAxLCJzdGVwIjogMC4wMX0seyJ0eXBlIjogImhzbGlkZXIiLCJsYWJlbCI6ICJnYWluIiwic2hvcnRuYW1lIjogImdhaW4iLCJhZGRyZXNzIjogIi9mYXVzdGdlbi0xL2dhaW4iLCJpbmRleCI6IDQyNTYsIm1ldGEiOiBbeyAiMyI6ICIiIH1dLCJpbml0IjogMC4yLCJtaW4iOiAwLCJtYXgiOiAxLCJzdGVwIjogMC4wMX0seyJ0eXBlIjogIm5lbnRyeSIsImxhYmVsIjogInN0ZXJlb1NoaWZ0Iiwic2hvcnRuYW1lIjogInN0ZXJlb1NoaWZ0IiwiYWRkcmVzcyI6ICIvZmF1c3RnZW4tMS9zdGVyZW9TaGlmdCIsImluZGV4IjogMCwibWV0YSI6IFt7ICI0IjogIiIgfV0sImluaXQiOiAwLCJtaW4iOiAwLCJtYXgiOiA3LCJzdGVwIjogMX1dfV19AF9kZXN0cm95bXlkc3AAX2NsYXNzSW5pdG15ZHNwAF9pbnN0YW5jZUNvbnN0YW50c215ZHNwAF9pbnN0YW5jZUNsZWFybXlkc3AAX2NvbXB1dGVteWRzcABfYWxsb2NhdGVteWRzcABfZ2V0SlNPTm15ZHNwAF9iemVybwBfdGFuAF9mbW9kAF9leHAyAGx0bXAxAGx0bXAwAAAAAAA=",
					"machinecode_size" : 45356,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "", "" ],
					"patching_rect" : [ 1387.280934866304733, 270.698582404373155, 62.0, 22.0 ],
					"sample_format" : 1,
					"serial_number" : "YJ2C099NJM64 bits",
					"sourcecode" : "import(\"stdfaust.lib\");\nn = 8;\ncoeff = (1, 4/3, 2/3, 4/5, 4/7, 4/9, 3/4, 1/2);\n//string length multipliers are:\n//1 for the fundamental\n//4/3 for the fourth below\n//2/3 for the perfect fifth above\n//4/5 for the perfect third above\n//4/7 for the seventh above\n//4/9 for the ninth above\n//3/4 for the perfect fourth above\n//1/2 for the octave above\nlength = 1.023; //E note\nlengthChange = pow(2, (64 - nentry(\"[0]reference\", 64, 0, 127, 1)) / 48);\nfactor = hslider(\"[1]factor\", 1, 0.01, 50, 0.01);\npluckPosition = hslider(\"[2]pluckPosition\", 0.5, 0, 1, 0.01);\ngain = hslider(\"[3]gain\", 0.2, 0, 1, 0.01) : si.smoo;\r\nstereoShift = nentry(\"[4]stereoShift\", 0, 0, n-1, 1);\ntoStereo(n, p) = par(i, n, sp.panner(1 - fmod(i+p, n) / (n-1))) :> (_, _);\nprocess = par(i, n, (no.sparse_noise(factor*(1+i*0.01)) : pm.guitar(length*ba.take(i+1, coeff)*lengthChange, pluckPosition, gain))) : toStereo(n, stereoShift);",
					"sourcecode_size" : 901,
					"text" : "faustgen~",
					"varname" : "faustgen-4613079200",
					"version" : "1.68"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1393.614268199637991, 1351.130077439067463, 79.0, 20.0 ],
					"text" : "record output"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1360.614268199637991, 1349.130077439067463, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1360.614268199637991, 1377.181216644283268, 67.0, 22.0 ],
					"text" : "open wave"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1360.614268199637991, 1413.181216644283268, 88.0, 22.0 ],
					"text" : "mc.sfrecord~ 7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1226.199999928474426, 239.698582404373155, 49.0, 22.0 ],
					"text" : "r speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 700.0, 213.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-24",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 288.666666666666515, 120.0, 69.0, 22.0 ],
									"text" : "r decFactor"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 50.0, 364.0, 134.714285714285325, 22.0 ],
									"text" : "mc.pack~ 7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 7,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 50.0, 304.0, 154.0, 22.0 ],
									"text" : "mc.unpack~ 7"
								}

							}
, 							{
								"box" : 								{
									"hidden" : 1,
									"id" : "obj-103",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 106.0, 97.0, 22.0 ],
									"text" : "loadmess 96000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-104",
									"maxclass" : "number",
									"maximum" : 262144,
									"minimum" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 171.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-102",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 211.0, 54.0, 22.0 ],
									"text" : "delay $1"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-14",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 170.428558000000066, 211.0, 90.0, 22.0 ],
									"text" : "functiontype $1"
								}

							}
, 							{
								"box" : 								{
									"hidden" : 1,
									"id" : "obj-25",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 170.428558000000066, 100.0, 70.0, 22.0 ],
									"text" : "loadmess 0"
								}

							}
, 							{
								"box" : 								{
									"hidden" : 1,
									"id" : "obj-15",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 112.928558000000066, 130.0, 73.0, 22.0 ],
									"text" : "loadmess 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"items" : [ "x", ",", "x^2", ",", "sin", ",", "log(1+x)", ",", "sqrt(x)", ",", "1-cos(Pi/2*x)", ",", "(1-cos(Pi*x))/2", ",", "1-(1-x)^2", ",", "composite1", ",", "x^3", ",", "1-(1-x)^3", ",", "composite2", ",", "x^4", ",", "1-(1-x)^4", ",", "composite3", ",", "x^5", ",", "1-(1-x)^5", ",", "composite4", ",", "2^(10(x-1))", ",", "composite5", ",", "1-sqrt(1-x^2)", ",", "sqrt(1-(x-1)^2)" ],
									"maxclass" : "umenu",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "", "" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 170.428558000000066, 171.0, 109.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 112.928558000000066, 211.0, 48.0, 22.0 ],
									"text" : "fdbk $1"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-16",
									"maxclass" : "flonum",
									"maximum" : 0.999,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 112.928558000000066, 171.0, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-17",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 288.666666666666515, 211.0, 55.0, 22.0 ],
									"text" : "factor $1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 30.0,
									"format" : 6,
									"id" : "obj-67",
									"maxclass" : "flonum",
									"maximum" : 1.0,
									"minimum" : 0.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 288.666666666666515, 160.0, 117.0, 42.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 7,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 50.0, 334.0, 154.0, 22.0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0,
										"parameter_mappable" : 0
									}
,
									"text" : "abc_2d_fx_decorrelation3~"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-47",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-48",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 446.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-102", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-104", 0 ],
									"hidden" : 1,
									"source" : [ "obj-103", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-102", 0 ],
									"source" : [ "obj-104", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"hidden" : 1,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 5 ],
									"source" : [ "obj-18", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 4 ],
									"source" : [ "obj-18", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 3 ],
									"source" : [ "obj-18", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 2 ],
									"source" : [ "obj-18", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 1 ],
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-48", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-14", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"hidden" : 1,
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 6 ],
									"source" : [ "obj-3", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 5 ],
									"source" : [ "obj-3", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 4 ],
									"source" : [ "obj-3", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 3 ],
									"source" : [ "obj-3", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 2 ],
									"source" : [ "obj-3", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 1 ],
									"source" : [ "obj-3", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-47", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"source" : [ "obj-67", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1226.199999928474426, 501.505399673698207, 88.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p decorrelation"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-123",
					"maxclass" : "flonum",
					"maximum" : 0.0,
					"minimum" : -127.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1420.542839911694955, 1264.79858075928496, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1420.542839911694955, 1236.863409715411763, 93.0, 22.0 ],
					"text" : "r masterVolume"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1420.542839911694955, 1291.798580759285187, 48.0, 22.0 ],
					"text" : "gain $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1299.614268483123396, 1069.000000953674316, 51.0, 22.0 ],
					"text" : "r drywet"
				}

			}
, 			{
				"box" : 				{
					"args" : [ -127 ],
					"bgmode" : 0,
					"border" : 1,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-132",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "abc_gaincontrol.maxpat",
					"numinlets" : 0,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "" ],
					"patching_rect" : [ 1155.199999928474426, 1133.430094842727158, 72.0, 110.0 ],
					"presentation" : 1,
					"presentation_rect" : [ -45.000005424022675, 100.498582177875505, 72.0, 110.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1192.449999928474426, 268.698582404373155, 29.5, 22.0 ],
					"text" : "0."
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"id" : "obj-134",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1277.699999928474426, 385.698582404373155, 63.0, 20.0 ],
					"text" : "t.s-1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1226.199999928474426, 268.698582404373155, 80.0, 22.0 ],
					"text" : "loadmess 0.1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-142",
					"maxclass" : "flonum",
					"maximum" : 100.0,
					"minimum" : -100.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1226.199999928474426, 309.698582404373155, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
					"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
					"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
					"bgfillcolor_proportion" : 0.5,
					"bgfillcolor_type" : "color",
					"gradient" : 1,
					"id" : "obj-143",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1226.199999928474426, 420.698582404373155, 58.0, 22.0 ],
					"text" : "speed $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1275.280940443942654, 1203.863410131747969, 87.0, 22.0 ],
					"text" : "loadmess 0.96"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-77",
									"maxclass" : "number",
									"maximum" : 1000,
									"minimum" : 0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 91.428571428571502, 84.999997322116826, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"id" : "obj-80",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 143.428571428571502, 84.999997322116826, 63.0, 20.0 ],
									"text" : "(msec)"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-83",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 91.428571428571502, 43.999997322116826, 77.0, 22.0 ],
									"text" : "loadmess 20"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-84",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 91.428571428571502, 119.999997322116826, 79.0, 22.0 ],
									"text" : "returntime $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 50.0, 276.333328664302826, 118.428571428571502, 22.0 ],
									"text" : "mc.pack~ 7"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 1,
									"fontsize" : 14.0,
									"id" : "obj-38",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 7,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 50.000000000000114, 218.0, 135.0, 24.0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0,
										"parameter_mappable" : 0
									}
,
									"text" : "abc_2d_encoder3~",
									"textcolor" : [ 0.968627450980392, 0.968627450980392, 0.968627450980392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 49.999992716514612, 39.999997322116826, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-26",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 49.999992716514612, 358.333317322116841, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 6 ],
									"source" : [ "obj-38", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 5 ],
									"source" : [ "obj-38", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 4 ],
									"source" : [ "obj-38", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 3 ],
									"source" : [ "obj-38", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 2 ],
									"source" : [ "obj-38", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"source" : [ "obj-38", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-38", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-26", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"source" : [ "obj-77", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-77", 0 ],
									"source" : [ "obj-83", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-38", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1226.199999928474426, 463.514544080981977, 67.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p encoding"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 1267.0, 723.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-45",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 133.833305716514587, 434.133337581157775, 54.0, 22.0 ],
									"text" : "mc.dac~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-44",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 133.833305716514587, 362.0, 70.0, 22.0 ],
									"text" : "mc.pack~ 4"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 8,
									"numoutlets" : 0,
									"patching_rect" : [ 138.83332747220993, 285.533342385292144, 134.0, 22.0 ],
									"text" : "dac~ 1 2 3 4 5 6 7 8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 274.83332747220993, 238.0, 82.0, 22.0 ],
									"text" : "mc.pack~ 7"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 274.83332747220993, 313.533342385292144, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-30",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 138.83332747220993, 141.795652151107788, 40.0, 22.0 ],
									"text" : "mc.*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 71.0, 100.0, 29.5, 22.0 ],
									"text" : "!- 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 50.0, 141.795652151107788, 40.0, 22.0 ],
									"text" : "mc.*~"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 7,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 50.0, 167.295644342899323, 84.0, 22.0 ],
									"text" : "mc.unpack~ 7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 7,
									"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 138.83332747220993, 167.295644342899323, 129.285714285714221, 22.0 ],
									"text" : "mc.unpack~ 7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 7,
									"numoutlets" : 4,
									"outlettype" : [ "signal", "signal", "signal", "signal" ],
									"patching_rect" : [ 138.83332747220993, 238.0, 125.0, 22.0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0,
										"parameter_mappable" : 0
									}
,
									"text" : "abc_2d_decoder3_4~"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-13",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 49.999992716514612, 39.999989583663933, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-15",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 98.916679716514636, 39.999989583663933, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-17",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "multichannelsignal" ],
									"patching_rect" : [ 138.833305716514587, 39.999989583663933, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-20",
									"index" : 4,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 173.833305716514587, 39.999989583663933, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 0 ],
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"order" : 1,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 1 ],
									"order" : 0,
									"source" : [ "obj-15", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-30", 0 ],
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 6 ],
									"order" : 1,
									"source" : [ "obj-18", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 5 ],
									"order" : 1,
									"source" : [ "obj-18", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 4 ],
									"order" : 1,
									"source" : [ "obj-18", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 3 ],
									"order" : 1,
									"source" : [ "obj-18", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 2 ],
									"order" : 1,
									"source" : [ "obj-18", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"order" : 1,
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 1,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 6 ],
									"order" : 0,
									"source" : [ "obj-18", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 5 ],
									"order" : 0,
									"source" : [ "obj-18", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 4 ],
									"order" : 0,
									"source" : [ "obj-18", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 3 ],
									"order" : 0,
									"source" : [ "obj-18", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 2 ],
									"order" : 0,
									"source" : [ "obj-18", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-18", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-28", 1 ],
									"source" : [ "obj-29", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-30", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 6 ],
									"order" : 1,
									"source" : [ "obj-4", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 5 ],
									"order" : 1,
									"source" : [ "obj-4", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 4 ],
									"order" : 1,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 3 ],
									"order" : 1,
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 2 ],
									"order" : 1,
									"source" : [ "obj-4", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 1 ],
									"order" : 1,
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 1,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 6 ],
									"order" : 0,
									"source" : [ "obj-4", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 5 ],
									"order" : 0,
									"source" : [ "obj-4", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 4 ],
									"order" : 0,
									"source" : [ "obj-4", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 3 ],
									"order" : 0,
									"source" : [ "obj-4", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 2 ],
									"order" : 0,
									"source" : [ "obj-4", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 1 ],
									"order" : 0,
									"source" : [ "obj-4", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-45", 0 ],
									"source" : [ "obj-44", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 4 ],
									"source" : [ "obj-6", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 5 ],
									"source" : [ "obj-6", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 1 ],
									"source" : [ "obj-6", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1360.614268199637991, 1236.863409715411763, 52.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p output",
					"varname" : "patcher[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1355.614268483123396, 1139.432955164195391, 52.0, 20.0 ],
					"text" : "Dry/Wet"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-148",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1426.542839911694955, 1172.188042001540907, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-149",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1426.542839911694955, 1203.863410131747969, 59.0, 22.0 ],
					"text" : "stereo $1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-157",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1318.614268483123396, 1163.063409523780592, 50.0, 22.0 ],
					"varname" : "drywet[1]"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "instr" ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-160",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 1405.614268199637991, 495.726187717914286, 194.0, 564.840859025238956 ],
					"presentation" : 1,
					"presentation_rect" : [ 597.084170819357382, 619.230281195762245, 194.0, 564.840859025238956 ],
					"varname" : "bbdmi_live.granulator~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-275",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 559.399928778409958, 157.015226643799451, 48.0, 22.0 ],
					"text" : "del 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-274",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 559.399928778409958, 130.615225499390363, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-171",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 559.399928778409958, 207.948585468052443, 65.0, 22.0 ],
					"text" : "PRESET"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-173",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 601.260341376066208, 241.857281165836412, 39.0, 21.0 ],
					"text" : "store",
					"texton" : "store",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-225",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 559.399928778409958, 241.857281165836412, 39.0, 21.0 ],
					"text" : "recall",
					"texton" : "recall",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-175",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 675.899928778409958, 352.274672466038282, 66.0, 21.0 ],
					"text" : "writeagain",
					"texton" : "writeagain",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-226",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 631.149928778409958, 352.274672466038282, 39.0, 21.0 ],
					"text" : "write",
					"texton" : "write",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-177",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 36.0, 79.0, 228.0, 264.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-24",
									"index" : 3,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 114.357142857142833, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 69.357142857142833, 66.5, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 11.357142857142833, 66.5, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 11.357142857142833, 143.0, 54.0, 22.0 ],
									"text" : "recall $1"
								}

							}
, 							{
								"box" : 								{
									"color" : [ 1.0, 0.61176472902298, 0.61176472902298, 1.0 ],
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 147.799525669642833, 67.5, 70.0, 22.0 ],
									"text" : "loadmess 1"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-64",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 11.357142857142833, 114.5, 39.0, 22.0 ],
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 69.357142857142833, 114.5, 40.5, 22.0 ],
									"text" : "i"
								}

							}
, 							{
								"box" : 								{
									"fontface" : 0,
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-68",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 69.357142857142833, 143.0, 52.0, 22.0 ],
									"text" : "store $1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-17",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 10.5, 218.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-16",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 69.357142857142833, 12.5, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-14",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 11.357142857142833, 12.5, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"midpoints" : [ 20.857142857142833, 45.0, 20.857142857142833, 45.0 ],
									"source" : [ "obj-14", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"midpoints" : [ 78.857142857142833, 45.0, 78.857142857142833, 45.0 ],
									"source" : [ "obj-16", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 20.857142857142833, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-19", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"midpoints" : [ 20.857142857142833, 93.0, 20.857142857142833, 93.0 ],
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"midpoints" : [ 78.857142857142833, 93.0, 78.857142857142833, 93.0 ],
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 1 ],
									"midpoints" : [ 123.857142857142833, 99.0, 40.857142857142833, 99.0 ],
									"order" : 1,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 1 ],
									"midpoints" : [ 123.857142857142833, 99.0, 100.357142857142833, 99.0 ],
									"order" : 0,
									"source" : [ "obj-24", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 1 ],
									"midpoints" : [ 157.299525669642833, 99.0, 40.857142857142833, 99.0 ],
									"order" : 1,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 1 ],
									"midpoints" : [ 157.299525669642833, 99.0, 100.357142857142833, 99.0 ],
									"order" : 0,
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"midpoints" : [ 20.857142857142833, 138.0, 20.857142857142833, 138.0 ],
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-68", 0 ],
									"midpoints" : [ 78.857142857142833, 138.0, 78.857142857142833, 138.0 ],
									"source" : [ "obj-66", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"midpoints" : [ 78.857142857142833, 204.0, 20.0, 204.0 ],
									"source" : [ "obj-68", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 559.399928778409958, 320.448585468051988, 101.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p settings"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-179",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 755.399928778409958, 352.274672466038282, 39.0, 21.0 ],
					"text" : "clear",
					"texton" : "clear",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "incdec",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 642.260341376066208, 241.857281165836412, 41.0, 21.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ 1 ],
							"parameter_initial_enable" : 1,
							"parameter_invisible" : 1,
							"parameter_longname" : "incdec",
							"parameter_modmode" : 0,
							"parameter_shortname" : "incdec",
							"parameter_type" : 3
						}

					}
,
					"varname" : "incdec[1]"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-228",
					"maxclass" : "number",
					"minimum" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 642.260341376066208, 270.835541921375352, 40.0, 22.0 ],
					"triangle" : 0,
					"varname" : "number[1]"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.137254901960784, 0.137254901960784, 0.137254901960784, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-182",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 586.399928778409958, 352.274672466038282, 39.0, 21.0 ],
					"text" : "read",
					"texton" : "read",
					"textoncolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-229",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 36.0, 79.0, 160.0, 238.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "Default Max 7",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-85",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 33.25, 152.5, 120.0, 22.0 ],
									"text" : "prepend pattrstorage"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-78",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 14.0, 76.0, 63.0, 22.0 ],
									"text" : "route read"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-66",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 5,
									"outlettype" : [ "", "", "", "", "" ],
									"patching_rect" : [ 14.0, 125.384386777777763, 96.0, 22.0 ],
									"text" : "regexp (.+)\\\\..+$"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-72",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "int" ],
									"patching_rect" : [ 14.0, 101.384386777777763, 57.0, 22.0 ],
									"text" : "strippath"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-86",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 14.000000023437501, 16.000012222222267, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-87",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 33.250000023437494, 192.500012222222267, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-85", 0 ],
									"source" : [ "obj-66", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"source" : [ "obj-72", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-72", 0 ],
									"source" : [ "obj-78", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"source" : [ "obj-85", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-78", 0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 559.399928778409958, 439.6529333169442, 52.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p preset"
				}

			}
, 			{
				"box" : 				{
					"bubblesize" : 20,
					"embed" : 0,
					"id" : "obj-230",
					"maxclass" : "preset",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "preset", "int", "preset", "int", "" ],
					"patching_rect" : [ 559.399928801847636, 468.448585468051988, 196.999999976562549, 29.000000000000114 ]
				}

			}
, 			{
				"box" : 				{
					"autorestore" : "preset.json",
					"id" : "obj-231",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 559.399928778409958, 396.948585468051988, 235.000000000000114, 35.0 ],
					"priority" : 					{
						"bbdmi.multislider[3]::Multislider" : 1,
						"bbdmi.multislider[2]::Multislider" : 1,
						"bbdmi.crosspatch[1]::Crosspatch" : 1
					}
,
					"saved_object_attributes" : 					{
						"client_rect" : [ 100, 100, 500, 600 ],
						"parameter_enable" : 0,
						"parameter_mappable" : 0,
						"storage_rect" : [ 200, 200, 800, 500 ]
					}
,
					"text" : "pattrstorage amelia @savemode 1 @autorestore 1 @changemode 1",
					"varname" : "amelia"
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-207",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 550.899928790128797, 199.448585468052443, 256.999999976562549, 305.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-119",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 550.899928790128797, 517.000000953674316, 312.0, 336.698581450698839 ],
					"presentation" : 1,
					"presentation_rect" : [ 821.166667699813843, 649.230281195762245, 152.0, 267.0 ],
					"proportion" : 0.5,
					"rounded" : 0
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 1 ],
					"midpoints" : [ 792.699999928474426, 586.000000953674316, 692.649928778409958, 586.000000953674316 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-100", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"source" : [ "obj-100", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"midpoints" : [ 610.649928778409958, 802.000000953674316, 610.649928778409958, 802.000000953674316 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"midpoints" : [ 518.750071078538895, 1459.000000953674316, 405.696765080094451, 1459.000000953674316 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"midpoints" : [ 578.750071078538895, 1459.000000953674316, 691.696765080094451, 1459.000000953674316 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"midpoints" : [ 610.649928778409958, 733.000000953674316, 610.649928778409958, 733.000000953674316 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 2 ],
					"midpoints" : [ 548.696765080094451, 1525.000000953674316, 405.696765080094451, 1525.000000953674316 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 2 ],
					"midpoints" : [ 691.696765080094451, 1525.000000953674316, 405.696765080094451, 1525.000000953674316 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"order" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"order" : 1,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"source" : [ "obj-125", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 3 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 3 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-133", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 2 ],
					"order" : 1,
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 0 ],
					"order" : 0,
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 1601.780934866304733, 254.698582404373155, 1396.780934866304733, 254.698582404373155 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"midpoints" : [ 1284.780940443942654, 1236.705442147967915, 1355.447573916152578, 1236.705442147967915, 1355.447573916152578, 1227.705442147967915, 1367.447573916152578, 1227.705442147967915, 1367.447573916152578, 1200.705442147967915, 1328.114268483123396, 1200.705442147967915 ],
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-146", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"midpoints" : [ 1436.042839911694955, 1197.705442147967915, 1436.042839911694955, 1197.705442147967915 ],
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 3 ],
					"midpoints" : [ 1436.042839911694955, 1227.705442147967915, 1403.114268199637991, 1227.705442147967915 ],
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 1 ],
					"midpoints" : [ 1328.114268483123396, 1227.705442147967915, 1381.114268199637991, 1227.705442147967915 ],
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 1 ],
					"source" : [ "obj-160", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 0 ],
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 1 ],
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"order" : 2,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"order" : 1,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"order" : 0,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 2 ],
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 1 ],
					"order" : 0,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 1,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 1485.780934866304733, 254.698582404373155, 1396.780934866304733, 254.698582404373155 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-172", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 1 ],
					"midpoints" : [ 610.760341376066208, 264.448585468051988, 609.899928778409958, 264.448585468051988 ],
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 708.899928778409958, 375.448585468052443, 568.899928778409958, 375.448585468052443 ],
					"source" : [ "obj-175", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 568.899928778409958, 342.448585468052443, 568.899928778409958, 342.448585468052443 ],
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 774.899928778409958, 375.448585468052443, 568.899928778409958, 375.448585468052443 ],
					"source" : [ "obj-179", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 605.899928778409958, 375.448585468052443, 568.899928778409958, 375.448585468052443 ],
					"source" : [ "obj-182", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 9 ],
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 0 ],
					"source" : [ "obj-198", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 8 ],
					"source" : [ "obj-199", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 1804.78093486630496, 254.698582404373155, 1396.780934866304733, 254.698582404373155 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 7 ],
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 6 ],
					"source" : [ "obj-202", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 5 ],
					"source" : [ "obj-203", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 4 ],
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 3 ],
					"source" : [ "obj-206", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 2 ],
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 1 ],
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 0 ],
					"order" : 0,
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 1 ],
					"order" : 1,
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 0 ],
					"source" : [ "obj-214", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 3 ],
					"source" : [ "obj-216", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 0 ],
					"midpoints" : [ 568.899928778409958, 264.448585468051988, 568.899928778409958, 264.448585468051988 ],
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"midpoints" : [ 650.649928778409958, 375.448585468052443, 568.899928778409958, 375.448585468052443 ],
					"source" : [ "obj-226", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-228", 0 ],
					"midpoints" : [ 651.760341376066208, 264.448585468051988, 651.760341376066208, 264.448585468051988 ],
					"source" : [ "obj-227", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 2 ],
					"midpoints" : [ 651.760341376066208, 315.448585468052443, 650.899928778409958, 315.448585468052443 ],
					"order" : 0,
					"source" : [ "obj-228", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-227", 0 ],
					"midpoints" : [ 651.760341376066208, 303.448585468052443, 693.399928754972279, 303.448585468052443, 693.399928754972279, 228.448585468052443, 651.760341376066208, 228.448585468052443 ],
					"order" : 1,
					"source" : [ "obj-228", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 0 ],
					"midpoints" : [ 568.899928778409958, 462.448585468052443, 568.899928801847636, 462.448585468052443 ],
					"source" : [ "obj-229", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"midpoints" : [ 610.649928778409958, 655.000000953674316, 610.649928778409958, 655.000000953674316 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-229", 0 ],
					"midpoints" : [ 568.899928778409958, 432.448585468052443, 568.899928778409958, 432.448585468052443 ],
					"source" : [ "obj-231", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-240", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 2 ],
					"order" : 1,
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"order" : 0,
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"midpoints" : [ 692.649928778409958, 655.000000953674316, 692.649928778409958, 655.000000953674316 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"source" : [ "obj-250", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"source" : [ "obj-252", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-298", 0 ],
					"order" : 1,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 1 ],
					"midpoints" : [ 133.366668283939362, 835.000000953674316, 199.199999928474426, 835.000000953674316, 199.199999928474426, 787.000000953674316, 253.199999928474426, 787.000000953674316, 253.199999928474426, 694.000000953674316, 541.199999928474426, 694.000000953674316, 541.199999928474426, 559.000000953674316, 580.699999928474426, 559.000000953674316 ],
					"order" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"order" : 0,
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 1 ],
					"order" : 1,
					"source" : [ "obj-260", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-252", 0 ],
					"source" : [ "obj-261", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"source" : [ "obj-269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"source" : [ "obj-27", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 0 ],
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"source" : [ "obj-272", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-275", 0 ],
					"source" : [ "obj-274", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"source" : [ "obj-275", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 1 ],
					"source" : [ "obj-288", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"source" : [ "obj-292", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-298", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"midpoints" : [ 1698.780934866304733, 254.698582404373155, 1396.780934866304733, 254.698582404373155 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 1 ],
					"midpoints" : [ 692.649928778409958, 733.000000953674316, 692.649928778409958, 733.000000953674316 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-250", 0 ],
					"order" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"order" : 1,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 2 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"midpoints" : [ 610.649928778409958, 613.000000953674316, 610.649928778409958, 613.000000953674316 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"midpoints" : [ 692.649928778409958, 613.000000953674316, 692.649928778409958, 613.000000953674316 ],
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-259", 0 ],
					"order" : 1,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"order" : 0,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 1 ],
					"midpoints" : [ 692.649928778409958, 580.000000953674316, 692.649928778409958, 580.000000953674316 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-143", 0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"midpoints" : [ 637.963439717888946, 1462.000000953674316, 548.696765080094451, 1462.000000953674316 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 2 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"midpoints" : [ 610.649928778409958, 769.000000953674316, 610.649928778409958, 769.000000953674316 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 1 ],
					"midpoints" : [ 706.649928778409958, 697.000000953674316, 624.649928778409958, 697.000000953674316 ],
					"order" : 1,
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 1 ],
					"midpoints" : [ 706.649928778409958, 688.000000953674316, 706.649928778409958, 688.000000953674316 ],
					"order" : 0,
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-12::obj-107::obj-33" : [ "tab[110]", "tab[1]", 0 ],
			"obj-12::obj-123::obj-33" : [ "tab[108]", "tab[1]", 0 ],
			"obj-12::obj-34::obj-33" : [ "tab[111]", "tab[1]", 0 ],
			"obj-12::obj-36::obj-33" : [ "tab[112]", "tab[1]", 0 ],
			"obj-12::obj-40::obj-33" : [ "tab[117]", "tab[1]", 0 ],
			"obj-12::obj-41::obj-33" : [ "tab[118]", "tab[1]", 0 ],
			"obj-12::obj-42::obj-33" : [ "tab[119]", "tab[1]", 0 ],
			"obj-12::obj-43::obj-33" : [ "tab[120]", "tab[1]", 0 ],
			"obj-12::obj-44::obj-33" : [ "tab[121]", "tab[1]", 0 ],
			"obj-12::obj-45::obj-33" : [ "tab[122]", "tab[1]", 0 ],
			"obj-12::obj-46::obj-33" : [ "tab[123]", "tab[1]", 0 ],
			"obj-12::obj-47::obj-33" : [ "tab[124]", "tab[1]", 0 ],
			"obj-12::obj-48::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-12::obj-49::obj-33" : [ "tab[126]", "tab[1]", 0 ],
			"obj-12::obj-50::obj-33" : [ "tab[127]", "tab[1]", 0 ],
			"obj-12::obj-74::obj-33" : [ "tab[109]", "tab[1]", 0 ],
			"obj-160::obj-101" : [ "number[7]", "number[7]", 0 ],
			"obj-160::obj-104" : [ "number[5]", "number[5]", 0 ],
			"obj-160::obj-109" : [ "number[6]", "number[6]", 0 ],
			"obj-160::obj-113" : [ "number[9]", "number[9]", 0 ],
			"obj-160::obj-118" : [ "number[2]", "number[2]", 0 ],
			"obj-160::obj-123" : [ "number[3]", "number[3]", 0 ],
			"obj-160::obj-28" : [ "number[12]", "number[12]", 0 ],
			"obj-160::obj-43" : [ "number[14]", "number[4]", 0 ],
			"obj-160::obj-44" : [ "number[13]", "number[13]", 0 ],
			"obj-160::obj-61" : [ "number[15]", "number[9]", 0 ],
			"obj-160::obj-68" : [ "number[4]", "number[4]", 0 ],
			"obj-160::obj-72" : [ "number[11]", "number[11]", 0 ],
			"obj-160::obj-77" : [ "number[10]", "number[10]", 0 ],
			"obj-160::obj-90" : [ "number", "number", 0 ],
			"obj-160::obj-93" : [ "number[8]", "number[8]", 0 ],
			"obj-160::obj-97" : [ "number[1]", "number[1]", 0 ],
			"obj-162::obj-107::obj-27::obj-18" : [ "toggle[17]", "toggle", 0 ],
			"obj-162::obj-107::obj-48" : [ "SendTo-TXT[17]", "SendTo-TXT", 0 ],
			"obj-162::obj-107::obj-8" : [ "tab[94]", "tab[1]", 0 ],
			"obj-162::obj-123::obj-27::obj-18" : [ "toggle[64]", "toggle", 0 ],
			"obj-162::obj-123::obj-48" : [ "SendTo-TXT[43]", "SendTo-TXT", 0 ],
			"obj-162::obj-123::obj-8" : [ "tab[141]", "tab[1]", 0 ],
			"obj-162::obj-34::obj-27::obj-18" : [ "toggle[18]", "toggle", 0 ],
			"obj-162::obj-34::obj-48" : [ "SendTo-TXT[18]", "SendTo-TXT", 0 ],
			"obj-162::obj-34::obj-8" : [ "tab[95]", "tab[1]", 0 ],
			"obj-162::obj-36::obj-27::obj-18" : [ "toggle[19]", "toggle", 0 ],
			"obj-162::obj-36::obj-48" : [ "SendTo-TXT[19]", "SendTo-TXT", 0 ],
			"obj-162::obj-36::obj-8" : [ "tab[96]", "tab[1]", 0 ],
			"obj-162::obj-40::obj-27::obj-18" : [ "toggle[20]", "toggle", 0 ],
			"obj-162::obj-40::obj-48" : [ "SendTo-TXT[20]", "SendTo-TXT", 0 ],
			"obj-162::obj-40::obj-8" : [ "tab[97]", "tab[1]", 0 ],
			"obj-162::obj-41::obj-27::obj-18" : [ "toggle[21]", "toggle", 0 ],
			"obj-162::obj-41::obj-48" : [ "SendTo-TXT[21]", "SendTo-TXT", 0 ],
			"obj-162::obj-41::obj-8" : [ "tab[98]", "tab[1]", 0 ],
			"obj-162::obj-42::obj-27::obj-18" : [ "toggle[22]", "toggle", 0 ],
			"obj-162::obj-42::obj-48" : [ "SendTo-TXT[22]", "SendTo-TXT", 0 ],
			"obj-162::obj-42::obj-8" : [ "tab[99]", "tab[1]", 0 ],
			"obj-162::obj-43::obj-27::obj-18" : [ "toggle[23]", "toggle", 0 ],
			"obj-162::obj-43::obj-48" : [ "SendTo-TXT[23]", "SendTo-TXT", 0 ],
			"obj-162::obj-43::obj-8" : [ "tab[100]", "tab[1]", 0 ],
			"obj-162::obj-44::obj-27::obj-18" : [ "toggle[24]", "toggle", 0 ],
			"obj-162::obj-44::obj-48" : [ "SendTo-TXT[24]", "SendTo-TXT", 0 ],
			"obj-162::obj-44::obj-8" : [ "tab[101]", "tab[1]", 0 ],
			"obj-162::obj-45::obj-27::obj-18" : [ "toggle[25]", "toggle", 0 ],
			"obj-162::obj-45::obj-48" : [ "SendTo-TXT[25]", "SendTo-TXT", 0 ],
			"obj-162::obj-45::obj-8" : [ "tab[102]", "tab[1]", 0 ],
			"obj-162::obj-46::obj-27::obj-18" : [ "toggle[26]", "toggle", 0 ],
			"obj-162::obj-46::obj-48" : [ "SendTo-TXT[44]", "SendTo-TXT", 0 ],
			"obj-162::obj-46::obj-8" : [ "tab[103]", "tab[1]", 0 ],
			"obj-162::obj-47::obj-27::obj-18" : [ "toggle[27]", "toggle", 0 ],
			"obj-162::obj-47::obj-48" : [ "SendTo-TXT[45]", "SendTo-TXT", 0 ],
			"obj-162::obj-47::obj-8" : [ "tab[104]", "tab[1]", 0 ],
			"obj-162::obj-48::obj-27::obj-18" : [ "toggle[28]", "toggle", 0 ],
			"obj-162::obj-48::obj-48" : [ "SendTo-TXT[46]", "SendTo-TXT", 0 ],
			"obj-162::obj-48::obj-8" : [ "tab[105]", "tab[1]", 0 ],
			"obj-162::obj-49::obj-27::obj-18" : [ "toggle[29]", "toggle", 0 ],
			"obj-162::obj-49::obj-48" : [ "SendTo-TXT[47]", "SendTo-TXT", 0 ],
			"obj-162::obj-49::obj-8" : [ "tab[106]", "tab[1]", 0 ],
			"obj-162::obj-50::obj-27::obj-18" : [ "toggle[30]", "toggle", 0 ],
			"obj-162::obj-50::obj-48" : [ "SendTo-TXT[48]", "SendTo-TXT", 0 ],
			"obj-162::obj-50::obj-8" : [ "tab[107]", "tab[1]", 0 ],
			"obj-162::obj-74::obj-27::obj-18" : [ "toggle[16]", "toggle", 0 ],
			"obj-162::obj-74::obj-48" : [ "SendTo-TXT[16]", "SendTo-TXT", 0 ],
			"obj-162::obj-74::obj-8" : [ "tab[93]", "tab[1]", 0 ],
			"obj-163::obj-107::obj-33" : [ "tab[79]", "tab[1]", 0 ],
			"obj-163::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-163::obj-34::obj-33" : [ "tab[80]", "tab[1]", 0 ],
			"obj-163::obj-36::obj-33" : [ "tab[81]", "tab[1]", 0 ],
			"obj-163::obj-40::obj-33" : [ "tab[82]", "tab[1]", 0 ],
			"obj-163::obj-41::obj-33" : [ "tab[83]", "tab[1]", 0 ],
			"obj-163::obj-42::obj-33" : [ "tab[84]", "tab[1]", 0 ],
			"obj-163::obj-43::obj-33" : [ "tab[85]", "tab[1]", 0 ],
			"obj-163::obj-44::obj-33" : [ "tab[86]", "tab[1]", 0 ],
			"obj-163::obj-45::obj-33" : [ "tab[87]", "tab[1]", 0 ],
			"obj-163::obj-46::obj-33" : [ "tab[88]", "tab[1]", 0 ],
			"obj-163::obj-47::obj-33" : [ "tab[89]", "tab[1]", 0 ],
			"obj-163::obj-48::obj-33" : [ "tab[90]", "tab[1]", 0 ],
			"obj-163::obj-49::obj-33" : [ "tab[91]", "tab[1]", 0 ],
			"obj-163::obj-50::obj-33" : [ "tab[92]", "tab[1]", 0 ],
			"obj-163::obj-74::obj-33" : [ "tab[78]", "tab[1]", 0 ],
			"obj-165::obj-25" : [ "live.numbox[3]", "live.numbox", 0 ],
			"obj-165::obj-96" : [ "live.numbox[4]", "live.numbox", 0 ],
			"obj-227" : [ "incdec", "incdec", 0 ],
			"obj-36::obj-107::obj-27::obj-18" : [ "toggle[33]", "toggle", 0 ],
			"obj-36::obj-107::obj-48" : [ "SendTo-TXT[28]", "SendTo-TXT", 0 ],
			"obj-36::obj-107::obj-8" : [ "tab[130]", "tab[1]", 0 ],
			"obj-36::obj-123::obj-27::obj-18" : [ "toggle[31]", "toggle", 0 ],
			"obj-36::obj-123::obj-48" : [ "SendTo-TXT[26]", "SendTo-TXT", 0 ],
			"obj-36::obj-123::obj-8" : [ "tab[128]", "tab[1]", 0 ],
			"obj-36::obj-34::obj-27::obj-18" : [ "toggle[34]", "toggle", 0 ],
			"obj-36::obj-34::obj-48" : [ "SendTo-TXT[29]", "SendTo-TXT", 0 ],
			"obj-36::obj-34::obj-8" : [ "tab[113]", "tab[1]", 0 ],
			"obj-36::obj-36::obj-27::obj-18" : [ "toggle[35]", "toggle", 0 ],
			"obj-36::obj-36::obj-48" : [ "SendTo-TXT[49]", "SendTo-TXT", 0 ],
			"obj-36::obj-36::obj-8" : [ "tab[114]", "tab[1]", 0 ],
			"obj-36::obj-40::obj-27::obj-18" : [ "toggle[36]", "toggle", 0 ],
			"obj-36::obj-40::obj-48" : [ "SendTo-TXT[30]", "SendTo-TXT", 0 ],
			"obj-36::obj-40::obj-8" : [ "tab[131]", "tab[1]", 0 ],
			"obj-36::obj-41::obj-27::obj-18" : [ "toggle[37]", "toggle", 0 ],
			"obj-36::obj-41::obj-48" : [ "SendTo-TXT[50]", "SendTo-TXT", 0 ],
			"obj-36::obj-41::obj-8" : [ "tab[132]", "tab[1]", 0 ],
			"obj-36::obj-42::obj-27::obj-18" : [ "toggle[38]", "toggle", 0 ],
			"obj-36::obj-42::obj-48" : [ "SendTo-TXT[51]", "SendTo-TXT", 0 ],
			"obj-36::obj-42::obj-8" : [ "tab[133]", "tab[1]", 0 ],
			"obj-36::obj-43::obj-27::obj-18" : [ "toggle[39]", "toggle", 0 ],
			"obj-36::obj-43::obj-48" : [ "SendTo-TXT[31]", "SendTo-TXT", 0 ],
			"obj-36::obj-43::obj-8" : [ "tab[134]", "tab[1]", 0 ],
			"obj-36::obj-44::obj-27::obj-18" : [ "toggle[40]", "toggle", 0 ],
			"obj-36::obj-44::obj-48" : [ "SendTo-TXT[52]", "SendTo-TXT", 0 ],
			"obj-36::obj-44::obj-8" : [ "tab[135]", "tab[1]", 0 ],
			"obj-36::obj-45::obj-27::obj-18" : [ "toggle[41]", "toggle", 0 ],
			"obj-36::obj-45::obj-48" : [ "SendTo-TXT[53]", "SendTo-TXT", 0 ],
			"obj-36::obj-45::obj-8" : [ "tab[136]", "tab[1]", 0 ],
			"obj-36::obj-46::obj-27::obj-18" : [ "toggle[42]", "toggle", 0 ],
			"obj-36::obj-46::obj-48" : [ "SendTo-TXT[32]", "SendTo-TXT", 0 ],
			"obj-36::obj-46::obj-8" : [ "tab[137]", "tab[1]", 0 ],
			"obj-36::obj-47::obj-27::obj-18" : [ "toggle[43]", "toggle", 0 ],
			"obj-36::obj-47::obj-48" : [ "SendTo-TXT[54]", "SendTo-TXT", 0 ],
			"obj-36::obj-47::obj-8" : [ "tab[138]", "tab[1]", 0 ],
			"obj-36::obj-48::obj-27::obj-18" : [ "toggle[65]", "toggle", 0 ],
			"obj-36::obj-48::obj-48" : [ "SendTo-TXT[33]", "SendTo-TXT", 0 ],
			"obj-36::obj-48::obj-8" : [ "tab[139]", "tab[1]", 0 ],
			"obj-36::obj-49::obj-27::obj-18" : [ "toggle[66]", "toggle", 0 ],
			"obj-36::obj-49::obj-48" : [ "SendTo-TXT[55]", "SendTo-TXT", 0 ],
			"obj-36::obj-49::obj-8" : [ "tab[142]", "tab[1]", 0 ],
			"obj-36::obj-50::obj-27::obj-18" : [ "toggle[67]", "toggle", 0 ],
			"obj-36::obj-50::obj-48" : [ "SendTo-TXT[56]", "SendTo-TXT", 0 ],
			"obj-36::obj-50::obj-8" : [ "tab[140]", "tab[1]", 0 ],
			"obj-36::obj-74::obj-27::obj-18" : [ "toggle[32]", "toggle", 0 ],
			"obj-36::obj-74::obj-48" : [ "SendTo-TXT[27]", "SendTo-TXT", 0 ],
			"obj-36::obj-74::obj-8" : [ "tab[129]", "tab[1]", 0 ],
			"obj-59::obj-107::obj-33" : [ "tab[160]", "tab[1]", 0 ],
			"obj-59::obj-123::obj-33" : [ "tab[158]", "tab[1]", 0 ],
			"obj-59::obj-34::obj-33" : [ "tab[161]", "tab[1]", 0 ],
			"obj-59::obj-36::obj-33" : [ "tab[162]", "tab[1]", 0 ],
			"obj-59::obj-40::obj-33" : [ "tab[163]", "tab[1]", 0 ],
			"obj-59::obj-41::obj-33" : [ "tab[164]", "tab[1]", 0 ],
			"obj-59::obj-42::obj-33" : [ "tab[165]", "tab[1]", 0 ],
			"obj-59::obj-43::obj-33" : [ "tab[166]", "tab[1]", 0 ],
			"obj-59::obj-44::obj-33" : [ "tab[167]", "tab[1]", 0 ],
			"obj-59::obj-45::obj-33" : [ "tab[168]", "tab[1]", 0 ],
			"obj-59::obj-46::obj-33" : [ "tab[169]", "tab[1]", 0 ],
			"obj-59::obj-47::obj-33" : [ "tab[170]", "tab[1]", 0 ],
			"obj-59::obj-48::obj-33" : [ "tab[171]", "tab[1]", 0 ],
			"obj-59::obj-49::obj-33" : [ "tab[172]", "tab[1]", 0 ],
			"obj-59::obj-50::obj-33" : [ "tab[173]", "tab[1]", 0 ],
			"obj-59::obj-74::obj-33" : [ "tab[159]", "tab[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"dependency_cache" : [ 			{
				"name" : "OSC-route.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_2d_decoder3_4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_2d_encoder3~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_2d_fx_decorrelation3~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_gaincontrol.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/events/2022-06-21_US1/patches",
				"patcherrelativepath" : "../../2022-06-21_US1/patches",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.2max.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../../max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.regress.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/regress",
				"patcherrelativepath" : "../../../max/control_processing/regress",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.timethresh.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/events/2023-03-17_ICM/pacthers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_joystick_loop_ui1~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "bbdmi_live_granulator~.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/sound_synthesis/live_granulator~",
				"patcherrelativepath" : "../../../max/sound_synthesis/live_granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_multi_granulator4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "faustgen~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "mxj.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/source/js",
				"patcherrelativepath" : "../../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.2max.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/output/2max",
				"patcherrelativepath" : "../../../max/output/2max",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitLab/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "preset.json",
				"bootpath" : "~/Documents/GitLab/bbdmi/events/2023-03-17_ICM/presets",
				"patcherrelativepath" : "../presets",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rapid.regression.mxo",
				"type" : "iLaX"
			}
 ],
		"autosave" : 0,
		"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
		"bgcolor" : [ 0.27843137254902, 0.27843137254902, 0.27843137254902, 1.0 ]
	}

}
