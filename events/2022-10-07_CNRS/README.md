# CNRS

This folder contains patches used during the CNRS Visite Insolite in 2022.

This patch needs the [abclib](https://github.com/alainbonardi/abclib/tree/master) in order to work correctly