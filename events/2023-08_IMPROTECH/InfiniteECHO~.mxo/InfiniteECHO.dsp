import("stdfaust.lib");

infiniteECHO = _ <: _,_*(inputgain) :_,delayECHO * infiniteECHOgain :> _
with{
    delayECHO = (+:de.sdelay(180 * ma.SR,4096,deltimeECHO* ma.SR:int))~(_*(0.75));

    infiniteECHOgain = hslider("[0]infiniteECHOgain [style:knob]", 0, 0, 1, 0.01): si.smoo;
	inputgain = hslider("[1]inputgain [style:knob]", 0, 0, 1, 0.01): si.smoo;
    deltimeECHO = hslider("[2]deltimeECHO [unit:secs]", 180, 0.5, 180, 0.01) : si.smoo;//try very small delays, very intresting
    };

process =  infiniteECHO;
