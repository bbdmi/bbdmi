import("stdfaust.lib");

//-------`bbdmi_multiTranspEcho`----------
//Generates '2*N' channels with a random delay time and transposition ammount. It merges the channel to get 'N' channels at the output. 
//
// #### Usage
//
// ```
// _ : multiTranspEcho(N) : _
// ```
//
// Where:
//
// * `ch`: Number of channels.

//-----------------------------



bbdmi_multiTranspEcho(ch,factor,maxtransp,transpSide,fxFeedbacklevel,delayFx) = si.bus(ch) <: si.bus(2*ch) : par(i,2*ch, +~(de.sdelay(10 * ma.SR,1024, delayFx * ma.SR * ((i)/(2*ch)):int)*(fxFeedbacklevel) : transp(random(i) : *(2*maxtransp)))): par(i,2*ch,transp(random(i) : *(maxtransp))) :> si.bus(ch)
with{
    transp(tr) = ef.transpose(10000,10000,tr);
    exciter(i) = no.sparse_noise(factor*(1+i*0.01)), 0.5 : > ;
    random(i) = no.noise : ba.sAndH(exciter(i)) : +(transpSide) : /(2);
    };



    //-------`bbdmi_multiTranspEcho_ui`----------
//GUI for the module 'bbdmi_multiTranspEcho'
//
// #### Usage
//
// ```
// _ : bbdmi_multiTranspEcho_ui(N) : _
// ```
//
// Where:
//
// * `N`: Number of channels.
// 
//-----------------------------
bbdmi_multiTranspEcho_ui(N,ch) = vgroup("Multi EEG simulator", par(i,N,bbdmi_multiTranspEcho(ch,factor,maxtransp,transpSide,fxFeedbacklevel,delayFx)
with{
    factor = hslider("bbdmi_multiTranspEcho [0]factor", 1.5, 0.01, 2, 0.01);
    maxtransp = hslider("bbdmi_multiTranspEcho [1] maxtransp", 8, 0, 20, 0.01) : si.smoo;
    transpSide = hslider("bbdmi_multiTranspEcho [2] transpSide", 0, -1, 1, 0.01) : si.smoo;
    fxFeedbacklevel = hslider("bbdmi_multiTranspEcho [3] fxFeedbacklevel", 0.7, 0, 1, 0.01) : si.smoo;
    delayFx = hslider("bbdmi_multiTranspEcho [4] delayFx", 0.2, 0.01, 10, 0.01) : si.smoo;
}));



process =  bbdmi_multiTranspEcho_ui(1,11);// <: _,_;