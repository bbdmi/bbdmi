# CNRS

This folder contains patches used during the CNRS Visite Insolite in 2023.

# Copyright and license
Code and documentation is copyright (c) 2021–2022 by [BBDMI](https://bbdmi.nakala.fr/), released for free under the [MIT License](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/blob/main/LICENSE).
