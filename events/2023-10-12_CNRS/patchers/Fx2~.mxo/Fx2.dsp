import("stdfaust.lib");

nfx2 = 22;


exciter(i) = no.sparse_noise(factor*(1+i*0.01)), 0.5 : > ;
random(i) = no.noise : ba.sAndH(exciter(i)) : +(transpSide) : /(2);

fx2 = si.bus(nfx2/2) <: si.bus(nfx2) : par(i,nfx2, ef.reverseEchoN(nfx2,512) :> /(2): +~(de.sdelay(10 * ma.SR,1024, delayFx2 * ma.SR * ((i)/nfx2):int)*(fx2Feedbacklevel) : transp(random(i) : *(2*maxtransp)))) :> si.bus(nfx2/2);//_ : /(nfx2); 

transp(tr) = ef.transpose(10000,10000,tr);

factor = hslider("[1]factor", 1.5, 0.01, 2, 0.01);
maxtransp = hslider("[7] maxtransp", 8, 0, 20, 0.01) : si.smoo;
transpSide = hslider("[7] transpSide", 0, -1, 1, 0.01) : si.smoo;
fx2Feedbacklevel = hslider("[5] fx2Feedbacklevel", 0.7, 0, 1, 0.01) : si.smoo;
delayFx2 = hslider("[6] delayFx2", 0.2, 0.01, 10, 0.01) : si.smoo;

process =  fx2;
