{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 6,
			"revision" : 2,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 76.0, 105.0, 1600.0, 594.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1298.400006651878357, 433.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 862.75, 433.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 491.5, 440.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 533.5, 662.0, 29.5, 22.0 ],
					"text" : "t i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 533.5, 742.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 533.5, 717.0, 34.0, 22.0 ],
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 533.5, 693.0, 33.0, 22.0 ],
					"text" : "== 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 511.5, 531.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 511.5, 596.0, 29.5, 22.0 ],
					"text" : "+"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 533.5, 634.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 511.5, 475.0, 106.0, 22.0 ],
					"text" : "r viola-gran-preset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 511.5, 501.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"activecolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ],
					"disabled" : [ 0, 0, 0, 0, 0, 0, 0, 0 ],
					"id" : "obj-57",
					"itemtype" : 0,
					"maxclass" : "radiogroup",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 491.5, 664.0, 18.0, 130.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1247.0, 22.0, 21.0, 130.0 ],
					"shape" : 2,
					"size" : 8,
					"value" : 5
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 905.0, 651.5, 29.5, 22.0 ],
					"text" : "t i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 905.0, 731.5, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 905.0, 706.5, 34.0, 22.0 ],
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 905.0, 682.5, 33.0, 22.0 ],
					"text" : "== 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 883.0, 520.5, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 883.0, 585.5, 29.5, 22.0 ],
					"text" : "+"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 905.0, 623.5, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 883.0, 464.5, 107.0, 22.0 ],
					"text" : "r bclar-gran-preset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 883.0, 490.5, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"activecolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ],
					"disabled" : [ 0, 0, 0, 0, 0, 0, 0, 0 ],
					"id" : "obj-47",
					"itemtype" : 0,
					"maxclass" : "radiogroup",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 863.0, 653.5, 18.0, 130.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1448.0, 22.0, 21.0, 130.0 ],
					"shape" : 2,
					"size" : 8,
					"value" : 4
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 1340.0, 664.0, 29.5, 22.0 ],
					"text" : "t i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1340.0, 744.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1340.0, 719.0, 34.0, 22.0 ],
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1340.0, 695.0, 33.0, 22.0 ],
					"text" : "== 8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1318.0, 533.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1318.0, 598.0, 29.5, 22.0 ],
					"text" : "+"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1340.0, 636.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1318.0, 477.0, 111.0, 22.0 ],
					"text" : "r piano-gran-preset"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1172.0, 199.0, 107.0, 22.0 ],
					"text" : "r~ to-piano-gran-R"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1152.0, 926.400017082691193, 95.0, 22.0 ],
					"text" : "s~ piano-gran-R"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 746.0, 926.400017082691193, 92.0, 22.0 ],
					"text" : "s~ bclar-gran-R"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 359.0, 921.400017082691193, 90.0, 22.0 ],
					"text" : "s~ viola-gran-R"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1055.0, 199.0, 105.0, 22.0 ],
					"text" : "r~ to-piano-gran-L"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 651.0, 199.0, 91.0, 22.0 ],
					"text" : "r~ to-bclar-gran"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 269.0, 199.0, 90.0, 22.0 ],
					"text" : "r~ to-viola-gran"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 204.5, 642.5, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 89.0, 592.0, 73.0, 22.0 ],
					"text" : "r nk2-fader8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 431.75, 860.999983370304108, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 379.25, 860.999983370304108, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 325.75, 860.999983370304108, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 267.5, 860.999983370304108, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 269.0, 833.0, 84.0, 22.0 ],
					"text" : "mc.unpack~ 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 819.5, 871.0, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 761.25, 871.0, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 707.75, 871.0, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 649.5, 871.0, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 651.0, 834.000016629695892, 84.0, 22.0 ],
					"text" : "mc.unpack~ 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1453.5, 838.0, 29.5, 22.0 ],
					"text" : "110"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1420.5, 838.0, 29.5, 22.0 ],
					"text" : "80"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1387.5, 838.0, 29.5, 22.0 ],
					"text" : "50"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1356.0, 838.0, 29.5, 22.0 ],
					"text" : "18"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1377.0, 810.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1223.5, 872.0, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1165.25, 872.0, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1111.75, 872.0, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1053.5, 872.0, 50.5, 22.0 ],
					"text" : "pan2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "signal", "signal" ],
					"patching_rect" : [ 1055.0, 835.000016629695892, 84.0, 22.0 ],
					"text" : "mc.unpack~ 4"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 14.0,
					"id" : "obj-110",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 997.0, 260.835540967701036, 51.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1734.5, -1.0, 47.0, 22.0 ],
					"text" : "Piano"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 14.0,
					"id" : "obj-109",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 585.897401055321097, 260.835540967701036, 51.0, 53.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1511.5, -1.0, 98.0, 22.0 ],
					"text" : "Bass Clarinet"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 14.0,
					"id" : "obj-108",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 470.0, 260.835540967701036, 51.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1332.5, -1.0, 42.0, 22.0 ],
					"text" : "Viola"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 269.0, 921.400017082691193, 88.0, 22.0 ],
					"text" : "s~ viola-gran-L"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 651.0, 926.400017082691193, 90.0, 22.0 ],
					"text" : "s~ bclar-gran-L"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1055.0, 926.400017082691193, 93.0, 22.0 ],
					"text" : "s~ piano-gran-L"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1318.0, 503.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"id" : "obj-42",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 470.0, 812.0, 160.0, 20.0 ],
					"text" : "viola parameters here",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 287.0, 1777.0, 1119.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-34",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 310.447237074375153, 805.0, 133.0, 263.0 ],
									"text" : ";\rviola transpout 0.;\rviola feedback 0.5;\rviola gain 1.;\rviola variability 0.;\rviola transpgrain 0.;\rviola modfreqmod 0.;\rviola modmorph 0.;\rviola modfreq 2.9;\rviola modfactor 0.7;\rviola spacing 17;\rviola grainoffset 40;\rviola grainsize 50;\rviola maxdelay 300.;\rviola lpffreq 20000.;\rviola hpffreq 20.;\rviola grainenvmorph 1.;\rviola indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 147.0, 805.0, 133.0, 263.0 ],
									"text" : ";\rviola transpout 0.;\rviola feedback 0.;\rviola gain 1.;\rviola variability 0.;\rviola transpgrain 0.;\rviola modfreqmod 0.5;\rviola modmorph 0.;\rviola modfreq 1000.;\rviola modfactor 0.;\rviola spacing 16;\rviola grainoffset 0;\rviola grainsize 219;\rviola maxdelay 2000.;\rviola lpffreq 20000.;\rviola hpffreq 20.;\rviola grainenvmorph 1.;\rviola indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-131",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1643.518338441848755, 450.670968413352966, 111.0, 76.0 ],
									"text" : ";\rviola2set feedback;\rviola2min 0.1;\rviola2max 1.;\rviola2curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-132",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1510.590909481048584, 450.670968413352966, 111.0, 76.0 ],
									"text" : ";\rviola1set grainsize;\rviola1min 20.;\rviola1max 72.;\rviola1curve 1.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-133",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1643.518338441848755, 371.125516176223755, 123.0, 76.0 ],
									"text" : ";\rviola2set transpgrain;\rviola2min -0.5;\rviola2max 0.5;\rviola2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-134",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1506.5, 371.125516176223755, 111.0, 76.0 ],
									"text" : ";\rviola1set grainsize;\rviola1min 96.;\rviola1max 24.;\rviola1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-135",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1643.518338441848755, 293.125516176223755, 107.0, 76.0 ],
									"text" : ";\rviola2set modfreq;\rviola2min 1.;\rviola2max 20.;\rviola2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-136",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1506.5, 293.125516176223755, 111.0, 76.0 ],
									"text" : ";\rviola1set grainsize;\rviola1min 133.;\rviola1max 333.;\rviola1curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-137",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1643.518338441848755, 209.318182468414307, 104.0, 76.0 ],
									"text" : ";\rviola2set spacing;\rviola2min 120.;\rviola2max 40.;\rviola2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-138",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1506.5, 209.318182468414307, 111.0, 76.0 ],
									"text" : ";\rviola1set grainsize;\rviola1min 56.;\rviola1max 0.;\rviola1curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-130",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1357.725188255310059, 453.85278594493866, 104.0, 76.0 ],
									"text" : ";\rviola2set spacing;\rviola2min 90;\rviola2max 50;\rviola2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-129",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1224.797759294509888, 453.85278594493866, 111.0, 76.0 ],
									"text" : ";\rviola1set grainsize;\rviola1min 85.;\rviola1max 20.;\rviola1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-127",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1357.725188255310059, 374.307333707809448, 123.0, 76.0 ],
									"text" : ";\rviola2set transpgrain;\rviola2min -6.7;\rviola2max -4.5;\rviola2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-128",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1220.706849813461304, 374.307333707809448, 111.0, 76.0 ],
									"text" : ";\rviola1set grainsize;\rviola1min 200.;\rviola1max 50.;\rviola1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-126",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1357.725188255310059, 296.307333707809448, 107.0, 76.0 ],
									"text" : ";\rviola2set modfreq;\rviola2min 1.;\rviola2max 2200.;\rviola2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-125",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1220.706849813461304, 296.307333707809448, 111.0, 76.0 ],
									"text" : ";\rviola1set grainsize;\rviola1min 8.;\rviola1max 100.;\rviola1curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-124",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1357.725188255310059, 212.5, 104.0, 76.0 ],
									"text" : ";\rviola2set spacing;\rviola2min 1.;\rviola2max 100.;\rviola2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-123",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1220.706849813461304, 212.5, 111.0, 76.0 ],
									"text" : ";\rviola1set grainsize;\rviola1min 100.;\rviola1max 1.;\rviola1curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 9,
									"numoutlets" : 9,
									"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "" ],
									"patching_rect" : [ 272.894474148750305, 63.0, 894.0, 22.0 ],
									"text" : "select 0 1 2 3 4 5 6 7"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 966.634957671165466, 399.333328664302826, 173.0, 263.0 ],
									"text" : ";\rviola transpout 15.;\rviola feedback 0.813036;\rviola gain 1.;\rviola variability 0.519368;\rviola transpgrain -15.;\rviola modfreqmod 0.079739;\rviola modmorph 2.652866;\rviola modfreq 360.;\rviola modfactor 0.6;\rviola spacing 79;\rviola grainoffset 44;\rviola grainsize 48;\rviola maxdelay 400.;\rviola lpffreq 7000.;\rviola hpffreq 399.;\rviola grainenvmorph 0.480679;\rviola indexdistr 14;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 760.027107119560242, 399.333328664302826, 140.0, 263.0 ],
									"text" : ";\rviola transpout 0.;\rviola feedback 0.7;\rviola gain 1.;\rviola variability 0.8;\rviola transpgrain 0.2;\rviola modfreqmod 0.5;\rviola modmorph 0.1;\rviola modfreq 10.;\rviola modfactor 0.5;\rviola spacing 187;\rviola grainoffset 10;\rviola grainsize 233;\rviola maxdelay 2000.;\rviola lpffreq 12000.;\rviola hpffreq 20.;\rviola grainenvmorph 0.5;\rviola indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 887.634957671165466, 119.0, 173.0, 263.0 ],
									"text" : ";\rviola transpout 2.;\rviola feedback 0.8;\rviola gain 1.;\rviola variability 0.8;\rviola transpgrain 0.25;\rviola modfreqmod 0.5;\rviola modmorph 0.1;\rviola modfreq 10.;\rviola modfactor 0.5;\rviola spacing 130;\rviola grainoffset 160;\rviola grainsize 42;\rviola maxdelay 400.;\rviola lpffreq 16000.;\rviola hpffreq 20.;\rviola grainenvmorph 0.5;\rviola indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 529.105525851249695, 399.333328664302826, 173.0, 263.0 ],
									"text" : ";\rviola transpout -2.;\rviola feedback 0.8;\rviola gain 1.;\rviola variability 0.3;\rviola transpgrain 1.;\rviola modfreqmod 0.5;\rviola modmorph 0.1;\rviola modfreq 8.;\rviola modfactor 0.5;\rviola spacing 87;\rviola grainoffset 10;\rviola grainsize 49;\rviola maxdelay 140.;\rviola lpffreq 2000.;\rviola hpffreq 20.;\rviola grainenvmorph 0.5;\rviola indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 685.105525851249695, 119.0, 173.0, 263.0 ],
									"text" : ";\rviola transpout 1.2;\rviola feedback 0.5;\rviola gain 1.;\rviola variability 0.8;\rviola transpgrain 0.2;\rviola modfreqmod 0.7;\rviola modmorph 0.1;\rviola modfreq 8.;\rviola modfactor 0.7;\rviola spacing 60;\rviola grainoffset 200;\rviola grainsize 36;\rviola maxdelay 100.;\rviola lpffreq 6000.;\rviola hpffreq 20.;\rviola grainenvmorph 0.5;\rviola indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 475.0, 119.0, 173.0, 263.0 ],
									"text" : ";\rviola transpout -20.;\rviola feedback 0.5;\rviola gain 1.;\rviola variability 0.3;\rviola transpgrain -3.;\rviola modfreqmod 0.5;\rviola modmorph 0.;\rviola modfreq 500.;\rviola modfactor 0.;\rviola spacing 40;\rviola grainoffset 20;\rviola grainsize 80;\rviola maxdelay 2000.;\rviola lpffreq 15000.;\rviola hpffreq 20.;\rviola grainenvmorph 0.5;\rviola indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 321.105525851249695, 399.333328664302826, 161.0, 263.0 ],
									"text" : ";\rviola transpout -1.567927;\rviola feedback 0.3;\rviola gain 1.;\rviola variability 0.420905;\rviola transpgrain -0.30263;\rviola modfreqmod 0.451733;\rviola modmorph 0.449987;\rviola modfreq 1500.;\rviola modfactor 0.644277;\rviola spacing 59;\rviola grainoffset 499;\rviola grainsize 25;\rviola maxdelay 5433.37;\rviola lpffreq 14539.336625;\rviola hpffreq 1000.;\rviola grainenvmorph 0.8;\rviola indexdistr 4;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 279.0, 119.0, 173.0, 263.0 ],
									"text" : ";\rviola transpout 0.;\rviola feedback 0.4;\rviola gain 1.;\rviola variability 0.;\rviola transpgrain 0.;\rviola modfreqmod 0.;\rviola modmorph 0.;\rviola modfreq 0.;\rviola modfactor 0.;\rviola spacing 1;\rviola grainoffset 0;\rviola grainsize 350;\rviola maxdelay 500.;\rviola lpffreq 20000.;\rviola hpffreq 20.;\rviola grainenvmorph 1.;\rviola indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 21.0, 119.0, 173.0, 263.0 ],
									"text" : ";\rviola transpout 15.;\rviola feedback 0.813036;\rviola gain 1.;\rviola variability 0.519368;\rviola transpgrain -15.;\rviola modfreqmod 0.079739;\rviola modmorph 2.652866;\rviola modfreq 360.;\rviola modfactor 0.6;\rviola spacing 79;\rviola grainoffset 44;\rviola grainsize 48;\rviola maxdelay 400.;\rviola lpffreq 7000.;\rviola hpffreq 399.;\rviola grainenvmorph 0.480679;\rviola indexdistr 14;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 285.0, 89.0, 35.0, 20.0 ],
									"text" : "reset"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 257.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-123", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-124", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-125", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-126", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-127", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-128", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-129", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-130", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-131", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-132", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-133", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-134", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-135", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-138", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"source" : [ "obj-3", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 449.0, 831.0, 103.0, 26.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p presets",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 269.0, 234.333328664302826, 70.0, 22.0 ],
					"text" : "mc.pack~ 4"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "viola", "@Channels", 7 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-61",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 269.0, 260.835540967701036, 199.0, 564.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1254.0, 23.0, 199.0, 564.0 ],
					"varname" : "bbdmi_live.granulator~[1]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"activecolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ],
					"disabled" : [ 0, 0, 0, 0, 0, 0, 0, 0 ],
					"id" : "obj-67",
					"itemtype" : 0,
					"maxclass" : "radiogroup",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1298.400006651878357, 666.0, 18.0, 130.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1649.0, 22.0, 21.0, 130.0 ],
					"shape" : 2,
					"size" : 8,
					"value" : 4
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"id" : "obj-17",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1259.40000706911087, 812.0, 160.0, 20.0 ],
					"text" : "piano parameters here",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"id" : "obj-27",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 852.0, 812.0, 160.0, 20.0 ],
					"text" : "bass clar parameters here",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ -1844.0, -316.0, 1779.0, 833.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-2",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1629.018338441848755, 594.807333707809448, 116.0, 76.0 ],
									"text" : ";\rpiano2set feedback;\rpiano2min 0.1;\rpiano2max 1.;\rpiano2curve 1.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1629.018338441848755, 517.989151239395142, 117.0, 76.0 ],
									"text" : ";\rpiano2set transpout;\rpiano2min 24.;\rpiano2max 48.;\rpiano2curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1624.311488628387451, 432.807333707809448, 111.0, 76.0 ],
									"text" : ";\rpiano2set modfreq;\rpiano2min 0.;\rpiano2max 2.;\rpiano2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 594.807333707809448, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 600.;\rpiano1max 50.;\rpiano1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 514.807333707809448, 127.0, 76.0 ],
									"text" : ";\rpiano1set transpgrain;\rpiano1min -12.;\rpiano1max -48.;\rpiano1curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 432.807333707809448, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 300.;\rpiano1max 50.;\rpiano1curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1493.0, 349.0, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 600.;\rpiano1max 150.;\rpiano1curve 1.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1332.018338441848755, 597.989151239395142, 109.0, 76.0 ],
									"text" : ";\rpiano2set spacing;\rpiano2min 175.;\rpiano2max 70.;\rpiano2curve 1.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1193.5, 597.989151239395142, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 600.;\rpiano1max 300.;\rpiano1curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1332.018338441848755, 517.989151239395142, 117.0, 76.0 ],
									"text" : ";\rpiano2set transpout;\rpiano2min 7.;\rpiano2max 14.;\rpiano2curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1193.5, 517.989151239395142, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 100.;\rpiano1max 1.;\rpiano1curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1332.018338441848755, 435.989151239395142, 111.0, 76.0 ],
									"text" : ";\rpiano2set modfreq;\rpiano2min 1000.;\rpiano2max 6000.;\rpiano2curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1193.5, 435.989151239395142, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 300.;\rpiano1max 50.;\rpiano1curve 1.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1185.5, 316.0, 150.0, 20.0 ],
									"text" : "correspond to presets 1-8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-137",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1617.811488628387451, 349.0, 124.0, 76.0 ],
									"text" : ";\rbassclar2set spacing;\rbassclar2min 200.;\rbassclar2max 40.;\rbassclar2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-124",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1332.018338441848755, 352.181817531585693, 109.0, 76.0 ],
									"text" : ";\rpiano2set spacing;\rpiano2min 1.;\rpiano2max 100.;\rpiano2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-123",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1195.0, 352.181817531585693, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 300.;\rpiano1max 50.;\rpiano1curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 9,
									"numoutlets" : 9,
									"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "" ],
									"patching_rect" : [ 257.894474148750305, 48.0, 894.0, 22.0 ],
									"text" : "select 0 1 2 3 4 5 6 7"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 257.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 6,
											"revision" : 2,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 59.0, 118.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-13",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 308.105525851249695, 410.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-11",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 571.0, 392.0, 150.0, 20.0 ],
													"text" : "voice 11 feb"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-124",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 574.105525851249695, 418.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 10.;\rinstr feedback 0.991;\rinstr gain 1.;\rinstr variability 0.44;\rinstr transpgrain 5.;\rinstr modfreqmod 0.9;\rinstr modmorph 0.098;\rinstr modfreq 199.;\rinstr modfactor 0.41;\rinstr spacing 190;\rinstr grainoffset 0;\rinstr grainsize 211;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.445;\rinstr indexdistr 1;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 72.0, 682.0, 150.0, 20.0 ],
													"text" : "piano transpose 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 72.0, 708.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 1.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 322.0, 682.0, 150.0, 20.0 ],
													"text" : "piano transpose 2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-7",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 326.0, 704.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 6.;\rinstr feedback 0.7;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 68.0, 407.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.5;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 50;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 68.0, 379.0, 150.0, 20.0 ],
													"text" : "clarinet"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 68.0, 36.0, 150.0, 20.0 ],
													"text" : "viola"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 296.447237074375153, 66.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.5;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 2.9;\rinstr modfactor 0.7;\rinstr spacing 17;\rinstr grainoffset 40;\rinstr grainsize 50;\rinstr maxdelay 300.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 57.0, 66.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 16;\rinstr grainoffset 0;\rinstr grainsize 219;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
 ],
										"lines" : [  ]
									}
,
									"patching_rect" : [ 95.0, 45.0, 35.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p old"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"linecount" : 22,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1001.0, 118.0, 158.0, 317.0 ],
									"text" : ";\rpiano transpout 11.044334;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.234717;\rpiano transpgrain 9.041521;\rpiano modfreqmod 0.395239;\rpiano modmorph 1.888272;\rpiano modfreq 5181.219287;\rpiano modfactor 0.59711;\rpiano spacing 257;\rpiano grainoffset 243;\rpiano grainsize 200;\rpiano maxdelay 3477.948085;\rpiano lpffreq 17645.617938;\rpiano hpffreq 136.927723;\rpiano grainenvmorph 0.251452;\rpiano indexdistr 17;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"linecount" : 23,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 833.105525851249695, 392.00221230339821, 157.894474148750305, 330.0 ],
									"text" : ";\rpiano transpout 42.7;\rpiano feedback 0.833806;\rpiano gain 1.;\rpiano variability 0.103467;\rpiano transpgrain -42.353346;\rpiano modfreqmod 0.19328;\rpiano modmorph 2.926464;\rpiano modfreq 1355.294321;\rpiano modfactor 0.024476;\rpiano spacing 87;\rpiano grainoffset 546;\rpiano grainsize 200;\rpiano maxdelay 4888.035099;\rpiano lpffreq 9899.430395;\rpiano hpffreq 849.965203;\rpiano grainenvmorph 0.245914;\rpiano indexdistr 6;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 846.0, 80.666671335697174, 150.0, 20.0 ],
									"text" : "guitar"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 813.0, 108.168881672142106, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 0.8;\rpiano transpgrain 0.;\rpiano modfreqmod 0.;\rpiano modmorph 0.8;\rpiano modfreq 0.5;\rpiano modfactor 0.8;\rpiano spacing 83;\rpiano grainoffset 50;\rpiano grainsize 165;\rpiano maxdelay 500.;\rpiano lpffreq 7000.;\rpiano hpffreq 200.;\rpiano grainenvmorph 0.;\rpiano indexdistr 3;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 625.947237074375153, 108.168881672142106, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 2.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 0.8;\rpiano transpgrain 2.;\rpiano modfreqmod 0.;\rpiano modmorph 0.5;\rpiano modfreq 1.;\rpiano modfactor 0.5;\rpiano spacing 121;\rpiano grainoffset 50;\rpiano grainsize 441;\rpiano maxdelay 2000.;\rpiano lpffreq 5000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.;\rpiano indexdistr 2;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 645.105525851249695, 417.0, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout -1.;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.8;\rpiano transpgrain -0.5;\rpiano modfreqmod 0.;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 128;\rpiano grainoffset 151;\rpiano grainsize 445;\rpiano maxdelay 2000.;\rpiano lpffreq 5000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.;\rpiano indexdistr 1;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 446.105525851249695, 432.0, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 10.;\rpiano feedback 0.8;\rpiano gain 1.;\rpiano variability 0.3;\rpiano transpgrain 0.;\rpiano modfreqmod 0.;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 80;\rpiano grainoffset 40;\rpiano grainsize 40;\rpiano maxdelay 2000.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.5;\rpiano indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"linecount" : 22,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 424.105525851249695, 108.168881672142106, 158.0, 317.0 ],
									"text" : ";\rpiano transpout 11.044334;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.234717;\rpiano transpgrain 9.041521;\rpiano modfreqmod 0.395239;\rpiano modmorph 1.888272;\rpiano modfreq 5181.219287;\rpiano modfactor 0.59711;\rpiano spacing 257;\rpiano grainoffset 243;\rpiano grainsize 200;\rpiano maxdelay 3477.948085;\rpiano lpffreq 17645.617938;\rpiano hpffreq 136.927723;\rpiano grainenvmorph 0.251452;\rpiano indexdistr 17;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 104.0, 138.0, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.;\rpiano gain 1.;\rpiano variability 0.;\rpiano transpgrain 0.;\rpiano modfreqmod 0.;\rpiano modmorph 0.;\rpiano modfreq 0.;\rpiano modfactor 0.;\rpiano spacing 1;\rpiano grainoffset 0;\rpiano grainsize 250;\rpiano maxdelay 500.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 1.;\rpiano indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 6.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 1.;\rpiano transpgrain 0.;\rpiano modfreqmod 0.5;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 80;\rpiano grainoffset 40;\rpiano grainsize 40;\rpiano maxdelay 2000.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.5;\rpiano indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 270.0, 74.0, 150.0, 20.0 ],
									"text" : "reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 387.0, 138.0, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 0.3;\rpiano transpgrain 0.;\rpiano modfreqmod 0.5;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 80;\rpiano grainoffset 40;\rpiano grainsize 40;\rpiano maxdelay 2000.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 1.;\rpiano indexdistr 0;\r"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-123", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-124", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"source" : [ "obj-3", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1237.400006651878357, 834.000016629695892, 80.0, 26.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p presets",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 6,
							"revision" : 2,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 34.0, 87.0, 1851.0, 785.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-131",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1628.518338441848755, 435.670968413352966, 131.0, 76.0 ],
									"text" : ";\rbassclar2set feedback;\rbassclar2min 0.1;\rbassclar2max 1.;\rbassclar2curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-132",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1495.590909481048584, 435.670968413352966, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 20.;\rbassclar1max 72.;\rbassclar1curve 1.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-133",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1628.518338441848755, 356.125516176223755, 143.0, 76.0 ],
									"text" : ";\rbassclar2set transpgrain;\rbassclar2min -0.5;\rbassclar2max 0.5;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-134",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 356.125516176223755, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 96.;\rbassclar1max 24.;\rbassclar1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-135",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1628.518338441848755, 278.125516176223755, 127.0, 76.0 ],
									"text" : ";\rbassclar2set modfreq;\rbassclar2min 1.;\rbassclar2max 20.;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-136",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 278.125516176223755, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 133.;\rbassclar1max 333.;\rbassclar1curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-137",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1628.518338441848755, 194.318182468414307, 124.0, 76.0 ],
									"text" : ";\rbassclar2set spacing;\rbassclar2min 120.;\rbassclar2max 40.;\rbassclar2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-138",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 194.318182468414307, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 56.;\rbassclar1max 0.;\rbassclar1curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-130",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1342.725188255310059, 438.85278594493866, 124.0, 76.0 ],
									"text" : ";\rbassclar2set spacing;\rbassclar2min 90;\rbassclar2max 50;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-129",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1209.797759294509888, 438.85278594493866, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 85.;\rbassclar1max 20.;\rbassclar1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-127",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1342.725188255310059, 359.307333707809448, 143.0, 76.0 ],
									"text" : ";\rbassclar2set transpgrain;\rbassclar2min -6.7;\rbassclar2max -4.5;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-128",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1205.706849813461304, 359.307333707809448, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 200.;\rbassclar1max 50.;\rbassclar1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-126",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1342.725188255310059, 281.307333707809448, 127.0, 76.0 ],
									"text" : ";\rbassclar2set modfreq;\rbassclar2min 1.;\rbassclar2max 2200.;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-125",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1205.706849813461304, 281.307333707809448, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 8.;\rbassclar1max 100.;\rbassclar1curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-124",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1342.725188255310059, 197.5, 124.0, 76.0 ],
									"text" : ";\rbassclar2set spacing;\rbassclar2min 1.;\rbassclar2max 100.;\rbassclar2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-123",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1205.706849813461304, 197.5, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 100.;\rbassclar1max 1.;\rbassclar1curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 9,
									"numoutlets" : 9,
									"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "" ],
									"patching_rect" : [ 257.894474148750305, 48.0, 894.0, 22.0 ],
									"text" : "select 0 1 2 3 4 5 6 7"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 257.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"linecount" : 22,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 951.634957671165466, 384.333328664302826, 174.894474148750305, 317.0 ],
									"text" : ";\rbassclar transpout 13.740772;\rbassclar feedback 0.813036;\rbassclar gain 1.;\rbassclar variability 0.519368;\rbassclar transpgrain -17.093793;\rbassclar modfreqmod 0.079739;\rbassclar modmorph 2.652866;\rbassclar modfreq 391.3611;\rbassclar modfactor 0.457532;\rbassclar spacing 79;\rbassclar grainoffset 44;\rbassclar grainsize 36;\rbassclar maxdelay 636.551476;\rbassclar lpffreq 5734.015345;\rbassclar hpffreq 651.065726;\rbassclar grainenvmorph 0.480679;\rbassclar indexdistr 14;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 745.027107119560242, 384.333328664302826, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 0.;\rbassclar feedback 0.7;\rbassclar gain 1.;\rbassclar variability 0.8;\rbassclar transpgrain 0.2;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 187;\rbassclar grainoffset 10;\rbassclar grainsize 233;\rbassclar maxdelay 2000.;\rbassclar lpffreq 12000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 872.634957671165466, 104.0, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 2.;\rbassclar feedback 0.7;\rbassclar gain 1.;\rbassclar variability 0.8;\rbassclar transpgrain 0.2;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 130;\rbassclar grainoffset 200;\rbassclar grainsize 48;\rbassclar maxdelay 500.;\rbassclar lpffreq 12000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 514.105525851249695, 384.333328664302826, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout -2.;\rbassclar feedback 0.8;\rbassclar gain 1.;\rbassclar variability 0.3;\rbassclar transpgrain 1.;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 87;\rbassclar grainoffset 10;\rbassclar grainsize 42;\rbassclar maxdelay 100.;\rbassclar lpffreq 2000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 670.105525851249695, 104.0, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 1.;\rbassclar feedback 0.4;\rbassclar gain 1.;\rbassclar variability 0.8;\rbassclar transpgrain 0.2;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 80;\rbassclar grainoffset 200;\rbassclar grainsize 36;\rbassclar maxdelay 100.;\rbassclar lpffreq 6000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 460.0, 104.0, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout -20.;\rbassclar feedback 0.5;\rbassclar gain 1.;\rbassclar variability 0.3;\rbassclar transpgrain -5.;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.;\rbassclar modfreq 500.;\rbassclar modfactor 0.;\rbassclar spacing 40;\rbassclar grainoffset 20;\rbassclar grainsize 100;\rbassclar maxdelay 2000.;\rbassclar lpffreq 20000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"linecount" : 21,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 306.105525851249695, 384.333328664302826, 174.894474148750305, 303.0 ],
									"text" : ";\rbassclar transpout -1.567927;\rbassclar feedback 0.217766;\rbassclar gain 1.;\rbassclar variability 0.420905;\rbassclar transpgrain -0.30263;\rbassclar modfreqmod 0.451733;\rbassclar modmorph 0.449987;\rbassclar modfreq 2022.356999;\rbassclar modfactor 0.644277;\rbassclar spacing 69;\rbassclar grainoffset 599;\rbassclar grainsize 35;\rbassclar maxdelay 5433.37;\rbassclar lpffreq 14539.336625;\rbassclar hpffreq 1311.302966;\rbassclar grainenvmorph 0.890082;\rbassclar indexdistr 4;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 6,
											"revision" : 2,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 59.0, 118.0, 868.0, 655.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-13",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 530.105525851249695, 389.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-11",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 793.0, 371.0, 150.0, 20.0 ],
													"text" : "voice 11 feb"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-124",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 796.105525851249695, 397.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 10.;\rinstr feedback 0.991;\rinstr gain 1.;\rinstr variability 0.44;\rinstr transpgrain 5.;\rinstr modfreqmod 0.9;\rinstr modmorph 0.098;\rinstr modfreq 199.;\rinstr modfactor 0.41;\rinstr spacing 190;\rinstr grainoffset 0;\rinstr grainsize 211;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.445;\rinstr indexdistr 1;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 294.0, 661.0, 150.0, 20.0 ],
													"text" : "piano transpose 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 294.0, 687.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 1.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 544.0, 661.0, 150.0, 20.0 ],
													"text" : "piano transpose 2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-7",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 548.0, 683.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 6.;\rinstr feedback 0.7;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 290.0, 386.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.5;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 50;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 290.0, 358.0, 150.0, 20.0 ],
													"text" : "clarinet"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 290.0, 15.0, 150.0, 20.0 ],
													"text" : "viola"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 518.447237074375153, 45.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.5;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 2.9;\rinstr modfactor 0.7;\rinstr spacing 17;\rinstr grainoffset 40;\rinstr grainsize 50;\rinstr maxdelay 300.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 279.0, 45.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 16;\rinstr grainoffset 0;\rinstr grainsize 219;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-12",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 8.0, 336.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
 ],
										"lines" : [  ]
									}
,
									"patching_rect" : [ 15.0, 48.0, 35.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p old"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 104.0, 153.0, 263.0 ],
									"text" : ";\rbassclar transpout 0.;\rbassclar feedback 0.;\rbassclar gain 1.;\rbassclar variability 0.;\rbassclar transpgrain 0.;\rbassclar modfreqmod 0.;\rbassclar modmorph 0.;\rbassclar modfreq 0.;\rbassclar modfactor 0.;\rbassclar spacing 1;\rbassclar grainoffset 0;\rbassclar grainsize 250;\rbassclar maxdelay 500.;\rbassclar lpffreq 20000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 1.;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 6.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout -1.567927;\rbassclar feedback 0.217766;\rbassclar gain 1.;\rbassclar variability 0.420905;\rbassclar transpgrain -0.189;\rbassclar modfreqmod 0.451733;\rbassclar modmorph 0.449987;\rbassclar modfreq 1.;\rbassclar modfactor 0.644277;\rbassclar spacing 69;\rbassclar grainoffset 599;\rbassclar grainsize 8;\rbassclar maxdelay 5433.37;\rbassclar lpffreq 14539.336625;\rbassclar hpffreq 1311.302966;\rbassclar grainenvmorph 0.890082;\rbassclar indexdistr 4;\r"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 270.0, 74.0, 150.0, 20.0 ],
									"text" : "reset"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-123", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-124", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-125", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-126", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-127", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-128", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-129", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-130", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-131", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-132", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-133", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-134", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-135", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-138", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"source" : [ "obj-3", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 833.0, 835.000016629695892, 103.0, 26.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p presets",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 18.0,
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 0,
					"patching_rect" : [ 21.0, 640.0, 182.0, 29.0 ],
					"text" : "Granulator_control3",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-26",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 21.0, 698.333328664302826, 165.0, 47.0 ],
					"text" : "need to specify instrument and gran parameter in SEND b-patchers",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 21.0, 346.0, 150.0, 20.0 ],
					"text" : "EAVI input and mapping"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-24",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "EAVI-3.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 21.0, 382.0, 235.0, 187.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 3.0, -1.0, 1249.0, 773.0 ],
					"varname" : "EAVI-2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 1055.0, 234.333328664302826, 70.0, 22.0 ],
					"text" : "mc.pack~ 4"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "piano", "@Channels", 7 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-44",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 1055.0, 260.835540967701036, 199.0, 564.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1656.0, 23.0, 199.0, 564.0 ],
					"varname" : "bbdmi_live.granulator~[3]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 651.0, 234.333328664302826, 70.0, 22.0 ],
					"text" : "mc.pack~ 4"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "bassclar", "@Channels", 7 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-30",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 651.0, 260.835540967701036, 199.0, 564.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1455.0, 23.0, 199.0, 564.0 ],
					"varname" : "bbdmi_live.granulator~[2]",
					"viewvisibility" : 1
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 0 ],
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"source" : [ "obj-115", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"source" : [ "obj-115", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"source" : [ "obj-115", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-120", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-121", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-122", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-123", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"order" : 3,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"order" : 2,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"order" : 1,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"order" : 0,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 2 ],
					"order" : 0,
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-133", 2 ],
					"order" : 1,
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 2 ],
					"order" : 2,
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 2 ],
					"order" : 0,
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 2 ],
					"order" : 1,
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 2 ],
					"order" : 2,
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 2 ],
					"order" : 0,
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 2 ],
					"order" : 1,
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-140", 2 ],
					"order" : 2,
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 2 ],
					"order" : 0,
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 2 ],
					"order" : 1,
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 2 ],
					"order" : 2,
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 1 ],
					"midpoints" : [ 1349.5, 774.0, 1415.0, 774.0, 1415.0, 574.0, 1338.0, 574.0 ],
					"order" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"midpoints" : [ 1349.5, 777.0, 1437.0, 777.0, 1437.0, 564.0, 1327.5, 564.0 ],
					"order" : 1,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-130", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-131", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-132", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-133", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-133", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"source" : [ "obj-138", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 0 ],
					"source" : [ "obj-138", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"source" : [ "obj-138", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-133", 0 ],
					"source" : [ "obj-138", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-139", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 1 ],
					"midpoints" : [ 1360.0, 692.0, 1401.0, 692.0, 1401.0, 585.0, 1338.0, 585.0 ],
					"source" : [ "obj-14", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-140", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-141", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-142", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"source" : [ "obj-147", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-140", 0 ],
					"source" : [ "obj-147", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"source" : [ "obj-147", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 1 ],
					"midpoints" : [ 925.0, 679.5, 965.599993348121643, 679.5, 965.599993348121643, 572.5, 903.0, 572.5 ],
					"source" : [ "obj-15", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 3 ],
					"order" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 2 ],
					"order" : 1,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 1 ],
					"order" : 2,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"order" : 3,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 6 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-30", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 3 ],
					"order" : 0,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 1 ],
					"order" : 1,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 1 ],
					"midpoints" : [ 914.5, 761.5, 979.599993348121643, 761.5, 979.599993348121643, 561.5, 903.0, 561.5 ],
					"order" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"midpoints" : [ 914.5, 764.5, 1001.599993348121643, 764.5, 1001.599993348121643, 551.5, 892.5, 551.5 ],
					"order" : 1,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 2 ],
					"order" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"order" : 1,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-44", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 1 ],
					"order" : 0,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"order" : 1,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 1 ],
					"midpoints" : [ 553.5, 690.0, 594.099993348121643, 690.0, 594.099993348121643, 583.0, 531.5, 583.0 ],
					"source" : [ "obj-48", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 1 ],
					"midpoints" : [ 543.0, 772.0, 608.099993348121643, 772.0, 608.099993348121643, 572.0, 531.5, 572.0 ],
					"order" : 0,
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"midpoints" : [ 543.0, 775.0, 630.099993348121643, 775.0, 630.099993348121643, 562.0, 521.0, 562.0 ],
					"order" : 1,
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 3 ],
					"order" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 2 ],
					"order" : 1,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 1 ],
					"order" : 2,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"order" : 3,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 1 ],
					"order" : 0,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"order" : 1,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-147", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-61", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 1 ],
					"order" : 1,
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 0,
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-24::obj-13::obj-107::obj-33" : [ "tab[80]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-123::obj-33" : [ "tab[78]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-36::obj-33" : [ "tab[82]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-40::obj-33" : [ "tab[83]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-41::obj-33" : [ "tab[84]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-44::obj-33" : [ "tab[87]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-45::obj-33" : [ "tab[88]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-46::obj-33" : [ "tab[117]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-47::obj-33" : [ "tab[118]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-48::obj-33" : [ "tab[119]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-49::obj-33" : [ "tab[120]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-50::obj-33" : [ "tab[121]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-6::obj-33" : [ "tab[86]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-74::obj-33" : [ "tab[79]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-8::obj-33" : [ "tab[85]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-9::obj-33" : [ "tab[81]", "tab[1]", 0 ],
			"obj-24::obj-15" : [ "live.gain~[1]", "leloup~", 0 ],
			"obj-24::obj-175::obj-107::obj-33" : [ "tab[64]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-123::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-36::obj-33" : [ "tab[66]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-40::obj-33" : [ "tab[67]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-41::obj-33" : [ "tab[68]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-44::obj-33" : [ "tab[71]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-45::obj-33" : [ "tab[72]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-46::obj-33" : [ "tab[73]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-47::obj-33" : [ "tab[74]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-48::obj-33" : [ "tab[75]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-49::obj-33" : [ "tab[76]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-50::obj-33" : [ "tab[77]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-6::obj-33" : [ "tab[70]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-74::obj-33" : [ "tab[63]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-8::obj-33" : [ "tab[69]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-9::obj-33" : [ "tab[65]", "tab[1]", 0 ],
			"obj-24::obj-20::obj-122" : [ "number[49]", "number[1]", 0 ],
			"obj-24::obj-20::obj-16" : [ "number[48]", "number[1]", 0 ],
			"obj-24::obj-24::obj-107::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-123::obj-33" : [ "tab[16]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-34::obj-33" : [ "tab[19]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-36::obj-33" : [ "tab[20]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-40::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-41::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-42::obj-33" : [ "tab[23]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-43::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-44::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-45::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-46::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-47::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-48::obj-33" : [ "tab[29]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-49::obj-33" : [ "tab[30]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-50::obj-33" : [ "tab[31]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-74::obj-33" : [ "tab[17]", "tab[1]", 0 ],
			"obj-24::obj-2::obj-122" : [ "number[2]", "number[1]", 0 ],
			"obj-24::obj-2::obj-16" : [ "number[1]", "number[1]", 0 ],
			"obj-24::obj-31::obj-107::obj-33" : [ "tab[33]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-34::obj-33" : [ "tab[34]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-36::obj-33" : [ "tab[35]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-40::obj-33" : [ "tab[36]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-41::obj-33" : [ "tab[37]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-42::obj-33" : [ "tab[38]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-43::obj-33" : [ "tab[39]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-44::obj-33" : [ "tab[40]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-45::obj-33" : [ "tab[41]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-46::obj-33" : [ "tab[42]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-47::obj-33" : [ "tab[43]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-48::obj-33" : [ "tab[44]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-49::obj-33" : [ "tab[45]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-50::obj-33" : [ "tab[46]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-74::obj-33" : [ "tab[32]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-107::obj-33" : [ "tab[49]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-123::obj-33" : [ "tab[47]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-34::obj-33" : [ "tab[50]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-36::obj-33" : [ "tab[51]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-40::obj-33" : [ "tab[52]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-41::obj-33" : [ "tab[53]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-42::obj-33" : [ "tab[54]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-43::obj-33" : [ "tab[55]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-44::obj-33" : [ "tab[56]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-45::obj-33" : [ "tab[57]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-46::obj-33" : [ "tab[58]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-47::obj-33" : [ "tab[59]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-48::obj-33" : [ "tab[60]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-49::obj-33" : [ "tab[61]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-50::obj-33" : [ "tab[62]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-74::obj-33" : [ "tab[48]", "tab[1]", 0 ],
			"obj-24::obj-39" : [ "live.gain~[13]", "live.gain~[2]", 0 ],
			"obj-24::obj-42::obj-107::obj-33" : [ "tab[98]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-123::obj-33" : [ "tab[97]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-36::obj-33" : [ "tab[99]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-40::obj-33" : [ "tab[152]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-41::obj-33" : [ "tab[100]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-44::obj-33" : [ "tab[154]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-45::obj-33" : [ "tab[102]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-46::obj-33" : [ "tab[155]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-47::obj-33" : [ "tab[103]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-48::obj-33" : [ "tab[156]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-49::obj-33" : [ "tab[104]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-50::obj-33" : [ "tab[183]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-6::obj-33" : [ "tab[101]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-74::obj-33" : [ "tab[150]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-8::obj-33" : [ "tab[153]", "tab[1]", 0 ],
			"obj-24::obj-42::obj-9::obj-33" : [ "tab[151]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-107::obj-33" : [ "tab[140]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-123::obj-33" : [ "tab[138]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-34::obj-33" : [ "tab[141]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-36::obj-33" : [ "tab[142]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-40::obj-33" : [ "tab[143]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-41::obj-33" : [ "tab[144]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-42::obj-33" : [ "tab[145]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-43::obj-33" : [ "tab[93]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-44::obj-33" : [ "tab[146]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-45::obj-33" : [ "tab[94]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-46::obj-33" : [ "tab[147]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-47::obj-33" : [ "tab[95]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-48::obj-33" : [ "tab[148]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-49::obj-33" : [ "tab[96]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-50::obj-33" : [ "tab[149]", "tab[1]", 0 ],
			"obj-24::obj-49::obj-74::obj-33" : [ "tab[139]", "tab[1]", 0 ],
			"obj-24::obj-52::obj-122" : [ "number[53]", "number[1]", 0 ],
			"obj-24::obj-52::obj-16" : [ "number[50]", "number[1]", 0 ],
			"obj-24::obj-57::obj-107::obj-33" : [ "tab[128]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-123::obj-33" : [ "tab[126]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-34::obj-33" : [ "tab[89]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-36::obj-33" : [ "tab[90]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-40::obj-33" : [ "tab[91]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-41::obj-33" : [ "tab[92]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-42::obj-33" : [ "tab[129]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-43::obj-33" : [ "tab[130]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-44::obj-33" : [ "tab[131]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-45::obj-33" : [ "tab[132]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-46::obj-33" : [ "tab[133]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-47::obj-33" : [ "tab[134]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-48::obj-33" : [ "tab[135]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-49::obj-33" : [ "tab[136]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-50::obj-33" : [ "tab[137]", "tab[1]", 0 ],
			"obj-24::obj-57::obj-74::obj-33" : [ "tab[127]", "tab[1]", 0 ],
			"obj-24::obj-67" : [ "live.gain~", "leloup~", 0 ],
			"obj-24::obj-9" : [ "live.gain~[3]", "leloup~", 0 ],
			"obj-24::obj-93::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-123::obj-33" : [ "tab[182]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-34::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-42::obj-33" : [ "tab[7]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-43::obj-33" : [ "tab[8]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-44::obj-33" : [ "tab[9]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-45::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-46::obj-33" : [ "tab[11]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-47::obj-33" : [ "tab[12]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-48::obj-33" : [ "tab[13]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-49::obj-33" : [ "tab[14]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-50::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"obj-30::obj-101" : [ "number[23]", "number[7]", 0 ],
			"obj-30::obj-104" : [ "number[21]", "number[5]", 0 ],
			"obj-30::obj-109" : [ "number[30]", "number[6]", 0 ],
			"obj-30::obj-113" : [ "number[17]", "number[9]", 0 ],
			"obj-30::obj-118" : [ "number[27]", "number[2]", 0 ],
			"obj-30::obj-123" : [ "number[19]", "number[3]", 0 ],
			"obj-30::obj-28" : [ "number[25]", "number[12]", 0 ],
			"obj-30::obj-43" : [ "number[29]", "number[4]", 0 ],
			"obj-30::obj-44" : [ "number[28]", "number[13]", 0 ],
			"obj-30::obj-61" : [ "number[24]", "number[9]", 0 ],
			"obj-30::obj-68" : [ "number[20]", "number[4]", 0 ],
			"obj-30::obj-72" : [ "number[18]", "number[11]", 0 ],
			"obj-30::obj-77" : [ "number[16]", "number[10]", 0 ],
			"obj-30::obj-90" : [ "number[26]", "number", 0 ],
			"obj-30::obj-93" : [ "number[22]", "number[8]", 0 ],
			"obj-30::obj-97" : [ "number[31]", "number[1]", 0 ],
			"obj-44::obj-101" : [ "number[45]", "number[7]", 0 ],
			"obj-44::obj-104" : [ "number[46]", "number[5]", 0 ],
			"obj-44::obj-109" : [ "number[47]", "number[6]", 0 ],
			"obj-44::obj-113" : [ "number[44]", "number[9]", 0 ],
			"obj-44::obj-118" : [ "number[33]", "number[2]", 0 ],
			"obj-44::obj-123" : [ "number[42]", "number[3]", 0 ],
			"obj-44::obj-28" : [ "number[32]", "number[12]", 0 ],
			"obj-44::obj-43" : [ "number[39]", "number[4]", 0 ],
			"obj-44::obj-44" : [ "number[41]", "number[13]", 0 ],
			"obj-44::obj-61" : [ "number[38]", "number[9]", 0 ],
			"obj-44::obj-68" : [ "number[34]", "number[4]", 0 ],
			"obj-44::obj-72" : [ "number[35]", "number[11]", 0 ],
			"obj-44::obj-77" : [ "number[40]", "number[10]", 0 ],
			"obj-44::obj-90" : [ "number[43]", "number", 0 ],
			"obj-44::obj-93" : [ "number[37]", "number[8]", 0 ],
			"obj-44::obj-97" : [ "number[36]", "number[1]", 0 ],
			"obj-61::obj-101" : [ "number[7]", "number[7]", 0 ],
			"obj-61::obj-104" : [ "number[5]", "number[5]", 0 ],
			"obj-61::obj-109" : [ "number[6]", "number[6]", 0 ],
			"obj-61::obj-113" : [ "number[9]", "number[9]", 0 ],
			"obj-61::obj-118" : [ "number[52]", "number[2]", 0 ],
			"obj-61::obj-123" : [ "number[3]", "number[3]", 0 ],
			"obj-61::obj-28" : [ "number[12]", "number[12]", 0 ],
			"obj-61::obj-43" : [ "number[14]", "number[4]", 0 ],
			"obj-61::obj-44" : [ "number[13]", "number[13]", 0 ],
			"obj-61::obj-61" : [ "number[15]", "number[9]", 0 ],
			"obj-61::obj-68" : [ "number[4]", "number[4]", 0 ],
			"obj-61::obj-72" : [ "number[11]", "number[11]", 0 ],
			"obj-61::obj-77" : [ "number[10]", "number[10]", 0 ],
			"obj-61::obj-90" : [ "number", "number", 0 ],
			"obj-61::obj-93" : [ "number[8]", "number[8]", 0 ],
			"obj-61::obj-97" : [ "number[54]", "number[1]", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"parameter_overrides" : 			{
				"obj-24::obj-39" : 				{
					"parameter_longname" : "live.gain~[13]"
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"parameter_map" : 		{
			"midi" : 			{
				"number" : 				{
					"srcname" : "2.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[1]" : 				{
					"srcname" : "3.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[2]" : 				{
					"srcname" : "4.ctrl.0.chan.midi",
					"min" : 20.0,
					"max" : 20000.0,
					"flags" : 2
				}
,
				"number[3]" : 				{
					"srcname" : "5.ctrl.0.chan.midi",
					"min" : 20.0,
					"max" : 20000.0,
					"flags" : 2
				}
,
				"number[4]" : 				{
					"srcname" : "6.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 10000.0,
					"flags" : 2
				}
,
				"number[5]" : 				{
					"srcname" : "8.ctrl.0.chan.midi",
					"min" : 1.0,
					"max" : 2000.0,
					"flags" : 2
				}
,
				"number[6]" : 				{
					"srcname" : "9.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1000.0,
					"flags" : 2
				}
,
				"number[7]" : 				{
					"srcname" : "12.ctrl.0.chan.midi",
					"min" : 1.0,
					"max" : 10000.0,
					"flags" : 2
				}
,
				"number[8]" : 				{
					"srcname" : "13.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[9]" : 				{
					"srcname" : "14.ctrl.0.chan.midi",
					"min" : -48.0,
					"max" : 48.0,
					"flags" : 2
				}
,
				"number[10]" : 				{
					"srcname" : "15.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[11]" : 				{
					"srcname" : "16.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 15000.0,
					"flags" : 2
				}
,
				"number[12]" : 				{
					"srcname" : "17.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 3.0,
					"flags" : 2
				}
,
				"number[13]" : 				{
					"srcname" : "18.ctrl.0.chan.midi",
					"min" : -48.0,
					"max" : 48.0,
					"flags" : 2
				}

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "EAVI-3.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2024-11-19_ArsMusica/20240415convergence",
				"patcherrelativepath" : "../20240415convergence",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Granulator_control3.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2024-11-19_ArsMusica/20240415convergence",
				"patcherrelativepath" : "../20240415convergence",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "RD-perf-a.json",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2023-11-24_Athenor/RD/preset",
				"patcherrelativepath" : "../../2023-11-24_Athenor/RD/preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../../../max/input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../../max/utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../../max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../../max/feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.send.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/send",
				"patcherrelativepath" : "../../../max/utilities/send",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.smooth.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/smooth",
				"patcherrelativepath" : "../../../max/control_processing/smooth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_live_granulator~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/sound_synthesis/live_granulator~",
				"patcherrelativepath" : "../../../max/sound_synthesis/live_granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_multi_granulator4~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/js",
				"patcherrelativepath" : "../../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "pan2.maxpat",
				"bootpath" : "~/Library/Application Support/Cycling '74/Max 8/Examples/spatialization/panning/lib",
				"patcherrelativepath" : "../../../../../../Library/Application Support/Cycling '74/Max 8/Examples/spatialization/panning/lib",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
