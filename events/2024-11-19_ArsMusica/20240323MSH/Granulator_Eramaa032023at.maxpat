{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 5,
			"revision" : 6,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 20.0, 159.0, 1446.0, 795.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 541.0, 1029.0, 150.0, 20.0 ],
					"text" : "set up mappings here"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 14.0,
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 541.0, 1059.0, 138.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 232.0, 327.800000071525574, 138.0, 24.0 ],
					"text" : "Granulator_Control",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"activecolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ],
					"disabled" : [ 0, 0, 0, 0, 0, 0, 0, 0 ],
					"id" : "obj-67",
					"itemtype" : 0,
					"maxclass" : "radiogroup",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 950.0, 951.0, 18.0, 130.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 378.200002253055573, 221.800000071525574, 21.0, 130.0 ],
					"shape" : 2,
					"size" : 8,
					"value" : 0
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 10.0,
					"id" : "obj-66",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 250.0, 545.0, 143.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.0, 523.0, 174.0, 18.0 ],
					"text" : "BC gran  PN gran    5      6      7      8"
				}

			}
, 			{
				"box" : 				{
					"activecolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ],
					"disabled" : [ 0, 0, 0, 0, 0, 0, 0, 0 ],
					"id" : "obj-59",
					"itemtype" : 0,
					"maxclass" : "radiogroup",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 476.0, 951.0, 18.0, 130.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.400000095367432, 221.800000071525574, 21.0, 130.0 ],
					"shape" : 2,
					"size" : 8,
					"value" : 0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 973.5, 326.700004279613495, 57.0, 22.0 ],
					"text" : "ctlin 88 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 914.4000044465065, 326.700004279613495, 57.0, 22.0 ],
					"text" : "ctlin 87 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 855.4000044465065, 326.700004279613495, 57.0, 22.0 ],
					"text" : "ctlin 86 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 796.4000044465065, 326.700004279613495, 57.0, 22.0 ],
					"text" : "ctlin 85 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 943.0, 385.0, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 368.0, 381.0, 20.0, 140.0 ],
					"style" : "rnbolight"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 921.0, 385.0, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 346.0, 381.0, 20.0, 140.0 ],
					"style" : "rnbolight"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 899.0, 385.0, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 324.0, 381.0, 20.0, 140.0 ],
					"style" : "rnbolight"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 877.0, 385.0, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 302.0, 381.0, 20.0, 140.0 ],
					"style" : "rnbolight"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 855.0, 385.0, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 280.0, 381.0, 20.0, 140.0 ],
					"style" : "rnbolight"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 833.0, 385.0, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 258.0, 381.0, 20.0, 140.0 ],
					"style" : "rnbolight"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 811.0, 385.0, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 236.0, 381.0, 20.0, 140.0 ],
					"style" : "rnbolight"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 789.0, 385.0, 20.0, 140.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.0, 381.0, 20.0, 140.0 ],
					"style" : "rnbolight"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 444.300006508827209, 1421.600021183490753, 68.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 262.400005698204041, 581.000003039836884, 68.0, 20.0 ],
					"text" : "Master Out"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 760.600011467933655, 1216.000018119812012, 101.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 498.000006258487701, 585.000003099441528, 101.0, 20.0 ],
					"text" : "Gran piano guitar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 593.600008845329285, 664.000009894371033, 119.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 365.200004279613495, 580.200003027915955, 119.0, 20.0 ],
					"text" : "EncodedPianoGuitar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 167.600002527236938, 1248.800018608570099, 87.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 125.000004708766937, 578.600003004074097, 87.0, 20.0 ],
					"text" : "Gran clarinette"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 0.0, 0.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 376.00000411272049, 53.0, 22.0 ],
									"text" : "clip 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 320.800003290176392, 104.0, 22.0 ],
									"text" : "r granulationLevel"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-124",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 50.0, 352.000003755092621, 47.0, 22.0 ],
									"text" : "line 20."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 167.600001752376556, 376.00000411272049, 53.0, 22.0 ],
									"text" : "clip 0. 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 167.600001752376556, 320.000003278255463, 91.0, 22.0 ],
									"text" : "r encodedLevel"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-125",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 167.600001752376556, 348.800003707408905, 47.0, 22.0 ],
									"text" : "line 20."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 284.400003492832184, 717.600009202957153, 143.0, 62.0 ],
									"text" : ";\rencodedLevel 0.5 500;\rgranulationLevel 0.5 500;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-42",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 131.600001215934753, 500.000005960464478, 137.0, 62.0 ],
									"text" : ";\rencodedLevel 0.9 500;\rgranulationLevel 0. 500;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-23",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 284.400003492832184, 582.40000718832016, 137.0, 62.0 ],
									"text" : ";\rencodedLevel 0.1 500;\rgranulationLevel 1. 500;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-33",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 281.200003445148468, 500.000005960464478, 143.0, 62.0 ],
									"text" : ";\rencodedLevel 0.9 500;\rgranulationLevel 0.1 500;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-32",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 330.000004172325134, 333.600003480911255, 40.0, 20.0 ],
									"text" : "Reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-28",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 487.600006520748138, 333.600003480911255, 37.0, 20.0 ],
									"text" : "1 sec"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 604.400008261203766, 350.400003731250763, 137.0, 62.0 ],
									"text" : ";\rencodedLevel 0.3 500;\rgranulationLevel 1. 500;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 437.200005769729614, 356.000003814697266, 137.0, 62.0 ],
									"text" : ";\rencodedLevel 0.3 500;\rgranulationLevel 1. 500;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"linecount" : 3,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 281.200003445148468, 356.000003814697266, 137.0, 62.0 ],
									"text" : ";\rencodedLevel 0.3 500;\rgranulationLevel 1. 500;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"maxclass" : "newobj",
									"numinlets" : 4,
									"numoutlets" : 4,
									"outlettype" : [ "bang", "bang", "bang", "" ],
									"patching_rect" : [ 281.200003445148468, 293.600002884864807, 67.0, 22.0 ],
									"text" : "sel 0 1 300"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 281.200003445148468, 260.000002384185791, 74.0, 22.0 ],
									"text" : "r globalTime"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-359",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 285.200003504753113, 224.000001847743988, 67.0, 20.0 ],
									"text" : "globalTime"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-357",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 285.200003504753113, 197.600001454353333, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-344",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 285.200003504753113, 168.000001013278961, 74.0, 22.0 ],
									"text" : "r globalTime"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-351",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "int", "int" ],
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 5,
											"revision" : 6,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 838.0, 176.0, 640.0, 772.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"format" : 6,
													"id" : "obj-10",
													"maxclass" : "flonum",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 259.0, 131.0, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-6",
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 66.000021222423584, 97.0, 29.5, 22.0 ],
													"text" : "0"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "newobj",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 206.5, 76.5, 91.0, 22.0 ],
													"text" : "r counterGOTO"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-346",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 234.5, 234.0, 63.0, 22.0 ],
													"text" : "s secones"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-345",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 62.5, 234.0, 60.0, 22.0 ],
													"text" : "s minutes"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-344",
													"maxclass" : "newobj",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 50.0, 195.33666805744167, 76.0, 22.0 ],
													"text" : "s globalTime"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-332",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 163.599997222423553, 199.5, 36.0, 22.0 ],
													"text" : "% 60"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-331",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 127.599997222423553, 199.5, 29.5, 22.0 ],
													"text" : "/ 60"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-322",
													"maxclass" : "number",
													"numinlets" : 1,
													"numoutlets" : 2,
													"outlettype" : [ "", "bang" ],
													"parameter_enable" : 0,
													"patching_rect" : [ 127.599997222423553, 167.5, 50.0, 22.0 ]
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-319",
													"maxclass" : "newobj",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 127.599997222423553, 85.0, 69.0, 22.0 ],
													"text" : "metro 1000"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-318",
													"maxclass" : "newobj",
													"numinlets" : 5,
													"numoutlets" : 4,
													"outlettype" : [ "int", "", "", "int" ],
													"patching_rect" : [ 127.599997222423553, 133.833330998918541, 61.0, 22.0 ],
													"text" : "counter"
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-347",
													"index" : 1,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "bang" ],
													"patching_rect" : [ 66.000021222423584, 40.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-348",
													"index" : 2,
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"outlettype" : [ "int" ],
													"patching_rect" : [ 127.599997222423553, 40.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-349",
													"index" : 1,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 127.599997222423553, 316.0, 30.0, 30.0 ]
												}

											}
, 											{
												"box" : 												{
													"comment" : "",
													"id" : "obj-350",
													"index" : 2,
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 163.599997222423553, 316.0, 30.0, 30.0 ]
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"destination" : [ "obj-318", 3 ],
													"source" : [ "obj-10", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-318", 3 ],
													"source" : [ "obj-2", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-322", 0 ],
													"source" : [ "obj-318", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-318", 0 ],
													"source" : [ "obj-319", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-331", 0 ],
													"order" : 1,
													"source" : [ "obj-322", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-332", 0 ],
													"order" : 0,
													"source" : [ "obj-322", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-344", 0 ],
													"order" : 2,
													"source" : [ "obj-322", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-345", 0 ],
													"order" : 1,
													"source" : [ "obj-331", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-349", 0 ],
													"order" : 0,
													"source" : [ "obj-331", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-346", 0 ],
													"order" : 0,
													"source" : [ "obj-332", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-350", 0 ],
													"order" : 1,
													"source" : [ "obj-332", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-6", 0 ],
													"source" : [ "obj-347", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-319", 0 ],
													"source" : [ "obj-348", 0 ]
												}

											}
, 											{
												"patchline" : 												{
													"destination" : [ "obj-318", 3 ],
													"source" : [ "obj-6", 0 ]
												}

											}
 ]
									}
,
									"patching_rect" : [ 363.60000467300415, 168.000001013278961, 49.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p Timer"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-340",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 415.600005447864532, 224.000001847743988, 62.0, 20.0 ],
									"text" : "Secondes"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-338",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 363.60000467300415, 224.000001847743988, 51.0, 20.0 ],
									"text" : "Minutes"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-336",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 415.600005447864532, 200.000001490116119, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-334",
									"maxclass" : "number",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 363.60000467300415, 200.000001490116119, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-328",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 425.20000559091568, 136.000000536441803, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-323",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 401.200005233287811, 112.000000178813934, 71.0, 20.0 ],
									"text" : "Start/Pause"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-324",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 355.600004553794861, 112.000000178813934, 40.0, 20.0 ],
									"text" : "Reset"
								}

							}
, 							{
								"box" : 								{
									"blinkcolor" : [ 0.501961, 0.501961, 0.501961, 1.0 ],
									"id" : "obj-325",
									"maxclass" : "button",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"parameter_enable" : 1,
									"patching_rect" : [ 363.60000467300415, 136.000000536441803, 24.0, 24.0 ],
									"saved_attribute_attributes" : 									{
										"valueof" : 										{
											"parameter_enum" : [ "off", "on" ],
											"parameter_longname" : "button[2]",
											"parameter_mmax" : 1,
											"parameter_shortname" : "button",
											"parameter_type" : 2
										}

									}
,
									"varname" : "button[2]"
								}

							}
, 							{
								"box" : 								{
									"angle" : 270.0,
									"bgcolor" : [ 0.823529411764706, 1.0, 0.0, 1.0 ],
									"id" : "obj-134",
									"maxclass" : "panel",
									"mode" : 0,
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 281.200003445148468, 100.0, 219.947710029103519, 147.129882872104645 ],
									"proportion" : 0.5
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-124", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-124", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-125", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-125", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-351", 0 ],
									"source" : [ "obj-325", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-351", 1 ],
									"source" : [ "obj-328", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-357", 0 ],
									"source" : [ "obj-344", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-334", 0 ],
									"source" : [ "obj-351", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-336", 0 ],
									"source" : [ "obj-351", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"source" : [ "obj-73", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"source" : [ "obj-74", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"source" : [ "obj-74", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"source" : [ "obj-74", 2 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1088.800016224384308, 100.000001490116119, 154.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p timeAutomationNotUSED"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 680.800010144710541, 171.200002551078796, 41.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 354.600002288818359, 43.800000369548798, 41.0, 20.0 ],
					"text" : "Guitar"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 769.600011467933655, 1236.800018429756165, 83.0, 122.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 506.80000638961792, 605.800003409385681, 83.0, 122.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 169.600002527236938, 1276.000019013881683, 83.0, 122.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 126.600004732608795, 605.800003409385681, 83.0, 122.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-84",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 746.400011122226715, 1172.000017464160919, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-82",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 634.400009453296661, 505.6000075340271, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-80",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 299.200004458427429, 1184.800017654895782, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-78",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 532.800007939338684, 505.6000075340271, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 298.4000044465065, 1146.400017082691193, 98.0, 22.0 ],
					"text" : "zmap 0 127 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 746.400011122226715, 1143.200017035007477, 98.0, 22.0 ],
					"text" : "zmap 0 127 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 634.400009453296661, 472.800007045269012, 98.0, 22.0 ],
					"text" : "zmap 0 127 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 532.800007939338684, 471.200007021427155, 98.0, 22.0 ],
					"text" : "zmap 0 127 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 928.5, 299.700004279613495, 57.0, 22.0 ],
					"text" : "ctlin 84 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 869.4000044465065, 299.700004279613495, 57.0, 22.0 ],
					"text" : "ctlin 83 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 810.4000044465065, 299.700004279613495, 57.0, 22.0 ],
					"text" : "ctlin 82 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 751.4000044465065, 299.700004279613495, 57.0, 22.0 ],
					"text" : "ctlin 81 1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-62",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1016.800015151500702, 268.0000039935112, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-60",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 956.80001425743103, 266.400003969669342, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-58",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 900.800013422966003, 266.400003969669342, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "int", "int" ],
					"patching_rect" : [ 900.800013422966003, 230.400003433227539, 40.0, 22.0 ],
					"text" : "ctlin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 704.800010502338409, 1203.200017929077148, 60.0, 22.0 ],
					"text" : "mc.*~ 0.3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 258.400003850460052, 1216.000018119812012, 60.0, 22.0 ],
					"text" : "mc.*~ 0.3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 611.400009453296661, 689.600010275840759, 83.0, 122.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 382.800004541873932, 605.800003409385681, 83.0, 122.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 488.000007271766663, 689.600010275840759, 83.0, 122.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 21.800003170967102, 602.600003361701965, 83.0, 122.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 436.800006508827209, 1446.400021553039551, 83.0, 122.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 255.20000559091568, 605.800003409385681, 83.0, 122.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 422.400006294250488, 1323.200019717216492, 41.0, 41.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 238.400005340576172, 741.200005412101746, 22.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 422.400006294250488, 1376.000020503997803, 59.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 262.400005340576172, 741.200005412101746, 59.0, 22.0 ],
					"text" : "stereo $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 476.000007271766663, 664.000009894371033, 107.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 9.800002992153168, 577.000002980232239, 107.0, 20.0 ],
					"text" : "EncodedClarinette"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 634.400009453296661, 541.600008070468903, 60.0, 22.0 ],
					"text" : "mc.*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 492.000007331371307, 541.600008070468903, 60.0, 22.0 ],
					"text" : "mc.*~ 0.1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 678.400010108947754, 200.800002992153168, 47.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 352.200002253055573, 73.400000810623169, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[12]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[9]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 580.000008642673492, 200.800002992153168, 47.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 301.000001490116119, 70.200000762939453, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[11]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[3]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 458.400006830692291, 200.800002992153168, 47.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 213.800000190734863, 70.200000762939453, 47.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "live.gain~[10]",
							"parameter_mmax" : 6.0,
							"parameter_mmin" : -70.0,
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "live.gain~[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 580.000008642673492, 144.800002157688141, 55.0, 22.0 ],
					"text" : "adc~ 6 7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 119.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 58.0, 70.0, 22.0 ],
									"text" : "loadmess 1"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-96",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 172.666678905487061, 366.000007927417755, 55.0, 22.0 ],
									"text" : "angle $1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-94",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 182.000012516975403, 396.666675508022308, 106.0, 22.0 ],
									"text" : "loadmess speed 0"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-90",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 172.666678905487061, 329.333340167999268, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-91",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 172.666678905487061, 294.000005781650543, 47.0, 22.0 ],
									"text" : "line 20."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-92",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 172.666678905487061, 264.666671574115753, 52.0, 22.0 ],
									"text" : "$1 6000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-93",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 172.666678905487061, 234.666670680046082, 103.0, 22.0 ],
									"text" : "scale 0. 1. 1. 180."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-86",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 59.000008583068848, 398.666675567626953, 106.0, 22.0 ],
									"text" : "loadmess speed 0"
								}

							}
, 							{
								"box" : 								{
									"format" : 6,
									"id" : "obj-64",
									"maxclass" : "flonum",
									"numinlets" : 1,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.000008583068848, 329.333340167999268, 50.0, 22.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-84",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "bang" ],
									"patching_rect" : [ 50.0, 294.000005781650543, 47.0, 22.0 ],
									"text" : "line 20."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-80",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 264.666671574115753, 52.0, 22.0 ],
									"text" : "$1 6000"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-76",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 234.666670680046082, 111.0, 22.0 ],
									"text" : "scale 0. 1. -1. -180."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 172.666678905487061, 200.66666966676712, 39.0, 22.0 ],
									"text" : "/ 100."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-59",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 50.0, 200.66666966676712, 39.0, 22.0 ],
									"text" : "/ 100."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-58",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 172.666678905487061, 172.000002145767212, 73.0, 22.0 ],
									"text" : "random 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-56",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 50.0, 172.000002145767212, 73.0, 22.0 ],
									"text" : "random 100"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-54",
									"maxclass" : "toggle",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"parameter_enable" : 0,
									"patching_rect" : [ 50.0, 100.0, 24.0, 24.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "bang" ],
									"patching_rect" : [ 50.0, 134.666667699813843, 69.0, 22.0 ],
									"text" : "metro 6000"
								}

							}
, 							{
								"box" : 								{
									"bgcolor" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgcolor2" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_angle" : 270.0,
									"bgfillcolor_autogradient" : 0.0,
									"bgfillcolor_color" : [ 0.498039215686275, 0.498039215686275, 0.498039215686275, 1.0 ],
									"bgfillcolor_color1" : [ 0.301961, 0.301961, 0.301961, 1.0 ],
									"bgfillcolor_color2" : [ 0.2, 0.2, 0.2, 1.0 ],
									"bgfillcolor_proportion" : 0.5,
									"bgfillcolor_type" : "color",
									"gradient" : 1,
									"id" : "obj-69",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.000008583068848, 365.666628181934357, 55.0, 22.0 ],
									"text" : "angle $1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-108",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 48.5, 480.666628181934357, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-109",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 171.333344000000011, 480.666628181934357, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-54", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-56", 0 ],
									"order" : 1,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-58", 0 ],
									"order" : 0,
									"source" : [ "obj-43", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"source" : [ "obj-54", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-59", 0 ],
									"source" : [ "obj-56", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"source" : [ "obj-58", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-76", 0 ],
									"source" : [ "obj-59", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-93", 0 ],
									"source" : [ "obj-61", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-69", 0 ],
									"source" : [ "obj-64", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"source" : [ "obj-69", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-80", 0 ],
									"source" : [ "obj-76", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-84", 0 ],
									"source" : [ "obj-80", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"source" : [ "obj-84", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-108", 0 ],
									"source" : [ "obj-86", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"source" : [ "obj-90", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-90", 0 ],
									"source" : [ "obj-91", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-91", 0 ],
									"source" : [ "obj-92", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-92", 0 ],
									"source" : [ "obj-93", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-94", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-109", 0 ],
									"source" : [ "obj-96", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 458.400006830692291, 352.800005257129669, 83.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p positionCTL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 634.400009453296661, 391.200005829334259, 115.0, 22.0 ],
					"text" : "abc.hoa.encoder~ 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 532.000007927417755, 1400.000020861625671, 54.0, 22.0 ],
					"text" : "mc.dac~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 799.200011909008026, 81.600001215934753, 266.0, 47.0 ],
					"text" : "ADC 1/2 = micro bass clar\nADC 3 = clavier elctronique piano\nADC 4 = micro guitar electrique préparée"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 892.800013303756714, 133.600001990795135, 150.0, 33.0 ],
					"text" : "Piano recording\npiano-lo-hit-44.wav"
				}

			}
, 			{
				"box" : 				{
					"basictuning" : 440,
					"clipheight" : 28.0,
					"data" : 					{
						"clips" : [ 							{
								"absolutepath" : "piano-lo-hit-44.wav",
								"filename" : "piano-lo-hit-44.wav",
								"filekind" : "audiofile",
								"id" : "u518011897",
								"selection" : [ 0.0, 1.0 ],
								"loop" : 1,
								"content_state" : 								{
									"loop" : 1
								}

							}
 ]
					}
,
					"followglobaltempo" : 0,
					"formantcorrection" : 0,
					"id" : "obj-40",
					"maxclass" : "playlist~",
					"mode" : "basic",
					"numinlets" : 1,
					"numoutlets" : 5,
					"originallength" : [ 0.0, "ticks" ],
					"originaltempo" : 120.0,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 580.000008642673492, 81.600001215934753, 181.490043975412846, 29.0 ],
					"pitchcorrection" : 0,
					"quality" : "basic",
					"timestretch" : [ 0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 300.000004470348358, 44.000000655651093, 150.0, 33.0 ],
					"text" : "Bass clarinet recording\nbassclar-01.wav"
				}

			}
, 			{
				"box" : 				{
					"basictuning" : 440,
					"clipheight" : 31.162246728900755,
					"data" : 					{
						"clips" : [ 							{
								"absolutepath" : "bassclar-01.wav",
								"filename" : "bassclar-01.wav",
								"filekind" : "audiofile",
								"id" : "u532004044",
								"selection" : [ 0.0, 1.0 ],
								"loop" : 1,
								"content_state" : 								{
									"loop" : 1
								}

							}
 ]
					}
,
					"followglobaltempo" : 0,
					"formantcorrection" : 0,
					"id" : "obj-5",
					"maxclass" : "playlist~",
					"mode" : "basic",
					"numinlets" : 1,
					"numoutlets" : 5,
					"originallength" : [ 0.0, "ticks" ],
					"originaltempo" : 120.0,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 458.400006830692291, 44.800000667572021, 181.490043975412846, 32.162246728900755 ],
					"pitchcorrection" : 0,
					"quality" : "basic",
					"timestretch" : [ 0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 584.000008642673492, 174.400002598762512, 39.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 305.000001549720764, 43.800000369548798, 39.0, 20.0 ],
					"text" : "Piano"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 451.900006830692291, 174.400002598762512, 60.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.400000095367432, 43.800000369548798, 60.0, 20.0 ],
					"text" : "Clarinette"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 458.400006830692291, 144.800002157688141, 45.0, 22.0 ],
					"text" : "adc~ 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 458.400006830692291, 384.800005733966827, 115.0, 22.0 ],
					"text" : "abc.hoa.encoder~ 5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 944.000014066696167, 2644.800039410591125, 100.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2300.000034272670746, 2176.800032436847687, 100.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 650.40000969171524, 1397.600020825862885, 55.0, 22.0 ],
					"text" : "pipe 200"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 640.000009536743164, 1365.600020349025726, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"linecount" : 13,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 640.000009536743164, 1432.00002133846283, 91.0, 196.0 ],
					"text" : ";\rdec a00 -20.;\rdec a01 -20.;\rdec a02 47.;\rdec a03 65.;\rdec a04 90.;\rdec a05 115.5;\rdec a06 150;\rdec a07 -150;\rdec a08 -115.5;\rdec a09 -90.;\rdec a10 -65.;\rdec a11 -47.;\r"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 488.000007271766663, 1341.600019991397858, 35.0, 22.0 ],
					"text" : "r dec"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-402",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "multichannelsignal" ],
					"patching_rect" : [ 532.000007927417755, 1341.600019991397858, 166.0, 22.0 ],
					"text" : "abc.hoa.decoder~ 5 @spk 12"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 3,
					"id" : "obj-26",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 38.40000057220459, 287.200004279613495, 165.0, 47.0 ],
					"text" : "need to specify instrument and gran parameter in SEND b-patchers",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 38.40000057220459, 228.800003409385681, 150.0, 20.0 ],
					"text" : "EAVI input and mapping"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-24",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "EAVI-2.maxpat",
					"numinlets" : 1,
					"numoutlets" : 0,
					"offset" : [ 0.0, 0.0 ],
					"patching_rect" : [ 1125.000000655651093, 213.400003969669342, 834.0, 771.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 608.00000786781311, 4.800000250339508, 834.0, 771.0 ],
					"varname" : "EAVI-2",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"id" : "obj-21",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 887.800013601779938, 1147.400017082691193, 160.0, 20.0 ],
					"text" : "piano parameters here",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"id" : "obj-19",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 438.400006532669067, 1146.400017082691193, 160.0, 20.0 ],
					"text" : "bass clar parameters here",
					"textcolor" : [ 0.941176470588235, 0.219607843137255, 0.219607843137255, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ -1844.0, -316.0, 1779.0, 833.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-2",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1629.018338441848755, 594.807333707809448, 116.0, 76.0 ],
									"text" : ";\rpiano2set feedback;\rpiano2min 0.1;\rpiano2max 1.;\rpiano2curve 1.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1629.018338441848755, 517.989151239395142, 117.0, 76.0 ],
									"text" : ";\rpiano2set transpout;\rpiano2min 24.;\rpiano2max 48.;\rpiano2curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1624.311488628387451, 432.807333707809448, 111.0, 76.0 ],
									"text" : ";\rpiano2set modfreq;\rpiano2min 0.;\rpiano2max 2.;\rpiano2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-8",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 594.807333707809448, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 600.;\rpiano1max 50.;\rpiano1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 514.807333707809448, 127.0, 76.0 ],
									"text" : ";\rpiano1set transpgrain;\rpiano1min -12.;\rpiano1max -48.;\rpiano1curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-10",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 432.807333707809448, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 300.;\rpiano1max 50.;\rpiano1curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-11",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1493.0, 349.0, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 600.;\rpiano1max 150.;\rpiano1curve 1.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-15",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1332.018338441848755, 597.989151239395142, 109.0, 76.0 ],
									"text" : ";\rpiano2set spacing;\rpiano2min 175.;\rpiano2max 70.;\rpiano2curve 1.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-16",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1193.5, 597.989151239395142, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 600.;\rpiano1max 300.;\rpiano1curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-17",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1332.018338441848755, 517.989151239395142, 117.0, 76.0 ],
									"text" : ";\rpiano2set transpout;\rpiano2min 7.;\rpiano2max 14.;\rpiano2curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-18",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1193.5, 517.989151239395142, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 100.;\rpiano1max 1.;\rpiano1curve 0.75"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-19",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1332.018338441848755, 435.989151239395142, 111.0, 76.0 ],
									"text" : ";\rpiano2set modfreq;\rpiano2min 1000.;\rpiano2max 6000.;\rpiano2curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-21",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1193.5, 435.989151239395142, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 300.;\rpiano1max 50.;\rpiano1curve 1.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-22",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 1185.5, 316.0, 150.0, 20.0 ],
									"text" : "correspond to presets 1-8"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-137",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1617.811488628387451, 349.0, 124.0, 76.0 ],
									"text" : ";\rbassclar2set spacing;\rbassclar2min 200.;\rbassclar2max 40.;\rbassclar2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-124",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1332.018338441848755, 352.181817531585693, 109.0, 76.0 ],
									"text" : ";\rpiano2set spacing;\rpiano2min 1.;\rpiano2max 100.;\rpiano2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-123",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1195.0, 352.181817531585693, 115.0, 76.0 ],
									"text" : ";\rpiano1set grainsize;\rpiano1min 300.;\rpiano1max 50.;\rpiano1curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 9,
									"numoutlets" : 9,
									"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "" ],
									"patching_rect" : [ 257.894474148750305, 48.0, 894.0, 22.0 ],
									"text" : "select 0 1 2 3 4 5 6 7"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 257.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 5,
											"revision" : 6,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 59.0, 118.0, 640.0, 480.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-13",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 308.105525851249695, 410.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-11",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 571.0, 392.0, 150.0, 20.0 ],
													"text" : "voice 11 feb"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-124",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 574.105525851249695, 418.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 10.;\rinstr feedback 0.991;\rinstr gain 1.;\rinstr variability 0.44;\rinstr transpgrain 5.;\rinstr modfreqmod 0.9;\rinstr modmorph 0.098;\rinstr modfreq 199.;\rinstr modfactor 0.41;\rinstr spacing 190;\rinstr grainoffset 0;\rinstr grainsize 211;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.445;\rinstr indexdistr 1;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 72.0, 682.0, 150.0, 20.0 ],
													"text" : "piano transpose 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 72.0, 708.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 1.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 322.0, 682.0, 150.0, 20.0 ],
													"text" : "piano transpose 2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-7",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 326.0, 704.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 6.;\rinstr feedback 0.7;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 68.0, 407.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.5;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 50;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 68.0, 379.0, 150.0, 20.0 ],
													"text" : "clarinet"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 68.0, 36.0, 150.0, 20.0 ],
													"text" : "viola"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 296.447237074375153, 66.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.5;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 2.9;\rinstr modfactor 0.7;\rinstr spacing 17;\rinstr grainoffset 40;\rinstr grainsize 50;\rinstr maxdelay 300.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 57.0, 66.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 16;\rinstr grainoffset 0;\rinstr grainsize 219;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
 ],
										"lines" : [  ]
									}
,
									"patching_rect" : [ 95.0, 45.0, 35.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p old"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-74",
									"linecount" : 22,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1001.0, 118.0, 158.0, 317.0 ],
									"text" : ";\rpiano transpout 11.044334;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.234717;\rpiano transpgrain 9.041521;\rpiano modfreqmod 0.395239;\rpiano modmorph 1.888272;\rpiano modfreq 5181.219287;\rpiano modfactor 0.59711;\rpiano spacing 257;\rpiano grainoffset 243;\rpiano grainsize 200;\rpiano maxdelay 3477.948085;\rpiano lpffreq 17645.617938;\rpiano hpffreq 136.927723;\rpiano grainenvmorph 0.251452;\rpiano indexdistr 17;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-73",
									"linecount" : 23,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 833.105525851249695, 392.00221230339821, 157.894474148750305, 330.0 ],
									"text" : ";\rpiano transpout 42.7;\rpiano feedback 0.833806;\rpiano gain 1.;\rpiano variability 0.103467;\rpiano transpgrain -42.353346;\rpiano modfreqmod 0.19328;\rpiano modmorph 2.926464;\rpiano modfreq 1355.294321;\rpiano modfactor 0.024476;\rpiano spacing 87;\rpiano grainoffset 546;\rpiano grainsize 200;\rpiano maxdelay 4888.035099;\rpiano lpffreq 9899.430395;\rpiano hpffreq 849.965203;\rpiano grainenvmorph 0.245914;\rpiano indexdistr 6;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-72",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 846.0, 80.666671335697174, 150.0, 20.0 ],
									"text" : "guitar"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-67",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 813.0, 108.168881672142106, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 0.8;\rpiano transpgrain 0.;\rpiano modfreqmod 0.;\rpiano modmorph 0.8;\rpiano modfreq 0.5;\rpiano modfactor 0.8;\rpiano spacing 83;\rpiano grainoffset 50;\rpiano grainsize 165;\rpiano maxdelay 500.;\rpiano lpffreq 7000.;\rpiano hpffreq 200.;\rpiano grainenvmorph 0.;\rpiano indexdistr 3;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-66",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 625.947237074375153, 108.168881672142106, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 2.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 0.8;\rpiano transpgrain 2.;\rpiano modfreqmod 0.;\rpiano modmorph 0.5;\rpiano modfreq 1.;\rpiano modfactor 0.5;\rpiano spacing 121;\rpiano grainoffset 50;\rpiano grainsize 441;\rpiano maxdelay 2000.;\rpiano lpffreq 5000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.;\rpiano indexdistr 2;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-65",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 645.105525851249695, 417.0, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout -1.;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.8;\rpiano transpgrain -0.5;\rpiano modfreqmod 0.;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 128;\rpiano grainoffset 151;\rpiano grainsize 445;\rpiano maxdelay 2000.;\rpiano lpffreq 5000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.;\rpiano indexdistr 1;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-63",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 446.105525851249695, 432.0, 157.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 10.;\rpiano feedback 0.8;\rpiano gain 1.;\rpiano variability 0.3;\rpiano transpgrain 0.;\rpiano modfreqmod 0.;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 80;\rpiano grainoffset 40;\rpiano grainsize 40;\rpiano maxdelay 2000.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 0.5;\rpiano indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-43",
									"linecount" : 22,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 424.105525851249695, 108.168881672142106, 158.0, 317.0 ],
									"text" : ";\rpiano transpout 11.044334;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.234717;\rpiano transpgrain 9.041521;\rpiano modfreqmod 0.395239;\rpiano modmorph 1.888272;\rpiano modfreq 5181.219287;\rpiano modfactor 0.59711;\rpiano spacing 257;\rpiano grainoffset 243;\rpiano grainsize 200;\rpiano maxdelay 3477.948085;\rpiano lpffreq 17645.617938;\rpiano hpffreq 136.927723;\rpiano grainenvmorph 0.251452;\rpiano indexdistr 17;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 104.0, 138.0, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.;\rpiano gain 1.;\rpiano variability 0.;\rpiano transpgrain 0.;\rpiano modfreqmod 0.;\rpiano modmorph 0.;\rpiano modfreq 0.;\rpiano modfactor 0.;\rpiano spacing 1;\rpiano grainoffset 0;\rpiano grainsize 250;\rpiano maxdelay 500.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 1.;\rpiano indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 6.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rpiano transpout 11.044334;\rpiano feedback 0.4;\rpiano gain 1.;\rpiano variability 0.234717;\rpiano transpgrain 9.041521;\rpiano modfreqmod 0.395239;\rpiano modmorph 1.888272;\rpiano modfreq 5181.219287;\rpiano modfactor 0.59711;\rpiano spacing 257;\rpiano grainoffset 243;\rpiano grainsize 200;\rpiano maxdelay 3477.948085;\rpiano lpffreq 17645.617938;\rpiano hpffreq 136.927723;\rpiano grainenvmorph 0.251452;\rpiano indexdistr 17;\r"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 270.0, 74.0, 150.0, 20.0 ],
									"text" : "reset"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-12",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 387.0, 138.0, 263.0 ],
									"text" : ";\rpiano transpout 0.;\rpiano feedback 0.2;\rpiano gain 1.;\rpiano variability 0.3;\rpiano transpgrain 0.;\rpiano modfreqmod 0.5;\rpiano modmorph 0.;\rpiano modfreq 500.;\rpiano modfactor 0.;\rpiano spacing 80;\rpiano grainoffset 40;\rpiano grainsize 40;\rpiano maxdelay 2000.;\rpiano lpffreq 20000.;\rpiano hpffreq 20.;\rpiano grainenvmorph 1.;\rpiano indexdistr 0;\r"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-123", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-124", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-15", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-16", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-19", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-43", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-63", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-65", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-66", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-67", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-73", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-74", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"source" : [ "obj-3", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 884.800013184547424, 1116.000016629695892, 80.0, 26.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p presets",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ "piano", "@Channels", 7 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-44",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 704.800010502338409, 541.600008070468903, 199.0, 564.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 407.00000786781311, 4.800000250339508, 199.0, 564.0 ],
					"varname" : "bbdmi_live.granulator~[3]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"color" : [ 1.0, 0.694117647058824, 0.0, 1.0 ],
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 5,
							"revision" : 6,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ -1886.0, -277.0, 1851.0, 785.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"assistshowspatchername" : 0,
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-131",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1628.518338441848755, 435.670968413352966, 131.0, 76.0 ],
									"text" : ";\rbassclar2set feedback;\rbassclar2min 0.1;\rbassclar2max 1.;\rbassclar2curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-132",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1495.590909481048584, 435.670968413352966, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 20.;\rbassclar1max 72.;\rbassclar1curve 1.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-133",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1628.518338441848755, 356.125516176223755, 143.0, 76.0 ],
									"text" : ";\rbassclar2set transpgrain;\rbassclar2min -0.5;\rbassclar2max 0.5;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-134",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 356.125516176223755, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 96.;\rbassclar1max 24.;\rbassclar1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-135",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1628.518338441848755, 278.125516176223755, 127.0, 76.0 ],
									"text" : ";\rbassclar2set modfreq;\rbassclar2min 1.;\rbassclar2max 20.;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-136",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 278.125516176223755, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 133.;\rbassclar1max 333.;\rbassclar1curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-137",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1628.518338441848755, 194.318182468414307, 124.0, 76.0 ],
									"text" : ";\rbassclar2set spacing;\rbassclar2min 120.;\rbassclar2max 40.;\rbassclar2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-138",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1491.5, 194.318182468414307, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 56.;\rbassclar1max 0.;\rbassclar1curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-130",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1342.725188255310059, 438.85278594493866, 124.0, 76.0 ],
									"text" : ";\rbassclar2set spacing;\rbassclar2min 90;\rbassclar2max 50;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-129",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1209.797759294509888, 438.85278594493866, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 85.;\rbassclar1max 20.;\rbassclar1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-127",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1342.725188255310059, 359.307333707809448, 143.0, 76.0 ],
									"text" : ";\rbassclar2set transpgrain;\rbassclar2min -6.7;\rbassclar2max -4.5;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-128",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1205.706849813461304, 359.307333707809448, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 200.;\rbassclar1max 50.;\rbassclar1curve 0.5"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-126",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1342.725188255310059, 281.307333707809448, 127.0, 76.0 ],
									"text" : ";\rbassclar2set modfreq;\rbassclar2min 1.;\rbassclar2max 2200.;\rbassclar2curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-125",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1205.706849813461304, 281.307333707809448, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 8.;\rbassclar1max 100.;\rbassclar1curve 1."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-124",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1342.725188255310059, 197.5, 124.0, 76.0 ],
									"text" : ";\rbassclar2set spacing;\rbassclar2min 1.;\rbassclar2max 100.;\rbassclar2curve 2."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-123",
									"linecount" : 5,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 1205.706849813461304, 197.5, 131.0, 76.0 ],
									"text" : ";\rbassclar1set grainsize;\rbassclar1min 100.;\rbassclar1max 1.;\rbassclar1curve 0.25"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-13",
									"maxclass" : "newobj",
									"numinlets" : 9,
									"numoutlets" : 9,
									"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "bang", "bang", "bang", "" ],
									"patching_rect" : [ 257.894474148750305, 48.0, 894.0, 22.0 ],
									"text" : "select 0 1 2 3 4 5 6 7"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-1",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 257.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-97",
									"linecount" : 22,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 951.634957671165466, 384.333328664302826, 174.894474148750305, 317.0 ],
									"text" : ";\rbassclar transpout 13.740772;\rbassclar feedback 0.813036;\rbassclar gain 1.;\rbassclar variability 0.519368;\rbassclar transpgrain -17.093793;\rbassclar modfreqmod 0.079739;\rbassclar modmorph 2.652866;\rbassclar modfreq 391.3611;\rbassclar modfactor 0.457532;\rbassclar spacing 79;\rbassclar grainoffset 44;\rbassclar grainsize 36;\rbassclar maxdelay 636.551476;\rbassclar lpffreq 5734.015345;\rbassclar hpffreq 651.065726;\rbassclar grainenvmorph 0.480679;\rbassclar indexdistr 14;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-96",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 745.027107119560242, 384.333328664302826, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 0.;\rbassclar feedback 0.7;\rbassclar gain 1.;\rbassclar variability 0.8;\rbassclar transpgrain 0.2;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 187;\rbassclar grainoffset 10;\rbassclar grainsize 233;\rbassclar maxdelay 2000.;\rbassclar lpffreq 12000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-95",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 872.634957671165466, 104.0, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 2.;\rbassclar feedback 0.7;\rbassclar gain 1.;\rbassclar variability 0.8;\rbassclar transpgrain 0.2;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 130;\rbassclar grainoffset 200;\rbassclar grainsize 48;\rbassclar maxdelay 500.;\rbassclar lpffreq 12000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-64",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 514.105525851249695, 384.333328664302826, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout -2.;\rbassclar feedback 0.8;\rbassclar gain 1.;\rbassclar variability 0.3;\rbassclar transpgrain 1.;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 87;\rbassclar grainoffset 10;\rbassclar grainsize 42;\rbassclar maxdelay 100.;\rbassclar lpffreq 2000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-62",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 670.105525851249695, 104.0, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout 1.;\rbassclar feedback 0.4;\rbassclar gain 1.;\rbassclar variability 0.8;\rbassclar transpgrain 0.2;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.1;\rbassclar modfreq 10.;\rbassclar modfactor 0.5;\rbassclar spacing 80;\rbassclar grainoffset 200;\rbassclar grainsize 36;\rbassclar maxdelay 100.;\rbassclar lpffreq 6000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-61",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 460.0, 104.0, 174.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout -20.;\rbassclar feedback 0.5;\rbassclar gain 1.;\rbassclar variability 0.3;\rbassclar transpgrain -5.;\rbassclar modfreqmod 0.5;\rbassclar modmorph 0.;\rbassclar modfreq 500.;\rbassclar modfactor 0.;\rbassclar spacing 40;\rbassclar grainoffset 20;\rbassclar grainsize 100;\rbassclar maxdelay 2000.;\rbassclar lpffreq 20000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 0.5;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-29",
									"linecount" : 21,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 306.105525851249695, 384.333328664302826, 174.894474148750305, 303.0 ],
									"text" : ";\rbassclar transpout -1.567927;\rbassclar feedback 0.217766;\rbassclar gain 1.;\rbassclar variability 0.420905;\rbassclar transpgrain -0.30263;\rbassclar modfreqmod 0.451733;\rbassclar modmorph 0.449987;\rbassclar modfreq 2022.356999;\rbassclar modfactor 0.644277;\rbassclar spacing 69;\rbassclar grainoffset 599;\rbassclar grainsize 35;\rbassclar maxdelay 5433.37;\rbassclar lpffreq 14539.336625;\rbassclar hpffreq 1311.302966;\rbassclar grainenvmorph 0.890082;\rbassclar indexdistr 4;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-14",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 0,
									"patcher" : 									{
										"fileversion" : 1,
										"appversion" : 										{
											"major" : 8,
											"minor" : 5,
											"revision" : 6,
											"architecture" : "x64",
											"modernui" : 1
										}
,
										"classnamespace" : "box",
										"rect" : [ 59.0, 118.0, 868.0, 655.0 ],
										"bglocked" : 0,
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 1,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 1,
										"objectsnaponopen" : 1,
										"statusbarvisible" : 2,
										"toolbarvisible" : 1,
										"lefttoolbarpinned" : 0,
										"toptoolbarpinned" : 0,
										"righttoolbarpinned" : 0,
										"bottomtoolbarpinned" : 0,
										"toolbars_unpinned_last_save" : 0,
										"tallnewobj" : 0,
										"boxanimatetime" : 200,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"description" : "",
										"digest" : "",
										"tags" : "",
										"style" : "",
										"subpatcher_template" : "",
										"assistshowspatchername" : 0,
										"boxes" : [ 											{
												"box" : 												{
													"id" : "obj-13",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 530.105525851249695, 389.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-11",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 793.0, 371.0, 150.0, 20.0 ],
													"text" : "voice 11 feb"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-124",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 796.105525851249695, 397.643398344516868, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 10.;\rinstr feedback 0.991;\rinstr gain 1.;\rinstr variability 0.44;\rinstr transpgrain 5.;\rinstr modfreqmod 0.9;\rinstr modmorph 0.098;\rinstr modfreq 199.;\rinstr modfactor 0.41;\rinstr spacing 190;\rinstr grainoffset 0;\rinstr grainsize 211;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 0.445;\rinstr indexdistr 1;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-10",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 294.0, 661.0, 150.0, 20.0 ],
													"text" : "piano transpose 1"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-9",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 294.0, 687.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 1.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-8",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 544.0, 661.0, 150.0, 20.0 ],
													"text" : "piano transpose 2"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-7",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 548.0, 683.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 6.;\rinstr feedback 0.7;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 0;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-5",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 290.0, 386.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.5;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 0.;\rinstr modfactor 0.;\rinstr spacing 1;\rinstr grainoffset 50;\rinstr grainsize 250;\rinstr maxdelay 500.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-2",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 290.0, 358.0, 150.0, 20.0 ],
													"text" : "clarinet"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-1",
													"maxclass" : "comment",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 290.0, 15.0, 150.0, 20.0 ],
													"text" : "viola"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-34",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 518.447237074375153, 45.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.5;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.;\rinstr modmorph 0.;\rinstr modfreq 2.9;\rinstr modfactor 0.7;\rinstr spacing 17;\rinstr grainoffset 40;\rinstr grainsize 50;\rinstr maxdelay 300.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-29",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 279.0, 45.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.;\rinstr gain 1.;\rinstr variability 0.;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 1000.;\rinstr modfactor 0.;\rinstr spacing 16;\rinstr grainoffset 0;\rinstr grainsize 219;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
, 											{
												"box" : 												{
													"id" : "obj-12",
													"linecount" : 18,
													"maxclass" : "message",
													"numinlets" : 2,
													"numoutlets" : 1,
													"outlettype" : [ "" ],
													"patching_rect" : [ 8.0, 336.0, 226.894474148750305, 263.0 ],
													"text" : ";\rinstr transpout 0.;\rinstr feedback 0.2;\rinstr gain 1.;\rinstr variability 0.3;\rinstr transpgrain 0.;\rinstr modfreqmod 0.5;\rinstr modmorph 0.;\rinstr modfreq 500.;\rinstr modfactor 0.;\rinstr spacing 80;\rinstr grainoffset 40;\rinstr grainsize 40;\rinstr maxdelay 2000.;\rinstr lpffreq 20000.;\rinstr hpffreq 20.;\rinstr grainenvmorph 1.;\rinstr indexdistr 0;\r"
												}

											}
 ],
										"lines" : [  ]
									}
,
									"patching_rect" : [ 15.0, 48.0, 35.0, 22.0 ],
									"saved_object_attributes" : 									{
										"description" : "",
										"digest" : "",
										"globalpatchername" : "",
										"tags" : ""
									}
,
									"text" : "p old"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 264.0, 104.0, 153.0, 263.0 ],
									"text" : ";\rbassclar transpout 0.;\rbassclar feedback 0.;\rbassclar gain 1.;\rbassclar variability 0.;\rbassclar transpgrain 0.;\rbassclar modfreqmod 0.;\rbassclar modmorph 0.;\rbassclar modfreq 0.;\rbassclar modfactor 0.;\rbassclar spacing 1;\rbassclar grainoffset 0;\rbassclar grainsize 250;\rbassclar maxdelay 500.;\rbassclar lpffreq 20000.;\rbassclar hpffreq 20.;\rbassclar grainenvmorph 1.;\rbassclar indexdistr 0;\r"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"linecount" : 18,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 6.0, 104.0, 226.894474148750305, 263.0 ],
									"text" : ";\rbassclar transpout -1.567927;\rbassclar feedback 0.217766;\rbassclar gain 1.;\rbassclar variability 0.420905;\rbassclar transpgrain -0.30263;\rbassclar modfreqmod 0.451733;\rbassclar modmorph 0.449987;\rbassclar modfreq 2022.356999;\rbassclar modfactor 0.644277;\rbassclar spacing 69;\rbassclar grainoffset 599;\rbassclar grainsize 35;\rbassclar maxdelay 5433.37;\rbassclar lpffreq 14539.336625;\rbassclar hpffreq 1311.302966;\rbassclar grainenvmorph 0.890082;\rbassclar indexdistr 4;\r"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 213.894474148750305, 12.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-20",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 270.0, 74.0, 150.0, 20.0 ],
									"text" : "reset"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-13", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-123", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-124", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-125", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-126", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-127", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-128", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-129", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-130", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-131", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-132", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-133", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-134", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-135", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-136", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-137", 0 ],
									"order" : 0,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-138", 0 ],
									"order" : 1,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-61", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-62", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-64", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-95", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 6 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-96", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 5 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-97", 0 ],
									"order" : 2,
									"source" : [ "obj-13", 7 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 1 ],
									"source" : [ "obj-3", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 438.400006532669067, 1116.000016629695892, 103.0, 26.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p presets",
					"textcolor" : [ 0.0, 0.996078431372549, 0.098039215686275, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"args" : [ "bassclar", "@Channels", 7 ],
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-30",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "bbdmi_live_granulator~.maxpat",
					"numinlets" : 2,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "multichannelsignal", "", "" ],
					"patching_rect" : [ 258.400003850460052, 541.600008070468903, 199.0, 564.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 4.800000071525574, 199.0, 564.0 ],
					"varname" : "bbdmi_live.granulator~[2]",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.725490196078431, 0.611764705882353, 1.0, 1.0 ],
					"id" : "obj-97",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 130.40000194311142, 56.800000846385956, 96.000001430511475, 36.000000536441803 ],
					"presentation" : 1,
					"presentation_rect" : [ 1.800002872943878, 569.000002861022949, 226.400003373622894, 171.200002551078796 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.494117647058824, 0.780392156862745, 0.925490196078431, 1.0 ],
					"id" : "obj-96",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 92.399999141693115, 37.600000560283661, 96.000001430511475, 36.000000536441803 ],
					"presentation" : 1,
					"presentation_rect" : [ 359.600004196166992, 569.000002861022949, 246.400003671646118, 171.200002551078796 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 0.341176470588235, 0.666666666666667, 0.129411764705882, 1.0 ],
					"id" : "obj-94",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 65.600000977516174, 24.000000357627869, 96.000001430511475, 36.000000536441803 ],
					"presentation" : 1,
					"presentation_rect" : [ 201.0, 35.800000250339508, 204.800003051757812, 173.600000709295273 ],
					"proportion" : 0.5
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"bgcolor" : [ 1.0, 0.450980392156863, 0.450980392156863, 1.0 ],
					"id" : "obj-98",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 38.40000057220459, 15.20000022649765, 96.000001430511475, 36.000000536441803 ],
					"presentation" : 1,
					"presentation_rect" : [ 238.400005340576172, 569.000002920627594, 111.800000667572021, 171.200002491474152 ],
					"proportion" : 0.5
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-110", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"source" : [ "obj-111", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"order" : 1,
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-402", 0 ],
					"order" : 0,
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-402", 0 ],
					"order" : 0,
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 1,
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"order" : 0,
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-402", 0 ],
					"order" : 1,
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 0,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-402", 0 ],
					"order" : 1,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"order" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"order" : 1,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-402", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-30", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-402", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"order" : 0,
					"source" : [ "obj-402", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"order" : 1,
					"source" : [ "obj-402", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-44", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"order" : 0,
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"order" : 1,
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-54", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-54", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 1 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 0,
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"order" : 1,
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 0,
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"order" : 1,
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 1 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"order" : 0,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"order" : 1,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"order" : 0,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"order" : 1,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 1 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 1 ],
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 1 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 1 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"order" : 0,
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"order" : 1,
					"source" : [ "obj-95", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-112" : [ "live.gain~[10]", "live.gain~", 0 ],
			"obj-113" : [ "live.gain~[11]", "live.gain~", 0 ],
			"obj-114" : [ "live.gain~[12]", "live.gain~", 0 ],
			"obj-24::obj-13::obj-107::obj-33" : [ "tab[80]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-123::obj-33" : [ "tab[78]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-36::obj-33" : [ "tab[82]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-40::obj-33" : [ "tab[83]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-41::obj-33" : [ "tab[84]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-44::obj-33" : [ "tab[87]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-45::obj-33" : [ "tab[88]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-46::obj-33" : [ "tab[89]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-47::obj-33" : [ "tab[90]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-48::obj-33" : [ "tab[91]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-49::obj-33" : [ "tab[92]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-50::obj-33" : [ "tab[93]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-6::obj-33" : [ "tab[86]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-74::obj-33" : [ "tab[79]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-8::obj-33" : [ "tab[85]", "tab[1]", 0 ],
			"obj-24::obj-13::obj-9::obj-33" : [ "tab[81]", "tab[1]", 0 ],
			"obj-24::obj-15" : [ "live.gain~[1]", "leloup~", 0 ],
			"obj-24::obj-175::obj-107::obj-33" : [ "tab[64]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-123::obj-33" : [ "tab[125]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-36::obj-33" : [ "tab[66]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-40::obj-33" : [ "tab[67]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-41::obj-33" : [ "tab[68]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-44::obj-33" : [ "tab[71]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-45::obj-33" : [ "tab[72]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-46::obj-33" : [ "tab[73]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-47::obj-33" : [ "tab[74]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-48::obj-33" : [ "tab[75]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-49::obj-33" : [ "tab[76]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-50::obj-33" : [ "tab[77]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-6::obj-33" : [ "tab[70]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-74::obj-33" : [ "tab[63]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-8::obj-33" : [ "tab[69]", "tab[1]", 0 ],
			"obj-24::obj-175::obj-9::obj-33" : [ "tab[65]", "tab[1]", 0 ],
			"obj-24::obj-20::obj-122" : [ "number[49]", "number[1]", 0 ],
			"obj-24::obj-20::obj-16" : [ "number[48]", "number[1]", 0 ],
			"obj-24::obj-24::obj-107::obj-33" : [ "tab[18]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-123::obj-33" : [ "tab[16]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-34::obj-33" : [ "tab[19]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-36::obj-33" : [ "tab[20]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-40::obj-33" : [ "tab[21]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-41::obj-33" : [ "tab[22]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-42::obj-33" : [ "tab[23]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-43::obj-33" : [ "tab[24]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-44::obj-33" : [ "tab[25]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-45::obj-33" : [ "tab[26]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-46::obj-33" : [ "tab[27]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-47::obj-33" : [ "tab[28]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-48::obj-33" : [ "tab[29]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-49::obj-33" : [ "tab[30]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-50::obj-33" : [ "tab[31]", "tab[1]", 0 ],
			"obj-24::obj-24::obj-74::obj-33" : [ "tab[17]", "tab[1]", 0 ],
			"obj-24::obj-2::obj-122" : [ "number[2]", "number[1]", 0 ],
			"obj-24::obj-2::obj-16" : [ "number[1]", "number[1]", 0 ],
			"obj-24::obj-31::obj-107::obj-33" : [ "tab[33]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-123::obj-33" : [ "tab[116]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-34::obj-33" : [ "tab[34]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-36::obj-33" : [ "tab[35]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-40::obj-33" : [ "tab[36]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-41::obj-33" : [ "tab[37]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-42::obj-33" : [ "tab[38]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-43::obj-33" : [ "tab[39]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-44::obj-33" : [ "tab[40]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-45::obj-33" : [ "tab[41]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-46::obj-33" : [ "tab[42]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-47::obj-33" : [ "tab[43]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-48::obj-33" : [ "tab[44]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-49::obj-33" : [ "tab[45]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-50::obj-33" : [ "tab[46]", "tab[1]", 0 ],
			"obj-24::obj-31::obj-74::obj-33" : [ "tab[32]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-107::obj-33" : [ "tab[49]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-123::obj-33" : [ "tab[47]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-34::obj-33" : [ "tab[50]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-36::obj-33" : [ "tab[51]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-40::obj-33" : [ "tab[52]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-41::obj-33" : [ "tab[53]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-42::obj-33" : [ "tab[54]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-43::obj-33" : [ "tab[55]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-44::obj-33" : [ "tab[56]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-45::obj-33" : [ "tab[57]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-46::obj-33" : [ "tab[58]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-47::obj-33" : [ "tab[59]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-48::obj-33" : [ "tab[60]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-49::obj-33" : [ "tab[61]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-50::obj-33" : [ "tab[62]", "tab[1]", 0 ],
			"obj-24::obj-34::obj-74::obj-33" : [ "tab[48]", "tab[1]", 0 ],
			"obj-24::obj-39" : [ "live.gain~[2]", "live.gain~[2]", 0 ],
			"obj-24::obj-67" : [ "live.gain~", "leloup~", 0 ],
			"obj-24::obj-93::obj-107::obj-33" : [ "tab[2]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-123::obj-33" : [ "tab[182]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-34::obj-33" : [ "tab[3]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-36::obj-33" : [ "tab[4]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-40::obj-33" : [ "tab[5]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-41::obj-33" : [ "tab[6]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-42::obj-33" : [ "tab[7]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-43::obj-33" : [ "tab[8]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-44::obj-33" : [ "tab[9]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-45::obj-33" : [ "tab[10]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-46::obj-33" : [ "tab[11]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-47::obj-33" : [ "tab[12]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-48::obj-33" : [ "tab[13]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-49::obj-33" : [ "tab[14]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-50::obj-33" : [ "tab[15]", "tab[1]", 0 ],
			"obj-24::obj-93::obj-74::obj-33" : [ "tab[1]", "tab[1]", 0 ],
			"obj-30::obj-101" : [ "number[23]", "number[7]", 0 ],
			"obj-30::obj-104" : [ "number[21]", "number[5]", 0 ],
			"obj-30::obj-109" : [ "number[30]", "number[6]", 0 ],
			"obj-30::obj-113" : [ "number[17]", "number[9]", 0 ],
			"obj-30::obj-118" : [ "number[27]", "number[2]", 0 ],
			"obj-30::obj-123" : [ "number[19]", "number[3]", 0 ],
			"obj-30::obj-28" : [ "number[25]", "number[12]", 0 ],
			"obj-30::obj-43" : [ "number[29]", "number[4]", 0 ],
			"obj-30::obj-44" : [ "number[28]", "number[13]", 0 ],
			"obj-30::obj-61" : [ "number[24]", "number[9]", 0 ],
			"obj-30::obj-68" : [ "number[20]", "number[4]", 0 ],
			"obj-30::obj-72" : [ "number[18]", "number[11]", 0 ],
			"obj-30::obj-77" : [ "number[16]", "number[10]", 0 ],
			"obj-30::obj-90" : [ "number[26]", "number", 0 ],
			"obj-30::obj-93" : [ "number[22]", "number[8]", 0 ],
			"obj-30::obj-97" : [ "number[31]", "number[1]", 0 ],
			"obj-44::obj-101" : [ "number[45]", "number[7]", 0 ],
			"obj-44::obj-104" : [ "number[46]", "number[5]", 0 ],
			"obj-44::obj-109" : [ "number[47]", "number[6]", 0 ],
			"obj-44::obj-113" : [ "number[44]", "number[9]", 0 ],
			"obj-44::obj-118" : [ "number[33]", "number[2]", 0 ],
			"obj-44::obj-123" : [ "number[42]", "number[3]", 0 ],
			"obj-44::obj-28" : [ "number[32]", "number[12]", 0 ],
			"obj-44::obj-43" : [ "number[39]", "number[4]", 0 ],
			"obj-44::obj-44" : [ "number[41]", "number[13]", 0 ],
			"obj-44::obj-61" : [ "number[38]", "number[9]", 0 ],
			"obj-44::obj-68" : [ "number[34]", "number[4]", 0 ],
			"obj-44::obj-72" : [ "number[35]", "number[11]", 0 ],
			"obj-44::obj-77" : [ "number[40]", "number[10]", 0 ],
			"obj-44::obj-90" : [ "number[43]", "number", 0 ],
			"obj-44::obj-93" : [ "number[37]", "number[8]", 0 ],
			"obj-44::obj-97" : [ "number[36]", "number[1]", 0 ],
			"obj-86::obj-325" : [ "button[2]", "button", 0 ],
			"parameterbanks" : 			{
				"0" : 				{
					"index" : 0,
					"name" : "",
					"parameters" : [ "-", "-", "-", "-", "-", "-", "-", "-" ]
				}

			}
,
			"inherited_shortname" : 1
		}
,
		"parameter_map" : 		{
			"midi" : 			{
				"number" : 				{
					"srcname" : "2.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[1]" : 				{
					"srcname" : "3.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[2]" : 				{
					"srcname" : "4.ctrl.0.chan.midi",
					"min" : 20.0,
					"max" : 20000.0,
					"flags" : 2
				}
,
				"number[3]" : 				{
					"srcname" : "5.ctrl.0.chan.midi",
					"min" : 20.0,
					"max" : 20000.0,
					"flags" : 2
				}
,
				"number[4]" : 				{
					"srcname" : "6.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 10000.0,
					"flags" : 2
				}
,
				"number[5]" : 				{
					"srcname" : "8.ctrl.0.chan.midi",
					"min" : 1.0,
					"max" : 2000.0,
					"flags" : 2
				}
,
				"number[6]" : 				{
					"srcname" : "9.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1000.0,
					"flags" : 2
				}
,
				"number[7]" : 				{
					"srcname" : "12.ctrl.0.chan.midi",
					"min" : 1.0,
					"max" : 10000.0,
					"flags" : 2
				}
,
				"number[8]" : 				{
					"srcname" : "13.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[9]" : 				{
					"srcname" : "14.ctrl.0.chan.midi",
					"min" : -48.0,
					"max" : 48.0,
					"flags" : 2
				}
,
				"number[10]" : 				{
					"srcname" : "15.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 1.0,
					"flags" : 2
				}
,
				"number[11]" : 				{
					"srcname" : "16.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 15000.0,
					"flags" : 2
				}
,
				"number[12]" : 				{
					"srcname" : "17.ctrl.0.chan.midi",
					"min" : 0.0,
					"max" : 3.0,
					"flags" : 2
				}
,
				"number[13]" : 				{
					"srcname" : "18.ctrl.0.chan.midi",
					"min" : -48.0,
					"max" : 48.0,
					"flags" : 2
				}

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "EAVI-2.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2024-11-19_ArsMusica",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "Granulator_Control.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2024-11-19_ArsMusica",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "RD-perf-a.json",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2023-11-24_Athenor/RD/preset",
				"patcherrelativepath" : "../2023-11-24_Athenor/RD/preset",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "abc.hoa.decoder~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/abclib/externals/global_abstractions",
				"patcherrelativepath" : "../../../../Max 8/Packages/abclib/externals/global_abstractions",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "abc.hoa.encoder~.maxpat",
				"bootpath" : "~/Documents/Max 8/Packages/abclib/externals/global_abstractions",
				"patcherrelativepath" : "../../../../Max 8/Packages/abclib/externals/global_abstractions",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "abc_2d_decoder5_12~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_2d_encoder5~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "abc_wrp.js",
				"bootpath" : "~/Documents/Max 8/Packages/abclib/javascript",
				"patcherrelativepath" : "../../../../Max 8/Packages/abclib/javascript",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bassclar-01.wav",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2024-11-19_ArsMusica",
				"patcherrelativepath" : ".",
				"type" : "WAVE",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.crosspatch.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/crosspatch",
				"patcherrelativepath" : "../../max/utilities/crosspatch",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.eavi.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/input/eavi",
				"patcherrelativepath" : "../../max/input/eavi",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.list2~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/list2~",
				"patcherrelativepath" : "../../max/utilities/list2~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.multislider.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/multislider",
				"patcherrelativepath" : "../../max/utilities/multislider",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.rms~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/feature_extraction/rms~",
				"patcherrelativepath" : "../../max/feature_extraction/rms~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.send.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/send",
				"patcherrelativepath" : "../../max/utilities/send",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi.smooth.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/smooth",
				"patcherrelativepath" : "../../max/control_processing/smooth",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_live_granulator~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/sound_synthesis/live_granulator~",
				"patcherrelativepath" : "../../max/sound_synthesis/live_granulator~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bbdmi_multi_granulator11~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "name.js",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/js",
				"patcherrelativepath" : "../../max/source/js",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "p.calibrate.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/calibrate",
				"patcherrelativepath" : "../../max/control_processing/calibrate",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.exposer.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/source/patchers",
				"patcherrelativepath" : "../../max/source/patchers",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.onepole~.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/utilities/onepole~",
				"patcherrelativepath" : "../../max/utilities/onepole~",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "p.scale.maxpat",
				"bootpath" : "~/Documents/GitHub/bbdmi/max/control_processing/scale",
				"patcherrelativepath" : "../../max/control_processing/scale",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "piano-lo-hit-44.wav",
				"bootpath" : "~/Documents/GitHub/bbdmi/events/2024-11-19_ArsMusica",
				"patcherrelativepath" : ".",
				"type" : "WAVE",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "rnbodefault",
				"default" : 				{
					"accentcolor" : [ 0.343034118413925, 0.506230533123016, 0.86220508813858, 1.0 ],
					"bgcolor" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
					"bgfillcolor" : 					{
						"angle" : 270.0,
						"autogradient" : 0.0,
						"color" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
						"color1" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
						"color2" : [ 0.263682, 0.004541, 0.038797, 1.0 ],
						"proportion" : 0.39,
						"type" : "color"
					}
,
					"color" : [ 0.929412, 0.929412, 0.352941, 1.0 ],
					"elementcolor" : [ 0.357540726661682, 0.515565991401672, 0.861786782741547, 1.0 ],
					"fontname" : [ "Lato" ],
					"fontsize" : [ 12.0 ],
					"stripecolor" : [ 0.258338063955307, 0.352425158023834, 0.511919498443604, 1.0 ],
					"textcolor_inverse" : [ 0.968627, 0.968627, 0.968627, 1 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rnbohighcontrast",
				"default" : 				{
					"accentcolor" : [ 0.666666666666667, 0.666666666666667, 0.666666666666667, 1.0 ],
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor" : 					{
						"angle" : 270.0,
						"autogradient" : 0.0,
						"color" : [ 0.0, 0.0, 0.0, 1.0 ],
						"color1" : [ 0.090196078431373, 0.090196078431373, 0.090196078431373, 1.0 ],
						"color2" : [ 0.156862745098039, 0.168627450980392, 0.164705882352941, 1.0 ],
						"proportion" : 0.5,
						"type" : "color"
					}
,
					"clearcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"color" : [ 1.0, 0.874509803921569, 0.141176470588235, 1.0 ],
					"editing_bgcolor" : [ 0.258823529411765, 0.258823529411765, 0.258823529411765, 1.0 ],
					"elementcolor" : [ 0.223386004567146, 0.254748553037643, 0.998085916042328, 1.0 ],
					"fontsize" : [ 13.0 ],
					"locked_bgcolor" : [ 0.258823529411765, 0.258823529411765, 0.258823529411765, 1.0 ],
					"selectioncolor" : [ 0.301960784313725, 0.694117647058824, 0.949019607843137, 1.0 ],
					"stripecolor" : [ 0.258823529411765, 0.258823529411765, 0.258823529411765, 1.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"textcolor_inverse" : [ 1.0, 1.0, 1.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rnbolight",
				"default" : 				{
					"accentcolor" : [ 0.443137254901961, 0.505882352941176, 0.556862745098039, 1.0 ],
					"bgcolor" : [ 0.796078431372549, 0.862745098039216, 0.925490196078431, 1.0 ],
					"bgfillcolor" : 					{
						"angle" : 270.0,
						"autogradient" : 0.0,
						"color" : [ 0.835294117647059, 0.901960784313726, 0.964705882352941, 1.0 ],
						"color1" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
						"color2" : [ 0.263682, 0.004541, 0.038797, 1.0 ],
						"proportion" : 0.39,
						"type" : "color"
					}
,
					"clearcolor" : [ 0.898039, 0.898039, 0.898039, 1.0 ],
					"color" : [ 0.815686274509804, 0.509803921568627, 0.262745098039216, 1.0 ],
					"editing_bgcolor" : [ 0.898039, 0.898039, 0.898039, 1.0 ],
					"elementcolor" : [ 0.337254901960784, 0.384313725490196, 0.462745098039216, 1.0 ],
					"fontname" : [ "Lato" ],
					"locked_bgcolor" : [ 0.898039, 0.898039, 0.898039, 1.0 ],
					"stripecolor" : [ 0.309803921568627, 0.698039215686274, 0.764705882352941, 1.0 ],
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "rnbomonokai",
				"default" : 				{
					"accentcolor" : [ 0.501960784313725, 0.501960784313725, 0.501960784313725, 1.0 ],
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor" : 					{
						"angle" : 270.0,
						"autogradient" : 0.0,
						"color" : [ 0.0, 0.0, 0.0, 1.0 ],
						"color1" : [ 0.031372549019608, 0.125490196078431, 0.211764705882353, 1.0 ],
						"color2" : [ 0.263682, 0.004541, 0.038797, 1.0 ],
						"proportion" : 0.39,
						"type" : "color"
					}
,
					"clearcolor" : [ 0.976470588235294, 0.96078431372549, 0.917647058823529, 1.0 ],
					"color" : [ 0.611764705882353, 0.125490196078431, 0.776470588235294, 1.0 ],
					"editing_bgcolor" : [ 0.976470588235294, 0.96078431372549, 0.917647058823529, 1.0 ],
					"elementcolor" : [ 0.749019607843137, 0.83921568627451, 1.0, 1.0 ],
					"fontname" : [ "Lato" ],
					"locked_bgcolor" : [ 0.976470588235294, 0.96078431372549, 0.917647058823529, 1.0 ],
					"stripecolor" : [ 0.796078431372549, 0.207843137254902, 1.0, 1.0 ],
					"textcolor" : [ 0.129412, 0.129412, 0.129412, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ],
		"bgcolor" : [ 0.407843137254902, 0.407843137254902, 0.407843137254902, 1.0 ]
	}

}
