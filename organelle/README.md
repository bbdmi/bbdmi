# BBDMI Organelle repository (work in progress)

## About

This part of the repository contains patches developed in [BBDMI](https://bbdmi.nakala.fr/) project for the purpose of interfacing with the [Organelle](https://www.critterandguitari.com/organelle). 

- The [Sound synthesis](https://gitlab.huma-num.fr/bbdmi/bbdmi/-/tree/main/organelle/sound_synthesis) module contains patches developed by the BBDMI project that deal with the production and modulations of sound.